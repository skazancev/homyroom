from contents.views import contacts
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^$', contacts, name='contacts'),
]