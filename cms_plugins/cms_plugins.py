# vim:fileencoding=utf-8
from cms.models import Page
from codemirror import CodeMirrorTextarea
from contents.models import TypeBuild, TypeRepair, TariffDesign, DesignService
from django import forms
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib import admin
from models import HtmlInsertModel, BlockModel, TextModel, BaseModelPl, PersonPluginModel, ReviewPluginModel, \
    TeasersPluginModel, Calc, UrlLine, BaseMenuPl, ProjectPluginModel, DesignPluginModel, SliderPluginModel, BasePlugin


class HtmlForm(forms.ModelForm):
    class Meta():
        model = HtmlInsertModel
        exclude = []

    html = forms.CharField(widget=CodeMirrorTextarea(mode="xml", theme="xq-dark"))


class HtmlInsertPlugin(CMSPluginBase):
    model = HtmlInsertModel
    name = u'Html вставка'
    form = HtmlForm
    render_template = 'plugins/html-insert.html'

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context


class BlockInsertPlugin(CMSPluginBase):
    model = BlockModel
    name = u'Блок'
    render_template = 'plugins/html-insert.html'

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context


class TextPlugin(CMSPluginBase):
    model = TextModel
    render_template = 'plugins/text.html'
    name = u'Текст'
    fieldsets = (
        (u'Базовые настройки', {'fields': ('name', 'h', ('color', 'style'), 'anchor')}),
        (u'Текст', {'fields': ('text',)}),
    )

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context


class PersonPlugin(CMSPluginBase):
    model = PersonPluginModel
    render_template = 'plugins/persons.html'
    name = u'Команда'
    filter_horizontal = ['commands']
    fieldsets = (
        (u'Базовые настройки', {'fields': ('name', 'title', 'h', ('color', 'style'), 'anchor')}),
        (u'', {'fields': ('commands',)}),
    )

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context


class ReviewPlugin(CMSPluginBase):
    model = ReviewPluginModel
    render_template = 'plugins/reviews.html'
    name = u'Отзывы'
    filter_horizontal = ['reviews']
    fieldsets = (
        (u'Базовые настройки', {'fields': ('name', 'title', 'h', ('color', 'style'), 'anchor')}),
        (u'', {'fields': ('reviews',)}),
    )

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context


class TeasersPlugin(CMSPluginBase):
    model = TeasersPluginModel
    render_template = 'plugins/teasers.html'
    name = u'Тизеры'
    filter_horizontal = ['teasers']
    fieldsets = (
        (u'Базовые настройки', {'fields': ('name', 'title', 'h', ('color', 'style'), 'anchor')}),
        (u'', {'fields': ('teasers',)}),
    )

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context


class LineAdmin(admin.TabularInline):
    model = UrlLine


class HtmlCalcForm(forms.ModelForm):
    class Meta():
        model = Calc
        exclude = []

    html = forms.CharField(widget=CodeMirrorTextarea(mode="xml", theme="xq-dark"), required=False)


class CalcPlugin(CMSPluginBase):
    model = Calc
    render_template = 'plugins/calc.html'
    name = u'Калькулятор'
    form = HtmlCalcForm
    inlines = [LineAdmin]
    fieldsets = (
        (u'Базовые настройки', {'fields': ('name', 'h', ('color', 'style'), 'anchor')}),
        (u'', {'fields': (('design', 'all_type'), 'html',)}),
    )

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        if not instance.design:
            context.update({'types': TypeBuild.objects.order_by('sort'),
                            'repairs': TypeRepair.objects.order_by('sort'),
                            'services': DesignService.objects.filter(active=True).order_by('sort'),
                            'tariffs': TariffDesign.objects.order_by('sort')})
        else:
            tariff = TariffDesign.objects.order_by('sort')
            services = DesignService.objects.filter(active=True).order_by('sort')
            h = {e:i.id for e, i in enumerate(tariff)}
            leng = len(h)
            f = [{'name': i.name, 'els': [check_arr(i, h[e]) for e in list(xrange(0, leng))]} for i in services]
            context.update({'plugin': instance,
                            'services': f,
                            'tariffs': TariffDesign.objects.order_by('sort'),
                            })
        return context


def check_arr(el, i):
    return i in el.tariffs.values_list('id', flat=True)


class FormPage(forms.ModelForm):
    class Meta():
        model = BaseMenuPl
        exclude = []
    pages = forms.ModelMultipleChoiceField(queryset=Page.objects.filter(publisher_is_draft=False), label=u'Страница')


class BaseMenuPlugin(CMSPluginBase):
    model = BaseMenuPl
    form = FormPage
    render_template = 'plugins/menu.html'
    name = u'Меню'
    filter_horizontal = ['pages']

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context


class ProjectPlugin(CMSPluginBase):
    model = ProjectPluginModel
    name = u'Проект'
    render_template = 'plugins/project.html'

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance, 'object': instance.project})
        return context


class DesignPlugin(CMSPluginBase):
    model = DesignPluginModel
    name = u'Дизайн-проекты'
    filter_horizontal = ['design']
    render_template = 'plugins/design.html'
    fieldsets = (
        (u'Базовые настройки', {'fields': ('design', 'title')}),
    )

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context


class SliderPlugin(CMSPluginBase):
    model = SliderPluginModel
    name = u'Слайдер'
    filter_horizontal = ['sliders']
    render_template = 'plugins/slider.html'

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context


class FeedBackPlugin(CMSPluginBase):
    model = BasePlugin
    name = u'Форма обратной связи'
    render_template = 'plugins/feedback.html'

    def render(self, context, instance, placeholder):
        context.update({'plugin': instance})
        return context

plugin_pool.register_plugin(SliderPlugin),
plugin_pool.register_plugin(DesignPlugin),
plugin_pool.register_plugin(ProjectPlugin),
plugin_pool.register_plugin(BaseMenuPlugin),
plugin_pool.register_plugin(CalcPlugin),
plugin_pool.register_plugin(TeasersPlugin),
plugin_pool.register_plugin(ReviewPlugin),
plugin_pool.register_plugin(PersonPlugin),
plugin_pool.register_plugin(TextPlugin),
plugin_pool.register_plugin(BlockInsertPlugin),
plugin_pool.register_plugin(HtmlInsertPlugin),
plugin_pool.register_plugin(FeedBackPlugin),