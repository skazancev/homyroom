from contents.views import ReviewList
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^$', ReviewList.as_view(), name='reviews'),
]