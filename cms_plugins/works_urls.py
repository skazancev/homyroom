from contents.views import ProjectList, ProjectDetail, view_object
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^(?P<slug>.*)/$', ProjectDetail.as_view(), name='project_detail'),
    url(r'^$', ProjectList.as_view(type_content="project"), name='projects'),
]
