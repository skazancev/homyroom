from contents.views import ProjectList
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^$', ProjectList.as_view(), name='designs'),
]
