from contents.views import template_view
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^$', template_view, name='template_view'),
]