from contents.views import PartnerView
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^$', PartnerView.as_view(), name='partners'),
]