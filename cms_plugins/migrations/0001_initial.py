# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-17 13:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import redactor.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cms', '0013_urlconfrevision'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseModelPl',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('sub_name', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041f\u043e\u0434\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('h', models.CharField(blank=True, choices=[(b'1', b'H1'), (b'2', b'H2'), (b'3', b'H3'), (b'4', b'H4'), (b'5', b'H5'), (b'6', b'H6')], max_length=255, null=True, verbose_name='\u0422\u0438\u043f \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043a\u0430')),
                ('color', models.CharField(blank=True, choices=[(b'brown-bg', '\u041a\u043e\u0440\u0438\u0447\u043d\u0435\u0432\u044b\u0439'), (b'gray-bg', '\u0421\u0435\u0440\u044b\u0439'), (b'white-bg', '\u0411\u0435\u043b\u044b\u0439')], max_length=255, null=True, verbose_name='\u0426\u0432\u0435\u0442 \u043b\u0438\u043d\u0438\u0438')),
                ('style', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041a\u043b\u0430\u0441\u0441 \u0431\u043b\u043e\u043a\u0430')),
                ('anchor', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u042f\u043a\u043e\u0440\u044c')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Block',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('html', models.TextField(verbose_name='\u041a\u043e\u0434')),
            ],
            options={
                'verbose_name': '\u0411\u043b\u043e\u043a',
                'verbose_name_plural': '\u0411\u043b\u043e\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='BlockModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('block', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='plugins', to='cms_plugins.Block', verbose_name='\u0411\u043b\u043e\u043a')),
            ],
            options={
                'verbose_name': 'HTML-\u0431\u043b\u043e\u043a',
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='FeedBack',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
                ('type_feed', models.CharField(choices=[(b'consult', '\u0417\u0430\u043a\u0430\u0437 \u043a\u043e\u043d\u0441\u0443\u043b\u044c\u0442\u0430\u0446\u0438\u0438'), (b'call', '\u041e\u0431\u0440\u0430\u0442\u043d\u044b\u0439 \u0437\u0432\u043e\u043d\u043e\u043a'), (b'order', '\u0417\u0430\u043a\u0430\u0437 \u0443\u0441\u043b\u0443\u0433\u0438')], default=b'call', max_length=255, verbose_name='\u0422\u0438\u043f \u0437\u0430\u044f\u0432\u043a\u0438')),
                ('path', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('phone', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('email', models.CharField(blank=True, max_length=255, null=True, verbose_name='E-mail')),
                ('comment', models.TextField(blank=True, null=True, verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u041e\u0431\u0440\u0430\u0442\u043d\u0430\u044f \u0441\u0432\u044f\u0437\u044c',
                'verbose_name_plural': '\u041e\u0431\u0440\u0430\u0442\u043d\u0430\u044f \u0441\u0432\u044f\u0437\u044c',
            },
        ),
        migrations.CreateModel(
            name='HtmlInsertModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('html', models.TextField(verbose_name='\u041a\u043e\u0434')),
            ],
            options={
                'verbose_name': 'HTML-\u0432\u0441\u0442\u0430\u0432\u043a\u0430',
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Keywords')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='keywords', to='cms.Page', verbose_name='\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430')),
            ],
        ),
        migrations.CreateModel(
            name='Robot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('content', models.TextField(max_length=255, verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435')),
                ('sort', models.IntegerField(default=0, verbose_name='\u041f\u0440\u0438\u043e\u0440\u0438\u0442\u0435\u0442')),
            ],
            options={
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='TextModel',
            fields=[
                ('basemodelpl_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='cms_plugins.BaseModelPl')),
                ('text', redactor.fields.RedactorField(blank=True, null=True, verbose_name='\u0422\u0435\u043a\u0441\u0442')),
            ],
            options={
                'verbose_name': '\u0422\u0435\u043a\u0441\u0442',
            },
            bases=('cms_plugins.basemodelpl',),
        ),
    ]
