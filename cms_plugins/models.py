# vim:fileencoding=utf-8
from contents.models import Person, Review, Teaser, CHOICE_ICO, Project, Popup, DesignProject, Slider
from django.template.defaultfilters import safe
from cms.models import CMSPlugin, Page
from django.db import models
from redactor.fields import RedactorField


class Block(models.Model):
    class Meta():
        verbose_name = u'Блок'
        verbose_name_plural = u'Блоки'

    name = models.CharField(verbose_name=u'Название', max_length=255)
    html = models.TextField(verbose_name=u'Код')

    def __unicode__(self):
        return self.name


class Robot(models.Model):
    class Meta():
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Наименование', max_length=255)
    content = models.TextField(verbose_name=u'Содержимое', max_length=255)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return self.name


class Keyword(models.Model):
    page = models.ForeignKey(Page, related_name='keywords', verbose_name=u'Страница')
    text = models.TextField(verbose_name=u'Keywords')

    def __unicode__(self):
        return self.page.get_title()


H_CHOICE = (
    ('1', 'H1'),
    ('2', 'H2'),
    ('3', 'H3'),
    ('4', 'H4'),
    ('5', 'H5'),
    ('6', 'H6'),
)

COLOR_CHOICE = (
    ('orange', u'Оранжевый'),
    ('green', u'Зеленый'),
)


class BaseModelPl(CMSPlugin):
    name = models.CharField(verbose_name=u'Заголовок', blank=True, null=True, max_length=255)
    sub_name = models.CharField(verbose_name=u'Подзаголовок', blank=True, null=True, max_length=255)
    h = models.CharField(verbose_name=u'Тип заголовка', max_length=255, choices=H_CHOICE, blank=True, null=True)
    color = models.CharField(verbose_name=u'Цвет линии', max_length=255, choices=COLOR_CHOICE, blank=True, null=True)
    style = models.CharField(verbose_name=u'Класс блока', max_length=255, blank=True, null=True)
    anchor = models.CharField(verbose_name=u'Якорь', max_length=255, blank=True, null=True)

    def __unicode__(self):
        if self.name:
            return safe(self.name)
        else:
            return u'Блок %s' % self.id


class TextModel(BaseModelPl):
    class Meta():
        verbose_name = u'Текст'

    text = RedactorField(verbose_name=u'Текст', blank=True, null=True)

    def __unicode__(self):
        if self.name:
            return safe(self.name)
        else:
            return u'Текст %s' % self.id


class HtmlInsertModel(CMSPlugin):
    class Meta():
        verbose_name = u'HTML-вставка'
    name = models.CharField(verbose_name=u'Название', max_length=255, blank=True, null=True)
    html = models.TextField(verbose_name=u'Код')

    def __unicode__(self):
        if self.name:
            return self.name


class BlockModel(CMSPlugin):
    class Meta():
        verbose_name = u'HTML-блок'
    block = models.ForeignKey(Block, related_name='plugins', verbose_name=u'Блок')

    def __unicode__(self):
        return self.block.name


TYPE_FEED = (
    ('consult', u'Заказ консультации'),
    ('call', u'Обратный звонок'),
    ('order', u'Заказ услуги'),
)


class FeedBack(models.Model):
    class Meta():
        verbose_name = u'Обратная связь'
        verbose_name_plural = u'Обратная связь'

    date = models.DateTimeField(verbose_name=u'Дата', auto_now_add=True)
    path = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'Страница')
    name = models.CharField(verbose_name=u'Имя', max_length=255)
    phone = models.CharField(verbose_name=u'Телефон', max_length=255, blank=True, null=True)
    email = models.CharField(verbose_name=u'E-mail', max_length=255, blank=True, null=True)
    comment = models.TextField(verbose_name=u'Сообщение', blank=True, null=True)

    def __unicode__(self):
        return self.name


class PersonPluginModel(BaseModelPl):
    class Meta():
        verbose_name = u'Команда'

    commands = models.ManyToManyField(Person, verbose_name=u'Команда', related_name='plugins')
    title = models.TextField(verbose_name=u'Заголовок', null=True, blank=True,
                             help_text=u'Внимание! Все переносы сохраняются')

    def copy_relations(self, oldinstance):
        self.commands = oldinstance.commands.all()


class ReviewPluginModel(BaseModelPl):
    class Meta():
        verbose_name = u'Отзывы'

    reviews = models.ManyToManyField(Review, verbose_name=u'Отзывы', related_name='plugins')
    title = models.TextField(verbose_name=u'Подзаголовок', null=True, blank=True)

    def copy_relations(self, oldinstance):
        self.reviews = oldinstance.reviews.all()


class TeasersPluginModel(BaseModelPl):
    class Meta():
        verbose_name = u'Тизеры'

    teasers = models.ManyToManyField(Teaser, verbose_name=u'Тизеры', related_name='plugins')
    title = models.TextField(verbose_name=u'Подзаголовок', null=True, blank=True)

    def copy_relations(self, oldinstance):
        self.teasers = oldinstance.teasers.all()


class Calc(BaseModelPl):
    class Meta():
        verbose_name = u'Калькулятор'

    design = models.BooleanField(verbose_name=u'Калькулятор дизайн-проекта', default=False)
    all_type = models.BooleanField(verbose_name=u'Отображать тип помещений', default=False)
    html = models.TextField(verbose_name=u'Блок после', blank=True, null=True)

    def copy_relations(self, oldinstance):
        for associated_item in oldinstance.links.all():
            associated_item.pk = None
            associated_item.plugin = self
            associated_item.save()


class UrlLine(models.Model):

    plugin = models.ForeignKey(Calc, related_name='links')
    name = models.CharField(verbose_name=u'Имя ссылки', max_length=255)
    url = models.CharField(verbose_name=u'Ссылка', max_length=255, blank=True, null=True)
    popup = models.ForeignKey(Popup, verbose_name=u'всплывающее окно', blank=True, null=True)
    ico = models.CharField(choices=CHOICE_ICO, max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name


class BaseMenuPl(CMSPlugin):

    pages = models.ManyToManyField(Page, verbose_name=u'Страницы', related_name='menuspl')

    def copy_relations(self, oldinstance):
        self.pages = oldinstance.pages.all()


class ProjectPluginModel(CMSPlugin):

    project = models.ForeignKey(Project, verbose_name=u'Проект', related_name='plugins')
    top_title = models.TextField(verbose_name=u'Верхний заголовок', null=True, blank=True,
                                 help_text=u'Внимание! Все переносы сохраняются')
    bottom_title = models.TextField(verbose_name=u'Заголовок секретов', null=True, blank=True,
                                    help_text=u'Внимание! Все переносы сохраняются')


class DesignPluginModel(BaseModelPl):
    class Meta():
        verbose_name = u'Дизайн-проекты'

    design = models.ManyToManyField(DesignProject, verbose_name=u'Дизайн-проекты', related_name='plugins')
    title = models.TextField(verbose_name=u'Заголовок', null=True, blank=True)

    def copy_relations(self, oldinstance):
        self.design = oldinstance.design.all()


class SliderPluginModel(CMSPlugin):
    class Meta():
        verbose_name = u'Слайдер'

    sliders = models.ManyToManyField(Slider, verbose_name=u'Слайды', related_name='plugins')

    def copy_relations(self, oldinstance):
        self.sliders = oldinstance.sliders.all()


class BasePlugin(CMSPlugin):
    name = models.CharField(verbose_name=u'Заголовок', max_length=255)

    def __unicode__(self):
        return self.name
