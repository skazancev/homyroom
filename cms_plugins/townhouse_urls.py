from contents.views import townhouse
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^$', townhouse, name='townhouse'),
]