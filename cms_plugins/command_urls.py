from contents.views import CommandView
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^$', CommandView.as_view(), name='commands'),
]