from contents.views import PriceView
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^$', PriceView.as_view(), name='commands'),
]