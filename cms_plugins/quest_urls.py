from contents.views import QuestList
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^$', QuestList.as_view(), name='questions'),
]