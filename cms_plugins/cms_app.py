# vim:fileencoding=utf-8
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool


class ReviewApp(CMSApp):
    name = u'Отзывы'
    urls = ["cms_plugins.review_urls"]


class PriceApp(CMSApp):
    name = u'Цены'
    urls = ["cms_plugins.price_urls"]


class CommandApp(CMSApp):
    name = u'Команда'
    urls = ["cms_plugins.command_urls"]


class PartnerApp(CMSApp):
    name = u'Партнёры'
    urls = ["cms_plugins.partner_urls"]


class ProjectApp(CMSApp):
    name = u'Работы'
    urls = ["cms_plugins.works_urls"]


class DesignApp(CMSApp):
    name = u'Дизайн работы'
    urls = ["cms_plugins.design_urls"]


class TourApp(CMSApp):
    name = u'3D туры'
    urls = ["cms_plugins.tdtour_urls"]


class QuestApp(CMSApp):
    name = u'Вопросы и ответы'
    urls = ["cms_plugins.quest_urls"]


@apphook_pool.register
class DesignProjectsApp(CMSApp):
    name = u'Дизайн-проекты'
    urls = ['cms_plugins.design_project_urls']


@apphook_pool.register
class TownHouseApp(CMSApp):
    name = u'Ремонт коттеджей и таунхаусов'
    urls = ['cms_plugins.townhouse_urls']


@apphook_pool.register
class TeamApp(CMSApp):
    name = u'О компании'
    urls = ['cms_plugins.team_urls']


@apphook_pool.register
class ContactsApp(CMSApp):
    name = u'Контакты'
    urls = ['cms_plugins.contacts_urls']


@apphook_pool.register
class FlatApp(CMSApp):
    name = u'Отделка квартиры под ключ'
    urls = ['cms_plugins.flat_urls']


@apphook_pool.register
class BaseApp(CMSApp):
    name = u'Шаблонная страница'
    urls = ['cms_plugins.template_urls']


apphook_pool.register(QuestApp)
apphook_pool.register(ReviewApp)
apphook_pool.register(TourApp)
apphook_pool.register(DesignApp)
apphook_pool.register(ProjectApp)
apphook_pool.register(PriceApp)
apphook_pool.register(PartnerApp)
apphook_pool.register(CommandApp)