# vim:fileencoding=utf-8
from datetime import datetime
import math
import random
from cms.models import Page
from django import template
from django.template import Template, Context
import re
import time


register = template.Library()


def render_field(val, arg):
    t = Template(val)
    c = Context({'item': arg})
    return t.render(c)


def stringToID(val):
    return int(val.split('-')[1].split(' ')[0])


def minus(value, arg):
    return value - int(arg)


def plus(value, arg):
    return int(value) + int(arg)


def multiply(value, arg):
    return int(value) * int(arg)


def split(values, arg):
    n = values.split(arg)
    return n


def replace_unicode(value):
    import re
    r = re.sub(r'&([A-Za-z]+);', '', value)
    return r


def parse_int(value):
    return int(value)


def get_date(value):
    return datetime.strptime(value, '%a, %d %b %Y %H:%M:%S %Z')


def files(values):
    n = int(math.floor(values / 2.00))
    return n


def parse_str(value):
    return str(value)


def price_format(value):
    decimal_points = 3
    separator = u' '
    value = str(value)
    if len(value) <= decimal_points:
        return value
    parts = []
    while value:
        parts.append(value[-decimal_points:])
        value = value[:-decimal_points]
    parts.reverse()
    return separator.join(parts)


def to_class_name(value):
    return value.__class__.__name__


def get_timer(d_from, d_to):
    from datetime import datetime
    now = datetime.now()
    e = time.mktime(d_to.timetuple())-time.mktime(now.timetuple())
    return int(e)


def sizes(value, arg):
    e = random.randint(int(arg.split(',')[0]), int(arg.split(',')[1]))
    return e


def get_million(value):
    return int((int(value)/1000000))


def parse_float(value):
    e, f = str(value).split('.')
    if int(f) > 0:
        return value
    return int(e)


def get_extension(value):
    f = str(value).split('.')
    return f[-1]


def get_arr_el(arr, id):
    return arr[id]


def is_entry(value, arg):
    if re.findall(arg, value) != 0:
        return True
    return False


def replace(value, param):
    f, e = param.split('#')
    try:
        value = str(value).replace(f, e)
    except:
        value = value.replace(f, e)
    return value


def die_ie(value):
    arr = ['MSIE 8', 'MSIE 7', 'MSIE 6']
    if len([i for i in arr if i in value]) > 0:
        return False
    return True


def truncate_p(value, param):
    if param > 1:
        return '</p>'.join(value.split('</p>')[:param])
    else:
        return value.split('</p>')[0]+'</p>'


register.filter('render_field', render_field)
register.filter('die_ie', die_ie)
register.filter('replace', replace)
register.filter('is_entry', is_entry)
register.filter('get_extension', get_extension)
register.filter('get_arr_el', get_arr_el)
register.filter('parse_float', parse_float)
register.filter('get_million', get_million)
register.filter('sizes', sizes)
register.filter('get_timer', get_timer)
register.filter('to_class_name', to_class_name)
register.filter('price', price_format)
register.filter('files', files)
register.filter('parse_date', get_date)
register.filter('str', parse_str)
register.filter('parse_int', parse_int)
register.filter('replace_unicode', replace_unicode)
register.filter('split', split)
register.filter('multiply', multiply)
register.filter('plus', plus)
register.filter('minus', minus)
register.filter('truncate_p',truncate_p)
