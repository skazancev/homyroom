from __future__ import unicode_literals

from django.apps import AppConfig


class Cms_pluginsConfig(AppConfig):
    name = 'cms_plugins'
