from contents.views import ProjectList, get_td_tour
from django.conf.urls import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^(?P<id>.*)/$', get_td_tour, name='tdtours_detail'),
    url(r'^$', ProjectList.as_view(type_content="tdtour"), name='tdtours'),
]