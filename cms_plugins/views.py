# vim:fileencoding=utf-8
from contents.models import Review
from django import forms
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render_to_response
from django.views.generic import View
from nts.utils import JSONResponseMixin, allowed_action
from models import Robot, FeedBack
from nts.settings import STATIC_URL, DEFAULT_FROM_EMAIL, DEFAULT_TO_EMAIL
from django.template import Context, loader
from django.core.mail import EmailMessage


def robots(request):
    ctx = {'robots': Robot.objects.all()}
    return render_to_response('plugins/robots.txt', ctx)


class FormFeedBack(forms.ModelForm):
    class Meta():
        model = FeedBack
        exclude = []


class FormReview(forms.ModelForm):
    class Meta():
        model = Review
        exclude = []


class AjaxMethods(JSONResponseMixin, View):

    @allowed_action
    def feedback(self, data):
        form = FormFeedBack(data)
        if form.is_valid():
            f = form.save()
            context = {'site': get_current_site(self.request), 'STATIC_URL': STATIC_URL, 'form': f}
            context = Context(context)
            temp = loader.get_template("mail/feedback.html")
            html = (temp.render(context)).encode('utf-8')
            msg = EmailMessage(u'Заявка с сайта', html, DEFAULT_FROM_EMAIL, DEFAULT_TO_EMAIL, headers={})
            msg.content_subtype = "html"
            msg.send()
            return {'success': True}
        return {'success': False}

    @allowed_action
    def review(self, data):
        form = FormReview(data)
        if form.is_valid():
            f = form.save()
            f.active = False
            f.save()
            context = {'site': get_current_site(self.request), 'STATIC_URL': STATIC_URL, 'form': f}
            context = Context(context)
            temp = loader.get_template("mail/review.html")
            html = (temp.render(context)).encode('utf-8')
            msg = EmailMessage(u'Новый отзыв', html, DEFAULT_FROM_EMAIL, DEFAULT_TO_EMAIL, headers={})
            msg.content_subtype = "html"
            msg.send()
            return {'success': True}
        return {'success': False}
