# coding=utf-8
from httplib import HTTPResponse
import json

from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from contents.models import Person, Partners, CategoryPrice, Project, Review, Question, TDTour, Title
from django import http
from django.shortcuts import render, render_to_response, get_object_or_404
from django.apps import apps
from django.views.generic import ListView, DetailView

from nts.settings import DEFAULT_FROM_EMAIL, DEFAULT_TO_EMAIL


class CommandView(ListView):
    template_name = 'contents/commands.html'
    model = Person

    def get_queryset(self):
        qs = super(CommandView, self).get_queryset().filter(active=True)
        return qs


class PartnerView(CommandView):
    template_name = 'contents/partners.html'
    model = Partners


class PriceView(CommandView):
    template_name = 'contents/prices.html'
    model = CategoryPrice


class ProjectList(ListView):
    paginate_by = 5
    type_content = 'project'
    template_name = 'contents/project-list.html'
    model = Project

    def render_to_response(self, context, **response_kwargs):
        if self.request.is_ajax():
            return self.get_json_response(self.convert_context_to_json(
                {'temp': render_to_string('contents/%s-endless.html' % self.type_content, context)}))
        return super(ProjectList, self).render_to_response(context, **response_kwargs)

    def get_json_response(self, content, **httpresponse_kwargs):
        return http.HttpResponse(content, content_type='application/json; charset=UTF-8', **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context)

    def get_queryset(self):
        model = apps.get_model(app_label='contents', model_name=self.type_content)
        qs = model.objects.filter(active=True)
        return qs

    def get_paginate_by(self, queryset):
        if self.request.GET.get('amount', False):
            self.paginate_by = int(self.request.GET['amount'])
        return self.paginate_by

    def get_context_data(self, **kwargs):
        ctx = super(ProjectList, self).get_context_data(**kwargs)
        try:
            ctx['top_title'] = Title.objects.get(pos='top')
        except Title.DoesNotExist:
            pass
        try:
            ctx['bottom_title'] = Title.objects.get(pos='bottom')
        except Title.DoesNotExist:
            pass
        ctx['pager'] = list(ctx['page_obj'].paginator.page_range)
        ctx['amount_list'] = range(5, 25, 5)
        ctx['temp'] = 'contents/%s-endless.html' % self.type_content
        if self.request.GET.get('amount', False):
            ctx['amount'] = self.request.GET['amount']
        return ctx


class ReviewList(ListView):
    paginate_by = 5
    template_name = 'contents/review-list.html'
    model = Review

    def render_to_response(self, context, **response_kwargs):
        if self.request.is_ajax():
            return self.get_json_response(
                self.convert_context_to_json({'temp': render_to_string('contents/reviews-endless.html', context)}))
        return super(ReviewList, self).render_to_response(context, **response_kwargs)

    def get_json_response(self, content, **httpresponse_kwargs):
        return http.HttpResponse(content, content_type='application/json; charset=UTF-8', **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context)

    def get_paginate_by(self, queryset):
        if self.request.GET.get('amount', False):
            self.paginate_by = int(self.request.GET['amount'])
        return self.paginate_by

    def get_queryset(self):
        qs = super(ReviewList, self).get_queryset().filter(active=True)
        return qs

    def get_context_data(self, **kwargs):
        ctx = super(ReviewList, self).get_context_data(**kwargs)
        ctx['pager'] = list(ctx['page_obj'].paginator.page_range)
        ctx['amount_list'] = range(5, 25, 5)
        if self.request.GET.get('amount', False):
            ctx['amount'] = self.request.GET['amount']
        return ctx


class ProjectDetail(DetailView):
    model = Project
    template_name = 'contents/project-detail.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectDetail, self).get_context_data(**kwargs)
        try:
            context['top_title'] = Title.objects.get(pos='top')
        except Title.DoesNotExist:
            pass
        try:
            context['bottom_title'] = Title.objects.get(pos='bottom')
        except Title.DoesNotExist:
            pass
        return context


class QuestList(ListView):
    model = Question
    template_name = 'contents/questions.html'

    def get_queryset(self):
        qs = super(QuestList, self).get_queryset().filter(active=True)
        return qs


def get_td_tour(request, id):
    tour = get_object_or_404(TDTour, id=id)
    return HttpResponse(open(tour.path).read())


@csrf_exempt
def view_object_bonus(request):
    error = True
    if request.is_ajax():
        if request.method == 'POST':
            phone = request.POST.get('phone')
            if phone:
                context = {'phone': request.POST.get('phone')}
                html = render_to_string('mail/view_object_bonus.html', context)
                msg = EmailMessage(u'Новая заявка', html, DEFAULT_FROM_EMAIL, DEFAULT_TO_EMAIL, headers={})
                msg.content_subtype = 'html'
                msg.send()
                error = False
    return HttpResponse(json.dumps({'error': error}), content_type='application/json')


@csrf_exempt
def view_object(request):
    error = True
    if request.is_ajax():
        if request.method == 'POST':
            phone = request.POST.get('phone')
            if phone:
                context = {'phone': request.POST.get('phone')}
                html = render_to_string('mail/view_object.html', context)
                msg = EmailMessage(u'Новая заявка', html, DEFAULT_FROM_EMAIL, DEFAULT_TO_EMAIL, headers={})
                msg.content_subtype = 'html'
                msg.send()
                error = False
    return HttpResponse(json.dumps({'error': error}), content_type='application/json')


def design(request):
    return render(request, 'contents/design.html')


def townhouse(request):
    return render(request, 'contents/townhouse.html')


def team(request):
    return render(request, 'contents/team.html')


def contacts(request):
    return render(request, 'contents/contacts.html')


def flat(request):
    return render(request, 'contents/flat.html')


def template_view(request):
    return render(request, 'contents/template_view.html')