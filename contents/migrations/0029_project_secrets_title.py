# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-10-06 01:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0028_auto_20161006_0419'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='secrets_title',
            field=models.TextField(default='', verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0435\u043a\u0440\u0435\u0442\u043e\u0432'),
            preserve_default=False,
        ),
    ]
