# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-18 08:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0005_categoryprice_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categoryprice',
            name='active',
            field=models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e\u0441\u0442\u044c'),
        ),
        migrations.AlterField(
            model_name='price',
            name='active',
            field=models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e\u0441\u0442\u044c'),
        ),
    ]
