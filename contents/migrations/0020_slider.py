# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-01 10:47
from __future__ import unicode_literals

from django.db import migrations, models
import nts.utils
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0019_auto_20160229_1519'),
    ]

    operations = [
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=nts.utils.get_file_path, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('area', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041f\u043b\u043e\u0449\u0430\u0434\u044c')),
                ('price', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0421\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c')),
                ('limit', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0421\u0440\u043e\u043a')),
                ('sort', models.IntegerField(default=0, verbose_name='\u041f\u0440\u0438\u043e\u0440\u0438\u0442\u0435\u0442')),
            ],
            options={
                'ordering': ['sort'],
                'verbose_name': '\u0421\u043b\u0430\u0439\u0434',
                'verbose_name_plural': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440',
            },
        ),
    ]
