# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-02 07:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0021_auto_20160301_1638'),
    ]

    operations = [
        migrations.AddField(
            model_name='tdtour',
            name='path',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041f\u0443\u0442\u044c'),
        ),
    ]
