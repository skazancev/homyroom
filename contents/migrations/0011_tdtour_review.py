# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-24 09:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0010_auto_20160224_1229'),
    ]

    operations = [
        migrations.AddField(
            model_name='tdtour',
            name='review',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tdtours', to='contents.Review', verbose_name='\u041e\u0442\u0437\u044b\u0432'),
        ),
    ]
