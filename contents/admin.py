from sorl.thumbnail.admin import AdminImageMixin
from django.contrib import admin
from models import Person, Partners, Price, CategoryPrice, DesignImage, DesignProject, Project, ProjectImage, Review, Secret, SecretImage, TDTour, \
    ReviewImage, Teaser, DesignService, TariffDesign, TypeBuild, TypeRepair, Popup, Question, Slider, FinalImage, Title


class PersonAdmin(admin.ModelAdmin):
    list_display = ['name', 'sort', 'active']
    list_editable = ['sort', 'active']


class SortAdmin(admin.ModelAdmin):
    list_display = ['name', 'sort']
    list_editable = ['sort']


class PriceInline(admin.TabularInline):
    model = Price


class DesignImageInline(AdminImageMixin, admin.TabularInline):
    model = DesignImage


class ProjectImageInline(admin.TabularInline):
    model = ProjectImage


class ProjectFinalImageInline(admin.TabularInline):
    model = FinalImage


class SecretImageInline(AdminImageMixin, admin.TabularInline):
    model = SecretImage


class CategoryPriceAdmin(PersonAdmin):
    inlines = [PriceInline]


class DesignProjectAdmin(PersonAdmin):
    inlines = [DesignImageInline]


class ProjectAdmin(PersonAdmin):
    prepopulated_fields = {"slug": ("name",)}
    filter_horizontal = ['secrets']
    inlines = [ProjectImageInline, ProjectFinalImageInline]


class SecretAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'sort']
    list_editable = ['sort']
    inlines = [SecretImageInline]


class ReviewImageAdmin(AdminImageMixin, admin.TabularInline):
    model = ReviewImage


class ReviewAdmin(PersonAdmin):
    inlines = [ReviewImageAdmin]


class TariffDesignAdmin(SortAdmin):
    filter_horizontal = ['service']


@admin.register(Title)
class TitleAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        if Title.objects.count() > 2:
            return False
        return True


admin.site.register(Popup)
admin.site.register(TariffDesign, TariffDesignAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(DesignProject, DesignProjectAdmin)
admin.site.register(Secret, SecretAdmin)
admin.site.register(CategoryPrice, CategoryPriceAdmin)
admin.site.register(Partners, PersonAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(TDTour, PersonAdmin)
admin.site.register(TypeRepair, SortAdmin)
admin.site.register(TypeBuild, SortAdmin)
admin.site.register(Slider, SortAdmin)
admin.site.register(Teaser, SortAdmin)
admin.site.register(DesignService, PersonAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(Question, PersonAdmin)
