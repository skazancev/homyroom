# vim:fileencoding=utf-8
import glob
import zipfile
from datetime import datetime
from cms.models import PlaceholderField
from django.db.models import permalink
from sorl.thumbnail import ImageField
from django.db import models
from nts.settings import MEDIA_URL, MEDIA_ROOT
from nts.utils import get_file_path, get_next_or_prev
import os
from redactor.fields import RedactorField


class Person(models.Model):
    class Meta():
        verbose_name = u'Сотрудник'
        verbose_name_plural = u'Сотрудники'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Имя', max_length=255)
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    image2 = ImageField(verbose_name=u'Большое изображение', upload_to=get_file_path)
    stage = models.CharField(verbose_name=u'Стаж', max_length=255, blank=True, null=True)
    post = models.CharField(verbose_name=u'Должность', max_length=255, blank=True, null=True)
    text = RedactorField(verbose_name=u'Описание', blank=True, null=True)
    active = models.BooleanField(verbose_name=u'Активность', default=True)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return self.name


class Partners(models.Model):
    class Meta():
        verbose_name = u'Партнёры'
        verbose_name_plural = u'Партнёры'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    text = models.TextField(verbose_name=u'Текст')
    url = models.CharField(verbose_name=u'Ссылка', max_length=255, blank=True, null=True)
    text_url = models.CharField(verbose_name=u'Текст ссылки', max_length=255, blank=True, null=True)
    active = models.BooleanField(verbose_name=u'Активность', default=True)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return self.name


class CategoryPrice(models.Model):
    class Meta():
        verbose_name = u'Позиция'
        verbose_name_plural = u'Прайс-лист'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)
    active = models.BooleanField(verbose_name=u'Активность', default=True)

    def __unicode__(self):
        return self.name

    def get_active_items(self):
        return self.items.filter(active=True)


class Price(models.Model):
    class Meta():
        verbose_name_plural = u'Цены'
        verbose_name = u'Цена'
        ordering = ['sort']

    category = models.ForeignKey(CategoryPrice, verbose_name=u'Категория', related_name='items')
    name = models.CharField(verbose_name=u'Название', max_length=255)
    unit = models.CharField(verbose_name=u'Единица измерения', max_length=255)
    price = models.CharField(verbose_name=u'Стоимость', max_length=255)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)
    active = models.BooleanField(verbose_name=u'Активность', default=True)

    def __unicode__(self):
        return self.name


class TDTour(models.Model):
    class Meta():
        verbose_name = u'3D тур'
        verbose_name_plural = u'3D туры'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    file = models.FileField(verbose_name=u'3D тур', upload_to=get_file_path, blank=True, null=True)
    path = models.CharField(verbose_name=u'Путь', max_length=255, blank=True, null=True)
    review = models.ForeignKey('Review', verbose_name=u'Отзыв', related_name='tdtours', blank=True, null=True)
    text = models.TextField(verbose_name=u'Описание')
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)
    active = models.BooleanField(verbose_name=u'Активность', default=True)

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        try:
            if self.file:
                zip = zipfile.ZipFile(self.file.path, 'r')
                name = self.file.name.split('/')[-1].split('.')[0]
                zip.extractall(os.path.join(MEDIA_ROOT, 'catalog/', 'tours/', name))
                zip.close()
                path = glob.glob(MEDIA_ROOT + 'catalog/tours/' + name + '/*/*.html')
                if len(path):
                    self.path = path[0]
                self.file = None
        except:
            pass
        return super(TDTour, self).save(force_insert=False, force_update=False, using=None, update_fields=None)

    # @permalink
    # def get_absolute_url(self):
    #     return 'tdtours_detail', (self.id, ), {}
    def get_absolute_url(self):
        if self.path:
            return '/media'+self.path.split('media')[-1]
        return self.image.path


class Review(models.Model):
    class Meta():
        verbose_name_plural = u'Отзывы'
        verbose_name = u'Отзыв'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Имя', max_length=255)
    proj_name = models.CharField(verbose_name=u'Название проекта', max_length=255)
    text = RedactorField(verbose_name=u'Текст отзыва')
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path, blank=True, null=True)
    rating = models.IntegerField(verbose_name=u'Оценка', help_text=u'От 1 до 5', default=1)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0, blank=True, null=True)
    active = models.BooleanField(verbose_name=u'Активность', default=True)

    def __unicode__(self):
        return self.name


class ReviewImage(models.Model):
    class Meta():
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения отзывов'
        ordering = ['sort']

    review = models.ForeignKey(Review, verbose_name=u'отзыв', related_name='images')
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return u'Изображение %s' % self.id


class DesignProject(models.Model):
    class Meta():
        verbose_name = u'Дизайн-проект'
        verbose_name_plural = u'Дизайн-проекты'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    text = models.TextField(verbose_name=u'Описание')
    review = models.ForeignKey(Review, verbose_name=u'Отзыв', related_name='designs', blank=True, null=True)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)
    active = models.BooleanField(verbose_name=u'Активность', default=True)

    def __unicode__(self):
        return self.name


class DesignImage(models.Model):
    class Meta():
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения дизайн-проектов'
        ordering = ['sort']

    project = models.ForeignKey(DesignProject, verbose_name=u'Дизайн проект', related_name='images')
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return u'Изображение %s' % self.id


class Secret(models.Model):
    class Meta():
        verbose_name_plural = u'Секреты'
        verbose_name = u'Секрет'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    sub_name = models.CharField(verbose_name=u'Название для определения', max_length=255)
    text = models.TextField(verbose_name=u'Описание')
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return self.sub_name


class SecretImage(models.Model):
    class Meta():
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения для секретов проекта'
        ordering = ['sort']

    project = models.ForeignKey(Secret, related_name='images')
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return u'Изображение %s' % self.id


class Project(models.Model):
    class Meta:
        verbose_name = u'Ремонт'
        verbose_name_plural = u'Ремонт'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    slug = models.SlugField(verbose_name=u'URL', max_length=255, unique=True)
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    area = models.CharField(verbose_name=u'Площадь', max_length=255, blank=True, null=True)
    price = models.CharField(verbose_name=u'Стоимость', max_length=255, blank=True, null=True)
    limit = models.CharField(verbose_name=u'Срок', max_length=255, blank=True, null=True)
    text = RedactorField(verbose_name=u'Описание', blank=True, null=True)
    person = models.ForeignKey(Person, verbose_name=u'Сотрудник', related_name='projects')
    secrets = models.ManyToManyField(Secret, verbose_name=u'Секреты проекта', blank=True)
    review = models.ForeignKey(Review, verbose_name=u'Отзыв', blank=True, null=True)
    design = models.OneToOneField(DesignProject, verbose_name=u'Дизайн проект', related_name='project', blank=True,
                                  null=True)
    tour = models.OneToOneField(TDTour, related_name='project', verbose_name=u'3D тур', blank=True, null=True)
    schtick = models.TextField(verbose_name=u'Фишка проекта', blank=True, null=True)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)
    active = models.BooleanField(verbose_name=u'Активность', default=True)
    place = PlaceholderField('place')
    meta_title = models.CharField(max_length=255, verbose_name=u'Тайтл', null=True, blank=True)
    meta_keywords = models.CharField(max_length=255, verbose_name=u'Keywords', null=True, blank=True)
    meta_description = models.TextField(max_length=255, verbose_name=u'Description', null=True, blank=True)

    def __unicode__(self):
        return self.name

    @permalink
    def get_absolute_url(self):
        return 'project_detail', (self.slug, ), {}


class FinalImage(models.Model):
    class Meta():
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения было/стало'
        ordering = ['sort']

    project = models.ForeignKey(Project, related_name='final_images')
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return u'Изображение %s' % self.id


class ProjectImage(models.Model):
    class Meta():
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения для выполненных ремонтов'
        ordering = ['sort']

    project = models.ForeignKey(Project, related_name='images')
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return u'Изображение %s' % self.id


CHOICE_ICO = (
    ('i1', u'5 лет гарантии'),
    ('i2', u'Материалы'),
    ('i3', u'Таймер'),
    ('i4', u'Календарь'),
    ('i5', u'Гарантия'),
    ('i6', u'Контроль'),
    ('i7', u'Оплата'),
    ('i8', u'Довольны заказчики'),
    ('i9', u'Индивидуальное проектирование'),
    ('i10', u'Увеличение площади'),
    ('ico1', u'Монеты'),
    ('ico2', u'Копилка'),
)


class Teaser(models.Model):
    class Meta():
        verbose_name_plural = u'Тизеры'
        verbose_name = u'Тизер'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    ico = models.CharField(verbose_name=u'Иконка', max_length=255, choices=CHOICE_ICO)
    text = models.TextField(verbose_name=u'Описание')
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return self.name


class TypeBuild(models.Model):
    class Meta():
        verbose_name = u'Тип'
        verbose_name_plural = u'Калькулятор: Типы помещений'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return self.name


class TypeRepair(models.Model):
    class Meta():
        verbose_name_plural = u'Калькулятор: Типы ремонтов'
        verbose_name = u'Тип'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    price = models.DecimalField(verbose_name=u'Стоимость', max_digits=15, decimal_places=2)
    text = models.TextField(verbose_name=u'Описание', blank=True, null=True)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return self.name


class DesignService(models.Model):
    class Meta():
        verbose_name = u'Услуга'
        verbose_name_plural = u'Калькулятор: Услуги дизайн-проекта'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    active = models.BooleanField(verbose_name=u'Активность', default=True)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return self.name


class TariffDesign(models.Model):
    class Meta():
        verbose_name_plural = u'Калькулятор: Тарифы дизайн-проекта'
        verbose_name = u'Тариф'

    name = models.CharField(verbose_name=u'Название', max_length=255)
    price = models.DecimalField(verbose_name=u'Стоимость', max_digits=15, decimal_places=2)
    sort = models.IntegerField(default=0, verbose_name=u'Приоритет')
    service = models.ManyToManyField(DesignService, verbose_name=u'Услуги', related_name='tariffs')

    def __unicode__(self):
        return self.name


class Popup(models.Model):
    class Meta():
        verbose_name = u'Попап'
        verbose_name_plural = u'Попапы'

    name = models.CharField(verbose_name=u'Название', max_length=255)
    text = RedactorField(verbose_name=u'Содержимое')

    def __unicode__(self):
        return self.name


class Question(models.Model):
    class Meta():
        verbose_name_plural = u'Вопросы и ответы'
        verbose_name = u'Вопрос'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Вопрос', max_length=255)
    answer = RedactorField(verbose_name=u'Ответ')
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)
    active = models.BooleanField(verbose_name=u'Активность', default=True)

    def __unicode__(self):
        return self.name


class Slider(models.Model):
    class Meta():
        verbose_name = u'Слайд'
        verbose_name_plural = u'Слайдер'
        ordering = ['sort']

    name = models.CharField(verbose_name=u'Название', max_length=255)
    image = ImageField(verbose_name=u'Изображение', upload_to=get_file_path)
    area = models.CharField(verbose_name=u'Площадь', max_length=255, blank=True, null=True)
    price = models.CharField(verbose_name=u'Стоимость', max_length=255, blank=True, null=True)
    limit = models.CharField(verbose_name=u'Срок', max_length=255, blank=True, null=True)
    sort = models.IntegerField(verbose_name=u'Приоритет', default=0)

    def __unicode__(self):
        return self.name

    def get_next(self):
        return get_next_or_prev(Slider.objects.all(), self)

    def get_prev(self):
        return get_next_or_prev(Slider.objects.all(), self, reverse=True)


class Title(models.Model):
    types = (
        ('top', u'Верхний заголовок'),
        ('bottom', u'Нижний заголовок'),
    )

    pos = models.CharField(choices=types, max_length=6, unique=True, verbose_name=u'Позиция')
    text = models.TextField(verbose_name=u'Текс')

    def __unicode__(self):
        return self.text

    class Meta:
        verbose_name = u'Заголовок'
        verbose_name_plural = u'Заголовки'
