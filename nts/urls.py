# from cms_plugins.views import AjaxMethods, robots
from cms_plugins.views import robots, AjaxMethods
from django.conf.urls import *
from django.contrib import admin

from contents.views import view_object, design, view_object_bonus
from nts import settings
from cms.sitemaps import CMSSitemap

sitemaps = {
    'cmspages': CMSSitemap,
}

urlpatterns = [
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^robots\.txt$', robots),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^ajax/$', AjaxMethods.as_view(), name='ajax'),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^view_object/$', view_object, name='view_object'),
    url(r'^view_object_bonus/$', view_object_bonus, name='view_object_bonus'),
    url(r'^', include('cms.urls')),
]

if settings.DEBUG:
    urlpatterns = [
                      url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                          {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
                      url(r'', include('django.contrib.staticfiles.urls')),
                  ] + urlpatterns
