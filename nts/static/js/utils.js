//$(document).ajaxSend(function (event, xhr, settings) {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    function sameOrigin(url) {
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
                // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
//});


function sendForm(options, form, onSuccess, onError, onErrorAjax) {
    var block = null;
    if (form.find('.send-form').hasClass('review')) {
        block = '#review_ok'
    }

    $.ajax({
        type: "POST",
        url: '/ajax/',
        contentType: 'application/json; charset=utf-8',
        data: $.toJSON(options),
        dataType: 'json',
        success: function (data) {
            if (data['success']) {
                onSuccess ? onSuccess(data, form, block) : false;
            }
            else {
                onError ? onError(data, form) : false;
            }
        },
        error: function (data) {
            onErrorAjax ? onErrorAjax() : false;
        }
    });
}

$(function () {
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
});

$(function () {
    $('.send-form').click(function (e) {
        var form = $(this).parents('form');
        var status = true;
        form.find('.req input, .req textarea, input.req').each(function () {
            if (!$(this).val()) {
                status = false;
                $(this).addClass('error');
            }
            else {
                $(this).removeClass('error');
            }
        });
        if ($('[name=honey]').val()) {
            status = false;
        }
        if (status) {
            form.find('.loader').css('visibility', 'visible');
            $(this).css('visibility', 'hidden');
            sendForm(form.serializeObject(), form, successSend, errorSend);
        }
        return false;
    });
});

function successSend(data, form, block) {
    block = block || '#ok';
    form[0].reset();
    errorSend(data, form);
    $.fancybox.open(block,{
        padding: 0,
        margin: 0
    })
}
function errorSend(data, form) {
    form.find('.send-form').css('visibility', 'visible');
    form.find('.loader').css('visibility', 'hidden');
}

function plural(i, s1, s3, s7) {
    s = String(i);
    e = s.slice(-2);
    f = parseInt(e);
    if (['11', '12', '13', '14'].indexOf(e) + 1 && (9 < f || f < 20 || f == 0)) {
        d = s7;
    }
    else {
        e = (s.slice(s.length - 1))[0];
        if (9 < i && i < 20 && i == 0) {
            d = s7;
        }
        else {
            if (e == '1') {
                d = s1;
            }
            else {
                if (['2', '3', '4'].indexOf(e) + 1) {
                    d = s3;
                }
                else {
                    d = s7;
                }
            }
        }
    }
    return d
}


$(function(){
    $('.respondes .tabs li a').click(function(e){
        e.preventDefault();
        if (!$(this).parents('li').is('.active')){
            $(this).parents('.tabs').find('li').removeClass('active');
            $(this).parents('li').addClass('active');
            if ($(this).data('id')){
                $('.respondes .resp').hide();
                $('.'+$(this).data('id')).show();
            }
            else $('.respondes .resp').show();
        }
    });
    $('.print').click(function(e){
        e.preventDefault();
        window.print();

    });
});

$(function(){
    $('.show').on('click', function(e){
        e.preventDefault();
        $(this).parents('.original').hide();
        $(this).parents('.original').next().show();
    });
    $('.show-all').on('click', function(e){
        e.preventDefault();
        $(this).next('.hide').show();
        $(this).hide();
    });
    $('.hid').on('click', function(e){
        e.preventDefault();
        $(this).parents('.hide').hide();
        $(this).parents('.hide').prev().show();
    });

    $('.pager a').on('click', function(e){
        e.preventDefault();
        var page = $('.endless_page_template');
        SendRequest(page, $(this).attr('href'));
    });
    $('select[name=amount]').on('change', function(e){
        e.preventDefault();
        var page = $('.endless_page_template');
        SendRequest(page, location.pathname+'?page=1');
    });

    $('.form .marks span.stars').click(function(){
        var obj = $(this);
        SetStar(obj);
    });
});

function SetStar(obj){
    obj.parents('.marks').find('span:not(.stars)').css('width', '0');
        obj.parents('.marks').find('span.stars').each(function(){
            if ($(this).index()<= obj.index()){
                $(this).find('span').css('width', '100%')
            }
        });
    obj.parents('.form').find('[name=rating]').val(obj.index()+1);
    $('.popup .rating').text(obj.index()+1 + plural(obj.index()+1, ' балл', ' балла', ' баллов'))

}

function SendRequest(page, path){
    $.ajax({
        type: "GET",
        url: path+'&amount=' + $('select[name=amount]').val(),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            $('body').animate({scrollTop: page.offset().top-180}, 400);
            page.html(data['temp']);
            $('select').styler();
        },
        error: function (data) {

        }
    });
}