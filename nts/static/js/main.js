$(function () {
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    $('.slider ul').carouFredSel({
        items: 1,
        circular: true,
        responsive: true,
        swipe: {
            onMouse: true,
            onTouch: true
        },
        next: '.next',
        prev: '.prev',
        scroll: {
            items: 1,
            fx: 'crossfade'
        },
        auto: {
            play: true,
            timeoutDuration: 10000
        }
    });

    $('select').styler();

    $('.faq').tooltipster({
        theme: 'tooltipster-light',
        position: 'right'
    });

    $('.prices .container>ul>li>a').click(function(e){
        e.preventDefault();
        $(this).next().slideToggle(300);
        $(this).parent().toggleClass('open');
    });

    $('input[name="phone"]').mask('+7(999)999-99-99');

    $('.fancy').fancybox({
        padding: 0,
        margin: 30

    });

    $('.fancy2').fancybox({
        padding: 0,
        margin: 30,
        helpers		: {
    			title	: { type : 'outside' },
    			buttons	: {position:'bottom'}
		    },
        arrows: !isMobile.any(),
        afterShow: function() {
            $('.fancybox-wrap').swipe({

                swipe : function(event, direction) {
                    if (direction === 'left' || direction === 'up') {
                      $.fancybox.next( direction );
                    } else {
                      $.fancybox.prev( direction );
                    }
                }
            });

        },
        beforeShow : function() {
        this.title = 'Изображение ' + (this.index + 1) + ' из ' + this.group.length;
        }
    });

    $('.member img').hover(function(){
        $(this).parents('.member').find('.member-more').css('display', 'block');
    }, function(){
        $(this).parents('.member').find('.member-more').css('display', 'none');
    });

});

$(function() {
    $('.top-menu').materialmenu({
        mobileWidth: 1210,
        buttonClass: 'burger',
        onChangeMobile: function (element) {
            element.after($('.serv-menu').clone());
        },
        onChangeDesktop: function (element) {
            $('.material-menu-wrapper .serv-menu').remove();
        }
    });

});

$(document).ready(function(){
    $('button.btn.anim.fw600.flr.fancy').click(function(){
        yaCounter37510950.reachGoal('Calcbtn');
        return true;
    })

    $("a.btn.anim.fancy.fw600[href='#callback']").click(function(){
        yaCounter37510950.reachGoal('Headerbtn');
        return true;
    })
    $("a.btn.anim.fancy.fw600[href='#calc-form']").click(function(){
        yaCounter37510950.reachGoal('Headerbtn');
        return true;
    })

})
