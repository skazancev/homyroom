-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 02 2016 г., 12:19
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `nts`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=191 ;

--
-- Дамп данных таблицы `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can use Structure mode', 1, 'use_structure'),
(2, '', 2, 'change_page'),
(3, 'Can add log entry', 3, 'add_logentry'),
(4, 'Can change log entry', 3, 'change_logentry'),
(5, 'Can delete log entry', 3, 'delete_logentry'),
(6, 'Can add permission', 4, 'add_permission'),
(7, 'Can change permission', 4, 'change_permission'),
(8, 'Can delete permission', 4, 'delete_permission'),
(9, 'Can add group', 5, 'add_group'),
(10, 'Can change group', 5, 'change_group'),
(11, 'Can delete group', 5, 'delete_group'),
(12, 'Can add user', 6, 'add_user'),
(13, 'Can change user', 6, 'change_user'),
(14, 'Can delete user', 6, 'delete_user'),
(15, 'Can add content type', 7, 'add_contenttype'),
(16, 'Can change content type', 7, 'change_contenttype'),
(17, 'Can delete content type', 7, 'delete_contenttype'),
(18, 'Can add session', 8, 'add_session'),
(19, 'Can change session', 8, 'change_session'),
(20, 'Can delete session', 8, 'delete_session'),
(21, 'Can add site', 9, 'add_site'),
(22, 'Can change site', 9, 'change_site'),
(23, 'Can delete site', 9, 'delete_site'),
(24, 'Can add kv store', 10, 'add_kvstore'),
(25, 'Can change kv store', 10, 'change_kvstore'),
(26, 'Can delete kv store', 10, 'delete_kvstore'),
(27, 'Can add user setting', 11, 'add_usersettings'),
(28, 'Can change user setting', 11, 'change_usersettings'),
(29, 'Can delete user setting', 11, 'delete_usersettings'),
(30, 'Can add page', 2, 'add_page'),
(31, 'Can delete page', 2, 'delete_page'),
(32, 'Can view page', 2, 'view_page'),
(33, 'Can publish page', 2, 'publish_page'),
(34, 'Can edit static placeholders', 2, 'edit_static_placeholder'),
(35, 'Can add Page global permission', 12, 'add_globalpagepermission'),
(36, 'Can change Page global permission', 12, 'change_globalpagepermission'),
(37, 'Can delete Page global permission', 12, 'delete_globalpagepermission'),
(38, 'Can add Page permission', 13, 'add_pagepermission'),
(39, 'Can change Page permission', 13, 'change_pagepermission'),
(40, 'Can delete Page permission', 13, 'delete_pagepermission'),
(41, 'Can add User (page)', 14, 'add_pageuser'),
(42, 'Can change User (page)', 14, 'change_pageuser'),
(43, 'Can delete User (page)', 14, 'delete_pageuser'),
(44, 'Can add User group (page)', 15, 'add_pageusergroup'),
(45, 'Can change User group (page)', 15, 'change_pageusergroup'),
(46, 'Can delete User group (page)', 15, 'delete_pageusergroup'),
(47, 'Can add placeholder', 1, 'add_placeholder'),
(48, 'Can change placeholder', 1, 'change_placeholder'),
(49, 'Can delete placeholder', 1, 'delete_placeholder'),
(50, 'Can add cms plugin', 16, 'add_cmsplugin'),
(51, 'Can change cms plugin', 16, 'change_cmsplugin'),
(52, 'Can delete cms plugin', 16, 'delete_cmsplugin'),
(53, 'Can add title', 17, 'add_title'),
(54, 'Can change title', 17, 'change_title'),
(55, 'Can delete title', 17, 'delete_title'),
(56, 'Can add placeholder reference', 18, 'add_placeholderreference'),
(57, 'Can change placeholder reference', 18, 'change_placeholderreference'),
(58, 'Can delete placeholder reference', 18, 'delete_placeholderreference'),
(59, 'Can add static placeholder', 19, 'add_staticplaceholder'),
(60, 'Can change static placeholder', 19, 'change_staticplaceholder'),
(61, 'Can delete static placeholder', 19, 'delete_staticplaceholder'),
(62, 'Can add alias plugin model', 20, 'add_aliaspluginmodel'),
(63, 'Can change alias plugin model', 20, 'change_aliaspluginmodel'),
(64, 'Can delete alias plugin model', 20, 'delete_aliaspluginmodel'),
(65, 'Can add urlconf revision', 21, 'add_urlconfrevision'),
(66, 'Can change urlconf revision', 21, 'change_urlconfrevision'),
(67, 'Can delete urlconf revision', 21, 'delete_urlconfrevision'),
(68, 'Can add cache key', 22, 'add_cachekey'),
(69, 'Can change cache key', 22, 'change_cachekey'),
(70, 'Can delete cache key', 22, 'delete_cachekey'),
(71, 'Can add revision', 23, 'add_revision'),
(72, 'Can change revision', 23, 'change_revision'),
(73, 'Can delete revision', 23, 'delete_revision'),
(74, 'Can add version', 24, 'add_version'),
(75, 'Can change version', 24, 'change_version'),
(76, 'Can delete version', 24, 'delete_version'),
(77, 'Can add Блок', 25, 'add_block'),
(78, 'Can change Блок', 25, 'change_block'),
(79, 'Can delete Блок', 25, 'delete_block'),
(80, 'Can add robot', 26, 'add_robot'),
(81, 'Can change robot', 26, 'change_robot'),
(82, 'Can delete robot', 26, 'delete_robot'),
(83, 'Can add keyword', 27, 'add_keyword'),
(84, 'Can change keyword', 27, 'change_keyword'),
(85, 'Can delete keyword', 27, 'delete_keyword'),
(86, 'Can add base model pl', 28, 'add_basemodelpl'),
(87, 'Can change base model pl', 28, 'change_basemodelpl'),
(88, 'Can delete base model pl', 28, 'delete_basemodelpl'),
(89, 'Can add Текст', 29, 'add_textmodel'),
(90, 'Can change Текст', 29, 'change_textmodel'),
(91, 'Can delete Текст', 29, 'delete_textmodel'),
(92, 'Can add HTML-вставка', 30, 'add_htmlinsertmodel'),
(93, 'Can change HTML-вставка', 30, 'change_htmlinsertmodel'),
(94, 'Can delete HTML-вставка', 30, 'delete_htmlinsertmodel'),
(95, 'Can add HTML-блок', 31, 'add_blockmodel'),
(96, 'Can change HTML-блок', 31, 'change_blockmodel'),
(97, 'Can delete HTML-блок', 31, 'delete_blockmodel'),
(98, 'Can add Обратная связь', 32, 'add_feedback'),
(99, 'Can change Обратная связь', 32, 'change_feedback'),
(100, 'Can delete Обратная связь', 32, 'delete_feedback'),
(101, 'Can add Сотрудник', 33, 'add_person'),
(102, 'Can change Сотрудник', 33, 'change_person'),
(103, 'Can delete Сотрудник', 33, 'delete_person'),
(104, 'Can add Команда', 34, 'add_personpluginmodel'),
(105, 'Can change Команда', 34, 'change_personpluginmodel'),
(106, 'Can delete Команда', 34, 'delete_personpluginmodel'),
(107, 'Can add Партнёры', 35, 'add_partners'),
(108, 'Can change Партнёры', 35, 'change_partners'),
(109, 'Can delete Партнёры', 35, 'delete_partners'),
(110, 'Can add Позиция', 36, 'add_categoryprice'),
(111, 'Can change Позиция', 36, 'change_categoryprice'),
(112, 'Can delete Позиция', 36, 'delete_categoryprice'),
(113, 'Can add Цена', 37, 'add_price'),
(114, 'Can change Цена', 37, 'change_price'),
(115, 'Can delete Цена', 37, 'delete_price'),
(116, 'Can add 3D тур', 38, 'add_tdtour'),
(117, 'Can change 3D тур', 38, 'change_tdtour'),
(118, 'Can delete 3D тур', 38, 'delete_tdtour'),
(119, 'Can add Отзыв', 39, 'add_review'),
(120, 'Can change Отзыв', 39, 'change_review'),
(121, 'Can delete Отзыв', 39, 'delete_review'),
(122, 'Can add Дизайн-проект', 40, 'add_designproject'),
(123, 'Can change Дизайн-проект', 40, 'change_designproject'),
(124, 'Can delete Дизайн-проект', 40, 'delete_designproject'),
(125, 'Can add Изображение', 41, 'add_designimage'),
(126, 'Can change Изображение', 41, 'change_designimage'),
(127, 'Can delete Изображение', 41, 'delete_designimage'),
(128, 'Can add Секрет', 42, 'add_secret'),
(129, 'Can change Секрет', 42, 'change_secret'),
(130, 'Can delete Секрет', 42, 'delete_secret'),
(131, 'Can add Изображение', 43, 'add_secretimage'),
(132, 'Can change Изображение', 43, 'change_secretimage'),
(133, 'Can delete Изображение', 43, 'delete_secretimage'),
(134, 'Can add Ремонт', 44, 'add_project'),
(135, 'Can change Ремонт', 44, 'change_project'),
(136, 'Can delete Ремонт', 44, 'delete_project'),
(137, 'Can add Изображение', 45, 'add_projectimage'),
(138, 'Can change Изображение', 45, 'change_projectimage'),
(139, 'Can delete Изображение', 45, 'delete_projectimage'),
(140, 'Can add Отзывы', 46, 'add_reviewpluginmodel'),
(141, 'Can change Отзывы', 46, 'change_reviewpluginmodel'),
(142, 'Can delete Отзывы', 46, 'delete_reviewpluginmodel'),
(143, 'Can add Изображение', 47, 'add_reviewimage'),
(144, 'Can change Изображение', 47, 'change_reviewimage'),
(145, 'Can delete Изображение', 47, 'delete_reviewimage'),
(146, 'Can add Тизер', 48, 'add_teaser'),
(147, 'Can change Тизер', 48, 'change_teaser'),
(148, 'Can delete Тизер', 48, 'delete_teaser'),
(149, 'Can add Тизеры', 49, 'add_teaserspluginmodel'),
(150, 'Can change Тизеры', 49, 'change_teaserspluginmodel'),
(151, 'Can delete Тизеры', 49, 'delete_teaserspluginmodel'),
(152, 'Can add Тип', 50, 'add_typebuild'),
(153, 'Can change Тип', 50, 'change_typebuild'),
(154, 'Can delete Тип', 50, 'delete_typebuild'),
(155, 'Can add Тип', 51, 'add_typerepair'),
(156, 'Can change Тип', 51, 'change_typerepair'),
(157, 'Can delete Тип', 51, 'delete_typerepair'),
(158, 'Can add Услуга', 52, 'add_designservice'),
(159, 'Can change Услуга', 52, 'change_designservice'),
(160, 'Can delete Услуга', 52, 'delete_designservice'),
(161, 'Can add Тариф', 53, 'add_tariffdesign'),
(162, 'Can change Тариф', 53, 'change_tariffdesign'),
(163, 'Can delete Тариф', 53, 'delete_tariffdesign'),
(164, 'Can add Калькулятор', 54, 'add_calc'),
(165, 'Can change Калькулятор', 54, 'change_calc'),
(166, 'Can delete Калькулятор', 54, 'delete_calc'),
(167, 'Can add url line', 55, 'add_urlline'),
(168, 'Can change url line', 55, 'change_urlline'),
(169, 'Can delete url line', 55, 'delete_urlline'),
(170, 'Can add base menu pl', 56, 'add_basemenupl'),
(171, 'Can change base menu pl', 56, 'change_basemenupl'),
(172, 'Can delete base menu pl', 56, 'delete_basemenupl'),
(173, 'Can add project plugin model', 57, 'add_projectpluginmodel'),
(174, 'Can change project plugin model', 57, 'change_projectpluginmodel'),
(175, 'Can delete project plugin model', 57, 'delete_projectpluginmodel'),
(176, 'Can add Попап', 58, 'add_popup'),
(177, 'Can change Попап', 58, 'change_popup'),
(178, 'Can delete Попап', 58, 'delete_popup'),
(179, 'Can add Дизайн-проекты', 59, 'add_designpluginmodel'),
(180, 'Can change Дизайн-проекты', 59, 'change_designpluginmodel'),
(181, 'Can delete Дизайн-проекты', 59, 'delete_designpluginmodel'),
(182, 'Can add Вопрос', 60, 'add_question'),
(183, 'Can change Вопрос', 60, 'change_question'),
(184, 'Can delete Вопрос', 60, 'delete_question'),
(185, 'Can add Слайдер', 61, 'add_sliderpluginmodel'),
(186, 'Can change Слайдер', 61, 'change_sliderpluginmodel'),
(187, 'Can delete Слайдер', 61, 'delete_sliderpluginmodel'),
(188, 'Can add Слайд', 62, 'add_slider'),
(189, 'Can change Слайд', 62, 'change_slider'),
(190, 'Can delete Слайд', 62, 'delete_slider');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$24000$F8YZuZP9zIuF$acvsX82ppY0Oeuc0yRFqaAoIh0v+inrRs+I0DlhjOOU=', '2016-03-01 09:38:18', 1, 'admin', '', '', 'email@mail.ru', 1, 1, '2016-02-17 13:03:13');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_aliaspluginmodel`
--

CREATE TABLE IF NOT EXISTS `cms_aliaspluginmodel` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `plugin_id` int(11) DEFAULT NULL,
  `alias_placeholder_id` int(11),
  PRIMARY KEY (`cmsplugin_ptr_id`),
  KEY `cms_aliaspluginmodel_plugin_id_9867676e_fk_cms_cmsplugin_id` (`plugin_id`),
  KEY `cms_aliaspluginmodel_921abf5c` (`alias_placeholder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_cmsplugin`
--

CREATE TABLE IF NOT EXISTS `cms_cmsplugin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` smallint(5) unsigned DEFAULT NULL,
  `language` varchar(15) NOT NULL,
  `plugin_type` varchar(50) NOT NULL,
  `creation_date` datetime NOT NULL,
  `changed_date` datetime NOT NULL,
  `parent_id` int(11),
  `placeholder_id` int(11),
  `depth` int(10) unsigned NOT NULL,
  `numchild` int(10) unsigned NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_cmsplugin_path_4917bb44_uniq` (`path`),
  KEY `cms_cmsplugin_8512ae7d` (`language`),
  KEY `cms_cmsplugin_b5e4cf8f` (`plugin_type`),
  KEY `cms_cmsplugin_6be37982` (`parent_id`),
  KEY `cms_cmsplugin_667a6151` (`placeholder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=151 ;

--
-- Дамп данных таблицы `cms_cmsplugin`
--

INSERT INTO `cms_cmsplugin` (`id`, `position`, `language`, `plugin_type`, `creation_date`, `changed_date`, `parent_id`, `placeholder_id`, `depth`, `numchild`, `path`) VALUES
(1, 0, 'ru', 'TextPlugin', '2016-02-18 07:01:44', '2016-02-18 07:03:32', NULL, 3, 1, 0, '0001'),
(2, 1, 'ru', 'HtmlInsertPlugin', '2016-02-18 07:06:57', '2016-02-18 07:07:14', NULL, 3, 1, 0, '0002'),
(3, 2, 'ru', 'PersonPlugin', '2016-02-18 07:14:06', '2016-02-18 07:14:36', NULL, 3, 1, 0, '0003'),
(4, 0, 'ru', 'TextPlugin', '2016-02-18 07:01:44', '2016-02-18 07:15:41', NULL, 17, 1, 0, '0004'),
(5, 1, 'ru', 'HtmlInsertPlugin', '2016-02-18 07:06:57', '2016-02-18 07:15:41', NULL, 17, 1, 0, '0005'),
(6, 2, 'ru', 'PersonPlugin', '2016-02-18 07:14:06', '2016-02-18 07:15:41', NULL, 17, 1, 0, '0006'),
(7, 0, 'ru', 'HtmlInsertPlugin', '2016-02-18 08:22:31', '2016-02-18 08:23:27', NULL, 23, 1, 0, '0007'),
(9, 0, 'ru', 'TextPlugin', '2016-02-18 09:05:33', '2016-02-18 09:06:11', NULL, 30, 1, 0, '0009'),
(10, 0, 'ru', 'HtmlInsertPlugin', '2016-02-18 09:07:24', '2016-02-18 09:07:53', NULL, 32, 1, 0, '000A'),
(11, 0, 'ru', 'TextPlugin', '2016-02-18 09:05:33', '2016-02-18 09:08:19', NULL, 31, 1, 0, '000B'),
(12, 0, 'ru', 'HtmlInsertPlugin', '2016-02-18 09:07:24', '2016-02-18 09:08:19', NULL, 33, 1, 0, '000C'),
(13, 0, 'ru', 'ReviewPlugin', '2016-02-24 08:32:47', '2016-02-24 08:33:09', NULL, 49, 1, 0, '000D'),
(14, 1, 'ru', 'HtmlInsertPlugin', '2016-02-24 09:03:04', '2016-02-24 09:03:19', NULL, 49, 1, 0, '000E'),
(15, 0, 'ru', 'ReviewPlugin', '2016-02-24 08:32:47', '2016-02-24 09:03:38', NULL, 51, 1, 0, '000F'),
(16, 1, 'ru', 'HtmlInsertPlugin', '2016-02-24 09:03:04', '2016-02-24 09:03:38', NULL, 51, 1, 0, '000G'),
(17, 0, 'ru', 'ReviewPlugin', '2016-02-24 11:36:55', '2016-02-24 11:37:20', NULL, 45, 1, 0, '000H'),
(18, 1, 'ru', 'HtmlInsertPlugin', '2016-02-24 11:37:49', '2016-02-24 11:38:04', NULL, 45, 1, 0, '000I'),
(19, 0, 'ru', 'ReviewPlugin', '2016-02-24 11:36:55', '2016-02-24 11:40:33', NULL, 47, 1, 0, '000J'),
(20, 1, 'ru', 'HtmlInsertPlugin', '2016-02-24 11:37:49', '2016-02-24 11:40:33', NULL, 47, 1, 0, '000K'),
(21, 0, 'ru', 'ReviewPlugin', '2016-02-24 12:01:25', '2016-02-24 12:01:54', NULL, 43, 1, 0, '000L'),
(22, 1, 'ru', 'HtmlInsertPlugin', '2016-02-24 12:02:19', '2016-02-24 12:03:22', NULL, 43, 1, 0, '000M'),
(23, 0, 'ru', 'ReviewPlugin', '2016-02-24 12:01:25', '2016-02-24 12:03:36', NULL, 39, 1, 0, '000N'),
(24, 1, 'ru', 'HtmlInsertPlugin', '2016-02-24 12:02:19', '2016-02-24 12:03:36', NULL, 39, 1, 0, '000O'),
(25, 0, 'ru', 'HtmlInsertPlugin', '2016-02-24 14:03:52', '2016-02-24 14:04:10', NULL, 56, 1, 0, '000P'),
(26, 1, 'ru', 'TeasersPlugin', '2016-02-29 09:03:26', '2016-02-29 09:03:39', NULL, 56, 1, 0, '000Q'),
(27, 2, 'ru', 'CalcPlugin', '2016-02-29 10:43:41', '2016-02-29 10:46:41', NULL, 56, 1, 0, '000R'),
(28, 3, 'ru', 'HtmlInsertPlugin', '2016-02-29 10:48:04', '2016-02-29 10:49:03', NULL, 56, 1, 0, '000S'),
(29, 4, 'ru', 'PersonPlugin', '2016-02-29 10:49:26', '2016-02-29 10:49:40', NULL, 56, 1, 0, '000T'),
(30, 5, 'ru', 'ReviewPlugin', '2016-02-29 10:49:55', '2016-02-29 10:50:08', NULL, 56, 1, 0, '000U'),
(31, 6, 'ru', 'HtmlInsertPlugin', '2016-02-29 10:50:33', '2016-02-29 10:50:45', NULL, 56, 1, 0, '000V'),
(32, 0, 'ru', 'BaseMenuPlugin', '2016-02-29 11:11:05', '2016-02-29 11:15:16', NULL, 73, 1, 0, '000Y'),
(33, 0, 'ru', 'BaseMenuPlugin', '2016-02-29 11:13:07', '2016-02-29 11:13:51', NULL, 63, 1, 0, '000X'),
(36, 0, 'ru', 'BaseMenuPlugin', '2016-02-29 11:11:05', '2016-02-29 11:15:59', NULL, 74, 1, 0, '000Z'),
(37, 0, 'ru', 'ProjectPlugin', '2016-02-29 11:20:27', '2016-02-29 11:21:50', NULL, 58, 1, 0, '0010'),
(38, 1, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:22:22', '2016-02-29 11:22:39', NULL, 58, 1, 0, '0011'),
(39, 2, 'ru', 'TeasersPlugin', '2016-02-29 11:22:56', '2016-02-29 11:23:12', NULL, 58, 1, 0, '0012'),
(40, 3, 'ru', 'CalcPlugin', '2016-02-29 11:30:42', '2016-02-29 11:32:32', NULL, 58, 1, 0, '0013'),
(41, 4, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:35:21', '2016-03-01 12:03:37', NULL, 58, 1, 0, '0014'),
(42, 5, 'ru', 'PersonPlugin', '2016-02-29 11:36:35', '2016-02-29 11:36:49', NULL, 58, 1, 0, '0015'),
(43, 6, 'ru', 'ReviewPlugin', '2016-02-29 11:37:02', '2016-02-29 11:37:20', NULL, 58, 1, 0, '0016'),
(44, 7, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:37:35', '2016-02-29 11:37:51', NULL, 58, 1, 0, '0017'),
(53, 0, 'ru', 'PlaceholderPlugin', '2016-02-29 11:39:32', '2016-02-29 11:39:32', NULL, 1, 1, 0, '001G'),
(54, 0, 'ru', 'ProjectPlugin', '2016-02-29 11:20:27', '2016-02-29 11:39:32', NULL, 75, 1, 0, '001H'),
(55, 1, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:22:22', '2016-02-29 11:39:32', NULL, 75, 1, 0, '001I'),
(56, 2, 'ru', 'TeasersPlugin', '2016-02-29 11:22:56', '2016-02-29 11:39:32', NULL, 75, 1, 0, '001J'),
(57, 3, 'ru', 'CalcPlugin', '2016-02-29 11:30:42', '2016-02-29 11:39:32', NULL, 75, 1, 0, '001K'),
(58, 4, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:35:21', '2016-02-29 11:39:32', NULL, 75, 1, 0, '001L'),
(59, 5, 'ru', 'PersonPlugin', '2016-02-29 11:36:35', '2016-02-29 11:39:32', NULL, 75, 1, 0, '001M'),
(60, 6, 'ru', 'ReviewPlugin', '2016-02-29 11:37:02', '2016-02-29 11:39:32', NULL, 75, 1, 0, '001N'),
(61, 7, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:37:35', '2016-02-29 11:39:32', NULL, 75, 1, 0, '001O'),
(62, 0, 'ru', 'ProjectPlugin', '2016-02-29 11:20:27', '2016-02-29 11:42:49', NULL, 60, 1, 0, '001P'),
(63, 1, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:22:22', '2016-02-29 11:40:02', NULL, 60, 1, 0, '001Q'),
(64, 2, 'ru', 'TeasersPlugin', '2016-02-29 11:22:56', '2016-02-29 11:40:03', NULL, 60, 1, 0, '001R'),
(65, 3, 'ru', 'CalcPlugin', '2016-02-29 11:30:42', '2016-02-29 11:43:12', NULL, 60, 1, 0, '001S'),
(66, 4, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:35:21', '2016-03-01 12:07:43', NULL, 60, 1, 0, '001T'),
(67, 5, 'ru', 'PersonPlugin', '2016-02-29 11:36:35', '2016-02-29 11:40:03', NULL, 60, 1, 0, '001U'),
(68, 6, 'ru', 'ReviewPlugin', '2016-02-29 11:37:02', '2016-02-29 11:40:03', NULL, 60, 1, 0, '001V'),
(69, 7, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:37:35', '2016-02-29 11:40:03', NULL, 60, 1, 0, '001W'),
(78, 0, 'ru', 'DesignPlugin', '2016-02-29 11:48:11', '2016-02-29 11:58:11', NULL, 62, 1, 0, '0025'),
(80, 1, 'ru', 'CalcPlugin', '2016-02-29 12:05:41', '2016-02-29 12:08:58', NULL, 62, 1, 0, '0026'),
(81, 2, 'ru', 'TeasersPlugin', '2016-02-29 12:11:31', '2016-02-29 12:15:02', NULL, 62, 1, 0, '0027'),
(82, 3, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:13:49', '2016-02-29 12:14:15', NULL, 62, 1, 0, '0028'),
(83, 4, 'ru', 'PersonPlugin', '2016-02-29 12:15:16', '2016-02-29 12:15:29', NULL, 62, 1, 0, '0029'),
(84, 5, 'ru', 'ReviewPlugin', '2016-02-29 12:15:45', '2016-02-29 12:15:57', NULL, 62, 1, 0, '002A'),
(85, 6, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:16:14', '2016-02-29 12:16:26', NULL, 62, 1, 0, '002B'),
(86, 0, 'ru', 'DesignPlugin', '2016-02-29 11:48:11', '2016-02-29 12:16:51', NULL, 70, 1, 0, '002C'),
(87, 1, 'ru', 'CalcPlugin', '2016-02-29 12:05:41', '2016-02-29 12:16:51', NULL, 70, 1, 0, '002D'),
(88, 2, 'ru', 'TeasersPlugin', '2016-02-29 12:11:31', '2016-02-29 12:16:51', NULL, 70, 1, 0, '002E'),
(89, 3, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:13:49', '2016-02-29 12:16:51', NULL, 70, 1, 0, '002F'),
(90, 4, 'ru', 'PersonPlugin', '2016-02-29 12:15:16', '2016-02-29 12:16:51', NULL, 70, 1, 0, '002G'),
(91, 5, 'ru', 'ReviewPlugin', '2016-02-29 12:15:45', '2016-02-29 12:16:51', NULL, 70, 1, 0, '002H'),
(92, 6, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:16:14', '2016-02-29 12:16:51', NULL, 70, 1, 0, '002I'),
(93, 0, 'ru', 'BaseMenuPlugin', '2016-02-29 11:13:07', '2016-02-29 12:24:22', NULL, 71, 1, 0, '002J'),
(94, 0, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:29:19', '2016-02-29 12:29:40', NULL, 7, 1, 0, '002K'),
(95, 1, 'ru', 'TeasersPlugin', '2016-02-29 12:29:55', '2016-02-29 12:30:08', NULL, 7, 1, 0, '002L'),
(96, 2, 'ru', 'CalcPlugin', '2016-02-29 12:30:22', '2016-02-29 12:30:57', NULL, 7, 1, 0, '002M'),
(97, 3, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:31:10', '2016-02-29 12:31:30', NULL, 7, 1, 0, '002N'),
(98, 4, 'ru', 'PersonPlugin', '2016-02-29 12:31:45', '2016-02-29 12:31:57', NULL, 7, 1, 0, '002O'),
(99, 5, 'ru', 'ReviewPlugin', '2016-02-29 12:32:10', '2016-02-29 12:32:29', NULL, 7, 1, 0, '002P'),
(100, 6, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:32:42', '2016-02-29 12:32:54', NULL, 7, 1, 0, '002Q'),
(109, 0, 'ru', 'SliderPlugin', '2016-03-01 10:50:27', '2016-03-01 10:50:41', NULL, 6, 1, 0, '002Y'),
(118, 0, 'ru', 'HtmlInsertPlugin', '2016-02-18 08:22:31', '2016-03-01 11:06:58', NULL, 25, 1, 0, '0037'),
(119, 0, 'ru', 'HtmlInsertPlugin', '2016-03-01 11:07:58', '2016-03-01 11:08:26', NULL, 81, 1, 0, '0038'),
(120, 1, 'ru', 'HtmlInsertPlugin', '2016-03-01 11:08:49', '2016-03-01 11:09:23', NULL, 81, 1, 0, '0039'),
(121, 0, 'ru', 'HtmlInsertPlugin', '2016-03-01 11:09:53', '2016-03-01 11:11:03', NULL, 83, 1, 0, '003A'),
(122, 1, 'ru', 'HtmlInsertPlugin', '2016-03-01 11:11:18', '2016-03-01 11:11:33', NULL, 83, 1, 0, '003B'),
(123, 0, 'ru', 'SliderPlugin', '2016-03-01 10:50:27', '2016-03-01 11:11:47', NULL, 10, 1, 0, '003C'),
(124, 0, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:29:19', '2016-03-01 11:11:47', NULL, 11, 1, 0, '003D'),
(125, 1, 'ru', 'TeasersPlugin', '2016-02-29 12:29:55', '2016-03-01 11:11:48', NULL, 11, 1, 0, '003E'),
(126, 2, 'ru', 'CalcPlugin', '2016-02-29 12:30:22', '2016-03-01 11:11:48', NULL, 11, 1, 0, '003F'),
(127, 3, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:31:10', '2016-03-01 11:11:48', NULL, 11, 1, 0, '003G'),
(128, 4, 'ru', 'PersonPlugin', '2016-02-29 12:31:45', '2016-03-01 11:11:48', NULL, 11, 1, 0, '003H'),
(129, 5, 'ru', 'ReviewPlugin', '2016-02-29 12:32:10', '2016-03-01 11:11:48', NULL, 11, 1, 0, '003I'),
(130, 6, 'ru', 'HtmlInsertPlugin', '2016-02-29 12:32:42', '2016-03-01 11:11:48', NULL, 11, 1, 0, '003J'),
(131, 0, 'ru', 'HtmlInsertPlugin', '2016-03-01 11:07:58', '2016-03-01 11:11:48', NULL, 82, 1, 0, '003K'),
(132, 1, 'ru', 'HtmlInsertPlugin', '2016-03-01 11:08:49', '2016-03-01 11:11:48', NULL, 82, 1, 0, '003L'),
(133, 0, 'ru', 'HtmlInsertPlugin', '2016-03-01 11:09:53', '2016-03-01 11:11:48', NULL, 84, 1, 0, '003M'),
(134, 1, 'ru', 'HtmlInsertPlugin', '2016-03-01 11:11:18', '2016-03-01 11:11:48', NULL, 84, 1, 0, '003N'),
(135, 0, 'ru', 'ProjectPlugin', '2016-02-29 11:20:27', '2016-03-01 12:04:50', NULL, 66, 1, 0, '003O'),
(136, 1, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:22:22', '2016-03-01 12:04:50', NULL, 66, 1, 0, '003P'),
(137, 2, 'ru', 'TeasersPlugin', '2016-02-29 11:22:56', '2016-03-01 12:04:50', NULL, 66, 1, 0, '003Q'),
(138, 3, 'ru', 'CalcPlugin', '2016-02-29 11:30:42', '2016-03-01 12:04:50', NULL, 66, 1, 0, '003R'),
(139, 4, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:35:21', '2016-03-01 12:04:50', NULL, 66, 1, 0, '003S'),
(140, 5, 'ru', 'PersonPlugin', '2016-02-29 11:36:35', '2016-03-01 12:04:51', NULL, 66, 1, 0, '003T'),
(141, 6, 'ru', 'ReviewPlugin', '2016-02-29 11:37:02', '2016-03-01 12:04:51', NULL, 66, 1, 0, '003U'),
(142, 7, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:37:35', '2016-03-01 12:04:51', NULL, 66, 1, 0, '003V'),
(143, 0, 'ru', 'ProjectPlugin', '2016-02-29 11:20:27', '2016-03-01 12:08:28', NULL, 68, 1, 0, '003W'),
(144, 1, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:22:22', '2016-03-01 12:08:28', NULL, 68, 1, 0, '003X'),
(145, 2, 'ru', 'TeasersPlugin', '2016-02-29 11:22:56', '2016-03-01 12:08:28', NULL, 68, 1, 0, '003Y'),
(146, 3, 'ru', 'CalcPlugin', '2016-02-29 11:30:42', '2016-03-01 12:08:28', NULL, 68, 1, 0, '003Z'),
(147, 4, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:35:21', '2016-03-01 12:08:28', NULL, 68, 1, 0, '0040'),
(148, 5, 'ru', 'PersonPlugin', '2016-02-29 11:36:35', '2016-03-01 12:08:28', NULL, 68, 1, 0, '0041'),
(149, 6, 'ru', 'ReviewPlugin', '2016-02-29 11:37:02', '2016-03-01 12:08:28', NULL, 68, 1, 0, '0042'),
(150, 7, 'ru', 'HtmlInsertPlugin', '2016-02-29 11:37:35', '2016-03-01 12:08:28', NULL, 68, 1, 0, '0043');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_globalpagepermission`
--

CREATE TABLE IF NOT EXISTS `cms_globalpagepermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `can_change` tinyint(1) NOT NULL,
  `can_add` tinyint(1) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  `can_change_advanced_settings` tinyint(1) NOT NULL,
  `can_publish` tinyint(1) NOT NULL,
  `can_change_permissions` tinyint(1) NOT NULL,
  `can_move_page` tinyint(1) NOT NULL,
  `can_view` tinyint(1) NOT NULL,
  `can_recover_page` tinyint(1) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_globalpagepermission_group_id_991b4733_fk_auth_group_id` (`group_id`),
  KEY `cms_globalpagepermission_user_id_a227cee1_fk_auth_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_globalpagepermission_sites`
--

CREATE TABLE IF NOT EXISTS `cms_globalpagepermission_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `globalpagepermission_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_globalpagepermission_s_globalpagepermission_id_db684f41_uniq` (`globalpagepermission_id`,`site_id`),
  KEY `cms_globalpagepermission_site_site_id_00460b53_fk_django_site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_page`
--

CREATE TABLE IF NOT EXISTS `cms_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) NOT NULL,
  `changed_by` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL,
  `changed_date` datetime NOT NULL,
  `publication_date` datetime DEFAULT NULL,
  `publication_end_date` datetime DEFAULT NULL,
  `in_navigation` tinyint(1) NOT NULL,
  `soft_root` tinyint(1) NOT NULL,
  `reverse_id` varchar(40) DEFAULT NULL,
  `navigation_extenders` varchar(80) DEFAULT NULL,
  `template` varchar(100) NOT NULL,
  `login_required` tinyint(1) NOT NULL,
  `limit_visibility_in_menu` smallint(6) DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL,
  `application_urls` varchar(200) DEFAULT NULL,
  `application_namespace` varchar(200) DEFAULT NULL,
  `publisher_is_draft` tinyint(1) NOT NULL,
  `languages` varchar(255) DEFAULT NULL,
  `revision_id` int(10) unsigned NOT NULL,
  `xframe_options` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `publisher_public_id` int(11) DEFAULT NULL,
  `site_id` int(11) NOT NULL,
  `depth` int(10) unsigned NOT NULL,
  `numchild` int(10) unsigned NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_page_path_d3a06f3d_uniq` (`path`),
  UNIQUE KEY `publisher_public_id` (`publisher_public_id`),
  UNIQUE KEY `cms_page_reverse_id_d5d1256a_uniq` (`reverse_id`,`site_id`,`publisher_is_draft`),
  UNIQUE KEY `cms_page_publisher_is_draft_8c776642_uniq` (`publisher_is_draft`,`site_id`,`application_namespace`),
  KEY `cms_page_parent_id_f89b72e4_fk_cms_page_id` (`parent_id`),
  KEY `cms_page_site_id_4323d3ff_fk_django_site_id` (`site_id`),
  KEY `cms_page_93b83098` (`publication_date`),
  KEY `cms_page_2247c5f0` (`publication_end_date`),
  KEY `cms_page_db3eb53f` (`in_navigation`),
  KEY `cms_page_1d85575d` (`soft_root`),
  KEY `cms_page_3d9ef52f` (`reverse_id`),
  KEY `cms_page_7b8acfa6` (`navigation_extenders`),
  KEY `cms_page_cb540373` (`limit_visibility_in_menu`),
  KEY `cms_page_4fa1c803` (`is_home`),
  KEY `cms_page_e721871e` (`application_urls`),
  KEY `cms_page_b7700099` (`publisher_is_draft`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Дамп данных таблицы `cms_page`
--

INSERT INTO `cms_page` (`id`, `created_by`, `changed_by`, `creation_date`, `changed_date`, `publication_date`, `publication_end_date`, `in_navigation`, `soft_root`, `reverse_id`, `navigation_extenders`, `template`, `login_required`, `limit_visibility_in_menu`, `is_home`, `application_urls`, `application_namespace`, `publisher_is_draft`, `languages`, `revision_id`, `xframe_options`, `parent_id`, `publisher_public_id`, `site_id`, `depth`, `numchild`, `path`) VALUES
(1, 'admin', 'admin', '2016-02-17 13:03:58', '2016-03-01 12:08:28', '2016-02-17 13:03:58', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 1, NULL, NULL, 1, 'ru', 0, 0, NULL, 2, 1, 1, 0, '0001'),
(2, 'admin', 'admin', '2016-02-17 13:03:58', '2016-03-01 12:08:28', '2016-02-17 13:03:58', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 1, NULL, NULL, 0, 'ru', 0, 0, NULL, 1, 1, 1, 0, '0002'),
(3, 'admin', 'admin', '2016-02-17 13:24:02', '2016-02-18 11:29:58', '2016-02-17 13:26:36', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, '', NULL, 1, 'ru', 0, 0, NULL, 11, 1, 1, 3, '0003'),
(4, 'admin', 'admin', '2016-02-17 13:24:16', '2016-02-17 14:18:05', '2016-02-17 13:26:41', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'CommandApp', NULL, 1, 'ru', 0, 0, NULL, 14, 1, 1, 0, '0005'),
(5, 'admin', 'admin', '2016-02-17 13:24:29', '2016-02-18 09:08:19', '2016-02-17 13:26:43', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'PriceApp', NULL, 1, 'ru', 0, 0, NULL, 15, 1, 1, 0, '0007'),
(6, 'admin', 'admin', '2016-02-17 13:24:42', '2016-02-17 13:26:44', '2016-02-17 13:26:44', NULL, 1, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 1, 'ru', 0, 0, NULL, 16, 1, 1, 0, '0009'),
(7, 'admin', 'admin', '2016-02-17 13:25:00', '2016-02-18 07:15:41', '2016-02-17 13:26:47', NULL, 1, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 1, 'ru', 0, 0, NULL, 17, 1, 1, 0, '000B'),
(8, 'admin', 'admin', '2016-02-17 13:25:26', '2016-02-24 12:03:36', '2016-02-17 13:26:38', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'ProjectApp', NULL, 1, 'ru', 0, 0, 3, 12, 1, 2, 0, '00030001'),
(9, 'admin', 'admin', '2016-02-17 13:25:41', '2016-02-24 11:40:33', '2016-02-17 13:26:39', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'DesignApp', NULL, 1, 'ru', 0, 0, 3, 13, 1, 2, 0, '00030002'),
(10, 'admin', 'admin', '2016-02-17 13:26:04', '2016-02-24 09:03:39', '2016-02-17 13:26:47', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'TourApp', NULL, 1, 'ru', 0, 0, 3, 18, 1, 2, 0, '00030003'),
(11, 'admin', 'admin', '2016-02-17 13:26:36', '2016-02-18 11:29:58', '2016-02-17 13:26:36', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, '', NULL, 0, 'ru', 0, 0, NULL, 3, 1, 1, 3, '0004'),
(12, 'admin', 'admin', '2016-02-17 13:26:38', '2016-02-24 12:03:36', '2016-02-17 13:26:38', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'ProjectApp', NULL, 0, 'ru', 0, 0, 11, 8, 1, 2, 0, '00040001'),
(13, 'admin', 'admin', '2016-02-17 13:26:40', '2016-02-24 11:40:33', '2016-02-17 13:26:39', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'DesignApp', NULL, 0, 'ru', 0, 0, 11, 9, 1, 2, 0, '00040002'),
(14, 'admin', 'admin', '2016-02-17 13:26:41', '2016-02-17 14:18:04', '2016-02-17 13:26:41', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'CommandApp', NULL, 0, 'ru', 0, 0, NULL, 4, 1, 1, 0, '0006'),
(15, 'admin', 'admin', '2016-02-17 13:26:43', '2016-02-18 09:08:19', '2016-02-17 13:26:43', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'PriceApp', NULL, 0, 'ru', 0, 0, NULL, 5, 1, 1, 0, '0008'),
(16, 'admin', 'admin', '2016-02-17 13:26:44', '2016-02-17 13:26:44', '2016-02-17 13:26:44', NULL, 1, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 0, 'ru', 0, 0, NULL, 6, 1, 1, 0, '000A'),
(17, 'admin', 'admin', '2016-02-17 13:26:47', '2016-02-18 07:15:41', '2016-02-17 13:26:47', NULL, 1, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 0, 'ru', 0, 0, NULL, 7, 1, 1, 0, '000C'),
(18, 'admin', 'admin', '2016-02-17 13:26:47', '2016-02-24 09:03:38', '2016-02-17 13:26:47', NULL, 1, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'TourApp', NULL, 0, 'ru', 0, 0, 11, 10, 1, 2, 0, '00040003'),
(19, 'admin', 'admin', '2016-02-18 07:47:24', '2016-02-18 07:47:55', '2016-02-18 07:47:54', NULL, 0, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'PartnerApp', NULL, 1, 'ru', 0, 0, NULL, 20, 1, 1, 0, '000D'),
(20, 'admin', 'admin', '2016-02-18 07:47:54', '2016-02-18 07:47:55', '2016-02-18 07:47:54', NULL, 0, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'PartnerApp', NULL, 0, 'ru', 0, 0, NULL, 19, 1, 1, 0, '000E'),
(21, 'admin', 'admin', '2016-02-18 08:21:45', '2016-03-01 11:06:58', '2016-02-18 08:21:58', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 1, 'ru', 0, 0, NULL, 22, 1, 1, 0, '000F'),
(22, 'admin', 'admin', '2016-02-18 08:21:58', '2016-03-01 11:06:58', '2016-02-18 08:21:58', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 0, 'ru', 0, 0, NULL, 21, 1, 1, 0, '000G'),
(23, 'admin', 'admin', '2016-02-24 09:19:27', '2016-02-24 09:20:42', '2016-02-24 09:20:41', NULL, 0, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'ReviewApp', NULL, 1, 'ru', 0, 0, NULL, 24, 1, 1, 0, '000H'),
(24, 'admin', 'admin', '2016-02-24 09:20:41', '2016-02-24 09:20:41', '2016-02-24 09:20:41', NULL, 0, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'ReviewApp', NULL, 0, 'ru', 0, 0, NULL, 23, 1, 1, 0, '000I'),
(25, 'admin', 'admin', '2016-02-29 11:09:13', '2016-03-01 12:04:51', '2016-02-29 11:10:40', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 1, 'ru', 0, 0, NULL, 29, 1, 1, 0, '000J'),
(26, 'admin', 'admin', '2016-02-29 11:09:39', '2016-03-01 12:08:28', '2016-02-29 11:10:42', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 1, 'ru', 0, 0, NULL, 30, 1, 1, 0, '000L'),
(27, 'admin', 'admin', '2016-02-29 11:09:59', '2016-02-29 12:16:51', '2016-02-29 11:10:43', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 1, 'ru', 0, 0, NULL, 31, 1, 1, 0, '000N'),
(28, 'admin', 'admin', '2016-02-29 11:10:19', '2016-02-29 12:24:22', '2016-02-29 11:10:45', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 1, 'ru', 0, 0, NULL, 32, 1, 1, 0, '000P'),
(29, 'admin', 'admin', '2016-02-29 11:10:40', '2016-03-01 12:04:51', '2016-02-29 11:10:40', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 0, 'ru', 0, 0, NULL, 25, 1, 1, 0, '000K'),
(30, 'admin', 'admin', '2016-02-29 11:10:42', '2016-03-01 12:08:28', '2016-02-29 11:10:42', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 0, 'ru', 0, 0, NULL, 26, 1, 1, 0, '000M'),
(31, 'admin', 'admin', '2016-02-29 11:10:43', '2016-02-29 12:16:51', '2016-02-29 11:10:43', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 0, 'ru', 0, 0, NULL, 27, 1, 1, 0, '000O'),
(32, 'admin', 'admin', '2016-02-29 11:10:45', '2016-02-29 12:24:22', '2016-02-29 11:10:45', NULL, 0, 0, NULL, NULL, 'INHERIT', 0, NULL, 0, NULL, NULL, 0, 'ru', 0, 0, NULL, 28, 1, 1, 0, '000Q'),
(33, 'admin', 'admin', '2016-02-29 12:23:48', '2016-02-29 12:24:26', '2016-02-29 12:24:25', NULL, 0, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'QuestApp', NULL, 1, 'ru', 0, 0, NULL, 34, 1, 1, 0, '000R'),
(34, 'admin', 'admin', '2016-02-29 12:24:25', '2016-02-29 12:24:26', '2016-02-29 12:24:25', NULL, 0, 0, NULL, '', 'INHERIT', 0, NULL, 0, 'QuestApp', NULL, 0, 'ru', 0, 0, NULL, 33, 1, 1, 0, '000S');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_pagepermission`
--

CREATE TABLE IF NOT EXISTS `cms_pagepermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `can_change` tinyint(1) NOT NULL,
  `can_add` tinyint(1) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  `can_change_advanced_settings` tinyint(1) NOT NULL,
  `can_publish` tinyint(1) NOT NULL,
  `can_change_permissions` tinyint(1) NOT NULL,
  `can_move_page` tinyint(1) NOT NULL,
  `can_view` tinyint(1) NOT NULL,
  `grant_on` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_pagepermission_group_id_af5af193_fk_auth_group_id` (`group_id`),
  KEY `cms_pagepermission_page_id_0ae9a163_fk_cms_page_id` (`page_id`),
  KEY `cms_pagepermission_user_id_0c7d2b3c_fk_auth_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_pageuser`
--

CREATE TABLE IF NOT EXISTS `cms_pageuser` (
  `user_ptr_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`user_ptr_id`),
  KEY `cms_pageuser_created_by_id_8e9fbf83_fk_auth_user_id` (`created_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_pageusergroup`
--

CREATE TABLE IF NOT EXISTS `cms_pageusergroup` (
  `group_ptr_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`group_ptr_id`),
  KEY `cms_pageusergroup_created_by_id_7d57fa39_fk_auth_user_id` (`created_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_page_placeholders`
--

CREATE TABLE IF NOT EXISTS `cms_page_placeholders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `placeholder_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_page_placeholders_page_id_ab7fbfb8_uniq` (`page_id`,`placeholder_id`),
  KEY `cms_page_placehold_placeholder_id_6b120886_fk_cms_placeholder_id` (`placeholder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Дамп данных таблицы `cms_page_placeholders`
--

INSERT INTO `cms_page_placeholders` (`id`, `page_id`, `placeholder_id`) VALUES
(5, 1, 6),
(6, 1, 7),
(9, 2, 10),
(10, 2, 11),
(33, 3, 40),
(34, 3, 41),
(3, 4, 4),
(4, 4, 5),
(23, 5, 26),
(24, 5, 27),
(11, 6, 12),
(12, 6, 13),
(1, 7, 2),
(2, 7, 3),
(35, 8, 42),
(36, 8, 43),
(37, 9, 44),
(38, 9, 45),
(41, 10, 48),
(42, 10, 49),
(29, 11, 36),
(30, 11, 37),
(31, 12, 38),
(32, 12, 39),
(39, 13, 46),
(40, 13, 47),
(7, 14, 8),
(8, 14, 9),
(25, 15, 28),
(26, 15, 29),
(27, 16, 34),
(28, 16, 35),
(13, 17, 16),
(14, 17, 17),
(43, 18, 50),
(44, 18, 51),
(15, 19, 18),
(16, 19, 19),
(17, 20, 20),
(18, 20, 21),
(19, 21, 22),
(20, 21, 23),
(21, 22, 24),
(22, 22, 25),
(45, 23, 52),
(46, 23, 53),
(47, 24, 54),
(48, 24, 55),
(49, 25, 57),
(50, 25, 58),
(51, 26, 59),
(52, 26, 60),
(53, 27, 61),
(54, 27, 62),
(55, 28, 63),
(56, 28, 64),
(57, 29, 65),
(58, 29, 66),
(59, 30, 67),
(60, 30, 68),
(61, 31, 69),
(62, 31, 70),
(63, 32, 71),
(64, 32, 72),
(65, 33, 77),
(66, 33, 78),
(67, 34, 79),
(68, 34, 80);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_placeholder`
--

CREATE TABLE IF NOT EXISTS `cms_placeholder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slot` varchar(255) NOT NULL,
  `default_width` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_placeholder_5e97994e` (`slot`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=85 ;

--
-- Дамп данных таблицы `cms_placeholder`
--

INSERT INTO `cms_placeholder` (`id`, `slot`, `default_width`) VALUES
(1, 'clipboard', NULL),
(2, 'slider', NULL),
(3, 'center', NULL),
(4, 'slider', NULL),
(5, 'center', NULL),
(6, 'slider', NULL),
(7, 'center', NULL),
(8, 'slider', NULL),
(9, 'center', NULL),
(10, 'slider', NULL),
(11, 'center', NULL),
(12, 'slider', NULL),
(13, 'center', NULL),
(14, 'counters', NULL),
(15, 'counters', NULL),
(16, 'slider', NULL),
(17, 'center', NULL),
(18, 'slider', NULL),
(19, 'center', NULL),
(20, 'slider', NULL),
(21, 'center', NULL),
(22, 'slider', NULL),
(23, 'center', NULL),
(24, 'slider', NULL),
(25, 'center', NULL),
(26, 'slider', NULL),
(27, 'center', NULL),
(28, 'slider', NULL),
(29, 'center', NULL),
(30, 'price_top', NULL),
(31, 'price_top', NULL),
(32, 'price_bottom', NULL),
(33, 'price_bottom', NULL),
(34, 'slider', NULL),
(35, 'center', NULL),
(36, 'slider', NULL),
(37, 'center', NULL),
(38, 'slider', NULL),
(39, 'center', NULL),
(40, 'slider', NULL),
(41, 'center', NULL),
(42, 'slider', NULL),
(43, 'center', NULL),
(44, 'slider', NULL),
(45, 'center', NULL),
(46, 'slider', NULL),
(47, 'center', NULL),
(48, 'slider', NULL),
(49, 'center', NULL),
(50, 'slider', NULL),
(51, 'center', NULL),
(52, 'slider', NULL),
(53, 'center', NULL),
(54, 'slider', NULL),
(55, 'center', NULL),
(56, 'place', NULL),
(57, 'slider', NULL),
(58, 'center', NULL),
(59, 'slider', NULL),
(60, 'center', NULL),
(61, 'slider', NULL),
(62, 'center', NULL),
(63, 'slider', NULL),
(64, 'center', NULL),
(65, 'slider', NULL),
(66, 'center', NULL),
(67, 'slider', NULL),
(68, 'center', NULL),
(69, 'slider', NULL),
(70, 'center', NULL),
(71, 'slider', NULL),
(72, 'center', NULL),
(73, 'menu', NULL),
(74, 'menu', NULL),
(75, 'clipboard', NULL),
(76, 'place', NULL),
(77, 'slider', NULL),
(78, 'center', NULL),
(79, 'slider', NULL),
(80, 'center', NULL),
(81, 'footer_left', NULL),
(82, 'footer_left', NULL),
(83, 'footer_right', NULL),
(84, 'footer_right', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_placeholderreference`
--

CREATE TABLE IF NOT EXISTS `cms_placeholderreference` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `placeholder_ref_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cmsplugin_ptr_id`),
  KEY `cms_placeholderreference_328d0afc` (`placeholder_ref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_placeholderreference`
--

INSERT INTO `cms_placeholderreference` (`cmsplugin_ptr_id`, `name`, `placeholder_ref_id`) VALUES
(53, 'Center', 75);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_basemenupl`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_basemenupl` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`cmsplugin_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_basemenupl`
--

INSERT INTO `cms_plugins_basemenupl` (`cmsplugin_ptr_id`) VALUES
(32),
(33),
(36),
(93);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_basemenupl_pages`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_basemenupl_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `basemenupl_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_plugins_basemenupl_pages_basemenupl_id_be05570b_uniq` (`basemenupl_id`,`page_id`),
  KEY `cms_plugins_basemenupl_pages_page_id_cef6ee75_fk_cms_page_id` (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `cms_plugins_basemenupl_pages`
--

INSERT INTO `cms_plugins_basemenupl_pages` (`id`, `basemenupl_id`, `page_id`) VALUES
(2, 32, 29),
(3, 32, 30),
(4, 32, 31),
(1, 32, 32),
(6, 33, 29),
(7, 33, 30),
(8, 33, 31),
(5, 33, 32),
(18, 36, 29),
(19, 36, 30),
(20, 36, 31),
(17, 36, 32),
(22, 93, 29),
(23, 93, 30),
(24, 93, 31),
(21, 93, 32);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_basemodelpl`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_basemodelpl` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sub_name` varchar(255) DEFAULT NULL,
  `h` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `anchor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cmsplugin_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_basemodelpl`
--

INSERT INTO `cms_plugins_basemodelpl` (`cmsplugin_ptr_id`, `name`, `sub_name`, `h`, `color`, `style`, `anchor`) VALUES
(1, 'О компании', NULL, '1', NULL, '', ''),
(3, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(4, 'О компании', NULL, '1', NULL, '', ''),
(6, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(9, 'Цены', NULL, '1', NULL, '', ''),
(11, 'Цены', NULL, '1', NULL, '', ''),
(13, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(15, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(17, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(19, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(21, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(23, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(26, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', '', ''),
(27, 'Калькулятор стоимости ремонта коттеджа (таунхауса)', NULL, '2', 'green', '', ''),
(29, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(30, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(39, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', '', ''),
(40, 'Калькулятор стоимости ремонта квартиры', NULL, '2', 'green', '', ''),
(42, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(43, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(56, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', '', ''),
(57, 'Калькулятор стоимости ремонта квартиры', NULL, '2', 'green', '', ''),
(59, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(60, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(64, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', '', ''),
(65, 'Калькулятор стоимости ремонта коттеджа (таунхауса)', NULL, '2', 'green', '', ''),
(67, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(68, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(78, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 'Калькулятор стоимости дизайн проекта', NULL, '2', 'green', '', ''),
(81, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', 'no-pad', ''),
(83, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(84, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(86, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 'Калькулятор стоимости дизайн проекта', NULL, '2', 'green', '', ''),
(88, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', 'no-pad', ''),
(90, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(91, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(95, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', '', ''),
(96, 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', NULL, '2', 'green', '', ''),
(98, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(99, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(125, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', '', ''),
(126, 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', NULL, '2', 'green', '', ''),
(128, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(129, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(137, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', '', ''),
(138, 'Калькулятор стоимости ремонта квартиры', NULL, '2', 'green', '', ''),
(140, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(141, 'Что Вы думаете о нас?', NULL, '2', 'green', '', ''),
(145, 'Вот почему Вы выбираете нас', NULL, '2', 'orange', '', ''),
(146, 'Калькулятор стоимости ремонта коттеджа (таунхауса)', NULL, '2', 'green', '', ''),
(148, 'У нас замечательная команда', NULL, '2', 'orange', '', ''),
(149, 'Что Вы думаете о нас?', NULL, '2', 'green', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_block`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `html` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_blockmodel`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_blockmodel` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  PRIMARY KEY (`cmsplugin_ptr_id`),
  KEY `cms_plugins_blockmodel_block_id_0181c1e7_fk_cms_plugins_block_id` (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_calc`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_calc` (
  `basemodelpl_ptr_id` int(11) NOT NULL,
  `design` tinyint(1) NOT NULL,
  `html` longtext,
  `all_type` tinyint(1) NOT NULL,
  PRIMARY KEY (`basemodelpl_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_calc`
--

INSERT INTO `cms_plugins_calc` (`basemodelpl_ptr_id`, `design`, `html`, `all_type`) VALUES
(27, 0, '', 0),
(40, 0, '', 0),
(57, 0, '', 0),
(65, 0, '', 0),
(80, 1, '<button class="btn anim fw600 fll fancy" href="#callback" type="submit">Получить консультацию бесплатно</button>\r\n\r\n                            <div class="call fll">\r\n                                <span class="fw600">Звоните по телефону:</span>\r\n                                <a href="tel:+74955177528" class="phone fw600 c-red">+7 (495) 517-75-28</a>\r\n                            </div>', 0),
(87, 1, '<button class="btn anim fw600 fll fancy" href="#callback" type="submit">Получить консультацию бесплатно</button>\r\n\r\n                            <div class="call fll">\r\n                                <span class="fw600">Звоните по телефону:</span>\r\n                                <a href="tel:+74955177528" class="phone fw600 c-red">+7 (495) 517-75-28</a>\r\n                            </div>', 0),
(96, 0, '', 1),
(126, 0, '', 1),
(138, 0, '', 0),
(146, 0, '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_designpluginmodel`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_designpluginmodel` (
  `basemodelpl_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`basemodelpl_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_designpluginmodel`
--

INSERT INTO `cms_plugins_designpluginmodel` (`basemodelpl_ptr_id`) VALUES
(78),
(86);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_designpluginmodel_design`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_designpluginmodel_design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designpluginmodel_id` int(11) NOT NULL,
  `designproject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_plugins_designpluginmodel_designpluginmodel_id_09029242_uniq` (`designpluginmodel_id`,`designproject_id`),
  KEY `cms_plugi_designproject_id_956e415f_fk_contents_designproject_id` (`designproject_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `cms_plugins_designpluginmodel_design`
--

INSERT INTO `cms_plugins_designpluginmodel_design` (`id`, `designpluginmodel_id`, `designproject_id`) VALUES
(1, 78, 1),
(2, 78, 2),
(3, 78, 3),
(4, 86, 1),
(5, 86, 2),
(6, 86, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_feedback`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `comment` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `cms_plugins_feedback`
--

INSERT INTO `cms_plugins_feedback` (`id`, `date`, `path`, `name`, `phone`, `email`, `comment`) VALUES
(1, '2016-03-01 11:33:15', '', '12', '+7(111)111-11-11', '', 'comment'),
(2, '2016-03-01 11:43:48', '', '1130', '+7(111)111-11-11', '', 'comments');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_htmlinsertmodel`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_htmlinsertmodel` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `html` longtext NOT NULL,
  PRIMARY KEY (`cmsplugin_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_htmlinsertmodel`
--

INSERT INTO `cms_plugins_htmlinsertmodel` (`cmsplugin_ptr_id`, `name`, `html`) VALUES
(2, 'Цифры и факты', '<div class="about-company">\r\n        <div class="container">\r\n             <div class="blue-bg">\r\n                    <span class="fw600">Цифры и факты</span>\r\n\r\n                    <div class="block">\r\n                        <span class="c-red">12+</span>\r\n                        <span class="fw300">лет на рынке</span>\r\n                    </div>\r\n                    <div class="block">\r\n                        <span class="c-red">19</span>\r\n                        <span class="fw300">мастеров\r\n<br/>высшей категории</span>\r\n                    </div>\r\n                    <div class="block">\r\n                        <span class="c-red">89</span>\r\n                        <span class="fw300">выполненных\r\n<br>объектов</span>\r\n                    </div>\r\n                    <div class="block">\r\n                        <span class="c-red">5</span>\r\n                        <span class="fw300">лет гарантии</span>\r\n                    </div>\r\n\r\n                </div>\r\n        </div>\r\n\r\n            </div>'),
(5, 'Цифры и факты', '<div class="about-company">\r\n        <div class="container">\r\n             <div class="blue-bg">\r\n                    <span class="fw600">Цифры и факты</span>\r\n\r\n                    <div class="block">\r\n                        <span class="c-red">12+</span>\r\n                        <span class="fw300">лет на рынке</span>\r\n                    </div>\r\n                    <div class="block">\r\n                        <span class="c-red">19</span>\r\n                        <span class="fw300">мастеров\r\n<br/>высшей категории</span>\r\n                    </div>\r\n                    <div class="block">\r\n                        <span class="c-red">89</span>\r\n                        <span class="fw300">выполненных\r\n<br>объектов</span>\r\n                    </div>\r\n                    <div class="block">\r\n                        <span class="c-red">5</span>\r\n                        <span class="fw300">лет гарантии</span>\r\n                    </div>\r\n\r\n                </div>\r\n        </div>\r\n\r\n            </div>'),
(7, 'Контакты', '<div class="contacts">\r\n            <div class="container">\r\n                <h1>Контакты</h1>\r\n\r\n                <a href="#" class="print anim c-red flr">Распечатать</a>\r\n\r\n                <span class="fw600">\r\n                    Компания "Никстест строй"\r\n                </span>\r\n\r\n                <div class="block fll">\r\n\r\n                    <div class="item">\r\n                        <label class="fll">Адрес:</label>\r\n\r\n                        <p>107113, г. Москва, ул. Сокольническая пл., д. 4А</p>\r\n                    </div>\r\n                    <div class="item">\r\n                        <label class="fll">Телефон:</label>\r\n\r\n                        <p>+7 (495) 517-75-28</p>\r\n                    </div>\r\n                    <div class="item">\r\n                        <label class="fll">E-mail:</label>\r\n\r\n                        <p>\r\n                            <a href="mailto:clients@remont-nts.ru" class="c-red">clients@remont-nts.ru</a>\r\n                            <br/><a href="mailto:offers@remont-nts.ru" class="c-red">offers@remont-nts.ru</a> (по\r\n                            вопросам сотрудничества)\r\n                        </p>\r\n                    </div>\r\n                    <div class="item">\r\n                        <label class="fll">Время работы:</label>\r\n\r\n                        <p>\r\n                            <span class="one">пн–пт</span> <span>9:00–20:00</span>\r\n                            <br/><span class="one">сб</span> <span>10:00–14:00</span>\r\n                            <br/><span class="one">вс</span> <span>выходной</span>\r\n                        </p>\r\n                    </div>\r\n\r\n                </div>\r\n\r\n                <div class="block flr">\r\n                    <div class="item">\r\n                        <label class="fll">Реквизиты:</label>\r\n\r\n                        <p>ООО «НикС-Тест»\r\n                            <br/>107113, г. Москва, ул. Сокольническая пл., д. 4А\r\n                            <br/>ИНН 7718768715\r\n                            <br/>КПП 771801001\r\n                            <br/>ОГРН 1097746389751\r\n                            <br/>ОАО Сбербанк России г. Москва\r\n                            <br/>К/с 30101810400000000225\r\n                            <br/>БИК 044525225\r\n                            <br/>Р/с 40702810438120061789</p>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="clr"></div>\r\n\r\n                <span class="fw600">Расположение на карте</span>\r\n\r\n                <div id="map">\r\n\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>\r\n<script>\r\n    var map;\r\n\r\n    ymaps.ready(init);\r\n\r\n    function init() {\r\n        map = new ymaps.Map(''map'', {\r\n            center: [55.698345, 37.625522],\r\n            zoom: 16,\r\n            controls: [''zoomControl''],\r\n            behaviors: [''drag'']\r\n        });\r\n\r\n\r\n        mark = new ymaps.Placemark([55.698345, 37.625522], {\r\n            balloonContentFooter: "",\r\n            hintContent: "Мы здесь"\r\n        });\r\n\r\n        map.geoObjects.add(mark);\r\n    }\r\n</script>'),
(10, 'Бесплатная консультация', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <p class="fw600">Расскажите нам свои пожелания по ремонту\r\n                        <br/>и мы предложим лучшее решение</p>\r\n\r\n                    <div class="item">\r\n                        <p>Консультация БЕСПЛАТНО, звоните!</p>\r\n                    </div>\r\n\r\n                    <div class="item">\r\n                        <a href="tel:+74955177528" class="phone fw600 c-blue">+7 (495) 517-75-28</a>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>'),
(12, 'Бесплатная консультация', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <p class="fw600">Расскажите нам свои пожелания по ремонту\r\n                        <br/>и мы предложим лучшее решение</p>\r\n\r\n                    <div class="item">\r\n                        <p>Консультация БЕСПЛАТНО, звоните!</p>\r\n                    </div>\r\n\r\n                    <div class="item">\r\n                        <a href="tel:+74955177528" class="phone fw600 c-blue">+7 (495) 517-75-28</a>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>'),
(14, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(16, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(18, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(20, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(22, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(24, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(25, 'Расскажите нам свои пожелания по ремонту', '<div class="consult three">\r\n        <div class="container">\r\n            <div class="pink-bg">\r\n                <p class="fw600">Расскажите нам свои пожелания по ремонту\r\n                    <br/>и мы предложим лучшее решение</p>\r\n\r\n                <div class="item">\r\n                    <p>Консультация БЕСПЛАТНО, звоните!</p>\r\n                </div>\r\n\r\n                <div class="item">\r\n                    <a href="tel:+74955177528" class="phone fw600 c-blue">+7 (495) 517-75-28</a>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>'),
(28, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n        <div class="container">\r\n            <div class="pink-bg">\r\n                <div class="left-part fll">\r\n                    <p>Хочу вызвать дизайнера и оценщика</p>\r\n                    <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                </div>\r\n                <div class="right-part flr">\r\n                    <a href="#callback" class="btn fancy fw600">Позвоните мне</a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>'),
(31, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(38, 'Расскажите нам свои пожелания по ремонту', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <p class="fw600">Расскажите нам свои пожелания по ремонту\r\n                        <br/>и мы предложим лучшее решение</p>\r\n\r\n                    <div class="item">\r\n                        <p>Консультация БЕСПЛАТНО, звоните!</p>\r\n                    </div>\r\n\r\n                    <div class="item">\r\n                        <a href="tel:+74955177528" class="phone fw600 c-blue">+7 (495) 517-75-28</a>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>'),
(41, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <p>Хочу вызвать дизайнера и оценщика</p>\r\n                        <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                    </div>\r\n                    <div class="right-part flr">\r\n                        <a href="#callback" class="btn fw600 fancy">Позвоните мне</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(44, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(55, 'Расскажите нам свои пожелания по ремонту', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <p class="fw600">Расскажите нам свои пожелания по ремонту\r\n                        <br/>и мы предложим лучшее решение</p>\r\n\r\n                    <div class="item">\r\n                        <p>Консультация БЕСПЛАТНО, звоните!</p>\r\n                    </div>\r\n\r\n                    <div class="item">\r\n                        <a href="tel:+74955177528" class="phone fw600 c-blue">+7 (495) 517-75-28</a>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>'),
(58, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <p>Хочу вызвать дизайнера и оценщика</p>\r\n                        <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                    </div>\r\n                    <div class="right-part flr">\r\n                        <a href="#" class="btn fw600">Позвоните мне</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(61, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(63, 'Расскажите нам свои пожелания по ремонту', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <p class="fw600">Расскажите нам свои пожелания по ремонту\r\n                        <br/>и мы предложим лучшее решение</p>\r\n\r\n                    <div class="item">\r\n                        <p>Консультация БЕСПЛАТНО, звоните!</p>\r\n                    </div>\r\n\r\n                    <div class="item">\r\n                        <a href="tel:+74955177528" class="phone fw600 c-blue">+7 (495) 517-75-28</a>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>'),
(66, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <p>Хочу вызвать дизайнера и оценщика</p>\r\n                        <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                    </div>\r\n                    <div class="right-part flr">\r\n                        <a href="#callback" class="btn fw600 fancy">Позвоните мне</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(69, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(82, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <p>Хочу вызвать дизайнера и оценщика</p>\r\n                        <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                    </div>\r\n                    <div class="right-part flr">\r\n                        <a href="#callback" class="btn fancy fw600">Позвоните мне</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(85, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(89, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <p>Хочу вызвать дизайнера и оценщика</p>\r\n                        <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                    </div>\r\n                    <div class="right-part flr">\r\n                        <a href="#callback" class="btn fancy fw600">Позвоните мне</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(92, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(94, 'Хочу бесплатную консультацию', '<div class="consult">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <span>Хочу бесплатную консультацию</span>\r\n\r\n                        <div class="clr"></div>\r\n                        <a href="#callback" class="btn fancy anim fw600 flr">Перезвоните мне</a>\r\n                    </div>\r\n\r\n                    <div class="right-part flr">\r\n                        <a href="tel:+74955177528" class="phone c-blue fw600">+7 (495) 517-75-28</a>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>'),
(97, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <p>Хочу вызвать дизайнера и оценщика</p>\r\n                        <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                    </div>\r\n                    <div class="right-part flr">\r\n                        <a href="#callback" class="btn fancy fw600">Позвоните мне</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(100, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(118, 'Контакты', '<div class="contacts">\r\n            <div class="container">\r\n                <h1>Контакты</h1>\r\n\r\n                <a href="#" class="print anim c-red flr">Распечатать</a>\r\n\r\n                <span class="fw600">\r\n                    Компания "Никстест строй"\r\n                </span>\r\n\r\n                <div class="block fll">\r\n\r\n                    <div class="item">\r\n                        <label class="fll">Адрес:</label>\r\n\r\n                        <p>107113, г. Москва, ул. Сокольническая пл., д. 4А</p>\r\n                    </div>\r\n                    <div class="item">\r\n                        <label class="fll">Телефон:</label>\r\n\r\n                        <p>+7 (495) 517-75-28</p>\r\n                    </div>\r\n                    <div class="item">\r\n                        <label class="fll">E-mail:</label>\r\n\r\n                        <p>\r\n                            <a href="mailto:clients@remont-nts.ru" class="c-red">clients@remont-nts.ru</a>\r\n                            <br/><a href="mailto:offers@remont-nts.ru" class="c-red">offers@remont-nts.ru</a> (по\r\n                            вопросам сотрудничества)\r\n                        </p>\r\n                    </div>\r\n                    <div class="item">\r\n                        <label class="fll">Время работы:</label>\r\n\r\n                        <p>\r\n                            <span class="one">пн–пт</span> <span>9:00–20:00</span>\r\n                            <br/><span class="one">сб</span> <span>10:00–14:00</span>\r\n                            <br/><span class="one">вс</span> <span>выходной</span>\r\n                        </p>\r\n                    </div>\r\n\r\n                </div>\r\n\r\n                <div class="block flr">\r\n                    <div class="item">\r\n                        <label class="fll">Реквизиты:</label>\r\n\r\n                        <p>ООО «НикС-Тест»\r\n                            <br/>107113, г. Москва, ул. Сокольническая пл., д. 4А\r\n                            <br/>ИНН 7718768715\r\n                            <br/>КПП 771801001\r\n                            <br/>ОГРН 1097746389751\r\n                            <br/>ОАО Сбербанк России г. Москва\r\n                            <br/>К/с 30101810400000000225\r\n                            <br/>БИК 044525225\r\n                            <br/>Р/с 40702810438120061789</p>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="clr"></div>\r\n\r\n                <span class="fw600">Расположение на карте</span>\r\n\r\n                <div id="map">\r\n\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>\r\n<script>\r\n    var map;\r\n\r\n    ymaps.ready(init);\r\n\r\n    function init() {\r\n        map = new ymaps.Map(''map'', {\r\n            center: [55.698345, 37.625522],\r\n            zoom: 16,\r\n            controls: [''zoomControl''],\r\n            behaviors: [''drag'']\r\n        });\r\n\r\n\r\n        mark = new ymaps.Placemark([55.698345, 37.625522], {\r\n            balloonContentFooter: "",\r\n            hintContent: "Мы здесь"\r\n        });\r\n\r\n        map.geoObjects.add(mark);\r\n    }\r\n</script>'),
(119, 'Контакты', '<a href="tel:84955177528" class="phone fw300">8 (495) 517-75-28</a>\r\n<p><a class="c-red anim" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a> – по вопросам\r\n                    дизайна и ремонта</p>\r\n<p><a class="c-red anim" href="mailto:offers@remont-nts.ru">offers@remont-nts.ru</a> – по вопросам\r\n                    сотрудничества</p>'),
(120, 'Logo Efffe', '<div class="made">\r\n  <a target="_blank" href="#"><img src="/media/static/images/effe.png" width="150" height="39" alt=""/></a>\r\n</div>'),
(121, 'Социальные сети', '<div class="soc">\r\n                    <span>Следите за нами:</span>\r\n                    <a href="#" class="f"></a><a href="#" class="i"></a><a href="#" class="v"></a>\r\n\r\n                    <div class="clr"></div>\r\n                </div>'),
(122, 'Меню', '<div class="footer-menu c-red anim">\r\n                    <ul class="marker-gray">\r\n                        <li><a href="#">Ремонт квартир</a></li>\r\n                        <li><a href="#">Ремонт коттеджей</a></li>\r\n                        <li><a href="#">Ремонт квартир под ключ</a></li>\r\n                    </ul>\r\n                    <ul class="marker-gray">\r\n                        <li><a href="#">Ремонт новостроек</a></li>\r\n                        <li><a href="#">Ремонт таунхаусов</a></li>\r\n                        <li><a href="#">Косметический ремонт</a></li>\r\n                    </ul>\r\n                </div>'),
(124, 'Хочу бесплатную консультацию', '<div class="consult">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <span>Хочу бесплатную консультацию</span>\r\n\r\n                        <div class="clr"></div>\r\n                        <a href="#callback" class="btn fancy anim fw600 flr">Перезвоните мне</a>\r\n                    </div>\r\n\r\n                    <div class="right-part flr">\r\n                        <a href="tel:+74955177528" class="phone c-blue fw600">+7 (495) 517-75-28</a>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>'),
(127, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <p>Хочу вызвать дизайнера и оценщика</p>\r\n                        <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                    </div>\r\n                    <div class="right-part flr">\r\n                        <a href="#callback" class="btn fancy fw600">Позвоните мне</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(130, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(131, 'Контакты', '<a href="tel:84955177528" class="phone fw300">8 (495) 517-75-28</a>\r\n<p><a class="c-red anim" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a> – по вопросам\r\n                    дизайна и ремонта</p>\r\n<p><a class="c-red anim" href="mailto:offers@remont-nts.ru">offers@remont-nts.ru</a> – по вопросам\r\n                    сотрудничества</p>'),
(132, 'Logo Efffe', '<div class="made">\r\n  <a target="_blank" href="#"><img src="/media/static/images/effe.png" width="150" height="39" alt=""/></a>\r\n</div>'),
(133, 'Социальные сети', '<div class="soc">\r\n                    <span>Следите за нами:</span>\r\n                    <a href="#" class="f"></a><a href="#" class="i"></a><a href="#" class="v"></a>\r\n\r\n                    <div class="clr"></div>\r\n                </div>'),
(134, 'Меню', '<div class="footer-menu c-red anim">\r\n                    <ul class="marker-gray">\r\n                        <li><a href="#">Ремонт квартир</a></li>\r\n                        <li><a href="#">Ремонт коттеджей</a></li>\r\n                        <li><a href="#">Ремонт квартир под ключ</a></li>\r\n                    </ul>\r\n                    <ul class="marker-gray">\r\n                        <li><a href="#">Ремонт новостроек</a></li>\r\n                        <li><a href="#">Ремонт таунхаусов</a></li>\r\n                        <li><a href="#">Косметический ремонт</a></li>\r\n                    </ul>\r\n                </div>'),
(136, 'Расскажите нам свои пожелания по ремонту', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <p class="fw600">Расскажите нам свои пожелания по ремонту\r\n                        <br/>и мы предложим лучшее решение</p>\r\n\r\n                    <div class="item">\r\n                        <p>Консультация БЕСПЛАТНО, звоните!</p>\r\n                    </div>\r\n\r\n                    <div class="item">\r\n                        <a href="tel:+74955177528" class="phone fw600 c-blue">+7 (495) 517-75-28</a>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>'),
(139, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <p>Хочу вызвать дизайнера и оценщика</p>\r\n                        <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                    </div>\r\n                    <div class="right-part flr">\r\n                        <a href="#callback" class="btn fw600 fancy">Позвоните мне</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(142, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(144, 'Расскажите нам свои пожелания по ремонту', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <p class="fw600">Расскажите нам свои пожелания по ремонту\r\n                        <br/>и мы предложим лучшее решение</p>\r\n\r\n                    <div class="item">\r\n                        <p>Консультация БЕСПЛАТНО, звоните!</p>\r\n                    </div>\r\n\r\n                    <div class="item">\r\n                        <a href="tel:+74955177528" class="phone fw600 c-blue">+7 (495) 517-75-28</a>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>'),
(147, 'Хочу вызвать дизайнера и оценщика', '<div class="consult two">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="left-part fll">\r\n                        <p>Хочу вызвать дизайнера и оценщика</p>\r\n                        <span class="fw600 c-blue">БЕСПЛАТНО</span>\r\n                    </div>\r\n                    <div class="right-part flr">\r\n                        <a href="#callback" class="btn fw600 fancy">Позвоните мне</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>'),
(150, 'Свяжитесь с нами', '<div class="consult three">\r\n            <div class="container">\r\n                <div class="pink-bg">\r\n                    <div class="name">Свяжитесь с нами</div>\r\n                    <div class="block items1">\r\n                        <span>Напишите на почту:</span>\r\n                        <a class="c-blue fw300" href="mailto:clients@remont-nts.ru">clients@remont-nts.ru</a>\r\n                    </div>\r\n\r\n                    <div class="block items2">\r\n                        <span>Звоните по телефону:</span>\r\n\r\n                        <a href="tel:+74955177528" class="c-blue fw300">+7 (495) 517-75-28</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_keyword`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_plugins_keyword_page_id_db921e51_fk_cms_page_id` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_personpluginmodel`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_personpluginmodel` (
  `basemodelpl_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`basemodelpl_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_personpluginmodel`
--

INSERT INTO `cms_plugins_personpluginmodel` (`basemodelpl_ptr_id`) VALUES
(3),
(6),
(29),
(42),
(59),
(67),
(83),
(90),
(98),
(128),
(140),
(148);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_personpluginmodel_commands`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_personpluginmodel_commands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personpluginmodel_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_plugins_personpluginmodel_personpluginmodel_id_a0529b72_uniq` (`personpluginmodel_id`,`person_id`),
  KEY `cms_plugins_personplugi_person_id_a3c09d8d_fk_contents_person_id` (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Дамп данных таблицы `cms_plugins_personpluginmodel_commands`
--

INSERT INTO `cms_plugins_personpluginmodel_commands` (`id`, `personpluginmodel_id`, `person_id`) VALUES
(1, 3, 1),
(2, 3, 2),
(3, 3, 3),
(4, 3, 4),
(5, 6, 1),
(6, 6, 2),
(7, 6, 3),
(8, 6, 4),
(9, 29, 1),
(10, 29, 2),
(11, 29, 3),
(12, 29, 4),
(13, 42, 1),
(14, 42, 2),
(15, 42, 3),
(16, 42, 4),
(21, 59, 1),
(22, 59, 2),
(23, 59, 3),
(24, 59, 4),
(25, 67, 1),
(26, 67, 2),
(27, 67, 3),
(28, 67, 4),
(33, 83, 1),
(34, 83, 2),
(35, 83, 3),
(36, 83, 4),
(37, 90, 1),
(38, 90, 2),
(39, 90, 3),
(40, 90, 4),
(41, 98, 1),
(42, 98, 2),
(43, 98, 3),
(44, 98, 4),
(53, 128, 1),
(54, 128, 2),
(55, 128, 3),
(56, 128, 4),
(57, 140, 1),
(58, 140, 2),
(59, 140, 3),
(60, 140, 4),
(61, 148, 1),
(62, 148, 2),
(63, 148, 3),
(64, 148, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_projectpluginmodel`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_projectpluginmodel` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`cmsplugin_ptr_id`),
  KEY `cms_plugins_projectpl_project_id_029c4f61_fk_contents_project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_projectpluginmodel`
--

INSERT INTO `cms_plugins_projectpluginmodel` (`cmsplugin_ptr_id`, `project_id`) VALUES
(37, 1),
(54, 1),
(135, 1),
(62, 2),
(143, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_reviewpluginmodel`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_reviewpluginmodel` (
  `basemodelpl_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`basemodelpl_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_reviewpluginmodel`
--

INSERT INTO `cms_plugins_reviewpluginmodel` (`basemodelpl_ptr_id`) VALUES
(13),
(15),
(17),
(19),
(21),
(23),
(30),
(43),
(60),
(68),
(84),
(91),
(99),
(129),
(141),
(149);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_reviewpluginmodel_reviews`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_reviewpluginmodel_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewpluginmodel_id` int(11) NOT NULL,
  `review_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_plugins_reviewpluginmodel_reviewpluginmodel_id_007959cf_uniq` (`reviewpluginmodel_id`,`review_id`),
  KEY `cms_plugins_reviewplugi_review_id_0975f1cb_fk_contents_review_id` (`review_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Дамп данных таблицы `cms_plugins_reviewpluginmodel_reviews`
--

INSERT INTO `cms_plugins_reviewpluginmodel_reviews` (`id`, `reviewpluginmodel_id`, `review_id`) VALUES
(1, 13, 1),
(2, 13, 2),
(3, 15, 1),
(4, 15, 2),
(5, 17, 1),
(6, 17, 2),
(7, 19, 1),
(8, 19, 2),
(9, 21, 1),
(10, 21, 2),
(11, 23, 1),
(12, 23, 2),
(13, 30, 1),
(14, 30, 2),
(15, 43, 1),
(16, 43, 2),
(19, 60, 1),
(20, 60, 2),
(21, 68, 1),
(22, 68, 2),
(25, 84, 1),
(26, 84, 2),
(27, 91, 1),
(28, 91, 2),
(29, 99, 1),
(30, 99, 2),
(35, 129, 1),
(36, 129, 2),
(37, 141, 1),
(38, 141, 2),
(39, 149, 1),
(40, 149, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_robot`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_robot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_sliderpluginmodel`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_sliderpluginmodel` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`cmsplugin_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_sliderpluginmodel`
--

INSERT INTO `cms_plugins_sliderpluginmodel` (`cmsplugin_ptr_id`) VALUES
(109),
(123);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_sliderpluginmodel_sliders`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_sliderpluginmodel_sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sliderpluginmodel_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_plugins_sliderpluginmodel_sliderpluginmodel_id_8a076bce_uniq` (`sliderpluginmodel_id`,`slider_id`),
  KEY `cms_plugins_sliderplugi_slider_id_92bb6a5e_fk_contents_slider_id` (`slider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `cms_plugins_sliderpluginmodel_sliders`
--

INSERT INTO `cms_plugins_sliderpluginmodel_sliders` (`id`, `sliderpluginmodel_id`, `slider_id`) VALUES
(1, 109, 1),
(2, 109, 2),
(3, 109, 3),
(7, 123, 1),
(8, 123, 2),
(9, 123, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_teaserspluginmodel`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_teaserspluginmodel` (
  `basemodelpl_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`basemodelpl_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_teaserspluginmodel`
--

INSERT INTO `cms_plugins_teaserspluginmodel` (`basemodelpl_ptr_id`) VALUES
(26),
(39),
(56),
(64),
(81),
(88),
(95),
(125),
(137),
(145);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_teaserspluginmodel_teasers`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_teaserspluginmodel_teasers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teaserspluginmodel_id` int(11) NOT NULL,
  `teaser_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_plugins_teaserspluginmod_teaserspluginmodel_id_6f861b56_uniq` (`teaserspluginmodel_id`,`teaser_id`),
  KEY `cms_plugins_teasersplug_teaser_id_eaae546c_fk_contents_teaser_id` (`teaser_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=105 ;

--
-- Дамп данных таблицы `cms_plugins_teaserspluginmodel_teasers`
--

INSERT INTO `cms_plugins_teaserspluginmodel_teasers` (`id`, `teaserspluginmodel_id`, `teaser_id`) VALUES
(1, 26, 1),
(2, 26, 2),
(3, 26, 3),
(4, 26, 4),
(5, 26, 5),
(6, 26, 6),
(7, 26, 7),
(8, 26, 8),
(9, 39, 1),
(10, 39, 2),
(11, 39, 3),
(12, 39, 4),
(13, 39, 5),
(14, 39, 6),
(15, 39, 7),
(16, 39, 8),
(25, 56, 1),
(26, 56, 2),
(27, 56, 3),
(28, 56, 4),
(29, 56, 5),
(30, 56, 6),
(31, 56, 7),
(32, 56, 8),
(33, 64, 1),
(34, 64, 2),
(35, 64, 3),
(36, 64, 4),
(37, 64, 5),
(38, 64, 6),
(39, 64, 7),
(40, 64, 8),
(52, 81, 5),
(49, 81, 8),
(50, 81, 9),
(51, 81, 10),
(56, 88, 5),
(53, 88, 8),
(54, 88, 9),
(55, 88, 10),
(57, 95, 1),
(58, 95, 2),
(59, 95, 3),
(60, 95, 4),
(61, 95, 5),
(62, 95, 6),
(63, 95, 7),
(64, 95, 8),
(81, 125, 1),
(82, 125, 2),
(83, 125, 3),
(84, 125, 4),
(85, 125, 5),
(86, 125, 6),
(87, 125, 7),
(88, 125, 8),
(89, 137, 1),
(90, 137, 2),
(91, 137, 3),
(92, 137, 4),
(93, 137, 5),
(94, 137, 6),
(95, 137, 7),
(96, 137, 8),
(97, 145, 1),
(98, 145, 2),
(99, 145, 3),
(100, 145, 4),
(101, 145, 5),
(102, 145, 6),
(103, 145, 7),
(104, 145, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_textmodel`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_textmodel` (
  `basemodelpl_ptr_id` int(11) NOT NULL,
  `text` longtext,
  PRIMARY KEY (`basemodelpl_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cms_plugins_textmodel`
--

INSERT INTO `cms_plugins_textmodel` (`basemodelpl_ptr_id`, `text`) VALUES
(1, '<p>Текст для образца: При выборе бухгалтерской компании очень важно понимать, с кем вы имеете дело. Очень многие умеют «подавать» себя красиво, но лишь немногие подтверждают красивые слова конкретными цифрами и фактами.</p><p>Мы очень хотим, чтобы Ваше общение с нами начиналось с доверия и уважения. Если представленной ниже информации Вам будет недостаточно – мы с радостью расскажем о себе подробнее.</p>'),
(4, '<p>Текст для образца: При выборе бухгалтерской компании очень важно понимать, с кем вы имеете дело. Очень многие умеют «подавать» себя красиво, но лишь немногие подтверждают красивые слова конкретными цифрами и фактами.</p><p>Мы очень хотим, чтобы Ваше общение с нами начиналось с доверия и уважения. Если представленной ниже информации Вам будет недостаточно – мы с радостью расскажем о себе подробнее.</p>'),
(9, '<p>Все компании указывают свои цены, но это не поможет вам составить точную смету по ремонту.\r\n</p><p>На стоимость влияет то-то и то-то... то-то и то-то... то-то и то-то... то-то и то-то... то-то и то-то... то-то и то-то... Поэтому мы предлагаем вам воспользоваться нашим бесплатным предложением - <a href="#" class="c-red">вызвать дизайнера и оценщика</a> на объект.\r\n</p><p>Это ни к чему вас не обязывает. А нам позволит лучше понять ваши потребности, и подготовить для вас самое выгодное предложение.\r\n</p><p>Ниже представлены цены на наши работы. Мы даем гарантию, что они не будут изменены на протежении всего ремонта.\r\n</p>'),
(11, '<p>Все компании указывают свои цены, но это не поможет вам составить точную смету по ремонту.\r\n</p><p>На стоимость влияет то-то и то-то... то-то и то-то... то-то и то-то... то-то и то-то... то-то и то-то... то-то и то-то... Поэтому мы предлагаем вам воспользоваться нашим бесплатным предложением - <a href="#" class="c-red">вызвать дизайнера и оценщика</a> на объект.\r\n</p><p>Это ни к чему вас не обязывает. А нам позволит лучше понять ваши потребности, и подготовить для вас самое выгодное предложение.\r\n</p><p>Ниже представлены цены на наши работы. Мы даем гарантию, что они не будут изменены на протежении всего ремонта.\r\n</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_plugins_urlline`
--

CREATE TABLE IF NOT EXISTS `cms_plugins_urlline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `ico` varchar(255) DEFAULT NULL,
  `plugin_id` int(11) NOT NULL,
  `popup_id` int(11),
  PRIMARY KEY (`id`),
  KEY `cms_pl_plugin_id_9cbeeece_fk_cms_plugins_calc_basemodelpl_ptr_id` (`plugin_id`),
  KEY `cms_plugins_urlline_dc1cac5d` (`popup_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `cms_plugins_urlline`
--

INSERT INTO `cms_plugins_urlline` (`id`, `name`, `url`, `ico`, `plugin_id`, `popup_id`) VALUES
(1, 'Что влияет на  стоимость?', '/', 'ico1', 27, NULL),
(2, '7 советов на чем  НЕЛЬЗЯ экономить!', '/', 'ico2', 27, NULL),
(3, 'Что влияет на  стоимость?', '', NULL, 40, 1),
(4, '7 советов на чем  НЕЛЬЗЯ экономить!', '', NULL, 40, 2),
(7, 'Что влияет на  стоимость?', '', NULL, 57, 1),
(8, '7 советов на чем  НЕЛЬЗЯ экономить!', '', NULL, 57, 2),
(9, 'Что влияет на  стоимость?', '', NULL, 65, 1),
(10, '7 советов на чем  НЕЛЬЗЯ экономить!', '', NULL, 65, 2),
(13, 'Что влияет на  стоимость?', '', NULL, 96, 1),
(14, '7 советов на чем  НЕЛЬЗЯ экономить!', '', NULL, 96, 2),
(19, 'Что влияет на  стоимость?', '', NULL, 126, 1),
(20, '7 советов на чем  НЕЛЬЗЯ экономить!', '', NULL, 126, 2),
(21, 'Что влияет на  стоимость?', '', NULL, 138, 1),
(22, '7 советов на чем  НЕЛЬЗЯ экономить!', '', NULL, 138, 2),
(23, 'Что влияет на  стоимость?', '', NULL, 146, 1),
(24, '7 советов на чем  НЕЛЬЗЯ экономить!', '', NULL, 146, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_staticplaceholder`
--

CREATE TABLE IF NOT EXISTS `cms_staticplaceholder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `dirty` tinyint(1) NOT NULL,
  `creation_method` varchar(20) NOT NULL,
  `draft_id` int(11) DEFAULT NULL,
  `public_id` int(11) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_staticplaceholder_code_21ba079c_uniq` (`code`,`site_id`),
  KEY `cms_staticplaceholder_site_id_dc6a85b6_fk_django_site_id` (`site_id`),
  KEY `cms_staticplaceholder_5cb48773` (`draft_id`),
  KEY `cms_staticplaceholder_1ee2744d` (`public_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `cms_staticplaceholder`
--

INSERT INTO `cms_staticplaceholder` (`id`, `name`, `code`, `dirty`, `creation_method`, `draft_id`, `public_id`, `site_id`) VALUES
(1, 'counters', 'counters', 0, 'template', 14, 15, NULL),
(2, 'price_top', 'price_top', 0, 'template', 30, 31, NULL),
(3, 'price_bottom', 'price_bottom', 0, 'template', 32, 33, NULL),
(4, 'menu', 'menu', 0, 'template', 73, 74, NULL),
(5, 'footer_left', 'footer_left', 0, 'template', 81, 82, NULL),
(6, 'footer_right', 'footer_right', 0, 'template', 83, 84, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_title`
--

CREATE TABLE IF NOT EXISTS `cms_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `menu_title` varchar(255) DEFAULT NULL,
  `meta_description` longtext,
  `slug` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `has_url_overwrite` tinyint(1) NOT NULL,
  `redirect` varchar(2048) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  `publisher_is_draft` tinyint(1) NOT NULL,
  `publisher_state` smallint(6) NOT NULL,
  `page_id` int(11) NOT NULL,
  `publisher_public_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_title_language_61aaf084_uniq` (`language`,`page_id`),
  UNIQUE KEY `publisher_public_id` (`publisher_public_id`),
  KEY `cms_title_8512ae7d` (`language`),
  KEY `cms_title_2dbcba41` (`slug`),
  KEY `cms_title_d6fe1d0b` (`path`),
  KEY `cms_title_1268de9a` (`has_url_overwrite`),
  KEY `cms_title_b7700099` (`publisher_is_draft`),
  KEY `cms_title_f7202fc0` (`publisher_state`),
  KEY `cms_title_page_id_5fade2a3_fk_cms_page_id` (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Дамп данных таблицы `cms_title`
--

INSERT INTO `cms_title` (`id`, `language`, `title`, `page_title`, `menu_title`, `meta_description`, `slug`, `path`, `has_url_overwrite`, `redirect`, `creation_date`, `published`, `publisher_is_draft`, `publisher_state`, `page_id`, `publisher_public_id`) VALUES
(1, 'ru', 'Главная', '', '', '', 'glavnaya', '', 0, NULL, '2016-02-17 13:03:58', 1, 1, 0, 1, 2),
(2, 'ru', 'Главная', '', '', '', 'glavnaya', '', 0, NULL, '2016-02-17 13:03:58', 1, 0, 1, 2, 1),
(3, 'ru', 'Наши работы', '', '', '', 'nashi-raboty', 'nashi-raboty', 0, '/nashi-raboty/vypolnennye-remonty/', '2016-02-17 13:24:03', 1, 1, 0, 3, 11),
(4, 'ru', 'Наши мастера', '', '', '', 'nashi-mastera', 'nashi-mastera', 0, '', '2016-02-17 13:24:16', 1, 1, 0, 4, 14),
(5, 'ru', 'Цены', '', '', '', 'ceny', 'ceny', 0, '', '2016-02-17 13:24:29', 1, 1, 0, 5, 15),
(6, 'ru', 'Школа ремонта', '', '', '', 'shkola-remonta', 'shkola-remonta', 0, NULL, '2016-02-17 13:24:42', 1, 1, 0, 6, 16),
(7, 'ru', 'О компании', '', '', '', 'o-kompanii', 'o-kompanii', 0, NULL, '2016-02-17 13:25:00', 1, 1, 0, 7, 17),
(8, 'ru', 'Выполненные ремонты', '', '', '', 'vypolnennye-remonty', 'nashi-raboty/vypolnennye-remonty', 0, '', '2016-02-17 13:25:26', 1, 1, 0, 8, 12),
(9, 'ru', 'Дизайн-проекты', '', '', '', 'dizajn-proekty', 'nashi-raboty/dizajn-proekty', 0, '', '2016-02-17 13:25:41', 1, 1, 0, 9, 13),
(10, 'ru', 'Виртуальные 3D туры', '', '', '', 'virtualnye-3d-tury', 'nashi-raboty/virtualnye-3d-tury', 0, '', '2016-02-17 13:26:04', 1, 1, 0, 10, 18),
(11, 'ru', 'Наши работы', '', '', '', 'nashi-raboty', 'nashi-raboty', 0, '/nashi-raboty/vypolnennye-remonty/', '2016-02-17 13:24:03', 1, 0, 0, 11, 3),
(12, 'ru', 'Выполненные ремонты', '', '', '', 'vypolnennye-remonty', 'nashi-raboty/vypolnennye-remonty', 0, '', '2016-02-17 13:25:26', 1, 0, 0, 12, 8),
(13, 'ru', 'Дизайн-проекты', '', '', '', 'dizajn-proekty', 'nashi-raboty/dizajn-proekty', 0, '', '2016-02-17 13:25:41', 1, 0, 0, 13, 9),
(14, 'ru', 'Наши мастера', '', '', '', 'nashi-mastera', 'nashi-mastera', 0, '', '2016-02-17 13:24:16', 1, 0, 0, 14, 4),
(15, 'ru', 'Цены', '', '', '', 'ceny', 'ceny', 0, '', '2016-02-17 13:24:29', 1, 0, 0, 15, 5),
(16, 'ru', 'Школа ремонта', '', '', '', 'shkola-remonta', 'shkola-remonta', 0, NULL, '2016-02-17 13:24:42', 1, 0, 0, 16, 6),
(17, 'ru', 'О компании', '', '', '', 'o-kompanii', 'o-kompanii', 0, NULL, '2016-02-17 13:25:00', 1, 0, 0, 17, 7),
(18, 'ru', 'Виртуальные 3D туры', '', '', '', 'virtualnye-3d-tury', 'nashi-raboty/virtualnye-3d-tury', 0, '', '2016-02-17 13:26:04', 1, 0, 0, 18, 10),
(19, 'ru', 'Наши партнеры', '', '', '', 'nashi-partnery', 'nashi-partnery', 0, '', '2016-02-18 07:47:24', 1, 1, 0, 19, 20),
(20, 'ru', 'Наши партнеры', '', '', '', 'nashi-partnery', 'nashi-partnery', 0, '', '2016-02-18 07:47:24', 1, 0, 0, 20, 19),
(21, 'ru', 'Контакты', '', '', '', 'contacts', 'contacts', 0, NULL, '2016-02-18 08:21:45', 1, 1, 0, 21, 22),
(22, 'ru', 'Контакты', '', '', '', 'contacts', 'contacts', 0, NULL, '2016-02-18 08:21:45', 1, 0, 0, 22, 21),
(23, 'ru', 'Отзывы', '', '', '', 'reviews', 'reviews', 0, '', '2016-02-24 09:19:28', 1, 1, 0, 23, 24),
(24, 'ru', 'Отзывы', '', '', '', 'reviews', 'reviews', 0, '', '2016-02-24 09:19:28', 1, 0, 0, 24, 23),
(25, 'ru', 'Ремонт квартир', '', '', '', 'remont-kvartir', 'remont-kvartir', 0, NULL, '2016-02-29 11:09:13', 1, 1, 0, 25, 29),
(26, 'ru', 'Ремонт коттеджей и таунхаусов', '', '', '', 'remont-kottedzhej-i-taunhausov', 'remont-kottedzhej-i-taunhausov', 0, NULL, '2016-02-29 11:09:39', 1, 1, 0, 26, 30),
(27, 'ru', 'Дизайн проекты', '', '', '', 'dizajn-proekty', 'dizajn-proekty', 0, NULL, '2016-02-29 11:09:59', 1, 1, 0, 27, 31),
(28, 'ru', 'Декор дома', '', '', '', 'dekor-doma', 'dekor-doma', 0, NULL, '2016-02-29 11:10:19', 1, 1, 0, 28, 32),
(29, 'ru', 'Ремонт квартир', '', '', '', 'remont-kvartir', 'remont-kvartir', 0, NULL, '2016-02-29 11:09:13', 1, 0, 1, 29, 25),
(30, 'ru', 'Ремонт коттеджей и таунхаусов', '', '', '', 'remont-kottedzhej-i-taunhausov', 'remont-kottedzhej-i-taunhausov', 0, NULL, '2016-02-29 11:09:39', 1, 0, 1, 30, 26),
(31, 'ru', 'Дизайн проекты', '', '', '', 'dizajn-proekty', 'dizajn-proekty', 0, NULL, '2016-02-29 11:09:59', 1, 0, 0, 31, 27),
(32, 'ru', 'Декор дома', '', '', '', 'dekor-doma', 'dekor-doma', 0, NULL, '2016-02-29 11:10:19', 1, 0, 0, 32, 28),
(33, 'ru', 'Вопросы и ответы', '', '', '', 'voprosy-i-otvety', 'voprosy-i-otvety', 0, '', '2016-02-29 12:23:48', 1, 1, 0, 33, 34),
(34, 'ru', 'Вопросы и ответы', '', '', '', 'voprosy-i-otvety', 'voprosy-i-otvety', 0, '', '2016-02-29 12:23:48', 1, 0, 0, 34, 33);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_urlconfrevision`
--

CREATE TABLE IF NOT EXISTS `cms_urlconfrevision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `revision` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `cms_urlconfrevision`
--

INSERT INTO `cms_urlconfrevision` (`id`, `revision`) VALUES
(1, '50a05613-df4a-46fc-b0eb-863f20d65926');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_usersettings`
--

CREATE TABLE IF NOT EXISTS `cms_usersettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(10) NOT NULL,
  `clipboard_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `cms_usersettings_clipboard_id_3ae17c19_fk_cms_placeholder_id` (`clipboard_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `cms_usersettings`
--

INSERT INTO `cms_usersettings` (`id`, `language`, `clipboard_id`, `user_id`) VALUES
(1, 'ru', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_categoryprice`
--

CREATE TABLE IF NOT EXISTS `contents_categoryprice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `contents_categoryprice`
--

INSERT INTO `contents_categoryprice` (`id`, `name`, `sort`, `active`) VALUES
(1, 'Демонтажные работы пол', 0, 1),
(2, 'Демонтажные работы стены', 0, 1),
(3, 'Демонтажные работы потолок', 0, 1),
(4, 'Демонтажные работы сантехника', 0, 1),
(5, 'Демонтажные работы электрика', 0, 1),
(6, 'Демонтажные работы двери и окна', 0, 1),
(7, 'Электромонтажные работы', 0, 1),
(8, 'Сантехнические работы', 0, 1),
(9, 'Работы с полом', 0, 1),
(10, 'Работы с потолком', 0, 1),
(11, 'Работы со стенами', 0, 1),
(12, 'Дополнительные работы', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_designimage`
--

CREATE TABLE IF NOT EXISTS `contents_designimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `sort` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contents_designimage_b098ad43` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `contents_designimage`
--

INSERT INTO `contents_designimage` (`id`, `image`, `sort`, `project_id`) VALUES
(1, 'catalog/f4af5bb9-52d1-4422-a2a0-42fced2a74aa.jpg', 0, 1),
(2, 'catalog/2e5be478-4149-45b4-b69e-a9bd3ef38057.jpg', 0, 1),
(3, 'catalog/d4b98e6b-4cc0-4a6c-b940-17058814b58b.jpg', 0, 1),
(4, 'catalog/93dd9bdb-c418-4858-bc92-ac4d2310b458.jpg', 0, 1),
(5, 'catalog/e659ae48-217a-41a8-8c34-6db300ec5a38.jpg', 0, 1),
(6, 'catalog/7247c59b-7a6c-4d45-a392-ee024a9ff936.jpg', 0, 2),
(7, 'catalog/245ba46d-f5cf-4c28-beba-d441a8674156.jpg', 0, 2),
(8, 'catalog/aa3ff049-dba8-4924-90af-ad3b65a848e0.jpg', 0, 2),
(9, 'catalog/625acfc2-8962-427c-a111-21f1e4477051.jpg', 0, 3),
(10, 'catalog/cf1c257c-de79-439a-b675-44b419dddb07.jpg', 0, 3),
(11, 'catalog/b0c8271a-20ff-46cb-8190-e80e594c6cea.jpg', 0, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_designproject`
--

CREATE TABLE IF NOT EXISTS `contents_designproject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `text` longtext NOT NULL,
  `sort` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `review_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contents_designproject_5bd2a989` (`review_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `contents_designproject`
--

INSERT INTO `contents_designproject` (`id`, `name`, `image`, `text`, `sort`, `active`, `review_id`) VALUES
(1, 'Дизайн проект в стиле модерн 1', 'catalog/dizain-proekt-v-stile-modern-1.jpg', 'Здесь мы описываем какая ставилась задача. Какие были пожелания и какого результата достигли. Насколько заказчик остался доволен. Текст должен нести пользу и описывать на чем строился выбор дизайнера...', 0, 1, 1),
(2, 'Дизайн проект в стиле модерн 2', 'catalog/dizain-proekt-v-stile-modern-2.jpg', 'Здесь мы описываем какая ставилась задача. Какие были пожелания и какого результата достигли. Насколько заказчик остался доволен. Текст должен нести пользу и описывать на чем строился выбор дизайнера...', 0, 1, 2),
(3, 'Дизайн проект в стиле модерн 3', 'catalog/dizain-proekt-v-stile-modern-3.jpg', 'Дизайн проект в стиле модерн', 0, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_designservice`
--

CREATE TABLE IF NOT EXISTS `contents_designservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `contents_designservice`
--

INSERT INTO `contents_designservice` (`id`, `name`, `active`, `sort`) VALUES
(1, 'Обмеры помещений', 1, 0),
(2, 'Варианты планировочного решения', 1, 0),
(3, 'Эскизы', 1, 0),
(4, 'План возводимых перегородок', 1, 0),
(5, 'Схемы потолков и электрики', 1, 0),
(6, '3D Max визуализация', 1, 0),
(7, 'Полный комплект рабочих чертежей', 1, 0),
(8, 'Развертки стен с прорисовкой декоративных элементов', 1, 0),
(9, 'Подбор материалов ванных комнат, спецификация', 1, 0),
(10, 'Подбор отделочных материалов', 1, 0),
(11, 'Подбор мебели, текстиля и элементов декора', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_partners`
--

CREATE TABLE IF NOT EXISTS `contents_partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `text` longtext NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `text_url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `contents_partners`
--

INSERT INTO `contents_partners` (`id`, `name`, `image`, `text`, `url`, `text_url`, `active`, `sort`) VALUES
(1, 'web2people', 'catalog/web2people.jpg', 'Группа компаний «Идеальный Камень» - крупнейший российский производитель искусственного облицовочного камня', 'www.idealstone.ru', 'www.idealstone.ru', 1, 0),
(2, 'QXSolutions', 'catalog/qxsolutions.jpg', 'Декоративные отделочные материалы 21 века VIRTESO являются уникальными современными изделиями, которые производятся лучшими итальянскими дизайнерами', 'www.virteso.com', 'www.virteso.com', 1, 0),
(3, 'Headex', 'catalog/headex.jpg', 'Компания «FDF Actual design studio" - производитель мебели и предметов интерьера сложных форм из натурального дерева', 'ru.actualdesignstudio.eu', 'ru.actualdesignstudio.eu', 1, 0),
(4, 'bee.by', 'catalog/bee-by.jpg', 'Салон штор Blinds-Shutters предлагает все виды текстильного декорирования помещений, а также карнизы, жалюзи, солнцезащитные системы и сопутствующие аксессуары', 'www.blinds-shutters.ru', 'www.blinds-shutters.ru', 1, 0),
(5, 'Pomidor', 'catalog/pomidor.jpg', 'Компания Килто - ведущий разработчик и производитель клеев для различных областей промышленности и строительства, строительных растворов, шпатлевок, грунтовок, герметиков, паркетных лаков, гидроизоляционных материалов, материалов для очистки и ухода за покрытиями', 'www.kiilto.ru', 'www.kiilto.ru', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_person`
--

CREATE TABLE IF NOT EXISTS `contents_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `text` longtext,
  `active` tinyint(1) NOT NULL,
  `sort` int(11) NOT NULL,
  `image2` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `contents_person`
--

INSERT INTO `contents_person` (`id`, `name`, `image`, `stage`, `post`, `text`, `active`, `sort`, `image2`) VALUES
(1, 'Николай', 'catalog/nikolai_hjLG8V6.jpg', '40 лет', 'исполнительный директор', '<p>Николай обладает огромным жизнелюбием и удивительной повышенной работоспособностью. Уже\r\n                                более 40 лет его день начинается еще до восхода солнца, а свой первый заряд энергии он\r\n                                получает с утренней пробежкой независимо от погоды и времени года. Возможно в этом и\r\n                                кроется\r\n                                секрет его успеха в работе? Четкая организация процессов снабжения объектов материалами\r\n                                и\r\n                                инструментами, планирование закупок, мониторинг, анализ только лучших поставщиков,\r\n                                переговоры... Для него в сутках более 24 часов!\r\n                            </p><p>Смогли бы вы описать Николая в 4 словах? Это нетрудно: “Человек слова и дела!” Наверное,\r\n                                поэтому его телефон всегда разрывается, так как все партнеры и коллеги, с кем он начинал\r\n                                когдато работать, до сих пор обращаются к нему за помощью и советом, или просто спешат\r\n                                поздравить с праздником.\r\n                            </p>', 1, 0, 'catalog/nikolai_iabIpRN.jpg'),
(2, 'Татьяна', 'catalog/tatiana.jpg', '20 лет', 'Руководитель отдела снабжения', '<p>Николай обладает огромным жизнелюбием и удивительной повышенной работоспособностью. Уже\r\n                                более 40 лет его день начинается еще до восхода солнца, а свой первый заряд энергии он\r\n                                получает с утренней пробежкой независимо от погоды и времени года. Возможно в этом и\r\n                                кроется\r\n                                секрет его успеха в работе? Четкая организация процессов снабжения объектов материалами\r\n                                и\r\n                                инструментами, планирование закупок, мониторинг, анализ только лучших поставщиков,\r\n                                переговоры... Для него в сутках более 24 часов!\r\n                            </p><p>Смогли бы вы описать Николая в 4 словах? Это нетрудно: “Человек слова и дела!” Наверное,\r\n                                поэтому его телефон всегда разрывается, так как все партнеры и коллеги, с кем он начинал\r\n                                когдато работать, до сих пор обращаются к нему за помощью и советом, или просто спешат\r\n                                поздравить с праздником.\r\n                            </p>', 1, 0, 'catalog/tatiana_zvMJzJN.jpg'),
(3, 'Василий', 'catalog/vasilii.jpg', '40 лет', 'отдел представительств', '<p>Николай обладает огромным жизнелюбием и удивительной повышенной работоспособностью. Уже\r\n                                более 40 лет его день начинается еще до восхода солнца, а свой первый заряд энергии он\r\n                                получает с утренней пробежкой независимо от погоды и времени года. Возможно в этом и\r\n                                кроется\r\n                                секрет его успеха в работе? Четкая организация процессов снабжения объектов материалами\r\n                                и\r\n                                инструментами, планирование закупок, мониторинг, анализ только лучших поставщиков,\r\n                                переговоры... Для него в сутках более 24 часов!\r\n                            </p><p>Смогли бы вы описать Николая в 4 словах? Это нетрудно: “Человек слова и дела!” Наверное,\r\n                                поэтому его телефон всегда разрывается, так как все партнеры и коллеги, с кем он начинал\r\n                                когдато работать, до сих пор обращаются к нему за помощью и советом, или просто спешат\r\n                                поздравить с праздником.\r\n                            </p>', 1, 0, 'catalog/vasilii_iMKSkQg.jpg'),
(4, 'Ирина', 'catalog/irina.jpg', '40 лет', 'директор по продажам', '<p>Николай обладает огромным жизнелюбием и удивительной повышенной работоспособностью. Уже\r\n                                более 40 лет его день начинается еще до восхода солнца, а свой первый заряд энергии он\r\n                                получает с утренней пробежкой независимо от погоды и времени года. Возможно в этом и\r\n                                кроется\r\n                                секрет его успеха в работе? Четкая организация процессов снабжения объектов материалами\r\n                                и\r\n                                инструментами, планирование закупок, мониторинг, анализ только лучших поставщиков,\r\n                                переговоры... Для него в сутках более 24 часов!\r\n                            </p><p>Смогли бы вы описать Николая в 4 словах? Это нетрудно: “Человек слова и дела!” Наверное,\r\n                                поэтому его телефон всегда разрывается, так как все партнеры и коллеги, с кем он начинал\r\n                                когдато работать, до сих пор обращаются к нему за помощью и советом, или просто спешат\r\n                                поздравить с праздником.\r\n                            </p>', 1, 0, 'catalog/irina_gXbbRk7.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `contents_popup`
--

CREATE TABLE IF NOT EXISTS `contents_popup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `contents_popup`
--

INSERT INTO `contents_popup` (`id`, `name`, `text`) VALUES
(1, 'Что влияет на  стоимость?', '<p>Что влияет на \r\nстоимость?\r\n</p>'),
(2, '7 советов на чем  НЕЛЬЗЯ экономить!', '<p>7 советов на чем \r\nНЕЛЬЗЯ экономить!\r\n</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `contents_price`
--

CREATE TABLE IF NOT EXISTS `contents_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contents_price_category_id_f341f671_fk_contents_categoryprice_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `contents_price`
--

INSERT INTO `contents_price` (`id`, `name`, `unit`, `price`, `sort`, `active`, `category_id`) VALUES
(1, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 1),
(2, 'Снятие паркетной доски, ламината', 'кв.м', '120,00-180,00', 0, 1, 1),
(3, 'Демонтаж линолеума, ковролина (за слой)', 'кв.м', '90,00-110,00', 0, 1, 1),
(4, 'Демонтаж фанеры (оргалита)', 'кв.м', '150,00-200,00', 0, 1, 1),
(5, 'Демонтаж деревянных/пластиковых плинтусов (без сохранения)', 'п.м', '40,00-60,00', 0, 1, 1),
(6, 'Демонтаж деревянных полов', 'кв.м', '180,00-220,00', 0, 1, 1),
(7, 'Демонтаж керамической плитки с пола', 'кв.м', '150,00-200,00', 0, 1, 1),
(8, 'Демонтаж керамических плинтусов (без сохранения)', 'кв.м', '60,00-80,00', 0, 1, 1),
(9, 'Демонтаж напольных керамогранитных плит', 'кв.м', '200,00-250,00', 0, 1, 1),
(10, 'Удаление бетонной стяжки до 30 мм', 'кв.м', '170,00-200,00', 0, 1, 1),
(11, 'Удаление бетонной стяжки до 50 мм', 'п.м', '250,00-300,00', 0, 1, 1),
(12, 'Удаление бетонной стяжки до 60 мм', 'кв.м', '300,00-350,00', 0, 1, 1),
(13, 'даление бетонной стяжки до 100 мм', 'кв.м', '150,00-200,00', 0, 1, 1),
(14, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 2),
(15, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 3),
(16, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 4),
(17, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 5),
(18, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 6),
(19, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 7),
(20, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 8),
(21, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 9),
(22, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 10),
(23, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 11),
(24, 'Демонтаж паркета', 'кв.м', '150,00-250,00', 0, 1, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_project`
--

CREATE TABLE IF NOT EXISTS `contents_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `area` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `limit` varchar(255) DEFAULT NULL,
  `text` longtext,
  `schtick` longtext,
  `sort` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `design_id` int(11) DEFAULT NULL,
  `person_id` int(11) NOT NULL,
  `review_id` int(11),
  `tour_id` int(11),
  `place_id` int(11),
  `meta_description` longtext,
  `meta_keywords` varchar(255),
  `meta_title` varchar(255),
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `design_id` (`design_id`),
  UNIQUE KEY `tour_id` (`tour_id`),
  KEY `contents_project_person_id_ad87864d_fk_contents_person_id` (`person_id`),
  KEY `contents_project_5bd2a989` (`review_id`),
  KEY `contents_project_62becf4a` (`place_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `contents_project`
--

INSERT INTO `contents_project` (`id`, `name`, `slug`, `image`, `area`, `price`, `limit`, `text`, `schtick`, `sort`, `active`, `design_id`, `person_id`, `review_id`, `tour_id`, `place_id`, `meta_description`, `meta_keywords`, `meta_title`) VALUES
(1, 'Ремонт 3-х комнатной квартиры, ул. Волжский бульвар, Москва', 'remont-3-h-komnatnoj-kvartiry-ul-volzhskij-bulvar-moskva', 'catalog/remont-3-kh-komnatnoi-kvartiry-ul-volzhskii-bulvar-moskva.jpg', '125', '2 300 000 руб.', '4 месяца', '<p>Прямые линии, контрастные цвета и небольшое количество мебели - вот основные принципы, которые были заложены в основу данного проекта.</p><p>Квартира предназначена для молодого человека, который любит светлые тона, натуральные материалы и игру контрастов.</p>', '<div class="lp fll">\r\n  <span class="fw600">+ 25 м <sup>2</sup></span>\r\n</div>\r\n<div class="rp fll">\r\n  <p>увеличили площадь дома за счет постройки полноценной мансарды</p>\r\n</div>', 0, 1, 1, 2, 1, 1, 56, '', '', ''),
(2, 'Ремонт таунхауса к/п Бристоль', 'remont-taunhausa-kp-bristol', 'catalog/remont-taunkhausa-k-p-bristol.jpg', '125', '2 300 000', '4 месяца', '<p>Прямые линии, контрастные цвета и небольшое количество мебели - вот основные принципы, которые были заложены в основу данного проекта.\r\n</p><p>Квартира предназначена для молодого человека, который любит светлые тона, натуральные материалы и игру контрастов.\r\n</p><a href="#" class="dash fancy c-red">Смотреть планировку (было/стало)</a>', '<div class="lp fll">\r\n                            <span class="fw600">+ 25 м <sup>2</sup></span>\r\n                        </div>\r\n                        <div class="rp fll">\r\n                            <p>увеличили площадь дома за счет постройки полноценной мансарды</p>\r\n                        </div>', 0, 1, NULL, 4, 2, 3, 76, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `contents_projectimage`
--

CREATE TABLE IF NOT EXISTS `contents_projectimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `sort` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contents_projectimage_project_id_08dcbc4a_fk_contents_project_id` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `contents_projectimage`
--

INSERT INTO `contents_projectimage` (`id`, `image`, `sort`, `project_id`) VALUES
(1, 'catalog/b4f7799f-5dbf-4e36-a9bb-5f55ad355869.jpg', 0, 1),
(2, 'catalog/11f6d41c-a6a5-473f-92ac-0f46bf14150c.jpg', 0, 1),
(3, 'catalog/ea89272a-ebfb-41ac-830d-41b770296fbc.jpg', 0, 1),
(4, 'catalog/9f33e303-cd3e-43f1-97f4-a64f2b77937b.jpg', 0, 1),
(5, 'catalog/6f7f58a4-2553-45dc-b652-cb1b26101909.jpg', 0, 2),
(6, 'catalog/6b8cecb9-f5d1-4fbb-88d0-5f6423fe1146.jpg', 0, 2),
(7, 'catalog/a1333a9c-b6fa-423f-96c6-172ebf194eba.jpg', 0, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_project_secrets`
--

CREATE TABLE IF NOT EXISTS `contents_project_secrets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `secret_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contents_project_secrets_project_id_f16f0b08_uniq` (`project_id`,`secret_id`),
  KEY `contents_project_secret_secret_id_cd686dbb_fk_contents_secret_id` (`secret_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `contents_project_secrets`
--

INSERT INTO `contents_project_secrets` (`id`, `project_id`, `secret_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3),
(9, 2, 4),
(10, 2, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_question`
--

CREATE TABLE IF NOT EXISTS `contents_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `answer` longtext NOT NULL,
  `active` tinyint(1) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `contents_question`
--

INSERT INTO `contents_question` (`id`, `name`, `answer`, `active`, `sort`) VALUES
(1, 'Из чего складывается цена ремонта?', '<p>Ремонтные работы подразделяются на черновые и отделочные работы. Стоимость черновых работ всегда неизменна и зависит лишь от типа ремонта (косметический, капитальный или евроремонт). Например, при евроремонте наша цель – максимально правильно выстроить геометрию помещения при помощи лазерного уровня, чтобы использовать как можно меньше чернового материала (штукатурки, смесей, стяжек, наливного пола и т.д.), следовательно, сократить расходы Заказчика. Стоимость ремонта меняется от сложности дизайн-проекта и характера отделки помещения. Чем больше декоративных элементов, встроенных ниш, витражей, криволинейных и многоуровневых поверхностей, тем выше стоимость ремонта.</p>', 1, 0),
(2, 'Какова стоимость 1 кв.м.?', '<p>Цена за 1 кв.м. зависит от типа ремонта:\r\n</p>\r\n<ul>\r\n	<li><a href="#" class="c-red">Косметический ремонт</a> – от 2000 руб.</li>\r\n	<li><a href="#" class="c-red">Капитальный ремонт</a> – от 5000 руб.</li>\r\n	<li><a href="#" class="c-red">Евроремонт</a> – от 7 000 руб.</li>\r\n	<li><a href="#" class="c-red">Ремонт «под ключ»</a> - от 10 000 руб.</li>\r\n</ul>', 1, 0),
(3, 'Какая система оплаты в Вашей фирме?', '<p>Мы работаем исключительно по факту выполненных работ. Заказчик ежемесячно получает подробный отчет о выполненных работах с указанием всех видов работ, метражей и стоимости единиц. До оплаты счета Заказчик всегда может придти на объект и оценить качество выполненных работ. Аналогичная схема работает и при закупке отделочных материалов.</p>', 1, 0),
(4, 'Кто работает в компании «Никстест-строй»?', '<p>В нашей компании работают только высококвалифицированные мастера, универсальные и узкопрофильные специалисты с опытом работы более 12 лет. Вы всегда можете посетить наши текущие объекты в процессе ремонта, чтобы убедиться в профессионализме бригады.</p>', 1, 0),
(5, 'В какие сроки осуществляется ремонт?', '<p>Сроки ремонта зависят от сложности дизайн-проекта, видов работ и характера отделочных материалов. Средняя продолжительность ремонта – не более 6 месяцев. Ремонт любого типа обязательно осуществляется с соблюдением всех технологических процессов. Каждый мастер бригады отвечает за свой фронт работы и выполняет ремонт только на одном объекте.</p>', 1, 0),
(6, 'Из чего складывается цена ремонта?', '<p>Ремонтные работы подразделяются на черновые и отделочные работы. Стоимость черновых работ всегда неизменна и зависит лишь от типа ремонта (косметический, капитальный или евроремонт). Например, при евроремонте наша цель – максимально правильно выстроить геометрию помещения при помощи лазерного уровня, чтобы использовать как можно меньше чернового материала (штукатурки, смесей, стяжек, наливного пола и т.д.), следовательно, сократить расходы Заказчика. Стоимость ремонта меняется от сложности дизайн-проекта и характера отделки помещения. Чем больше декоративных элементов, встроенных ниш, витражей, криволинейных и многоуровневых поверхностей, тем выше стоимость ремонта.</p>', 1, 0),
(7, 'Какая система оплаты в Вашей фирме?', '<p>Мы работаем исключительно по факту выполненных работ. Заказчик ежемесячно получает подробный отчет о выполненных работах с указанием всех видов работ, метражей и стоимости единиц. До оплаты счета Заказчик всегда может придти на объект и оценить качество выполненных работ. Аналогичная схема работает и при закупке отделочных материалов.</p>', 1, 0),
(8, 'Кто работает в компании «Никстест-строй»?', '<p>В нашей компании работают только высококвалифицированные мастера, универсальные и узкопрофильные специалисты с опытом работы более 12 лет. Вы всегда можете посетить наши текущие объекты в процессе ремонта, чтобы убедиться в профессионализме бригады.</p>', 1, 0),
(9, 'В какие сроки осуществляется ремонт?', '<p>Сроки ремонта зависят от сложности дизайн-проекта, видов работ и характера отделочных материалов. Средняя продолжительность ремонта – не более 6 месяцев. Ремонт любого типа обязательно осуществляется с соблюдением всех технологических процессов. Каждый мастер бригады отвечает за свой фронт работы и выполняет ремонт только на одном объекте.</p>', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_review`
--

CREATE TABLE IF NOT EXISTS `contents_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `proj_name` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `rating` int(11) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `image` varchar(100),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `contents_review`
--

INSERT INTO `contents_review` (`id`, `name`, `proj_name`, `text`, `rating`, `sort`, `active`, `image`) VALUES
(1, 'Маргарита Репина', 'Косметический ремонт трехкомнатной квартиры на м. Китай- Город', '<p>Хотим выразить благодарность фирме «Никстест-строй» за проведенный косметический ремонт в квартире, расположен- ной в старой Москве в районе Китай- город. Квартира не ремонтировалась 14 лет, а капитальный ремонт не проводился 40 лет.</p><p>В сжатые сроки был сделан демонтаж старого паркета, который, как выяснилось, выполнен в два слоя, а также восстановлен потолок с лепниной! Высококвалифицированные мастера удалили несколько слоев старых обоев, выровняли стены и поклеили обои со сложным рисунком.</p><p>Необходимые стройматериалы для отделочных работ были заказаны и доставлены фирмой домой. Ремонт выполнялся в 3-х комнатах, а также в коридоре.</p><p>Мы не имели возможности переехать в другое жилье и полностью освободить помещение для работ, но, несмотря на этот факт, мы не почувствовали никакого дискомфорта.</p><p>Ремонт для нас прошел очень легко, мы даже представить себе не могли, что может так быть! Мастера аккуратно выполняли работы, поэтому квартира всегда оставалась практически чистой.</p><p>Огромная благодарность техническому директору Ирине Анатольевне за компетентность и отзывчивость, а также высококвалифицированным мастерам Петру Ивановичу и Елене!</p>', 4, 0, 1, 'catalog/margarita-repina.jpg'),
(2, 'Татьяна Иванова', 'Ремонт коттеджа в г. Талдом, Московская область', '<p>Здравствуйте всем, кому интересен мой отзыв! Построив дом и проведя все коммуникации, столкнулись с проблемой выбора фирмы по отделке дома. Начали поиск в Интернете, положившись на интуицию. В наше время слишком много на этом рынке непрофессионалов, да и просто мошенников!</p><p>Звонок в «Никстест-строй» нас порадовал четкими и ясными ответами, приглашением на объекты с завершившимся ремонтом. Нас все устроило, и мы решились... Заказали дизайн-проект, с авторским надзором и не пожалели! Впоследствии все технические проблемы, покупка плитки, светового оборудования в условиях нашей занятости решились быстро (спасибо дизайнеру Насте!).</p><p>Ремонт начался в срок, знакомство со строителями нас тоже порадовало: приятные, квалифицированные ребята. Практически каждый выходной технический директор Ирина Анатольевна приезжала на объект, а это 100 км от Москвы! Даже те недочеты, которые мы не замечали, переделывались.</p><p>Главное - было полное доверие и взаимопонимание, общая увлеченность процессом, что большая редкость! Нельзя сказать, что было все гладко, но в целом, уже по прошествии года с момента окончания нашего ремонта, могу с уверенностью сказать - всем советую эту фирму, это честные и порядочные люди, знающие свое дело! В этом году собираемся с ними строить гараж.</p><p>Желаю всем удачи с ремонтом, а команде «Никстест-строй» благополучия и хороших заказчиков! :)</p>', 5, 0, 1, 'catalog/tatiana-ivanova.jpg'),
(3, 'Интересное в блоге', 'Косметический ремонт трехкомнатной квартиры на м. Китай- Город', 'gsdehjrdejk', 5, NULL, 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `contents_reviewimage`
--

CREATE TABLE IF NOT EXISTS `contents_reviewimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `sort` int(11) NOT NULL,
  `review_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contents_reviewimage_5bd2a989` (`review_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `contents_reviewimage`
--

INSERT INTO `contents_reviewimage` (`id`, `image`, `sort`, `review_id`) VALUES
(1, 'catalog/51296000-90fd-4e10-9ea4-575c72efa5ff.jpg', 0, 1),
(2, 'catalog/ae958ded-225c-4448-83bf-2655a1995b8c.jpg', 0, 1),
(3, 'catalog/0fac9a59-bf34-4e52-8096-ddcfed7ad82a.jpg', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_secret`
--

CREATE TABLE IF NOT EXISTS `contents_secret` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sub_name` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `contents_secret`
--

INSERT INTO `contents_secret` (`id`, `name`, `sub_name`, `text`, `sort`) VALUES
(1, 'В санузел из гостинной?', 'В санузел из гостинной?', 'На 1 этаже перенесли дверной проем ванной комнаты из гостиной в зону лестничного проема, т.к. вход в санузел из жилого помещения, предложенный застройщиком, является нарушением. Кроме того, согласитесь, гораздо приятнее смотреть в гостиной любимый фильм, нежели «любоваться» санузлом?', 0),
(2, 'Особенность раздвижных систем', 'Особенность раздвижных систем', 'Для удобства эксплуатации санузла заказчику было предложено установить именно раздвижные системы (кассеты с дверными полотнами), а не обычную дверь (как планировал заказчик). Аналогичное решение мы применили и в зоне небольшой прихожей с целью экономии пространства.', 0),
(3, 'Максимально увеличиваем кухонный фронт', 'Максимально увеличиваем кухонный фронт', 'Обрамляющая кухню ГКЛ конструкция (словно рамка) позволяет сделать кухню более компактной и визуально удаленной. А применение встроенной техники позволяет воспринимать кухонный гарнитур как декоративную мебель, без намека на наличие кухонной техники внутри.', 0),
(4, 'Визуально увеличиваем пространство', 'Визуально увеличиваем пространство', 'В достаточно темной зоне столовой рядом с лестницей расположили большие зеркала строго напротив оконных проемов противоположной стены, тем самым визуально увеличили пространство и сделали его равномерно освещенным. Благодаря этому, столовая становится комфортной и возникает ощущение, что сидишь у окна.', 0),
(5, '5 лет гарантии', '5 лет гарантии', '5 лет гарантии', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_secretimage`
--

CREATE TABLE IF NOT EXISTS `contents_secretimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `sort` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contents_secretimage_project_id_c8da668a_fk_contents_secret_id` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `contents_secretimage`
--

INSERT INTO `contents_secretimage` (`id`, `image`, `sort`, `project_id`) VALUES
(1, 'catalog/17b84ecd-7bec-43b6-89b9-83f5b1370f25.jpg', 0, 1),
(2, 'catalog/22c9b6a5-6e8c-4f71-8f1e-22e19ba391fc.jpg', 0, 1),
(3, 'catalog/78577ba1-b82e-413d-8405-1df71cac46de.jpg', 0, 1),
(4, 'catalog/94994e02-76f4-431c-952f-eb0becfce8a1.jpg', 0, 2),
(5, 'catalog/3cb63eca-6964-4b1d-9eec-196d61f75df7.jpg', 0, 2),
(6, 'catalog/8499b724-2b37-434e-8942-4c06e5c03aa7.jpg', 0, 2),
(7, 'catalog/e81ce1a5-b799-414f-a2ee-e4c5e104c426.jpg', 0, 3),
(8, 'catalog/3807f651-330a-4083-ab7c-153183f4b75f.jpg', 0, 3),
(9, 'catalog/889eedf7-26b2-4956-851c-2019dc63322e.jpg', 0, 3),
(10, 'catalog/c3d1c90d-7e8b-4fb8-a398-3c2a215c5817.jpg', 0, 3),
(11, 'catalog/de43a748-0a0b-40b8-b4eb-d8f964f2bb70.jpg', 0, 4),
(12, 'catalog/62aafbec-3933-41aa-b22b-a03bc3182b71.jpg', 0, 4),
(13, 'catalog/6949e970-7e56-488d-8133-58ea1d3b6fc6.jpg', 0, 4),
(14, 'catalog/aceda44c-71dd-44c7-a7c3-48fc33929f0b.jpg', 0, 4),
(15, 'catalog/d8c7d6e9-60e1-41eb-96ef-a867792b133b.jpg', 0, 4),
(16, 'catalog/d6d675ff-d228-4cff-9b91-badfd2e25cd8.jpg', 0, 5),
(17, 'catalog/e8889361-fdcd-41d4-af9b-2787ee519ea9.jpg', 0, 5),
(18, 'catalog/abfe650e-a799-45cb-874a-6e395f660753.jpg', 0, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_slider`
--

CREATE TABLE IF NOT EXISTS `contents_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `area` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `limit` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `contents_slider`
--

INSERT INTO `contents_slider` (`id`, `name`, `image`, `area`, `price`, `limit`, `sort`) VALUES
(1, 'Ремонт таунхауса <span>к/п Бристоль</span>', 'catalog/remont-taunkhausa-span-k-p-bristol-span.jpg', '125', '2 300 000 руб.', '4 месяца', 0),
(2, 'Косметический ремонт', 'catalog/kosmeticheskii-remont.jpg', '123', '1 200 200', '4 месяца', 0),
(3, 'Ремонт коттеджа', 'catalog/remont-kottedzha.jpg', '', '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_tariffdesign`
--

CREATE TABLE IF NOT EXISTS `contents_tariffdesign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `contents_tariffdesign`
--

INSERT INTO `contents_tariffdesign` (`id`, `name`, `price`, `sort`) VALUES
(1, 'Базовый', '1200.00', 0),
(2, 'Оптимальный', '2000.00', 0),
(3, 'Премиальный', '3000.00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_tariffdesign_service`
--

CREATE TABLE IF NOT EXISTS `contents_tariffdesign_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tariffdesign_id` int(11) NOT NULL,
  `designservice_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contents_tariffdesign_service_tariffdesign_id_c63c1e82_uniq` (`tariffdesign_id`,`designservice_id`),
  KEY `contents__designservice_id_8c6ad514_fk_contents_designservice_id` (`designservice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `contents_tariffdesign_service`
--

INSERT INTO `contents_tariffdesign_service` (`id`, `tariffdesign_id`, `designservice_id`) VALUES
(1, 1, 1),
(2, 1, 5),
(3, 1, 6),
(4, 1, 7),
(5, 1, 8),
(6, 1, 9),
(7, 1, 10),
(8, 2, 1),
(9, 2, 3),
(10, 2, 4),
(11, 2, 5),
(12, 2, 6),
(13, 2, 7),
(14, 2, 8),
(15, 2, 9),
(16, 2, 10),
(17, 3, 1),
(18, 3, 2),
(19, 3, 3),
(20, 3, 4),
(21, 3, 5),
(22, 3, 6),
(23, 3, 7),
(24, 3, 8),
(25, 3, 9),
(26, 3, 10),
(27, 3, 11);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_tdtour`
--

CREATE TABLE IF NOT EXISTS `contents_tdtour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `text` longtext NOT NULL,
  `sort` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `review_id` int(11),
  `path` varchar(255),
  PRIMARY KEY (`id`),
  KEY `contents_tdtour_5bd2a989` (`review_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `contents_tdtour`
--

INSERT INTO `contents_tdtour` (`id`, `name`, `image`, `file`, `text`, `sort`, `active`, `review_id`, `path`) VALUES
(1, '3D тур Вернадка 2 вариант', 'catalog/3d-tur-po-taunkhausu-k-p-bristol.jpg', '', 'Здесь мы описываем какая ставилась задача. Какие были пожелания и какого результата достигли. Насколько заказчик остался доволен. Текст должен нести пользу и описывать на чем строился выбор дизайнера...', 0, 1, 1, 'D:\\repository\\nts\\nts\\media/catalog/tours/3d-tur-po-taunkhausu-k-p-bristol\\tour_vern2\\Room 180.html'),
(2, 'Тур ул. Паустовского', 'catalog/remont-kvartir-i-ofisov-3-kh-komnatnaia-kvartira-v-moskve-na-ul-volzhskii-bulvar.jpg', '', 'Ремонт квартир и офисов: 3-х комнатная квартира, в Москве на ул. Волжский бульвар', 0, 1, NULL, 'D:\\repository\\nts\\nts\\media/catalog/tours/remont-kvartir-i-ofisov-3-kh-komnatnaia-kvartira-v-moskve-na-ul-volzhskii-bulvar\\paustovskogo\\Room 180.html'),
(3, 'Тур Чистые пруды', 'catalog/remont-kvartir-i-ofisov-kottedzh-v-g-taldom-moskovskaia-obl.jpg', '', 'Ремонт квартир и офисов: Коттедж в г. Талдом, Московская обл', 0, 1, NULL, 'D:\\repository\\nts\\nts\\media/catalog/tours/tur-chistye-prudy\\chistie_prudi\\Room 180.html'),
(4, 'Сухаревская 3D', 'catalog/remont-kvartir-i-ofisov-kvartira-v-moskve-v-kostianskom-pereulke.jpg', '', 'Ремонт квартир и офисов: Квартира в Москве в Костянском переулке', 0, 1, NULL, 'D:\\repository\\nts\\nts\\media/catalog/tours/sukharevskaia-3d\\syharevskaja_3D\\virtualtour.html'),
(5, 'Ремонт квартир и офисов: Однокомнатная квартира в Москве на пр. Вернадского', 'catalog/remont-kvartir-i-ofisov-odnokomnatnaia-kvartira-v-moskve-na-pr-vernadskogo.jpg', 'catalog/remont-kvartir-i-ofisov-odnokomnatnaia-kvartira-v-moskve-na-pr-vernadskogo_t7lmnJ7.jpg', 'Ремонт квартир и офисов: Однокомнатная квартира в Москве на пр. Вернадского', 0, 1, NULL, NULL),
(6, 'Ремонт квартир и офисов: Квартира в Москве на ул. Паустовского', 'catalog/remont-kvartir-i-ofisov-kvartira-v-moskve-na-ul-paustovskogo.jpg', 'catalog/remont-kvartir-i-ofisov-kvartira-v-moskve-na-ul-paustovskogo_PPWJl5r.jpg', 'Ремонт квартир и офисов: Квартира в Москве на ул. Паустовского', 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_teaser`
--

CREATE TABLE IF NOT EXISTS `contents_teaser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ico` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `contents_teaser`
--

INSERT INTO `contents_teaser` (`id`, `name`, `ico`, `text`, `sort`) VALUES
(1, '5 лет гарантии', 'i1', 'Мы делаем ремонт как для себя. Поэтому Вы получаете не только самый качественный ремонт, но и необходимые рекомендации архитектора и дизайнера', 10),
(2, 'Оплата материалов по факту', 'i2', 'Мы ценим Ваше время! Во избежание простоев мы за свои средства закупаем материалы, которые согласовали с Вами. Вы их оплачиваете по факту их доставки.', 20),
(3, 'Демонстрация объектов', 'i3', 'Вы можете посетить наши готовые объекты и объекты в процессе ремонта. Оценить работу на любых этапах ремонта, познакомиться с мастерами или лично пообщаться с заказчиками.', 30),
(4, 'Работаем 6 дней в неделю', 'i4', 'Для Вас мы всегда на связи! 6 дней в неделю. Мы любим работать в субботу!)', 40),
(5, 'Гарантия цены по договору', 'i5', 'Вы получаете фиксированные цены. Если возникнут новые пожелания, то оформляем дополнительное соглашение, так же с фиксированной ценой.', 50),
(6, 'Двойной контроль качества', 'i6', 'Ежедневно все работы на объекте контролирует прораб. Дополнительно, каждый этап работ принимает технический директор.', 60),
(7, 'Оплата работ по факту', 'i7', '1 раз в месяц Вы по факту оплачиваете наши услуги', 70),
(8, 'Заказчики довольны', 'i8', '<img src="/static/images/stars.png" alt=""> (см. <a href="#" class="c-red">отзывы</a>)', 80),
(9, 'Индивидуальное проектирование', 'i9', 'Мы говорим «нет» типовым проектам под застройщика. Только индивидуальный подход к каждому Заказчику.', 1),
(10, 'Увеличение площади (фактическое или визуальное)', 'i10', 'Грамотное решение объема помещения у нас в крови! Мы делаем жилье просторным и функциональным.', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_typebuild`
--

CREATE TABLE IF NOT EXISTS `contents_typebuild` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `contents_typebuild`
--

INSERT INTO `contents_typebuild` (`id`, `name`, `sort`) VALUES
(1, 'Квартира', 0),
(2, 'Коттедж', 0),
(3, 'Помещение', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `contents_typerepair`
--

CREATE TABLE IF NOT EXISTS `contents_typerepair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `text` longtext,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `contents_typerepair`
--

INSERT INTO `contents_typerepair` (`id`, `name`, `price`, `text`, `sort`) VALUES
(1, 'Косметический ремонт', '3500.00', 'Тут будет небольшая текстовая подсказка,\r\nдля данного пункта.', 0),
(2, 'Капитальный ремонт', '10000.00', '', 0),
(3, 'Евро ремонт', '12000.00', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=196 ;

--
-- Дамп данных таблицы `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2016-02-17 13:03:59', '1', 'Главная', 1, 'Initial version.', 2, 1),
(2, '2016-02-17 13:24:03', '3', 'Наши работы', 1, 'Initial version.', 2, 1),
(3, '2016-02-17 13:24:16', '4', 'Наши мастера', 1, 'Initial version.', 2, 1),
(4, '2016-02-17 13:24:29', '5', 'Цены', 1, 'Initial version.', 2, 1),
(5, '2016-02-17 13:24:42', '6', 'Школа ремонта', 1, 'Initial version.', 2, 1),
(6, '2016-02-17 13:25:00', '7', 'О компании', 1, 'Initial version.', 2, 1),
(7, '2016-02-17 13:25:26', '8', 'Выполненные ремонты', 1, 'Initial version.', 2, 1),
(8, '2016-02-17 13:25:41', '9', 'Дизайн-проекты', 1, 'Initial version.', 2, 1),
(9, '2016-02-17 13:26:04', '10', 'Виртуальные 3D туры', 1, 'Initial version.', 2, 1),
(10, '2016-02-17 13:26:34', '1', 'Главная', 2, '', 2, 1),
(11, '2016-02-17 13:26:36', '3', 'Наши работы', 2, '', 2, 1),
(12, '2016-02-17 13:26:38', '8', 'Выполненные ремонты', 2, '', 2, 1),
(13, '2016-02-17 13:26:40', '9', 'Дизайн-проекты', 2, '', 2, 1),
(14, '2016-02-17 13:26:41', '4', 'Наши мастера', 2, '', 2, 1),
(15, '2016-02-17 13:26:43', '5', 'Цены', 2, '', 2, 1),
(16, '2016-02-17 13:26:44', '6', 'Школа ремонта', 2, '', 2, 1),
(17, '2016-02-17 13:26:47', '7', 'О компании', 2, '', 2, 1),
(18, '2016-02-17 13:26:48', '10', 'Виртуальные 3D туры', 2, '', 2, 1),
(19, '2016-02-17 14:16:14', '4', 'Наши мастера', 2, 'Изменен application_urls и xframe_options.', 2, 1),
(20, '2016-02-17 14:18:05', '4', 'Наши мастера', 2, '', 2, 1),
(21, '2016-02-18 05:53:53', '1', 'Николай', 1, 'Добавлено.', 33, 1),
(22, '2016-02-18 05:59:55', '1', 'Николай', 2, 'Изменен text.', 33, 1),
(23, '2016-02-18 06:00:28', '1', 'Николай', 2, 'Изменен image.', 33, 1),
(24, '2016-02-18 06:03:46', '2', 'Татьяна', 1, 'Добавлено.', 33, 1),
(25, '2016-02-18 06:04:09', '3', 'Василий', 1, 'Добавлено.', 33, 1),
(26, '2016-02-18 06:04:29', '4', 'Ирина', 1, 'Добавлено.', 33, 1),
(27, '2016-02-18 06:05:15', '3', 'Василий', 2, 'Изменен post.', 33, 1),
(28, '2016-02-18 06:13:10', '1', 'Николай', 2, 'Изменен image и image2.', 33, 1),
(29, '2016-02-18 06:13:30', '2', 'Татьяна', 2, 'Изменен image2 и text.', 33, 1),
(30, '2016-02-18 06:13:33', '3', 'Василий', 2, 'Изменен image2.', 33, 1),
(31, '2016-02-18 06:13:37', '4', 'Ирина', 2, 'Изменен image2.', 33, 1),
(32, '2016-02-18 07:15:41', '7', 'О компании', 2, '', 2, 1),
(33, '2016-02-18 07:47:24', '19', 'Наши партнеры', 1, 'Initial version.', 2, 1),
(34, '2016-02-18 07:47:44', '19', 'Наши партнеры', 2, 'Изменен application_urls и xframe_options.', 2, 1),
(35, '2016-02-18 07:47:55', '19', 'Наши партнеры', 2, '', 2, 1),
(36, '2016-02-18 07:49:04', '1', 'web2people', 1, 'Добавлено.', 35, 1),
(37, '2016-02-18 07:50:54', '2', 'QXSolutions', 1, 'Добавлено.', 35, 1),
(38, '2016-02-18 07:51:17', '3', 'Headex', 1, 'Добавлено.', 35, 1),
(39, '2016-02-18 07:51:35', '4', 'bee.by', 1, 'Добавлено.', 35, 1),
(40, '2016-02-18 07:52:00', '5', 'Pomidor', 1, 'Добавлено.', 35, 1),
(41, '2016-02-18 08:21:45', '21', 'Контакты', 1, 'Initial version.', 2, 1),
(42, '2016-02-18 08:21:58', '21', 'Контакты', 2, '', 2, 1),
(43, '2016-02-18 08:23:40', '21', 'Контакты', 2, '', 2, 1),
(44, '2016-02-18 08:47:22', '1', 'Демонтажные работы пол', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета". Добавлен Цена "Снятие паркетной доски, ламината". Добавлен Цена "Демонтаж линолеума, ковролина (за слой)". Добавлен Цена "Демонтаж фанеры (оргалита)". Добавлен Цена "Демонтаж деревянных/пластиковых плинтусов (без сохранения)". Добавлен Цена "Демонтаж деревянных полов". Добавлен Цена "Демонтаж керамической плитки с пола". Добавлен Цена "Демонтаж керамических плинтусов (без сохранения)". Добавлен Цена "Демонтаж напольных керамогранитных плит". Добавлен Цена "Удаление бетонной стяжки до 30 мм". Добавлен Цена "Удаление бетонной стяжки до 50 мм". Добавлен Цена "Удаление бетонной стяжки до 60 мм". Добавлен Цена "даление бетонной стяжки до 100 мм".', 36, 1),
(45, '2016-02-18 08:49:38', '1', 'Демонтажные работы пол', 2, 'Изменены active для Цена "Демонтаж паркета". Изменены active для Цена "Снятие паркетной доски, ламината". Изменены active для Цена "Демонтаж линолеума, ковролина (за слой)". Изменены active для Цена "Демонтаж фанеры (оргалита)". Изменены active для Цена "Демонтаж деревянных/пластиковых плинтусов (без сохранения)". Изменены active для Цена "Демонтаж деревянных полов". Изменены active для Цена "Демонтаж керамической плитки с пола". Изменены active для Цена "Демонтаж керамических плинтусов (без сохранения)". Изменены active для Цена "Демонтаж напольных керамогранитных плит". Изменены active для Цена "Удаление бетонной стяжки до 30 мм". Изменены active для Цена "Удаление бетонной стяжки до 50 мм". Изменены active для Цена "Удаление бетонной стяжки до 60 мм". Изменены active для Цена "даление бетонной стяжки до 100 мм".', 36, 1),
(46, '2016-02-18 08:50:12', '2', 'Демонтажные работы стены', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(47, '2016-02-18 08:50:25', '3', 'Демонтажные работы потолок', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(48, '2016-02-18 08:50:38', '4', 'Демонтажные работы сантехника', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(49, '2016-02-18 08:50:55', '5', 'Демонтажные работы электрика', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(50, '2016-02-18 08:51:08', '6', 'Демонтажные работы двери и окна', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(51, '2016-02-18 08:51:20', '7', 'Электромонтажные работы', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(52, '2016-02-18 08:51:32', '8', 'Сантехнические работы', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(53, '2016-02-18 08:51:46', '9', 'Работы с полом', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(54, '2016-02-18 08:51:59', '10', 'Работы с потолком', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(55, '2016-02-18 08:52:13', '11', 'Работы со стенами', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(56, '2016-02-18 08:52:25', '12', 'Дополнительные работы', 1, 'Добавлено. Добавлен Цена "Демонтаж паркета".', 36, 1),
(57, '2016-02-18 09:04:55', '5', 'Цены', 2, 'Изменен application_urls и xframe_options.', 2, 1),
(58, '2016-02-18 09:05:04', '5', 'Цены', 2, '', 2, 1),
(59, '2016-02-18 09:08:19', '5', 'Цены', 2, '', 2, 1),
(60, '2016-02-18 11:29:45', '3', 'Наши работы', 2, 'Изменен redirect и xframe_options.', 2, 1),
(61, '2016-02-18 11:29:58', '3', 'Наши работы', 2, '', 2, 1),
(62, '2016-02-18 11:40:18', '8', 'Выполненные ремонты', 2, 'Изменен application_urls и xframe_options.', 2, 1),
(63, '2016-02-18 11:40:43', '9', 'Дизайн-проекты', 2, 'Изменен application_urls и xframe_options.', 2, 1),
(64, '2016-02-18 11:41:06', '10', 'Виртуальные 3D туры', 2, 'Изменен application_urls и xframe_options.', 2, 1),
(65, '2016-02-18 11:41:48', '8', 'Выполненные ремонты', 2, '', 2, 1),
(66, '2016-02-18 11:41:49', '9', 'Дизайн-проекты', 2, '', 2, 1),
(67, '2016-02-18 11:41:50', '10', 'Виртуальные 3D туры', 2, '', 2, 1),
(68, '2016-02-18 12:02:21', '1', '3D тур по таунхаусу к/п Бристоль', 1, 'Добавлено.', 38, 1),
(69, '2016-02-24 07:27:00', '2', 'Ремонт квартир и офисов: 3-х комнатная квартира, в Москве на ул. Волжский бульвар', 1, 'Добавлено.', 38, 1),
(70, '2016-02-24 07:27:19', '3', 'Ремонт квартир и офисов: Коттедж в г. Талдом, Московская обл', 1, 'Добавлено.', 38, 1),
(71, '2016-02-24 07:28:26', '4', 'Ремонт квартир и офисов: Квартира в Москве в Костянском переулке', 1, 'Добавлено.', 38, 1),
(72, '2016-02-24 07:28:47', '5', 'Ремонт квартир и офисов: Однокомнатная квартира в Москве на пр. Вернадского', 1, 'Добавлено.', 38, 1),
(73, '2016-02-24 07:29:15', '6', 'Ремонт квартир и офисов: Квартира в Москве на ул. Паустовского', 1, 'Добавлено.', 38, 1),
(74, '2016-02-24 08:31:12', '1', 'Маргарита Репина', 1, 'Добавлено. Добавлен Изображение "Изображение 1". Добавлен Изображение "Изображение 2". Добавлен Изображение "Изображение 3".', 39, 1),
(75, '2016-02-24 08:31:46', '2', 'Татьяна Иванова', 1, 'Добавлено.', 39, 1),
(76, '2016-02-24 09:03:39', '10', 'Виртуальные 3D туры', 2, '', 2, 1),
(77, '2016-02-24 09:19:28', '23', 'Отзывы', 1, 'Initial version.', 2, 1),
(78, '2016-02-24 09:20:25', '23', 'Отзывы', 2, 'Изменен application_urls и xframe_options.', 2, 1),
(79, '2016-02-24 09:20:42', '23', 'Отзывы', 2, '', 2, 1),
(80, '2016-02-24 09:29:50', '1', 'Дизайн проект в стиле модерн 1', 1, 'Добавлено. Добавлен Изображение "Изображение 1". Добавлен Изображение "Изображение 2". Добавлен Изображение "Изображение 3".', 40, 1),
(81, '2016-02-24 10:10:56', '1', '3D тур по таунхаусу к/п Бристоль', 2, 'Изменен review.', 38, 1),
(82, '2016-02-24 11:35:45', '1', 'Дизайн проект в стиле модерн 1', 2, 'Добавлен Изображение "Изображение 4". Добавлен Изображение "Изображение 5".', 40, 1),
(83, '2016-02-24 11:40:33', '9', 'Дизайн-проекты', 2, '', 2, 1),
(84, '2016-02-24 11:45:40', '1', 'Ремонт 3-х комнатной квартиры, ул. Волжский бульвар, Москва', 1, 'Добавлено. Добавлен Изображение "Изображение 1". Добавлен Изображение "Изображение 2". Добавлен Изображение "Изображение 3". Добавлен Изображение "Изображение 4".', 44, 1),
(85, '2016-02-24 12:03:36', '8', 'Выполненные ремонты', 2, '', 2, 1),
(86, '2016-02-24 14:03:33', '1', 'Ремонт 3-х комнатной квартиры, ул. Волжский бульвар, Москва', 2, 'Ни одно поле не изменено.', 44, 1),
(87, '2016-02-29 07:38:23', '1', 'В санузел из гостинной?', 1, 'Добавлено. Добавлен Изображение "Изображение 1". Добавлен Изображение "Изображение 2". Добавлен Изображение "Изображение 3".', 42, 1),
(88, '2016-02-29 07:38:45', '2', 'Особенность раздвижных систем', 1, 'Добавлено. Добавлен Изображение "Изображение 4". Добавлен Изображение "Изображение 5". Добавлен Изображение "Изображение 6".', 42, 1),
(89, '2016-02-29 07:39:08', '3', 'Максимально увеличиваем кухонный фронт', 1, 'Добавлено. Добавлен Изображение "Изображение 7". Добавлен Изображение "Изображение 8". Добавлен Изображение "Изображение 9". Добавлен Изображение "Изображение 10".', 42, 1),
(90, '2016-02-29 07:39:34', '4', 'Визуально увеличиваем пространство', 1, 'Добавлено. Добавлен Изображение "Изображение 11". Добавлен Изображение "Изображение 12". Добавлен Изображение "Изображение 13". Добавлен Изображение "Изображение 14". Добавлен Изображение "Изображение 15".', 42, 1),
(91, '2016-02-29 07:39:50', '5', '5 лет гарантии', 1, 'Добавлено. Добавлен Изображение "Изображение 16". Добавлен Изображение "Изображение 17". Добавлен Изображение "Изображение 18".', 42, 1),
(92, '2016-02-29 07:40:56', '1', 'Ремонт 3-х комнатной квартиры, ул. Волжский бульвар, Москва', 2, 'Изменен secrets.', 44, 1),
(93, '2016-02-29 08:59:41', '1', '5 лет гарантии', 1, 'Добавлено.', 48, 1),
(94, '2016-02-29 08:59:55', '2', 'Оплата материалов по факту', 1, 'Добавлено.', 48, 1),
(95, '2016-02-29 09:00:11', '3', 'Демонстрация объектов', 1, 'Добавлено.', 48, 1),
(96, '2016-02-29 09:00:25', '4', 'Работаем 6 дней в неделю', 1, 'Добавлено.', 48, 1),
(97, '2016-02-29 09:00:41', '5', 'Гарантия цены по договору', 1, 'Добавлено.', 48, 1),
(98, '2016-02-29 09:00:54', '6', 'Двойной контроль качества', 1, 'Добавлено.', 48, 1),
(99, '2016-02-29 09:01:08', '7', 'Оплата работ по факту', 1, 'Добавлено.', 48, 1),
(100, '2016-02-29 09:01:34', '8', 'Заказчики довольны', 1, 'Добавлено.', 48, 1),
(101, '2016-02-29 09:52:09', '1', 'Квартира', 1, 'Добавлено.', 50, 1),
(102, '2016-02-29 09:52:20', '2', 'Коттедж', 1, 'Добавлено.', 50, 1),
(103, '2016-02-29 09:52:48', '3', 'Помещение', 1, 'Добавлено.', 50, 1),
(104, '2016-02-29 09:57:21', '1', 'Косметический  ремонт', 1, 'Добавлено.', 51, 1),
(105, '2016-02-29 09:57:35', '2', 'Капитальный  ремонт', 1, 'Добавлено.', 51, 1),
(106, '2016-02-29 09:57:47', '3', 'Евро  ремонт', 1, 'Добавлено.', 51, 1),
(107, '2016-02-29 09:58:19', '1', 'Обмеры помещений', 1, 'Добавлено.', 52, 1),
(108, '2016-02-29 09:58:28', '2', 'Варианты планировочного решения', 1, 'Добавлено.', 52, 1),
(109, '2016-02-29 09:58:36', '3', 'Эскизы', 1, 'Добавлено.', 52, 1),
(110, '2016-02-29 09:58:44', '4', 'План возводимых перегородок', 1, 'Добавлено.', 52, 1),
(111, '2016-02-29 09:59:05', '5', 'Схемы потолков и электрики', 1, 'Добавлено.', 52, 1),
(112, '2016-02-29 10:00:12', '6', '3D Max визуализация', 1, 'Добавлено.', 52, 1),
(113, '2016-02-29 10:00:20', '7', 'Полный комплект рабочих чертежей', 1, 'Добавлено.', 52, 1),
(114, '2016-02-29 10:00:29', '8', 'Развертки стен с прорисовкой декоративных элементов', 1, 'Добавлено.', 52, 1),
(115, '2016-02-29 10:00:37', '9', 'Подбор материалов ванных комнат, спецификация', 1, 'Добавлено.', 52, 1),
(116, '2016-02-29 10:00:45', '10', 'Подбор отделочных материалов', 1, 'Добавлено.', 52, 1),
(117, '2016-02-29 10:00:55', '11', 'Подбор мебели, текстиля и элементов декора', 1, 'Добавлено.', 52, 1),
(118, '2016-02-29 10:23:49', '1', 'Базовый', 1, 'Добавлено.', 53, 1),
(119, '2016-02-29 10:24:04', '2', 'Оптимальный', 1, 'Добавлено.', 53, 1),
(120, '2016-02-29 10:24:16', '3', 'Премиальный', 1, 'Добавлено.', 53, 1),
(121, '2016-02-29 11:09:14', '25', 'Ремонт квартир', 1, 'Initial version.', 2, 1),
(122, '2016-02-29 11:09:39', '26', 'Ремонт коттеджей и таунхаусов', 1, 'Initial version.', 2, 1),
(123, '2016-02-29 11:09:59', '27', 'Дизайн проекты', 1, 'Initial version.', 2, 1),
(124, '2016-02-29 11:10:19', '28', 'Декор дома', 1, 'Initial version.', 2, 1),
(125, '2016-02-29 11:10:41', '25', 'Ремонт квартир', 2, '', 2, 1),
(126, '2016-02-29 11:10:42', '26', 'Ремонт коттеджей и таунхаусов', 2, '', 2, 1),
(127, '2016-02-29 11:10:44', '27', 'Дизайн проекты', 2, '', 2, 1),
(128, '2016-02-29 11:10:45', '28', 'Декор дома', 2, '', 2, 1),
(129, '2016-02-29 11:15:18', '1', 'Главная', 2, '', 2, 1),
(130, '2016-02-29 11:15:52', '1', 'Главная', 2, '', 2, 1),
(131, '2016-02-29 11:15:56', '1', 'Главная', 2, '', 2, 1),
(132, '2016-02-29 11:15:59', '1', 'Главная', 2, '', 2, 1),
(133, '2016-02-29 11:30:26', '1', 'Что влияет на  стоимость?', 1, 'Добавлено.', 58, 1),
(134, '2016-02-29 11:30:38', '2', '7 советов на чем  НЕЛЬЗЯ экономить!', 1, 'Добавлено.', 58, 1),
(135, '2016-02-29 11:38:07', '25', 'Ремонт квартир', 2, '', 2, 1),
(136, '2016-02-29 11:39:28', '1', 'clipboard', 3, '', 1, 1),
(137, '2016-02-29 11:42:34', '2', 'Ремонт таунхауса к/п Бристоль', 1, 'Добавлено. Добавлен Изображение "Изображение 5". Добавлен Изображение "Изображение 6". Добавлен Изображение "Изображение 7".', 44, 1),
(138, '2016-02-29 11:43:27', '26', 'Ремонт коттеджей и таунхаусов', 2, '', 2, 1),
(139, '2016-02-29 11:46:41', '2', 'Дизайн проект в стиле модерн 2', 1, 'Добавлено. Добавлен Изображение "Изображение 6". Добавлен Изображение "Изображение 7". Добавлен Изображение "Изображение 8".', 40, 1),
(140, '2016-02-29 11:47:01', '3', 'Дизайн проект в стиле модерн 3', 1, 'Добавлено. Добавлен Изображение "Изображение 9". Добавлен Изображение "Изображение 10". Добавлен Изображение "Изображение 11".', 40, 1),
(141, '2016-02-29 11:57:50', '79', '79', 3, '', 16, 1),
(142, '2016-02-29 12:12:16', '9', 'Индивидуальное проектирование', 1, 'Добавлено.', 48, 1),
(143, '2016-02-29 12:12:33', '10', 'Увеличение площади (фактическое или визуальное)', 1, 'Добавлено.', 48, 1),
(144, '2016-02-29 12:13:18', '8', 'Заказчики довольны', 2, 'Изменен sort.', 48, 1),
(145, '2016-02-29 12:13:18', '7', 'Оплата работ по факту', 2, 'Изменен sort.', 48, 1),
(146, '2016-02-29 12:13:18', '6', 'Двойной контроль качества', 2, 'Изменен sort.', 48, 1),
(147, '2016-02-29 12:13:18', '5', 'Гарантия цены по договору', 2, 'Изменен sort.', 48, 1),
(148, '2016-02-29 12:13:18', '4', 'Работаем 6 дней в неделю', 2, 'Изменен sort.', 48, 1),
(149, '2016-02-29 12:13:18', '3', 'Демонстрация объектов', 2, 'Изменен sort.', 48, 1),
(150, '2016-02-29 12:13:18', '2', 'Оплата материалов по факту', 2, 'Изменен sort.', 48, 1),
(151, '2016-02-29 12:13:18', '1', '5 лет гарантии', 2, 'Изменен sort.', 48, 1),
(152, '2016-02-29 12:13:31', '10', 'Увеличение площади (фактическое или визуальное)', 2, 'Изменен sort.', 48, 1),
(153, '2016-02-29 12:13:31', '9', 'Индивидуальное проектирование', 2, 'Изменен sort.', 48, 1),
(154, '2016-02-29 12:16:51', '27', 'Дизайн проекты', 2, '', 2, 1),
(155, '2016-02-29 12:23:48', '33', 'Вопросы и ответы', 1, 'Initial version.', 2, 1),
(156, '2016-02-29 12:24:11', '33', 'Вопросы и ответы', 2, 'Изменен application_urls и xframe_options.', 2, 1),
(157, '2016-02-29 12:24:22', '28', 'Декор дома', 2, '', 2, 1),
(158, '2016-02-29 12:24:26', '33', 'Вопросы и ответы', 2, '', 2, 1),
(159, '2016-02-29 12:25:24', '1', 'Из чего складывается цена ремонта?', 1, 'Добавлено.', 60, 1),
(160, '2016-02-29 12:25:35', '2', 'Какова стоимость 1 кв.м.?', 1, 'Добавлено.', 60, 1),
(161, '2016-02-29 12:25:48', '3', 'Какая система оплаты в Вашей фирме?', 1, 'Добавлено.', 60, 1),
(162, '2016-02-29 12:26:00', '4', 'Кто работает в компании «Никстест-строй»?', 1, 'Добавлено.', 60, 1),
(163, '2016-02-29 12:26:13', '5', 'В какие сроки осуществляется ремонт?', 1, 'Добавлено.', 60, 1),
(164, '2016-02-29 12:26:39', '6', 'Из чего складывается цена ремонта?', 1, 'Добавлено.', 60, 1),
(165, '2016-02-29 12:27:07', '7', 'Какая система оплаты в Вашей фирме?', 1, 'Добавлено.', 60, 1),
(166, '2016-02-29 12:27:20', '8', 'Кто работает в компании «Никстест-строй»?', 1, 'Добавлено.', 60, 1),
(167, '2016-02-29 12:27:33', '9', 'В какие сроки осуществляется ремонт?', 1, 'Добавлено.', 60, 1),
(168, '2016-02-29 12:28:18', '2', 'Какова стоимость 1 кв.м.?', 2, 'Изменен answer.', 60, 1),
(169, '2016-02-29 12:33:44', '1', 'Главная', 2, '', 2, 1),
(170, '2016-02-29 13:29:45', '1', 'Косметический ремонт', 2, 'Изменен name.', 51, 1),
(171, '2016-02-29 13:30:08', '2', 'Капитальный ремонт', 2, 'Изменен name.', 51, 1),
(172, '2016-02-29 13:30:36', '3', 'Евро ремонт', 2, 'Изменен name.', 51, 1),
(173, '2016-03-01 10:47:56', '108', '108', 3, '', 16, 1),
(174, '2016-03-01 10:49:36', '1', 'Ремонт таунхауса <span>к/п Бристоль</span>', 1, 'Добавлено.', 62, 1),
(175, '2016-03-01 10:50:01', '2', 'Косметический ремонт', 1, 'Добавлено.', 62, 1),
(176, '2016-03-01 10:50:21', '3', 'Ремонт коттеджа', 1, 'Добавлено.', 62, 1),
(177, '2016-03-01 10:50:55', '1', 'Главная', 2, '', 2, 1),
(178, '2016-03-01 11:05:57', '21', 'Контакты', 2, 'Изменен slug.', 2, 1),
(179, '2016-03-01 11:06:58', '21', 'Контакты', 2, '', 2, 1),
(180, '2016-03-01 11:11:48', '1', 'Главная', 2, '', 2, 1),
(181, '2016-03-01 12:04:51', '25', 'Ремонт квартир', 2, '', 2, 1),
(182, '2016-03-01 12:08:28', '26', 'Ремонт коттеджей и таунхаусов', 2, '', 2, 1),
(183, '2016-03-02 05:40:35', '1', '3D тур по таунхаусу к/п Бристоль', 2, 'Изменен file.', 38, 1),
(184, '2016-03-02 06:49:41', '1', '3D тур по таунхаусу к/п Бристоль', 2, 'Ни одно поле не изменено.', 38, 1),
(185, '2016-03-02 06:50:00', '1', '3D тур по таунхаусу к/п Бристоль', 2, 'Изменен file.', 38, 1),
(186, '2016-03-02 07:40:14', '1', '3D тур по таунхаусу к/п Бристоль', 2, 'Ни одно поле не изменено.', 38, 1),
(187, '2016-03-02 07:41:04', '1', '3D тур по таунхаусу к/п Бристоль', 2, 'Ни одно поле не изменено.', 38, 1),
(188, '2016-03-02 08:04:14', '1', '3D тур Вернадка 2 вариант', 2, 'Изменен name.', 38, 1),
(189, '2016-03-02 08:07:15', '2', 'Ремонт квартир и офисов: 3-х комнатная квартира, в Москве на ул. Волжский бульвар', 2, 'Изменен file.', 38, 1),
(190, '2016-03-02 08:07:30', '2', 'Ремонт квартир и офисов: 3-х комнатная квартира, в Москве на ул. Волжский бульвар', 2, 'Ни одно поле не изменено.', 38, 1),
(191, '2016-03-02 08:09:03', '2', 'Тур ул. Паустовского', 2, 'Изменен name и text.', 38, 1),
(192, '2016-03-02 08:09:41', '3', 'Тур Чистые пруды', 2, 'Изменен name и file.', 38, 1),
(193, '2016-03-02 08:10:01', '3', 'Тур Чистые пруды', 2, 'Ни одно поле не изменено.', 38, 1),
(194, '2016-03-02 08:12:00', '4', 'Сухаревская 3D', 2, 'Изменен name и file.', 38, 1),
(195, '2016-03-02 08:12:18', '4', 'Сухаревская 3D', 2, 'Ни одно поле не изменено.', 38, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Дамп данных таблицы `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(3, 'admin', 'logentry'),
(5, 'auth', 'group'),
(4, 'auth', 'permission'),
(6, 'auth', 'user'),
(20, 'cms', 'aliaspluginmodel'),
(16, 'cms', 'cmsplugin'),
(12, 'cms', 'globalpagepermission'),
(2, 'cms', 'page'),
(13, 'cms', 'pagepermission'),
(14, 'cms', 'pageuser'),
(15, 'cms', 'pageusergroup'),
(1, 'cms', 'placeholder'),
(18, 'cms', 'placeholderreference'),
(19, 'cms', 'staticplaceholder'),
(17, 'cms', 'title'),
(21, 'cms', 'urlconfrevision'),
(11, 'cms', 'usersettings'),
(56, 'cms_plugins', 'basemenupl'),
(28, 'cms_plugins', 'basemodelpl'),
(25, 'cms_plugins', 'block'),
(31, 'cms_plugins', 'blockmodel'),
(54, 'cms_plugins', 'calc'),
(59, 'cms_plugins', 'designpluginmodel'),
(32, 'cms_plugins', 'feedback'),
(30, 'cms_plugins', 'htmlinsertmodel'),
(27, 'cms_plugins', 'keyword'),
(34, 'cms_plugins', 'personpluginmodel'),
(57, 'cms_plugins', 'projectpluginmodel'),
(46, 'cms_plugins', 'reviewpluginmodel'),
(26, 'cms_plugins', 'robot'),
(61, 'cms_plugins', 'sliderpluginmodel'),
(49, 'cms_plugins', 'teaserspluginmodel'),
(29, 'cms_plugins', 'textmodel'),
(55, 'cms_plugins', 'urlline'),
(36, 'contents', 'categoryprice'),
(41, 'contents', 'designimage'),
(40, 'contents', 'designproject'),
(52, 'contents', 'designservice'),
(35, 'contents', 'partners'),
(33, 'contents', 'person'),
(58, 'contents', 'popup'),
(37, 'contents', 'price'),
(44, 'contents', 'project'),
(45, 'contents', 'projectimage'),
(60, 'contents', 'question'),
(39, 'contents', 'review'),
(47, 'contents', 'reviewimage'),
(42, 'contents', 'secret'),
(43, 'contents', 'secretimage'),
(62, 'contents', 'slider'),
(53, 'contents', 'tariffdesign'),
(38, 'contents', 'tdtour'),
(48, 'contents', 'teaser'),
(50, 'contents', 'typebuild'),
(51, 'contents', 'typerepair'),
(7, 'contenttypes', 'contenttype'),
(22, 'menus', 'cachekey'),
(23, 'reversion', 'revision'),
(24, 'reversion', 'version'),
(8, 'sessions', 'session'),
(9, 'sites', 'site'),
(10, 'thumbnail', 'kvstore');

-- --------------------------------------------------------

--
-- Структура таблицы `django_migrations`
--

CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=78 ;

--
-- Дамп данных таблицы `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2016-02-17 13:01:05'),
(2, 'auth', '0001_initial', '2016-02-17 13:01:16'),
(3, 'admin', '0001_initial', '2016-02-17 13:01:17'),
(4, 'admin', '0002_logentry_remove_auto_add', '2016-02-17 13:01:17'),
(5, 'contenttypes', '0002_remove_content_type_name', '2016-02-17 13:01:17'),
(6, 'auth', '0002_alter_permission_name_max_length', '2016-02-17 13:01:17'),
(7, 'auth', '0003_alter_user_email_max_length', '2016-02-17 13:01:18'),
(8, 'auth', '0004_alter_user_username_opts', '2016-02-17 13:01:18'),
(9, 'auth', '0005_alter_user_last_login_null', '2016-02-17 13:01:18'),
(10, 'auth', '0006_require_contenttypes_0002', '2016-02-17 13:01:18'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2016-02-17 13:01:18'),
(12, 'sites', '0001_initial', '2016-02-17 13:01:19'),
(13, 'cms', '0001_initial', '2016-02-17 13:01:30'),
(14, 'cms', '0002_auto_20140816_1918', '2016-02-17 13:01:42'),
(15, 'cms', '0003_auto_20140926_2347', '2016-02-17 13:01:42'),
(16, 'cms', '0004_auto_20140924_1038', '2016-02-17 13:01:43'),
(17, 'cms', '0005_auto_20140924_1039', '2016-02-17 13:01:43'),
(18, 'cms', '0006_auto_20140924_1110', '2016-02-17 13:01:44'),
(19, 'cms', '0007_auto_20141028_1559', '2016-02-17 13:01:44'),
(20, 'cms', '0008_auto_20150208_2149', '2016-02-17 13:01:45'),
(21, 'cms', '0008_auto_20150121_0059', '2016-02-17 13:01:45'),
(22, 'cms', '0009_merge', '2016-02-17 13:01:45'),
(23, 'cms', '0010_migrate_use_structure', '2016-02-17 13:01:45'),
(24, 'cms', '0011_auto_20150419_1006', '2016-02-17 13:01:45'),
(25, 'cms', '0012_auto_20150607_2207', '2016-02-17 13:01:46'),
(26, 'cms', '0013_urlconfrevision', '2016-02-17 13:01:46'),
(27, 'menus', '0001_initial', '2016-02-17 13:01:47'),
(28, 'reversion', '0001_initial', '2016-02-17 13:01:51'),
(29, 'reversion', '0002_auto_20141216_1509', '2016-02-17 13:01:51'),
(30, 'sessions', '0001_initial', '2016-02-17 13:01:52'),
(31, 'sites', '0002_alter_domain_unique', '2016-02-17 13:01:52'),
(32, 'thumbnail', '0001_initial', '2016-02-17 13:01:54'),
(33, 'cms_plugins', '0001_initial', '2016-02-17 13:34:53'),
(34, 'admin', '0003_auto_20160217_1718', '2016-02-17 14:19:12'),
(35, 'auth', '0008_auto_20160217_1718', '2016-02-17 14:19:13'),
(36, 'contenttypes', '0003_auto_20160217_1718', '2016-02-17 14:19:13'),
(37, 'cms', '0014_auto_20160217_1718', '2016-02-17 14:19:17'),
(38, 'contents', '0001_initial', '2016-02-17 14:19:18'),
(39, 'reversion', '0003_auto_20160217_1718', '2016-02-17 14:19:18'),
(40, 'sessions', '0002_auto_20160217_1718', '2016-02-17 14:19:18'),
(41, 'sites', '0003_auto_20160217_1718', '2016-02-17 14:19:18'),
(42, 'contents', '0002_person_image2', '2016-02-18 06:12:23'),
(43, 'cms_plugins', '0002_auto_20160218_0939', '2016-02-18 06:40:00'),
(44, 'cms_plugins', '0003_personpluginmodel', '2016-02-18 07:10:16'),
(45, 'cms_plugins', '0004_auto_20160218_1014', '2016-02-18 07:14:58'),
(46, 'contents', '0003_partners', '2016-02-18 07:18:24'),
(47, 'contents', '0004_auto_20160218_1020', '2016-02-18 07:20:04'),
(48, 'contents', '0005_categoryprice_price', '2016-02-18 08:43:02'),
(49, 'contents', '0006_auto_20160218_1148', '2016-02-18 08:48:07'),
(50, 'contents', '0007_auto_20160218_1420', '2016-02-18 11:20:39'),
(51, 'cms_plugins', '0005_reviewpluginmodel', '2016-02-24 08:26:51'),
(52, 'contents', '0008_auto_20160224_1128', '2016-02-24 08:28:59'),
(53, 'cms_plugins', '0006_auto_20160224_1227', '2016-02-24 09:27:59'),
(54, 'contents', '0009_remove_designproject_file', '2016-02-24 09:27:59'),
(55, 'contents', '0010_auto_20160224_1229', '2016-02-24 09:29:40'),
(56, 'contents', '0011_tdtour_review', '2016-02-24 09:33:41'),
(57, 'contents', '0012_project_place', '2016-02-24 12:23:15'),
(58, 'contents', '0013_auto_20160224_1527', '2016-02-24 12:27:34'),
(59, 'contents', '0014_teaser', '2016-02-24 14:16:49'),
(60, 'cms_plugins', '0007_teaserspluginmodel', '2016-02-24 14:17:46'),
(61, 'contents', '0015_auto_20160229_1246', '2016-02-29 09:47:00'),
(62, 'contents', '0016_auto_20160229_1322', '2016-02-29 10:22:47'),
(63, 'cms_plugins', '0008_calc_urlline', '2016-02-29 10:28:21'),
(64, 'cms_plugins', '0009_calc_all_type', '2016-02-29 11:00:02'),
(65, 'cms_plugins', '0010_basemenupl', '2016-02-29 11:06:45'),
(66, 'cms_plugins', '0011_projectpluginmodel', '2016-02-29 11:20:54'),
(67, 'contents', '0017_popup', '2016-02-29 11:29:12'),
(68, 'cms_plugins', '0012_auto_20160229_1429', '2016-02-29 11:29:55'),
(69, 'cms_plugins', '0013_designpluginmodel', '2016-02-29 11:48:22'),
(70, 'contents', '0018_question', '2016-02-29 12:19:59'),
(71, 'contents', '0019_auto_20160229_1519', '2016-02-29 12:19:59'),
(72, 'contents', '0020_slider', '2016-03-01 10:47:25'),
(73, 'cms_plugins', '0014_sliderpluginmodel', '2016-03-01 10:47:27'),
(74, 'cms_plugins', '0015_remove_feedback_type_feed', '2016-03-01 11:27:41'),
(75, 'contents', '0021_auto_20160301_1638', '2016-03-01 13:38:15'),
(76, 'contents', '0022_tdtour_path', '2016-03-02 07:01:03'),
(77, 'contents', '0023_auto_20160302_1040', '2016-03-02 07:40:58');

-- --------------------------------------------------------

--
-- Структура таблицы `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('3f1l47a058cgz1ccxhp7gbmnpuguekzl', 'MTY4ZWUxYTY4NGQxNjY1MGQ5Mjc0ZGYzYzY5ZjEyNmM2NGRkNDE5OTp7ImNtc19hZG1pbl9zaXRlIjoxLCJfYXV0aF91c2VyX2lkIjoiMSIsImNtc190b29sYmFyX2Rpc2FibGVkIjpmYWxzZSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJjbXNfZWRpdCI6dHJ1ZSwiX2F1dGhfdXNlcl9oYXNoIjoiMWIwMTc2NWNkYzQyNjhlYzhhNDU1YzViODcwZjJlYjk3OGU1MWQ0NyJ9', '2016-03-03 07:52:06'),
('546aj2lhh623oeopewf4gravurmsui3y', 'ZmRiNWM1ZTFmMGM4Y2I5YWU2MTkyZTMzZGFmNzY1MmI5NmE0ZTE3NTp7ImNtc19hZG1pbl9zaXRlIjoxLCJjbXNfdG9vbGJhcl9kaXNhYmxlZCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJjbXNfZWRpdCI6ZmFsc2UsIl9hdXRoX3VzZXJfaGFzaCI6IjFiMDE3NjVjZGM0MjY4ZWM4YTQ1NWM1Yjg3MGYyZWI5NzhlNTFkNDcifQ==', '2016-03-16 08:13:05'),
('a153mankkjbmvrzwwx1btumceo0wj4oh', 'NjA5NTEwMWQxZDI4MDc0Zjc2NGNjYzA3OTNhODliNmFiYmZkY2IwZDp7ImNtc19hZG1pbl9zaXRlIjoxLCJjbXNfdG9vbGJhcl9kaXNhYmxlZCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJjbXNfZWRpdCI6dHJ1ZSwiX2F1dGhfdXNlcl9oYXNoIjoiMWIwMTc2NWNkYzQyNjhlYzhhNDU1YzViODcwZjJlYjk3OGU1MWQ0NyJ9', '2016-03-09 14:04:18'),
('h7uda7uw5tbt4hsw8esr9neceyozcqer', 'YmZlMjFhZTJkZDRjMTBmYzcxNGZkZDUzZTczYjcxOTMyN2VjMGQzZDp7ImNtc19hZG1pbl9zaXRlIjoxLCJfYXV0aF91c2VyX2lkIjoiMSIsImNtc190b29sYmFyX2Rpc2FibGVkIjpmYWxzZSwid2l6YXJkX3dpemFyZF9jcmVhdGVfdmlldyI6eyJzdGVwX2ZpbGVzIjp7fSwic3RlcCI6IjAiLCJleHRyYV9kYXRhIjp7fSwic3RlcF9kYXRhIjp7fX0sIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiY21zX2VkaXQiOmZhbHNlLCJfYXV0aF91c2VyX2hhc2giOiIxYjAxNzY1Y2RjNDI2OGVjOGE0NTVjNWI4NzBmMmViOTc4ZTUxZDQ3In0=', '2016-03-14 13:30:39'),
('uwghi2yadx02c93lkveuk376ab63h1rx', 'N2E5MGM5YWRlYTBjZTYwNjRlYmFkYzk4NWQwNWNhMTBiZDNiNjQ5Njp7ImNtc19hZG1pbl9zaXRlIjoxLCJfYXV0aF91c2VyX2lkIjoiMSIsImNtc190b29sYmFyX2Rpc2FibGVkIjpmYWxzZSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJjbXNfZWRpdCI6ZmFsc2UsIl9hdXRoX3VzZXJfaGFzaCI6IjFiMDE3NjVjZGM0MjY4ZWM4YTQ1NWM1Yjg3MGYyZWI5NzhlNTFkNDcifQ==', '2016-03-03 12:04:32'),
('ywrpwq7ntr8i4evxglgnpq6eyhf255xo', 'N2E5MGM5YWRlYTBjZTYwNjRlYmFkYzk4NWQwNWNhMTBiZDNiNjQ5Njp7ImNtc19hZG1pbl9zaXRlIjoxLCJfYXV0aF91c2VyX2lkIjoiMSIsImNtc190b29sYmFyX2Rpc2FibGVkIjpmYWxzZSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJjbXNfZWRpdCI6ZmFsc2UsIl9hdXRoX3VzZXJfaGFzaCI6IjFiMDE3NjVjZGM0MjY4ZWM4YTQ1NWM1Yjg3MGYyZWI5NzhlNTFkNDcifQ==', '2016-03-03 07:15:43');

-- --------------------------------------------------------

--
-- Структура таблицы `django_site`
--

CREATE TABLE IF NOT EXISTS `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(1, 'example.com', 'example.com');

-- --------------------------------------------------------

--
-- Структура таблицы `menus_cachekey`
--

CREATE TABLE IF NOT EXISTS `menus_cachekey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) NOT NULL,
  `site` int(10) unsigned NOT NULL,
  `key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=96 ;

--
-- Дамп данных таблицы `menus_cachekey`
--

INSERT INTO `menus_cachekey` (`id`, `language`, `site`, `key`) VALUES
(95, 'ru', 1, 'menu_cache_menu_nodes_ru_1_1_user');

-- --------------------------------------------------------

--
-- Структура таблицы `reversion_revision`
--

CREATE TABLE IF NOT EXISTS `reversion_revision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_slug` varchar(191) NOT NULL,
  `date_created` datetime NOT NULL,
  `comment` longtext NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reversion_revision_b16b0f06` (`manager_slug`),
  KEY `reversion_revision_c69e55a4` (`date_created`),
  KEY `reversion_revision_user_id_17095f45_fk_auth_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=152 ;

--
-- Дамп данных таблицы `reversion_revision`
--

INSERT INTO `reversion_revision` (`id`, `manager_slug`, `date_created`, `comment`, `user_id`) VALUES
(1, 'default', '2016-02-17 13:03:59', 'Initial version.', 1),
(2, 'default', '2016-02-17 13:24:03', 'Initial version.', 1),
(3, 'default', '2016-02-17 13:24:16', 'Initial version.', 1),
(4, 'default', '2016-02-17 13:24:29', 'Initial version.', 1),
(5, 'default', '2016-02-17 13:24:42', 'Initial version.', 1),
(6, 'default', '2016-02-17 13:25:00', 'Initial version.', 1),
(7, 'default', '2016-02-17 13:25:26', 'Initial version.', 1),
(8, 'default', '2016-02-17 13:25:41', 'Initial version.', 1),
(9, 'default', '2016-02-17 13:26:04', 'Initial version.', 1),
(10, 'default', '2016-02-17 13:26:34', 'Publish', 1),
(11, 'default', '2016-02-17 13:26:36', 'Publish', 1),
(12, 'default', '2016-02-17 13:26:38', 'Publish', 1),
(13, 'default', '2016-02-17 13:26:40', 'Publish', 1),
(14, 'default', '2016-02-17 13:26:41', 'Publish', 1),
(15, 'default', '2016-02-17 13:26:43', 'Publish', 1),
(16, 'default', '2016-02-17 13:26:44', 'Publish', 1),
(17, 'default', '2016-02-17 13:26:47', 'Publish', 1),
(18, 'default', '2016-02-17 13:26:48', 'Publish', 1),
(19, 'default', '2016-02-17 14:16:14', 'Изменен application_urls и xframe_options.', 1),
(20, 'default', '2016-02-17 14:18:05', 'Publish', 1),
(21, 'default', '2016-02-18 07:01:44', 'Текст плагин добавлен в center', 1),
(22, 'default', '2016-02-18 07:03:32', 'Текст плагин, редактируемый в 0 в center', 1),
(23, 'default', '2016-02-18 07:06:57', 'Html вставка плагин добавлен в center', 1),
(24, 'default', '2016-02-18 07:07:14', 'Html вставка плагин, редактируемый в 1 в center', 1),
(25, 'default', '2016-02-18 07:14:07', 'Команда плагин добавлен в center', 1),
(26, 'default', '2016-02-18 07:14:36', 'Команда плагин, редактируемый в 2 в center', 1),
(27, 'default', '2016-02-18 07:15:41', 'Publish', 1),
(28, 'default', '2016-02-18 07:47:24', 'Initial version.', 1),
(29, 'default', '2016-02-18 07:47:44', 'Изменен application_urls и xframe_options.', 1),
(30, 'default', '2016-02-18 07:47:55', 'Publish', 1),
(31, 'default', '2016-02-18 08:21:45', 'Initial version.', 1),
(32, 'default', '2016-02-18 08:21:58', 'Publish', 1),
(33, 'default', '2016-02-18 08:22:32', 'Html вставка плагин добавлен в center', 1),
(34, 'default', '2016-02-18 08:23:27', 'Html вставка плагин, редактируемый в 0 в center', 1),
(35, 'default', '2016-02-18 08:23:40', 'Publish', 1),
(36, 'default', '2016-02-18 09:04:55', 'Изменен application_urls и xframe_options.', 1),
(37, 'default', '2016-02-18 09:05:04', 'Publish', 1),
(38, 'default', '2016-02-18 09:08:19', 'Publish', 1),
(39, 'default', '2016-02-18 11:29:45', 'Изменен redirect и xframe_options.', 1),
(40, 'default', '2016-02-18 11:29:58', 'Publish', 1),
(41, 'default', '2016-02-18 11:40:18', 'Изменен application_urls и xframe_options.', 1),
(42, 'default', '2016-02-18 11:40:43', 'Изменен application_urls и xframe_options.', 1),
(43, 'default', '2016-02-18 11:41:06', 'Изменен application_urls и xframe_options.', 1),
(44, 'default', '2016-02-18 11:41:48', 'Publish', 1),
(45, 'default', '2016-02-18 11:41:49', 'Publish', 1),
(46, 'default', '2016-02-18 11:41:50', 'Publish', 1),
(47, 'default', '2016-02-24 08:32:47', 'Отзывы плагин добавлен в center', 1),
(48, 'default', '2016-02-24 08:33:10', 'Отзывы плагин, редактируемый в 0 в center', 1),
(49, 'default', '2016-02-24 09:03:04', 'Html вставка плагин добавлен в center', 1),
(50, 'default', '2016-02-24 09:03:19', 'Html вставка плагин, редактируемый в 1 в center', 1),
(51, 'default', '2016-02-24 09:03:39', 'Publish', 1),
(52, 'default', '2016-02-24 09:19:28', 'Initial version.', 1),
(53, 'default', '2016-02-24 09:20:26', 'Изменен application_urls и xframe_options.', 1),
(54, 'default', '2016-02-24 09:20:42', 'Publish', 1),
(55, 'default', '2016-02-24 11:36:55', 'Отзывы плагин добавлен в center', 1),
(56, 'default', '2016-02-24 11:37:20', 'Отзывы плагин, редактируемый в 0 в center', 1),
(57, 'default', '2016-02-24 11:37:49', 'Html вставка плагин добавлен в center', 1),
(58, 'default', '2016-02-24 11:38:04', 'Html вставка плагин, редактируемый в 1 в center', 1),
(59, 'default', '2016-02-24 11:40:33', 'Publish', 1),
(60, 'default', '2016-02-24 12:01:26', 'Отзывы плагин добавлен в center', 1),
(61, 'default', '2016-02-24 12:01:54', 'Отзывы плагин, редактируемый в 0 в center', 1),
(62, 'default', '2016-02-24 12:02:19', 'Html вставка плагин добавлен в center', 1),
(63, 'default', '2016-02-24 12:03:23', 'Html вставка плагин, редактируемый в 1 в center', 1),
(64, 'default', '2016-02-24 12:03:36', 'Publish', 1),
(65, 'default', '2016-02-29 11:09:14', 'Initial version.', 1),
(66, 'default', '2016-02-29 11:09:39', 'Initial version.', 1),
(67, 'default', '2016-02-29 11:10:00', 'Initial version.', 1),
(68, 'default', '2016-02-29 11:10:19', 'Initial version.', 1),
(69, 'default', '2016-02-29 11:10:41', 'Publish', 1),
(70, 'default', '2016-02-29 11:10:42', 'Publish', 1),
(71, 'default', '2016-02-29 11:10:44', 'Publish', 1),
(72, 'default', '2016-02-29 11:10:45', 'Publish', 1),
(75, 'default', '2016-02-29 11:13:07', 'Меню плагин добавлен в slider', 1),
(76, 'default', '2016-02-29 11:13:51', 'Меню плагин, редактируемый в 0 в slider', 1),
(78, 'default', '2016-02-29 11:15:18', 'Publish', 1),
(79, 'default', '2016-02-29 11:15:52', 'Publish', 1),
(80, 'default', '2016-02-29 11:15:56', 'Publish', 1),
(81, 'default', '2016-02-29 11:15:59', 'Publish', 1),
(84, 'default', '2016-02-29 11:22:39', 'Html вставка плагин, редактируемый в 1 в center', 1),
(85, 'default', '2016-02-29 11:22:56', 'Тизеры плагин добавлен в center', 1),
(86, 'default', '2016-02-29 11:23:13', 'Тизеры плагин, редактируемый в 2 в center', 1),
(87, 'default', '2016-02-29 11:30:42', 'Калькулятор плагин добавлен в center', 1),
(88, 'default', '2016-02-29 11:32:32', 'Калькулятор плагин, редактируемый в 3 в center', 1),
(89, 'default', '2016-02-29 11:35:21', 'Html вставка плагин добавлен в center', 1),
(90, 'default', '2016-02-29 11:35:33', 'Html вставка плагин, редактируемый в 4 в center', 1),
(91, 'default', '2016-02-29 11:36:35', 'Команда плагин добавлен в center', 1),
(92, 'default', '2016-02-29 11:36:49', 'Команда плагин, редактируемый в 5 в center', 1),
(93, 'default', '2016-02-29 11:37:03', 'Отзывы плагин добавлен в center', 1),
(94, 'default', '2016-02-29 11:37:20', 'Отзывы плагин, редактируемый в 6 в center', 1),
(95, 'default', '2016-02-29 11:37:35', 'Html вставка плагин добавлен в center', 1),
(96, 'default', '2016-02-29 11:37:51', 'Html вставка плагин, редактируемый в 7 в center', 1),
(97, 'default', '2016-02-29 11:38:07', 'Publish', 1),
(98, 'default', '2016-02-29 11:40:03', 'Скопированы плагины в center', 1),
(99, 'default', '2016-02-29 11:42:49', 'Проект плагин, редактируемый в 0 в center', 1),
(100, 'default', '2016-02-29 11:43:12', 'Калькулятор плагин, редактируемый в 3 в center', 1),
(101, 'default', '2016-02-29 11:43:27', 'Publish', 1),
(106, 'default', '2016-02-29 12:07:01', 'Калькулятор плагин, редактируемый в 1 в center', 1),
(107, 'default', '2016-02-29 12:07:33', 'Калькулятор плагин, редактируемый в 1 в center', 1),
(108, 'default', '2016-02-29 12:08:58', 'Калькулятор плагин, редактируемый в 1 в center', 1),
(109, 'default', '2016-02-29 12:11:32', 'Тизеры плагин добавлен в center', 1),
(110, 'default', '2016-02-29 12:12:38', 'Тизеры плагин, редактируемый в 2 в center', 1),
(111, 'default', '2016-02-29 12:13:49', 'Html вставка плагин добавлен в center', 1),
(112, 'default', '2016-02-29 12:14:15', 'Html вставка плагин, редактируемый в 3 в center', 1),
(113, 'default', '2016-02-29 12:15:02', 'Тизеры плагин, редактируемый в 2 в center', 1),
(114, 'default', '2016-02-29 12:15:16', 'Команда плагин добавлен в center', 1),
(115, 'default', '2016-02-29 12:15:29', 'Команда плагин, редактируемый в 4 в center', 1),
(116, 'default', '2016-02-29 12:15:45', 'Отзывы плагин добавлен в center', 1),
(117, 'default', '2016-02-29 12:15:57', 'Отзывы плагин, редактируемый в 5 в center', 1),
(118, 'default', '2016-02-29 12:16:15', 'Html вставка плагин добавлен в center', 1),
(119, 'default', '2016-02-29 12:16:26', 'Html вставка плагин, редактируемый в 6 в center', 1),
(120, 'default', '2016-02-29 12:16:51', 'Publish', 1),
(121, 'default', '2016-02-29 12:23:48', 'Initial version.', 1),
(122, 'default', '2016-02-29 12:24:11', 'Изменен application_urls и xframe_options.', 1),
(123, 'default', '2016-02-29 12:24:22', 'Publish', 1),
(124, 'default', '2016-02-29 12:24:26', 'Publish', 1),
(129, 'default', '2016-02-29 12:30:22', 'Калькулятор плагин добавлен в center', 1),
(130, 'default', '2016-02-29 12:30:57', 'Калькулятор плагин, редактируемый в 2 в center', 1),
(131, 'default', '2016-02-29 12:31:11', 'Html вставка плагин добавлен в center', 1),
(132, 'default', '2016-02-29 12:31:30', 'Html вставка плагин, редактируемый в 3 в center', 1),
(133, 'default', '2016-02-29 12:31:45', 'Команда плагин добавлен в center', 1),
(134, 'default', '2016-02-29 12:31:58', 'Команда плагин, редактируемый в 4 в center', 1),
(135, 'default', '2016-02-29 12:32:10', 'Отзывы плагин добавлен в center', 1),
(136, 'default', '2016-02-29 12:32:29', 'Отзывы плагин, редактируемый в 5 в center', 1),
(137, 'default', '2016-02-29 12:32:42', 'Html вставка плагин добавлен в center', 1),
(138, 'default', '2016-02-29 12:32:54', 'Html вставка плагин, редактируемый в 6 в center', 1),
(139, 'default', '2016-02-29 12:33:44', 'Publish', 1),
(140, 'default', '2016-03-01 10:47:44', 'Слайдер плагин добавлен в slider', 1),
(141, 'default', '2016-03-01 10:47:57', 'Слайдер плагин в 0 в slider удалён.', 1),
(142, 'default', '2016-03-01 10:50:27', 'Слайдер плагин добавлен в slider', 1),
(143, 'default', '2016-03-01 10:50:41', 'Слайдер плагин, редактируемый в 0 в slider', 1),
(144, 'default', '2016-03-01 10:50:55', 'Publish', 1),
(145, 'default', '2016-03-01 11:05:57', 'Изменен slug.', 1),
(146, 'default', '2016-03-01 11:06:58', 'Publish', 1),
(147, 'default', '2016-03-01 11:11:48', 'Publish', 1),
(148, 'default', '2016-03-01 12:03:38', 'Html вставка плагин, редактируемый в 4 в center', 1),
(149, 'default', '2016-03-01 12:04:51', 'Publish', 1),
(150, 'default', '2016-03-01 12:07:43', 'Html вставка плагин, редактируемый в 4 в center', 1),
(151, 'default', '2016-03-01 12:08:28', 'Publish', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `reversion_version`
--

CREATE TABLE IF NOT EXISTS `reversion_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` longtext NOT NULL,
  `object_id_int` int(11) DEFAULT NULL,
  `format` varchar(255) NOT NULL,
  `serialized_data` longtext NOT NULL,
  `object_repr` longtext NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `revision_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reversion_ver_content_type_id_7d0ff25c_fk_django_content_type_id` (`content_type_id`),
  KEY `reversion_version_revision_id_af9f6a9d_fk_reversion_revision_id` (`revision_id`),
  KEY `reversion_version_0c9ba3a3` (`object_id_int`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1282 ;

--
-- Дамп данных таблицы `reversion_version`
--

INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(1, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58.903Z", "changed_date": "2016-02-17T13:03:59.074Z", "publication_date": "2016-02-17T13:03:58.965Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Главная', 2, 1),
(2, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 1),
(3, '3', 3, 'json', '[{"model": "cms.title", "pk": 3, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-raboty", "path": "nashi-raboty", "has_url_overwrite": false, "redirect": null, "page": 3, "creation_date": "2016-02-17T13:24:03Z", "published": false}}]', 'Наши работы (nashi-raboty, ru)', 17, 2),
(4, '3', 3, 'json', '[{"model": "cms.page", "pk": 3, "fields": {"path": "0003", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:02.993Z", "changed_date": "2016-02-17T13:24:03.033Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Наши работы', 2, 2),
(5, '4', 4, 'json', '[{"model": "cms.title", "pk": 4, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u043c\\u0430\\u0441\\u0442\\u0435\\u0440\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-mastera", "path": "nashi-mastera", "has_url_overwrite": false, "redirect": null, "page": 4, "creation_date": "2016-02-17T13:24:16Z", "published": false}}]', 'Наши мастера (nashi-mastera, ru)', 17, 3),
(6, '4', 4, 'json', '[{"model": "cms.page", "pk": 4, "fields": {"path": "0004", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:16.257Z", "changed_date": "2016-02-17T13:24:16.296Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Наши мастера', 2, 3),
(7, '5', 5, 'json', '[{"model": "cms.page", "pk": 5, "fields": {"path": "0005", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:29.659Z", "changed_date": "2016-02-17T13:24:29.695Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Цены', 2, 4),
(8, '5', 5, 'json', '[{"model": "cms.title", "pk": 5, "fields": {"language": "ru", "title": "\\u0426\\u0435\\u043d\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "ceny", "path": "ceny", "has_url_overwrite": false, "redirect": null, "page": 5, "creation_date": "2016-02-17T13:24:29Z", "published": false}}]', 'Цены (ceny, ru)', 17, 4),
(9, '6', 6, 'json', '[{"model": "cms.title", "pk": 6, "fields": {"language": "ru", "title": "\\u0428\\u043a\\u043e\\u043b\\u0430 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "shkola-remonta", "path": "shkola-remonta", "has_url_overwrite": false, "redirect": null, "page": 6, "creation_date": "2016-02-17T13:24:42Z", "published": false}}]', 'Школа ремонта (shkola-remonta, ru)', 17, 5),
(10, '6', 6, 'json', '[{"model": "cms.page", "pk": 6, "fields": {"path": "0006", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:42.340Z", "changed_date": "2016-02-17T13:24:42.378Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Школа ремонта', 2, 5),
(11, '7', 7, 'json', '[{"model": "cms.title", "pk": 7, "fields": {"language": "ru", "title": "\\u041e \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438", "page_title": "", "menu_title": "", "meta_description": "", "slug": "o-kompanii", "path": "o-kompanii", "has_url_overwrite": false, "redirect": null, "page": 7, "creation_date": "2016-02-17T13:25:00Z", "published": false}}]', 'О компании (o-kompanii, ru)', 17, 6),
(12, '7', 7, 'json', '[{"model": "cms.page", "pk": 7, "fields": {"path": "0007", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:25:00.294Z", "changed_date": "2016-02-17T13:25:00.333Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'О компании', 2, 6),
(13, '8', 8, 'json', '[{"model": "cms.page", "pk": 8, "fields": {"path": "00030001", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:26.538Z", "changed_date": "2016-02-17T13:25:26.597Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": null, "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Выполненные ремонты', 2, 7),
(14, '8', 8, 'json', '[{"model": "cms.title", "pk": 8, "fields": {"language": "ru", "title": "\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "vypolnennye-remonty", "path": "nashi-raboty/vypolnennye-remonty", "has_url_overwrite": false, "redirect": null, "page": 8, "creation_date": "2016-02-17T13:25:26Z", "published": false}}]', 'Выполненные ремонты (vypolnennye-remonty, ru)', 17, 7),
(15, '9', 9, 'json', '[{"model": "cms.page", "pk": 9, "fields": {"path": "00030002", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:41.179Z", "changed_date": "2016-02-17T13:25:41.199Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": null, "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Дизайн-проекты', 2, 8),
(16, '9', 9, 'json', '[{"model": "cms.title", "pk": 9, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d-\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "nashi-raboty/dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 9, "creation_date": "2016-02-17T13:25:41Z", "published": false}}]', 'Дизайн-проекты (dizajn-proekty, ru)', 17, 8),
(17, '10', 10, 'json', '[{"model": "cms.page", "pk": 10, "fields": {"path": "0008", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:26:04.762Z", "changed_date": "2016-02-17T13:26:04.806Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Виртуальные 3D туры', 2, 9),
(18, '10', 10, 'json', '[{"model": "cms.title", "pk": 10, "fields": {"language": "ru", "title": "\\u0412\\u0438\\u0440\\u0442\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 3D \\u0442\\u0443\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "virtualnye-3d-tury", "path": "virtualnye-3d-tury", "has_url_overwrite": false, "redirect": null, "page": 10, "creation_date": "2016-02-17T13:26:04Z", "published": false}}]', 'Виртуальные 3D туры (virtualnye-3d-tury, ru)', 17, 9),
(19, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-17T13:26:34.904Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Главная', 2, 10),
(20, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 10),
(21, '3', 3, 'json', '[{"model": "cms.title", "pk": 3, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-raboty", "path": "nashi-raboty", "has_url_overwrite": false, "redirect": null, "page": 3, "creation_date": "2016-02-17T13:24:03Z", "published": true}}]', 'Наши работы (nashi-raboty, ru)', 17, 11),
(22, '3', 3, 'json', '[{"model": "cms.page", "pk": 3, "fields": {"path": "0003", "depth": 1, "numchild": 2, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:02Z", "changed_date": "2016-02-17T13:26:36.637Z", "publication_date": "2016-02-17T13:26:36.447Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Наши работы', 2, 11),
(23, '8', 8, 'json', '[{"model": "cms.page", "pk": 8, "fields": {"path": "00030001", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:26Z", "changed_date": "2016-02-17T13:26:38.309Z", "publication_date": "2016-02-17T13:26:38.212Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Выполненные ремонты', 2, 12),
(24, '8', 8, 'json', '[{"model": "cms.title", "pk": 8, "fields": {"language": "ru", "title": "\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "vypolnennye-remonty", "path": "nashi-raboty/vypolnennye-remonty", "has_url_overwrite": false, "redirect": null, "page": 8, "creation_date": "2016-02-17T13:25:26Z", "published": true}}]', 'Выполненные ремонты (vypolnennye-remonty, ru)', 17, 12),
(25, '9', 9, 'json', '[{"model": "cms.page", "pk": 9, "fields": {"path": "00030002", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:41Z", "changed_date": "2016-02-17T13:26:40.248Z", "publication_date": "2016-02-17T13:26:39.958Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Дизайн-проекты', 2, 13),
(26, '9', 9, 'json', '[{"model": "cms.title", "pk": 9, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d-\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "nashi-raboty/dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 9, "creation_date": "2016-02-17T13:25:41Z", "published": true}}]', 'Дизайн-проекты (dizajn-proekty, ru)', 17, 13),
(27, '4', 4, 'json', '[{"model": "cms.title", "pk": 4, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u043c\\u0430\\u0441\\u0442\\u0435\\u0440\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-mastera", "path": "nashi-mastera", "has_url_overwrite": false, "redirect": null, "page": 4, "creation_date": "2016-02-17T13:24:16Z", "published": true}}]', 'Наши мастера (nashi-mastera, ru)', 17, 14),
(28, '4', 4, 'json', '[{"model": "cms.page", "pk": 4, "fields": {"path": "0005", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:16Z", "changed_date": "2016-02-17T13:26:41.559Z", "publication_date": "2016-02-17T13:26:41.422Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Наши мастера', 2, 14),
(29, '5', 5, 'json', '[{"model": "cms.page", "pk": 5, "fields": {"path": "0007", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:29Z", "changed_date": "2016-02-17T13:26:43.176Z", "publication_date": "2016-02-17T13:26:43.022Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Цены', 2, 15),
(30, '5', 5, 'json', '[{"model": "cms.title", "pk": 5, "fields": {"language": "ru", "title": "\\u0426\\u0435\\u043d\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "ceny", "path": "ceny", "has_url_overwrite": false, "redirect": null, "page": 5, "creation_date": "2016-02-17T13:24:29Z", "published": true}}]', 'Цены (ceny, ru)', 17, 15),
(31, '6', 6, 'json', '[{"model": "cms.title", "pk": 6, "fields": {"language": "ru", "title": "\\u0428\\u043a\\u043e\\u043b\\u0430 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "shkola-remonta", "path": "shkola-remonta", "has_url_overwrite": false, "redirect": null, "page": 6, "creation_date": "2016-02-17T13:24:42Z", "published": true}}]', 'Школа ремонта (shkola-remonta, ru)', 17, 16),
(32, '6', 6, 'json', '[{"model": "cms.page", "pk": 6, "fields": {"path": "0009", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:42Z", "changed_date": "2016-02-17T13:26:44.769Z", "publication_date": "2016-02-17T13:26:44.547Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Школа ремонта', 2, 16),
(33, '7', 7, 'json', '[{"model": "cms.title", "pk": 7, "fields": {"language": "ru", "title": "\\u041e \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438", "page_title": "", "menu_title": "", "meta_description": "", "slug": "o-kompanii", "path": "o-kompanii", "has_url_overwrite": false, "redirect": null, "page": 7, "creation_date": "2016-02-17T13:25:00Z", "published": true}}]', 'О компании (o-kompanii, ru)', 17, 17),
(34, '7', 7, 'json', '[{"model": "cms.page", "pk": 7, "fields": {"path": "000B", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:25:00Z", "changed_date": "2016-02-17T13:26:47.243Z", "publication_date": "2016-02-17T13:26:47.081Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'О компании', 2, 17),
(35, '10', 10, 'json', '[{"model": "cms.page", "pk": 10, "fields": {"path": "000D", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:26:04Z", "changed_date": "2016-02-17T13:26:48.496Z", "publication_date": "2016-02-17T13:26:47.854Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": []}}]', 'Виртуальные 3D туры', 2, 18),
(36, '10', 10, 'json', '[{"model": "cms.title", "pk": 10, "fields": {"language": "ru", "title": "\\u0412\\u0438\\u0440\\u0442\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 3D \\u0442\\u0443\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "virtualnye-3d-tury", "path": "virtualnye-3d-tury", "has_url_overwrite": false, "redirect": null, "page": 10, "creation_date": "2016-02-17T13:26:04Z", "published": true}}]', 'Виртуальные 3D туры (virtualnye-3d-tury, ru)', 17, 18),
(37, '4', 4, 'json', '[{"model": "cms.page", "pk": 4, "fields": {"path": "0005", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:16Z", "changed_date": "2016-02-17T14:16:13.989Z", "publication_date": "2016-02-17T13:26:41Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "CommandApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [4, 5]}}]', 'Наши мастера', 2, 19),
(38, '4', 4, 'json', '[{"model": "cms.placeholder", "pk": 4, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 19),
(39, '5', 5, 'json', '[{"model": "cms.placeholder", "pk": 5, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 19),
(40, '4', 4, 'json', '[{"model": "cms.title", "pk": 4, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u043c\\u0430\\u0441\\u0442\\u0435\\u0440\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-mastera", "path": "nashi-mastera", "has_url_overwrite": false, "redirect": "", "page": 4, "creation_date": "2016-02-17T13:24:16Z", "published": true}}]', 'Наши мастера (nashi-mastera, ru)', 17, 19),
(41, '4', 4, 'json', '[{"model": "cms.page", "pk": 4, "fields": {"path": "0005", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:16Z", "changed_date": "2016-02-17T14:18:04.990Z", "publication_date": "2016-02-17T13:26:41Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "CommandApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [4, 5]}}]', 'Наши мастера', 2, 20),
(42, '4', 4, 'json', '[{"model": "cms.placeholder", "pk": 4, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 20),
(43, '5', 5, 'json', '[{"model": "cms.placeholder", "pk": 5, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 20),
(44, '4', 4, 'json', '[{"model": "cms.title", "pk": 4, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u043c\\u0430\\u0441\\u0442\\u0435\\u0440\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-mastera", "path": "nashi-mastera", "has_url_overwrite": false, "redirect": "", "page": 4, "creation_date": "2016-02-17T13:24:16Z", "published": true}}]', 'Наши мастера (nashi-mastera, ru)', 17, 20),
(45, '7', 7, 'json', '[{"model": "cms.title", "pk": 7, "fields": {"language": "ru", "title": "\\u041e \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438", "page_title": "", "menu_title": "", "meta_description": "", "slug": "o-kompanii", "path": "o-kompanii", "has_url_overwrite": false, "redirect": null, "page": 7, "creation_date": "2016-02-17T13:25:00Z", "published": true}}]', 'О компании (o-kompanii, ru)', 17, 21),
(46, '1', 1, 'json', '[{"model": "cms.cmsplugin", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 0, "language": "ru", "plugin_type": "TextPlugin", "creation_date": "2016-02-18T07:01:44Z", "changed_date": "2016-02-18T07:01:44Z"}}]', '1', 16, 21),
(47, '2', 2, 'json', '[{"model": "cms.placeholder", "pk": 2, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 21),
(48, '3', 3, 'json', '[{"model": "cms.placeholder", "pk": 3, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 21),
(49, '7', 7, 'json', '[{"model": "cms.page", "pk": 7, "fields": {"path": "000B", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:25:00Z", "changed_date": "2016-02-17T13:26:47Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [2, 3]}}]', 'О компании', 2, 21),
(50, '1', 1, 'json', '[{"model": "cms_plugins.textmodel", "pk": 1, "fields": {"text": "<p>\\u0422\\u0435\\u043a\\u0441\\u0442 \\u0434\\u043b\\u044f \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430: \\u041f\\u0440\\u0438 \\u0432\\u044b\\u0431\\u043e\\u0440\\u0435 \\u0431\\u0443\\u0445\\u0433\\u0430\\u043b\\u0442\\u0435\\u0440\\u0441\\u043a\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u043e\\u0447\\u0435\\u043d\\u044c \\u0432\\u0430\\u0436\\u043d\\u043e \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u0442\\u044c, \\u0441 \\u043a\\u0435\\u043c \\u0432\\u044b \\u0438\\u043c\\u0435\\u0435\\u0442\\u0435 \\u0434\\u0435\\u043b\\u043e. \\u041e\\u0447\\u0435\\u043d\\u044c \\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u0443\\u043c\\u0435\\u044e\\u0442 \\u00ab\\u043f\\u043e\\u0434\\u0430\\u0432\\u0430\\u0442\\u044c\\u00bb \\u0441\\u0435\\u0431\\u044f \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u043e, \\u043d\\u043e \\u043b\\u0438\\u0448\\u044c \\u043d\\u0435\\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0430\\u044e\\u0442 \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u044b\\u0435 \\u0441\\u043b\\u043e\\u0432\\u0430 \\u043a\\u043e\\u043d\\u043a\\u0440\\u0435\\u0442\\u043d\\u044b\\u043c\\u0438 \\u0446\\u0438\\u0444\\u0440\\u0430\\u043c\\u0438 \\u0438 \\u0444\\u0430\\u043a\\u0442\\u0430\\u043c\\u0438.</p><p>\\u041c\\u044b \\u043e\\u0447\\u0435\\u043d\\u044c \\u0445\\u043e\\u0442\\u0438\\u043c, \\u0447\\u0442\\u043e\\u0431\\u044b \\u0412\\u0430\\u0448\\u0435 \\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435 \\u0441 \\u043d\\u0430\\u043c\\u0438 \\u043d\\u0430\\u0447\\u0438\\u043d\\u0430\\u043b\\u043e\\u0441\\u044c \\u0441 \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f \\u0438 \\u0443\\u0432\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f. \\u0415\\u0441\\u043b\\u0438 \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u043e\\u0439 \\u043d\\u0438\\u0436\\u0435 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0438 \\u0412\\u0430\\u043c \\u0431\\u0443\\u0434\\u0435\\u0442 \\u043d\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0442\\u043e\\u0447\\u043d\\u043e \\u2013 \\u043c\\u044b \\u0441 \\u0440\\u0430\\u0434\\u043e\\u0441\\u0442\\u044c\\u044e \\u0440\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0435\\u043c \\u043e \\u0441\\u0435\\u0431\\u0435 \\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435.</p>"}}]', 'О компании', 29, 22),
(51, '2', 2, 'json', '[{"model": "cms.placeholder", "pk": 2, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 22),
(52, '3', 3, 'json', '[{"model": "cms.placeholder", "pk": 3, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 22),
(53, '1', 1, 'json', '[{"model": "cms.cmsplugin", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 0, "language": "ru", "plugin_type": "TextPlugin", "creation_date": "2016-02-18T07:01:44Z", "changed_date": "2016-02-18T07:03:32Z"}}]', '1', 16, 22),
(54, '7', 7, 'json', '[{"model": "cms.page", "pk": 7, "fields": {"path": "000B", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:25:00Z", "changed_date": "2016-02-18T07:01:44Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [2, 3]}}]', 'О компании', 2, 22),
(55, '7', 7, 'json', '[{"model": "cms.title", "pk": 7, "fields": {"language": "ru", "title": "\\u041e \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438", "page_title": "", "menu_title": "", "meta_description": "", "slug": "o-kompanii", "path": "o-kompanii", "has_url_overwrite": false, "redirect": null, "page": 7, "creation_date": "2016-02-17T13:25:00Z", "published": true}}]', 'О компании (o-kompanii, ru)', 17, 22),
(56, '1', 1, 'json', '[{"model": "cms_plugins.textmodel", "pk": 1, "fields": {"text": "<p>\\u0422\\u0435\\u043a\\u0441\\u0442 \\u0434\\u043b\\u044f \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430: \\u041f\\u0440\\u0438 \\u0432\\u044b\\u0431\\u043e\\u0440\\u0435 \\u0431\\u0443\\u0445\\u0433\\u0430\\u043b\\u0442\\u0435\\u0440\\u0441\\u043a\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u043e\\u0447\\u0435\\u043d\\u044c \\u0432\\u0430\\u0436\\u043d\\u043e \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u0442\\u044c, \\u0441 \\u043a\\u0435\\u043c \\u0432\\u044b \\u0438\\u043c\\u0435\\u0435\\u0442\\u0435 \\u0434\\u0435\\u043b\\u043e. \\u041e\\u0447\\u0435\\u043d\\u044c \\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u0443\\u043c\\u0435\\u044e\\u0442 \\u00ab\\u043f\\u043e\\u0434\\u0430\\u0432\\u0430\\u0442\\u044c\\u00bb \\u0441\\u0435\\u0431\\u044f \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u043e, \\u043d\\u043e \\u043b\\u0438\\u0448\\u044c \\u043d\\u0435\\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0430\\u044e\\u0442 \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u044b\\u0435 \\u0441\\u043b\\u043e\\u0432\\u0430 \\u043a\\u043e\\u043d\\u043a\\u0440\\u0435\\u0442\\u043d\\u044b\\u043c\\u0438 \\u0446\\u0438\\u0444\\u0440\\u0430\\u043c\\u0438 \\u0438 \\u0444\\u0430\\u043a\\u0442\\u0430\\u043c\\u0438.</p><p>\\u041c\\u044b \\u043e\\u0447\\u0435\\u043d\\u044c \\u0445\\u043e\\u0442\\u0438\\u043c, \\u0447\\u0442\\u043e\\u0431\\u044b \\u0412\\u0430\\u0448\\u0435 \\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435 \\u0441 \\u043d\\u0430\\u043c\\u0438 \\u043d\\u0430\\u0447\\u0438\\u043d\\u0430\\u043b\\u043e\\u0441\\u044c \\u0441 \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f \\u0438 \\u0443\\u0432\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f. \\u0415\\u0441\\u043b\\u0438 \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u043e\\u0439 \\u043d\\u0438\\u0436\\u0435 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0438 \\u0412\\u0430\\u043c \\u0431\\u0443\\u0434\\u0435\\u0442 \\u043d\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0442\\u043e\\u0447\\u043d\\u043e \\u2013 \\u043c\\u044b \\u0441 \\u0440\\u0430\\u0434\\u043e\\u0441\\u0442\\u044c\\u044e \\u0440\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0435\\u043c \\u043e \\u0441\\u0435\\u0431\\u0435 \\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435.</p>"}}]', 'О компании', 29, 23),
(57, '2', 2, 'json', '[{"model": "cms.placeholder", "pk": 2, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 23),
(58, '3', 3, 'json', '[{"model": "cms.placeholder", "pk": 3, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 23),
(59, '1', 1, 'json', '[{"model": "cms.cmsplugin", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 0, "language": "ru", "plugin_type": "TextPlugin", "creation_date": "2016-02-18T07:01:44Z", "changed_date": "2016-02-18T07:03:32Z"}}]', '1', 16, 23),
(60, '7', 7, 'json', '[{"model": "cms.page", "pk": 7, "fields": {"path": "000B", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:25:00Z", "changed_date": "2016-02-18T07:03:32Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [2, 3]}}]', 'О компании', 2, 23),
(61, '7', 7, 'json', '[{"model": "cms.title", "pk": 7, "fields": {"language": "ru", "title": "\\u041e \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438", "page_title": "", "menu_title": "", "meta_description": "", "slug": "o-kompanii", "path": "o-kompanii", "has_url_overwrite": false, "redirect": null, "page": 7, "creation_date": "2016-02-17T13:25:00Z", "published": true}}]', 'О компании (o-kompanii, ru)', 17, 23),
(62, '2', 2, 'json', '[{"model": "cms.cmsplugin", "pk": 2, "fields": {"path": "0002", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T07:06:57Z", "changed_date": "2016-02-18T07:06:57Z"}}]', '2', 16, 23),
(63, '1', 1, 'json', '[{"model": "cms_plugins.textmodel", "pk": 1, "fields": {"text": "<p>\\u0422\\u0435\\u043a\\u0441\\u0442 \\u0434\\u043b\\u044f \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430: \\u041f\\u0440\\u0438 \\u0432\\u044b\\u0431\\u043e\\u0440\\u0435 \\u0431\\u0443\\u0445\\u0433\\u0430\\u043b\\u0442\\u0435\\u0440\\u0441\\u043a\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u043e\\u0447\\u0435\\u043d\\u044c \\u0432\\u0430\\u0436\\u043d\\u043e \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u0442\\u044c, \\u0441 \\u043a\\u0435\\u043c \\u0432\\u044b \\u0438\\u043c\\u0435\\u0435\\u0442\\u0435 \\u0434\\u0435\\u043b\\u043e. \\u041e\\u0447\\u0435\\u043d\\u044c \\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u0443\\u043c\\u0435\\u044e\\u0442 \\u00ab\\u043f\\u043e\\u0434\\u0430\\u0432\\u0430\\u0442\\u044c\\u00bb \\u0441\\u0435\\u0431\\u044f \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u043e, \\u043d\\u043e \\u043b\\u0438\\u0448\\u044c \\u043d\\u0435\\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0430\\u044e\\u0442 \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u044b\\u0435 \\u0441\\u043b\\u043e\\u0432\\u0430 \\u043a\\u043e\\u043d\\u043a\\u0440\\u0435\\u0442\\u043d\\u044b\\u043c\\u0438 \\u0446\\u0438\\u0444\\u0440\\u0430\\u043c\\u0438 \\u0438 \\u0444\\u0430\\u043a\\u0442\\u0430\\u043c\\u0438.</p><p>\\u041c\\u044b \\u043e\\u0447\\u0435\\u043d\\u044c \\u0445\\u043e\\u0442\\u0438\\u043c, \\u0447\\u0442\\u043e\\u0431\\u044b \\u0412\\u0430\\u0448\\u0435 \\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435 \\u0441 \\u043d\\u0430\\u043c\\u0438 \\u043d\\u0430\\u0447\\u0438\\u043d\\u0430\\u043b\\u043e\\u0441\\u044c \\u0441 \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f \\u0438 \\u0443\\u0432\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f. \\u0415\\u0441\\u043b\\u0438 \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u043e\\u0439 \\u043d\\u0438\\u0436\\u0435 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0438 \\u0412\\u0430\\u043c \\u0431\\u0443\\u0434\\u0435\\u0442 \\u043d\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0442\\u043e\\u0447\\u043d\\u043e \\u2013 \\u043c\\u044b \\u0441 \\u0440\\u0430\\u0434\\u043e\\u0441\\u0442\\u044c\\u044e \\u0440\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0435\\u043c \\u043e \\u0441\\u0435\\u0431\\u0435 \\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435.</p>"}}]', 'О компании', 29, 24),
(64, '2', 2, 'json', '[{"model": "cms.placeholder", "pk": 2, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 24),
(65, '3', 3, 'json', '[{"model": "cms.placeholder", "pk": 3, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 24),
(66, '1', 1, 'json', '[{"model": "cms.cmsplugin", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 0, "language": "ru", "plugin_type": "TextPlugin", "creation_date": "2016-02-18T07:01:44Z", "changed_date": "2016-02-18T07:03:32Z"}}]', '1', 16, 24),
(67, '7', 7, 'json', '[{"model": "cms.page", "pk": 7, "fields": {"path": "000B", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:25:00Z", "changed_date": "2016-02-18T07:06:57Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [2, 3]}}]', 'О компании', 2, 24),
(68, '2', 2, 'json', '[{"model": "cms.cmsplugin", "pk": 2, "fields": {"path": "0002", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T07:06:57Z", "changed_date": "2016-02-18T07:07:14Z"}}]', '2', 16, 24),
(69, '2', 2, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 2, "fields": {"name": "\\u0426\\u0438\\u0444\\u0440\\u044b \\u0438 \\u0444\\u0430\\u043a\\u0442\\u044b", "html": "<div class=\\"about-company\\">\\r\\n        <div class=\\"container\\">\\r\\n             <div class=\\"blue-bg\\">\\r\\n                    <span class=\\"fw600\\">\\u0426\\u0438\\u0444\\u0440\\u044b \\u0438 \\u0444\\u0430\\u043a\\u0442\\u044b</span>\\r\\n\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">12+</span>\\r\\n                        <span class=\\"fw300\\">\\u043b\\u0435\\u0442 \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">19</span>\\r\\n                        <span class=\\"fw300\\">\\u043c\\u0430\\u0441\\u0442\\u0435\\u0440\\u043e\\u0432\\r\\n<br/>\\u0432\\u044b\\u0441\\u0448\\u0435\\u0439 \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u0438</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">89</span>\\r\\n                        <span class=\\"fw300\\">\\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445\\r\\n<br>\\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u043e\\u0432</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">5</span>\\r\\n                        <span class=\\"fw300\\">\\u043b\\u0435\\u0442 \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0438</span>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n        </div>\\r\\n\\r\\n            </div>"}}]', 'Цифры и факты', 30, 24),
(70, '7', 7, 'json', '[{"model": "cms.title", "pk": 7, "fields": {"language": "ru", "title": "\\u041e \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438", "page_title": "", "menu_title": "", "meta_description": "", "slug": "o-kompanii", "path": "o-kompanii", "has_url_overwrite": false, "redirect": null, "page": 7, "creation_date": "2016-02-17T13:25:00Z", "published": true}}]', 'О компании (o-kompanii, ru)', 17, 24),
(71, '1', 1, 'json', '[{"model": "cms_plugins.textmodel", "pk": 1, "fields": {"text": "<p>\\u0422\\u0435\\u043a\\u0441\\u0442 \\u0434\\u043b\\u044f \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430: \\u041f\\u0440\\u0438 \\u0432\\u044b\\u0431\\u043e\\u0440\\u0435 \\u0431\\u0443\\u0445\\u0433\\u0430\\u043b\\u0442\\u0435\\u0440\\u0441\\u043a\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u043e\\u0447\\u0435\\u043d\\u044c \\u0432\\u0430\\u0436\\u043d\\u043e \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u0442\\u044c, \\u0441 \\u043a\\u0435\\u043c \\u0432\\u044b \\u0438\\u043c\\u0435\\u0435\\u0442\\u0435 \\u0434\\u0435\\u043b\\u043e. \\u041e\\u0447\\u0435\\u043d\\u044c \\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u0443\\u043c\\u0435\\u044e\\u0442 \\u00ab\\u043f\\u043e\\u0434\\u0430\\u0432\\u0430\\u0442\\u044c\\u00bb \\u0441\\u0435\\u0431\\u044f \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u043e, \\u043d\\u043e \\u043b\\u0438\\u0448\\u044c \\u043d\\u0435\\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0430\\u044e\\u0442 \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u044b\\u0435 \\u0441\\u043b\\u043e\\u0432\\u0430 \\u043a\\u043e\\u043d\\u043a\\u0440\\u0435\\u0442\\u043d\\u044b\\u043c\\u0438 \\u0446\\u0438\\u0444\\u0440\\u0430\\u043c\\u0438 \\u0438 \\u0444\\u0430\\u043a\\u0442\\u0430\\u043c\\u0438.</p><p>\\u041c\\u044b \\u043e\\u0447\\u0435\\u043d\\u044c \\u0445\\u043e\\u0442\\u0438\\u043c, \\u0447\\u0442\\u043e\\u0431\\u044b \\u0412\\u0430\\u0448\\u0435 \\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435 \\u0441 \\u043d\\u0430\\u043c\\u0438 \\u043d\\u0430\\u0447\\u0438\\u043d\\u0430\\u043b\\u043e\\u0441\\u044c \\u0441 \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f \\u0438 \\u0443\\u0432\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f. \\u0415\\u0441\\u043b\\u0438 \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u043e\\u0439 \\u043d\\u0438\\u0436\\u0435 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0438 \\u0412\\u0430\\u043c \\u0431\\u0443\\u0434\\u0435\\u0442 \\u043d\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0442\\u043e\\u0447\\u043d\\u043e \\u2013 \\u043c\\u044b \\u0441 \\u0440\\u0430\\u0434\\u043e\\u0441\\u0442\\u044c\\u044e \\u0440\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0435\\u043c \\u043e \\u0441\\u0435\\u0431\\u0435 \\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435.</p>"}}]', 'О компании', 29, 25),
(72, '2', 2, 'json', '[{"model": "cms.placeholder", "pk": 2, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 25),
(73, '3', 3, 'json', '[{"model": "cms.placeholder", "pk": 3, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 25),
(74, '1', 1, 'json', '[{"model": "cms.cmsplugin", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 0, "language": "ru", "plugin_type": "TextPlugin", "creation_date": "2016-02-18T07:01:44Z", "changed_date": "2016-02-18T07:03:32Z"}}]', '1', 16, 25),
(75, '7', 7, 'json', '[{"model": "cms.page", "pk": 7, "fields": {"path": "000B", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:25:00Z", "changed_date": "2016-02-18T07:07:14Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [2, 3]}}]', 'О компании', 2, 25),
(76, '2', 2, 'json', '[{"model": "cms.cmsplugin", "pk": 2, "fields": {"path": "0002", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T07:06:57Z", "changed_date": "2016-02-18T07:07:14Z"}}]', '2', 16, 25),
(77, '2', 2, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 2, "fields": {"name": "\\u0426\\u0438\\u0444\\u0440\\u044b \\u0438 \\u0444\\u0430\\u043a\\u0442\\u044b", "html": "<div class=\\"about-company\\">\\r\\n        <div class=\\"container\\">\\r\\n             <div class=\\"blue-bg\\">\\r\\n                    <span class=\\"fw600\\">\\u0426\\u0438\\u0444\\u0440\\u044b \\u0438 \\u0444\\u0430\\u043a\\u0442\\u044b</span>\\r\\n\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">12+</span>\\r\\n                        <span class=\\"fw300\\">\\u043b\\u0435\\u0442 \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">19</span>\\r\\n                        <span class=\\"fw300\\">\\u043c\\u0430\\u0441\\u0442\\u0435\\u0440\\u043e\\u0432\\r\\n<br/>\\u0432\\u044b\\u0441\\u0448\\u0435\\u0439 \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u0438</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">89</span>\\r\\n                        <span class=\\"fw300\\">\\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445\\r\\n<br>\\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u043e\\u0432</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">5</span>\\r\\n                        <span class=\\"fw300\\">\\u043b\\u0435\\u0442 \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0438</span>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n        </div>\\r\\n\\r\\n            </div>"}}]', 'Цифры и факты', 30, 25),
(78, '3', 3, 'json', '[{"model": "cms.cmsplugin", "pk": 3, "fields": {"path": "0003", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 2, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-18T07:14:06Z", "changed_date": "2016-02-18T07:14:07Z"}}]', '3', 16, 25),
(79, '7', 7, 'json', '[{"model": "cms.title", "pk": 7, "fields": {"language": "ru", "title": "\\u041e \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438", "page_title": "", "menu_title": "", "meta_description": "", "slug": "o-kompanii", "path": "o-kompanii", "has_url_overwrite": false, "redirect": null, "page": 7, "creation_date": "2016-02-17T13:25:00Z", "published": true}}]', 'О компании (o-kompanii, ru)', 17, 25),
(80, '3', 3, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 3, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 26);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(81, '1', 1, 'json', '[{"model": "cms_plugins.textmodel", "pk": 1, "fields": {"text": "<p>\\u0422\\u0435\\u043a\\u0441\\u0442 \\u0434\\u043b\\u044f \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430: \\u041f\\u0440\\u0438 \\u0432\\u044b\\u0431\\u043e\\u0440\\u0435 \\u0431\\u0443\\u0445\\u0433\\u0430\\u043b\\u0442\\u0435\\u0440\\u0441\\u043a\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u043e\\u0447\\u0435\\u043d\\u044c \\u0432\\u0430\\u0436\\u043d\\u043e \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u0442\\u044c, \\u0441 \\u043a\\u0435\\u043c \\u0432\\u044b \\u0438\\u043c\\u0435\\u0435\\u0442\\u0435 \\u0434\\u0435\\u043b\\u043e. \\u041e\\u0447\\u0435\\u043d\\u044c \\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u0443\\u043c\\u0435\\u044e\\u0442 \\u00ab\\u043f\\u043e\\u0434\\u0430\\u0432\\u0430\\u0442\\u044c\\u00bb \\u0441\\u0435\\u0431\\u044f \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u043e, \\u043d\\u043e \\u043b\\u0438\\u0448\\u044c \\u043d\\u0435\\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0430\\u044e\\u0442 \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u044b\\u0435 \\u0441\\u043b\\u043e\\u0432\\u0430 \\u043a\\u043e\\u043d\\u043a\\u0440\\u0435\\u0442\\u043d\\u044b\\u043c\\u0438 \\u0446\\u0438\\u0444\\u0440\\u0430\\u043c\\u0438 \\u0438 \\u0444\\u0430\\u043a\\u0442\\u0430\\u043c\\u0438.</p><p>\\u041c\\u044b \\u043e\\u0447\\u0435\\u043d\\u044c \\u0445\\u043e\\u0442\\u0438\\u043c, \\u0447\\u0442\\u043e\\u0431\\u044b \\u0412\\u0430\\u0448\\u0435 \\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435 \\u0441 \\u043d\\u0430\\u043c\\u0438 \\u043d\\u0430\\u0447\\u0438\\u043d\\u0430\\u043b\\u043e\\u0441\\u044c \\u0441 \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f \\u0438 \\u0443\\u0432\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f. \\u0415\\u0441\\u043b\\u0438 \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u043e\\u0439 \\u043d\\u0438\\u0436\\u0435 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0438 \\u0412\\u0430\\u043c \\u0431\\u0443\\u0434\\u0435\\u0442 \\u043d\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0442\\u043e\\u0447\\u043d\\u043e \\u2013 \\u043c\\u044b \\u0441 \\u0440\\u0430\\u0434\\u043e\\u0441\\u0442\\u044c\\u044e \\u0440\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0435\\u043c \\u043e \\u0441\\u0435\\u0431\\u0435 \\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435.</p>"}}]', 'О компании', 29, 26),
(82, '2', 2, 'json', '[{"model": "cms.placeholder", "pk": 2, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 26),
(83, '3', 3, 'json', '[{"model": "cms.placeholder", "pk": 3, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 26),
(84, '1', 1, 'json', '[{"model": "cms.cmsplugin", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 0, "language": "ru", "plugin_type": "TextPlugin", "creation_date": "2016-02-18T07:01:44Z", "changed_date": "2016-02-18T07:03:32Z"}}]', '1', 16, 26),
(85, '7', 7, 'json', '[{"model": "cms.page", "pk": 7, "fields": {"path": "000B", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:25:00Z", "changed_date": "2016-02-18T07:14:07Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [2, 3]}}]', 'О компании', 2, 26),
(86, '2', 2, 'json', '[{"model": "cms.cmsplugin", "pk": 2, "fields": {"path": "0002", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T07:06:57Z", "changed_date": "2016-02-18T07:07:14Z"}}]', '2', 16, 26),
(87, '2', 2, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 2, "fields": {"name": "\\u0426\\u0438\\u0444\\u0440\\u044b \\u0438 \\u0444\\u0430\\u043a\\u0442\\u044b", "html": "<div class=\\"about-company\\">\\r\\n        <div class=\\"container\\">\\r\\n             <div class=\\"blue-bg\\">\\r\\n                    <span class=\\"fw600\\">\\u0426\\u0438\\u0444\\u0440\\u044b \\u0438 \\u0444\\u0430\\u043a\\u0442\\u044b</span>\\r\\n\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">12+</span>\\r\\n                        <span class=\\"fw300\\">\\u043b\\u0435\\u0442 \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">19</span>\\r\\n                        <span class=\\"fw300\\">\\u043c\\u0430\\u0441\\u0442\\u0435\\u0440\\u043e\\u0432\\r\\n<br/>\\u0432\\u044b\\u0441\\u0448\\u0435\\u0439 \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u0438</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">89</span>\\r\\n                        <span class=\\"fw300\\">\\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445\\r\\n<br>\\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u043e\\u0432</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">5</span>\\r\\n                        <span class=\\"fw300\\">\\u043b\\u0435\\u0442 \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0438</span>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n        </div>\\r\\n\\r\\n            </div>"}}]', 'Цифры и факты', 30, 26),
(88, '3', 3, 'json', '[{"model": "cms.cmsplugin", "pk": 3, "fields": {"path": "0003", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 2, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-18T07:14:06Z", "changed_date": "2016-02-18T07:14:36Z"}}]', '3', 16, 26),
(89, '7', 7, 'json', '[{"model": "cms.title", "pk": 7, "fields": {"language": "ru", "title": "\\u041e \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438", "page_title": "", "menu_title": "", "meta_description": "", "slug": "o-kompanii", "path": "o-kompanii", "has_url_overwrite": false, "redirect": null, "page": 7, "creation_date": "2016-02-17T13:25:00Z", "published": true}}]', 'О компании (o-kompanii, ru)', 17, 26),
(90, '3', 3, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 3, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 27),
(91, '1', 1, 'json', '[{"model": "cms_plugins.textmodel", "pk": 1, "fields": {"text": "<p>\\u0422\\u0435\\u043a\\u0441\\u0442 \\u0434\\u043b\\u044f \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430: \\u041f\\u0440\\u0438 \\u0432\\u044b\\u0431\\u043e\\u0440\\u0435 \\u0431\\u0443\\u0445\\u0433\\u0430\\u043b\\u0442\\u0435\\u0440\\u0441\\u043a\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\u043e\\u0447\\u0435\\u043d\\u044c \\u0432\\u0430\\u0436\\u043d\\u043e \\u043f\\u043e\\u043d\\u0438\\u043c\\u0430\\u0442\\u044c, \\u0441 \\u043a\\u0435\\u043c \\u0432\\u044b \\u0438\\u043c\\u0435\\u0435\\u0442\\u0435 \\u0434\\u0435\\u043b\\u043e. \\u041e\\u0447\\u0435\\u043d\\u044c \\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u0443\\u043c\\u0435\\u044e\\u0442 \\u00ab\\u043f\\u043e\\u0434\\u0430\\u0432\\u0430\\u0442\\u044c\\u00bb \\u0441\\u0435\\u0431\\u044f \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u043e, \\u043d\\u043e \\u043b\\u0438\\u0448\\u044c \\u043d\\u0435\\u043c\\u043d\\u043e\\u0433\\u0438\\u0435 \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0430\\u044e\\u0442 \\u043a\\u0440\\u0430\\u0441\\u0438\\u0432\\u044b\\u0435 \\u0441\\u043b\\u043e\\u0432\\u0430 \\u043a\\u043e\\u043d\\u043a\\u0440\\u0435\\u0442\\u043d\\u044b\\u043c\\u0438 \\u0446\\u0438\\u0444\\u0440\\u0430\\u043c\\u0438 \\u0438 \\u0444\\u0430\\u043a\\u0442\\u0430\\u043c\\u0438.</p><p>\\u041c\\u044b \\u043e\\u0447\\u0435\\u043d\\u044c \\u0445\\u043e\\u0442\\u0438\\u043c, \\u0447\\u0442\\u043e\\u0431\\u044b \\u0412\\u0430\\u0448\\u0435 \\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435 \\u0441 \\u043d\\u0430\\u043c\\u0438 \\u043d\\u0430\\u0447\\u0438\\u043d\\u0430\\u043b\\u043e\\u0441\\u044c \\u0441 \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f \\u0438 \\u0443\\u0432\\u0430\\u0436\\u0435\\u043d\\u0438\\u044f. \\u0415\\u0441\\u043b\\u0438 \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u043e\\u0439 \\u043d\\u0438\\u0436\\u0435 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u0438 \\u0412\\u0430\\u043c \\u0431\\u0443\\u0434\\u0435\\u0442 \\u043d\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0442\\u043e\\u0447\\u043d\\u043e \\u2013 \\u043c\\u044b \\u0441 \\u0440\\u0430\\u0434\\u043e\\u0441\\u0442\\u044c\\u044e \\u0440\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0435\\u043c \\u043e \\u0441\\u0435\\u0431\\u0435 \\u043f\\u043e\\u0434\\u0440\\u043e\\u0431\\u043d\\u0435\\u0435.</p>"}}]', 'О компании', 29, 27),
(92, '2', 2, 'json', '[{"model": "cms.placeholder", "pk": 2, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 27),
(93, '3', 3, 'json', '[{"model": "cms.placeholder", "pk": 3, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 27),
(94, '1', 1, 'json', '[{"model": "cms.cmsplugin", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 0, "language": "ru", "plugin_type": "TextPlugin", "creation_date": "2016-02-18T07:01:44Z", "changed_date": "2016-02-18T07:03:32Z"}}]', '1', 16, 27),
(95, '7', 7, 'json', '[{"model": "cms.page", "pk": 7, "fields": {"path": "000B", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:25:00Z", "changed_date": "2016-02-18T07:15:41.469Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [2, 3]}}]', 'О компании', 2, 27),
(96, '2', 2, 'json', '[{"model": "cms.cmsplugin", "pk": 2, "fields": {"path": "0002", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T07:06:57Z", "changed_date": "2016-02-18T07:07:14Z"}}]', '2', 16, 27),
(97, '2', 2, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 2, "fields": {"name": "\\u0426\\u0438\\u0444\\u0440\\u044b \\u0438 \\u0444\\u0430\\u043a\\u0442\\u044b", "html": "<div class=\\"about-company\\">\\r\\n        <div class=\\"container\\">\\r\\n             <div class=\\"blue-bg\\">\\r\\n                    <span class=\\"fw600\\">\\u0426\\u0438\\u0444\\u0440\\u044b \\u0438 \\u0444\\u0430\\u043a\\u0442\\u044b</span>\\r\\n\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">12+</span>\\r\\n                        <span class=\\"fw300\\">\\u043b\\u0435\\u0442 \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">19</span>\\r\\n                        <span class=\\"fw300\\">\\u043c\\u0430\\u0441\\u0442\\u0435\\u0440\\u043e\\u0432\\r\\n<br/>\\u0432\\u044b\\u0441\\u0448\\u0435\\u0439 \\u043a\\u0430\\u0442\\u0435\\u0433\\u043e\\u0440\\u0438\\u0438</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">89</span>\\r\\n                        <span class=\\"fw300\\">\\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0445\\r\\n<br>\\u043e\\u0431\\u044a\\u0435\\u043a\\u0442\\u043e\\u0432</span>\\r\\n                    </div>\\r\\n                    <div class=\\"block\\">\\r\\n                        <span class=\\"c-red\\">5</span>\\r\\n                        <span class=\\"fw300\\">\\u043b\\u0435\\u0442 \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0438</span>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n        </div>\\r\\n\\r\\n            </div>"}}]', 'Цифры и факты', 30, 27),
(98, '3', 3, 'json', '[{"model": "cms.cmsplugin", "pk": 3, "fields": {"path": "0003", "depth": 1, "numchild": 0, "placeholder": 3, "parent": null, "position": 2, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-18T07:14:06Z", "changed_date": "2016-02-18T07:14:36Z"}}]', '3', 16, 27),
(99, '7', 7, 'json', '[{"model": "cms.title", "pk": 7, "fields": {"language": "ru", "title": "\\u041e \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438", "page_title": "", "menu_title": "", "meta_description": "", "slug": "o-kompanii", "path": "o-kompanii", "has_url_overwrite": false, "redirect": null, "page": 7, "creation_date": "2016-02-17T13:25:00Z", "published": true}}]', 'О компании (o-kompanii, ru)', 17, 27),
(100, '19', 19, 'json', '[{"model": "cms.title", "pk": 19, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-partnery", "path": "nashi-partnery", "has_url_overwrite": false, "redirect": null, "page": 19, "creation_date": "2016-02-18T07:47:24Z", "published": false}}]', 'Наши партнеры (nashi-partnery, ru)', 17, 28),
(101, '19', 19, 'json', '[{"model": "cms.placeholder", "pk": 19, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 28),
(102, '18', 18, 'json', '[{"model": "cms.placeholder", "pk": 18, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 28),
(103, '19', 19, 'json', '[{"model": "cms.page", "pk": 19, "fields": {"path": "000D", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T07:47:24.487Z", "changed_date": "2016-02-18T07:47:24.542Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [18, 19]}}]', 'Наши партнеры', 2, 28),
(104, '19', 19, 'json', '[{"model": "cms.page", "pk": 19, "fields": {"path": "000D", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T07:47:24Z", "changed_date": "2016-02-18T07:47:44.029Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "PartnerApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [18, 19]}}]', 'Наши партнеры', 2, 29),
(105, '19', 19, 'json', '[{"model": "cms.title", "pk": 19, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-partnery", "path": "nashi-partnery", "has_url_overwrite": false, "redirect": "", "page": 19, "creation_date": "2016-02-18T07:47:24Z", "published": false}}]', 'Наши партнеры (nashi-partnery, ru)', 17, 29),
(106, '18', 18, 'json', '[{"model": "cms.placeholder", "pk": 18, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 29),
(107, '19', 19, 'json', '[{"model": "cms.placeholder", "pk": 19, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 29),
(108, '19', 19, 'json', '[{"model": "cms.page", "pk": 19, "fields": {"path": "000D", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T07:47:24Z", "changed_date": "2016-02-18T07:47:55.194Z", "publication_date": "2016-02-18T07:47:54.960Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "PartnerApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [18, 19]}}]', 'Наши партнеры', 2, 30),
(109, '19', 19, 'json', '[{"model": "cms.title", "pk": 19, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u043f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-partnery", "path": "nashi-partnery", "has_url_overwrite": false, "redirect": "", "page": 19, "creation_date": "2016-02-18T07:47:24Z", "published": true}}]', 'Наши партнеры (nashi-partnery, ru)', 17, 30),
(110, '18', 18, 'json', '[{"model": "cms.placeholder", "pk": 18, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 30),
(111, '19', 19, 'json', '[{"model": "cms.placeholder", "pk": 19, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 30),
(112, '21', 21, 'json', '[{"model": "cms.title", "pk": 21, "fields": {"language": "ru", "title": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "kontakty", "path": "kontakty", "has_url_overwrite": false, "redirect": null, "page": 21, "creation_date": "2016-02-18T08:21:45Z", "published": false}}]', 'Контакты (kontakty, ru)', 17, 31),
(113, '21', 21, 'json', '[{"model": "cms.page", "pk": 21, "fields": {"path": "000F", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T08:21:45.233Z", "changed_date": "2016-02-18T08:21:45.288Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [22, 23]}}]', 'Контакты', 2, 31),
(114, '22', 22, 'json', '[{"model": "cms.placeholder", "pk": 22, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 31),
(115, '23', 23, 'json', '[{"model": "cms.placeholder", "pk": 23, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 31),
(116, '21', 21, 'json', '[{"model": "cms.title", "pk": 21, "fields": {"language": "ru", "title": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "kontakty", "path": "kontakty", "has_url_overwrite": false, "redirect": null, "page": 21, "creation_date": "2016-02-18T08:21:45Z", "published": true}}]', 'Контакты (kontakty, ru)', 17, 32),
(117, '21', 21, 'json', '[{"model": "cms.page", "pk": 21, "fields": {"path": "000F", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T08:21:45Z", "changed_date": "2016-02-18T08:21:58.517Z", "publication_date": "2016-02-18T08:21:58.345Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [22, 23]}}]', 'Контакты', 2, 32),
(118, '22', 22, 'json', '[{"model": "cms.placeholder", "pk": 22, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 32),
(119, '23', 23, 'json', '[{"model": "cms.placeholder", "pk": 23, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 32),
(120, '23', 23, 'json', '[{"model": "cms.placeholder", "pk": 23, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 33),
(121, '21', 21, 'json', '[{"model": "cms.title", "pk": 21, "fields": {"language": "ru", "title": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "kontakty", "path": "kontakty", "has_url_overwrite": false, "redirect": null, "page": 21, "creation_date": "2016-02-18T08:21:45Z", "published": true}}]', 'Контакты (kontakty, ru)', 17, 33),
(122, '21', 21, 'json', '[{"model": "cms.page", "pk": 21, "fields": {"path": "000F", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T08:21:45Z", "changed_date": "2016-02-18T08:21:58Z", "publication_date": "2016-02-18T08:21:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [22, 23]}}]', 'Контакты', 2, 33),
(123, '22', 22, 'json', '[{"model": "cms.placeholder", "pk": 22, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 33),
(124, '7', 7, 'json', '[{"model": "cms.cmsplugin", "pk": 7, "fields": {"path": "0007", "depth": 1, "numchild": 0, "placeholder": 23, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T08:22:31Z", "changed_date": "2016-02-18T08:22:31Z"}}]', '7', 16, 33),
(125, '7', 7, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 7, "fields": {"name": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "html": "<div class=\\"contacts\\">\\r\\n            <div class=\\"container\\">\\r\\n                <h1>\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b</h1>\\r\\n\\r\\n                <a href=\\"#\\" class=\\"print anim c-red flr\\">\\u0420\\u0430\\u0441\\u043f\\u0435\\u0447\\u0430\\u0442\\u0430\\u0442\\u044c</a>\\r\\n\\r\\n                <span class=\\"fw600\\">\\r\\n                    \\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\"\\u041d\\u0438\\u043a\\u0441\\u0442\\u0435\\u0441\\u0442 \\u0441\\u0442\\u0440\\u043e\\u0439\\"\\r\\n                </span>\\r\\n\\r\\n                <div class=\\"block fll\\">\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0410\\u0434\\u0440\\u0435\\u0441:</label>\\r\\n\\r\\n                        <p>107113, \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u0421\\u043e\\u043a\\u043e\\u043b\\u044c\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043b., \\u0434. 4\\u0410</p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d:</label>\\r\\n\\r\\n                        <p>+7 (495) 517-75-28</p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">E-mail:</label>\\r\\n\\r\\n                        <p>\\r\\n                            <a href=\\"mailto:clients@remont-nts.ru\\" class=\\"c-red\\">clients@remont-nts.ru</a>\\r\\n                            <br/><a href=\\"mailto:offers@remont-nts.ru\\" class=\\"c-red\\">offers@remont-nts.ru</a> (\\u043f\\u043e\\r\\n                            \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u0430\\u043c \\u0441\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430)\\r\\n                        </p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0412\\u0440\\u0435\\u043c\\u044f \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b:</label>\\r\\n\\r\\n                        <p>\\r\\n                            <span class=\\"one\\">\\u043f\\u043d\\u2013\\u043f\\u0442</span> <span>9:00\\u201320:00</span>\\r\\n                            <br/><span class=\\"one\\">\\u0441\\u0431</span> <span>10:00\\u201314:00</span>\\r\\n                            <br/><span class=\\"one\\">\\u0432\\u0441</span> <span>\\u0432\\u044b\\u0445\\u043e\\u0434\\u043d\\u043e\\u0439</span>\\r\\n                        </p>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n\\r\\n                <div class=\\"block flr\\">\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0420\\u0435\\u043a\\u0432\\u0438\\u0437\\u0438\\u0442\\u044b:</label>\\r\\n\\r\\n                        <p>\\u041e\\u041e\\u041e \\u00ab\\u041d\\u0438\\u043a\\u0421-\\u0422\\u0435\\u0441\\u0442\\u00bb\\r\\n                            <br/>107113, \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u0421\\u043e\\u043a\\u043e\\u043b\\u044c\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043b., \\u0434. 4\\u0410\\r\\n                            <br/>\\u0418\\u041d\\u041d 7718768715\\r\\n                            <br/>\\u041a\\u041f\\u041f 771801001\\r\\n                            <br/>\\u041e\\u0413\\u0420\\u041d 1097746389751\\r\\n                            <br/>\\u041e\\u0410\\u041e \\u0421\\u0431\\u0435\\u0440\\u0431\\u0430\\u043d\\u043a \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430\\r\\n                            <br/>\\u041a/\\u0441 30101810400000000225\\r\\n                            <br/>\\u0411\\u0418\\u041a 044525225\\r\\n                            <br/>\\u0420/\\u0441 40702810438120061789</p>\\r\\n                    </div>\\r\\n                </div>\\r\\n\\r\\n                <div class=\\"clr\\"></div>\\r\\n\\r\\n                <span class=\\"fw600\\">\\u0420\\u0430\\u0441\\u043f\\u043e\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043d\\u0430 \\u043a\\u0430\\u0440\\u0442\\u0435</span>\\r\\n\\r\\n                <div id=\\"map\\">\\r\\n\\r\\n                </div>\\r\\n\\r\\n            </div>\\r\\n        </div>\\r\\n<script src=\\"http://api-maps.yandex.ru/2.1/?lang=ru_RU\\" type=\\"text/javascript\\"></script>\\r\\n<script>\\r\\n    var map;\\r\\n\\r\\n    ymaps.ready(init);\\r\\n\\r\\n    function init() {\\r\\n        map = new ymaps.Map(''map'', {\\r\\n            center: [55.698345, 37.625522],\\r\\n            zoom: 16,\\r\\n            controls: [''zoomControl''],\\r\\n            behaviors: [''drag'']\\r\\n        });\\r\\n\\r\\n\\r\\n        mark = new ymaps.Placemark([55.698345, 37.625522], {\\r\\n            balloonContentFooter: \\"\\",\\r\\n            hintContent: \\"\\u041c\\u044b \\u0437\\u0434\\u0435\\u0441\\u044c\\"\\r\\n        });\\r\\n\\r\\n        map.geoObjects.add(mark);\\r\\n    }\\r\\n</script>"}}]', 'Контакты', 30, 34),
(126, '7', 7, 'json', '[{"model": "cms.cmsplugin", "pk": 7, "fields": {"path": "0007", "depth": 1, "numchild": 0, "placeholder": 23, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T08:22:31Z", "changed_date": "2016-02-18T08:23:27Z"}}]', '7', 16, 34),
(127, '21', 21, 'json', '[{"model": "cms.title", "pk": 21, "fields": {"language": "ru", "title": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "kontakty", "path": "kontakty", "has_url_overwrite": false, "redirect": null, "page": 21, "creation_date": "2016-02-18T08:21:45Z", "published": true}}]', 'Контакты (kontakty, ru)', 17, 34),
(128, '22', 22, 'json', '[{"model": "cms.placeholder", "pk": 22, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 34),
(129, '23', 23, 'json', '[{"model": "cms.placeholder", "pk": 23, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 34),
(130, '21', 21, 'json', '[{"model": "cms.page", "pk": 21, "fields": {"path": "000F", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T08:21:45Z", "changed_date": "2016-02-18T08:22:32Z", "publication_date": "2016-02-18T08:21:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [22, 23]}}]', 'Контакты', 2, 34),
(131, '7', 7, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 7, "fields": {"name": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "html": "<div class=\\"contacts\\">\\r\\n            <div class=\\"container\\">\\r\\n                <h1>\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b</h1>\\r\\n\\r\\n                <a href=\\"#\\" class=\\"print anim c-red flr\\">\\u0420\\u0430\\u0441\\u043f\\u0435\\u0447\\u0430\\u0442\\u0430\\u0442\\u044c</a>\\r\\n\\r\\n                <span class=\\"fw600\\">\\r\\n                    \\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\"\\u041d\\u0438\\u043a\\u0441\\u0442\\u0435\\u0441\\u0442 \\u0441\\u0442\\u0440\\u043e\\u0439\\"\\r\\n                </span>\\r\\n\\r\\n                <div class=\\"block fll\\">\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0410\\u0434\\u0440\\u0435\\u0441:</label>\\r\\n\\r\\n                        <p>107113, \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u0421\\u043e\\u043a\\u043e\\u043b\\u044c\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043b., \\u0434. 4\\u0410</p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d:</label>\\r\\n\\r\\n                        <p>+7 (495) 517-75-28</p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">E-mail:</label>\\r\\n\\r\\n                        <p>\\r\\n                            <a href=\\"mailto:clients@remont-nts.ru\\" class=\\"c-red\\">clients@remont-nts.ru</a>\\r\\n                            <br/><a href=\\"mailto:offers@remont-nts.ru\\" class=\\"c-red\\">offers@remont-nts.ru</a> (\\u043f\\u043e\\r\\n                            \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u0430\\u043c \\u0441\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430)\\r\\n                        </p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0412\\u0440\\u0435\\u043c\\u044f \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b:</label>\\r\\n\\r\\n                        <p>\\r\\n                            <span class=\\"one\\">\\u043f\\u043d\\u2013\\u043f\\u0442</span> <span>9:00\\u201320:00</span>\\r\\n                            <br/><span class=\\"one\\">\\u0441\\u0431</span> <span>10:00\\u201314:00</span>\\r\\n                            <br/><span class=\\"one\\">\\u0432\\u0441</span> <span>\\u0432\\u044b\\u0445\\u043e\\u0434\\u043d\\u043e\\u0439</span>\\r\\n                        </p>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n\\r\\n                <div class=\\"block flr\\">\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0420\\u0435\\u043a\\u0432\\u0438\\u0437\\u0438\\u0442\\u044b:</label>\\r\\n\\r\\n                        <p>\\u041e\\u041e\\u041e \\u00ab\\u041d\\u0438\\u043a\\u0421-\\u0422\\u0435\\u0441\\u0442\\u00bb\\r\\n                            <br/>107113, \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u0421\\u043e\\u043a\\u043e\\u043b\\u044c\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043b., \\u0434. 4\\u0410\\r\\n                            <br/>\\u0418\\u041d\\u041d 7718768715\\r\\n                            <br/>\\u041a\\u041f\\u041f 771801001\\r\\n                            <br/>\\u041e\\u0413\\u0420\\u041d 1097746389751\\r\\n                            <br/>\\u041e\\u0410\\u041e \\u0421\\u0431\\u0435\\u0440\\u0431\\u0430\\u043d\\u043a \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430\\r\\n                            <br/>\\u041a/\\u0441 30101810400000000225\\r\\n                            <br/>\\u0411\\u0418\\u041a 044525225\\r\\n                            <br/>\\u0420/\\u0441 40702810438120061789</p>\\r\\n                    </div>\\r\\n                </div>\\r\\n\\r\\n                <div class=\\"clr\\"></div>\\r\\n\\r\\n                <span class=\\"fw600\\">\\u0420\\u0430\\u0441\\u043f\\u043e\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043d\\u0430 \\u043a\\u0430\\u0440\\u0442\\u0435</span>\\r\\n\\r\\n                <div id=\\"map\\">\\r\\n\\r\\n                </div>\\r\\n\\r\\n            </div>\\r\\n        </div>\\r\\n<script src=\\"http://api-maps.yandex.ru/2.1/?lang=ru_RU\\" type=\\"text/javascript\\"></script>\\r\\n<script>\\r\\n    var map;\\r\\n\\r\\n    ymaps.ready(init);\\r\\n\\r\\n    function init() {\\r\\n        map = new ymaps.Map(''map'', {\\r\\n            center: [55.698345, 37.625522],\\r\\n            zoom: 16,\\r\\n            controls: [''zoomControl''],\\r\\n            behaviors: [''drag'']\\r\\n        });\\r\\n\\r\\n\\r\\n        mark = new ymaps.Placemark([55.698345, 37.625522], {\\r\\n            balloonContentFooter: \\"\\",\\r\\n            hintContent: \\"\\u041c\\u044b \\u0437\\u0434\\u0435\\u0441\\u044c\\"\\r\\n        });\\r\\n\\r\\n        map.geoObjects.add(mark);\\r\\n    }\\r\\n</script>"}}]', 'Контакты', 30, 35),
(132, '7', 7, 'json', '[{"model": "cms.cmsplugin", "pk": 7, "fields": {"path": "0007", "depth": 1, "numchild": 0, "placeholder": 23, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T08:22:31Z", "changed_date": "2016-02-18T08:23:27Z"}}]', '7', 16, 35),
(133, '21', 21, 'json', '[{"model": "cms.title", "pk": 21, "fields": {"language": "ru", "title": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "kontakty", "path": "kontakty", "has_url_overwrite": false, "redirect": null, "page": 21, "creation_date": "2016-02-18T08:21:45Z", "published": true}}]', 'Контакты (kontakty, ru)', 17, 35),
(134, '22', 22, 'json', '[{"model": "cms.placeholder", "pk": 22, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 35),
(135, '23', 23, 'json', '[{"model": "cms.placeholder", "pk": 23, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 35),
(136, '21', 21, 'json', '[{"model": "cms.page", "pk": 21, "fields": {"path": "000F", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T08:21:45Z", "changed_date": "2016-02-18T08:23:40.148Z", "publication_date": "2016-02-18T08:21:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [22, 23]}}]', 'Контакты', 2, 35),
(137, '26', 26, 'json', '[{"model": "cms.placeholder", "pk": 26, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 36),
(138, '27', 27, 'json', '[{"model": "cms.placeholder", "pk": 27, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 36),
(139, '5', 5, 'json', '[{"model": "cms.page", "pk": 5, "fields": {"path": "0007", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:29Z", "changed_date": "2016-02-18T09:04:55.675Z", "publication_date": "2016-02-17T13:26:43Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "PriceApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [26, 27]}}]', 'Цены', 2, 36),
(140, '5', 5, 'json', '[{"model": "cms.title", "pk": 5, "fields": {"language": "ru", "title": "\\u0426\\u0435\\u043d\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "ceny", "path": "ceny", "has_url_overwrite": false, "redirect": "", "page": 5, "creation_date": "2016-02-17T13:24:29Z", "published": true}}]', 'Цены (ceny, ru)', 17, 36),
(141, '26', 26, 'json', '[{"model": "cms.placeholder", "pk": 26, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 37),
(142, '27', 27, 'json', '[{"model": "cms.placeholder", "pk": 27, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 37),
(143, '5', 5, 'json', '[{"model": "cms.page", "pk": 5, "fields": {"path": "0007", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:29Z", "changed_date": "2016-02-18T09:05:04.520Z", "publication_date": "2016-02-17T13:26:43Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "PriceApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [26, 27]}}]', 'Цены', 2, 37),
(144, '5', 5, 'json', '[{"model": "cms.title", "pk": 5, "fields": {"language": "ru", "title": "\\u0426\\u0435\\u043d\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "ceny", "path": "ceny", "has_url_overwrite": false, "redirect": "", "page": 5, "creation_date": "2016-02-17T13:24:29Z", "published": true}}]', 'Цены (ceny, ru)', 17, 37),
(145, '26', 26, 'json', '[{"model": "cms.placeholder", "pk": 26, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 38),
(146, '27', 27, 'json', '[{"model": "cms.placeholder", "pk": 27, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 38),
(147, '5', 5, 'json', '[{"model": "cms.page", "pk": 5, "fields": {"path": "0007", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:29Z", "changed_date": "2016-02-18T09:08:19.372Z", "publication_date": "2016-02-17T13:26:43Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "PriceApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [26, 27]}}]', 'Цены', 2, 38),
(148, '5', 5, 'json', '[{"model": "cms.title", "pk": 5, "fields": {"language": "ru", "title": "\\u0426\\u0435\\u043d\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "ceny", "path": "ceny", "has_url_overwrite": false, "redirect": "", "page": 5, "creation_date": "2016-02-17T13:24:29Z", "published": true}}]', 'Цены (ceny, ru)', 17, 38),
(149, '40', 40, 'json', '[{"model": "cms.placeholder", "pk": 40, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 39),
(150, '41', 41, 'json', '[{"model": "cms.placeholder", "pk": 41, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 39),
(151, '3', 3, 'json', '[{"model": "cms.page", "pk": 3, "fields": {"path": "0003", "depth": 1, "numchild": 3, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:02Z", "changed_date": "2016-02-18T11:29:45.715Z", "publication_date": "2016-02-17T13:26:36Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [40, 41]}}]', 'Наши работы', 2, 39),
(152, '3', 3, 'json', '[{"model": "cms.title", "pk": 3, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-raboty", "path": "nashi-raboty", "has_url_overwrite": false, "redirect": "/nashi-raboty/vypolnennye-remonty/", "page": 3, "creation_date": "2016-02-17T13:24:03Z", "published": true}}]', 'Наши работы (nashi-raboty, ru)', 17, 39),
(153, '40', 40, 'json', '[{"model": "cms.placeholder", "pk": 40, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 40),
(154, '41', 41, 'json', '[{"model": "cms.placeholder", "pk": 41, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 40),
(155, '3', 3, 'json', '[{"model": "cms.page", "pk": 3, "fields": {"path": "0003", "depth": 1, "numchild": 3, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:24:02Z", "changed_date": "2016-02-18T11:29:58.176Z", "publication_date": "2016-02-17T13:26:36Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [40, 41]}}]', 'Наши работы', 2, 40),
(156, '3', 3, 'json', '[{"model": "cms.title", "pk": 3, "fields": {"language": "ru", "title": "\\u041d\\u0430\\u0448\\u0438 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "nashi-raboty", "path": "nashi-raboty", "has_url_overwrite": false, "redirect": "/nashi-raboty/vypolnennye-remonty/", "page": 3, "creation_date": "2016-02-17T13:24:03Z", "published": true}}]', 'Наши работы (nashi-raboty, ru)', 17, 40),
(157, '8', 8, 'json', '[{"model": "cms.page", "pk": 8, "fields": {"path": "00030001", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:26Z", "changed_date": "2016-02-18T11:40:18.280Z", "publication_date": "2016-02-17T13:26:38Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "ProjectApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [42, 43]}}]', 'Выполненные ремонты', 2, 41),
(158, '8', 8, 'json', '[{"model": "cms.title", "pk": 8, "fields": {"language": "ru", "title": "\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "vypolnennye-remonty", "path": "nashi-raboty/vypolnennye-remonty", "has_url_overwrite": false, "redirect": "", "page": 8, "creation_date": "2016-02-17T13:25:26Z", "published": true}}]', 'Выполненные ремонты (vypolnennye-remonty, ru)', 17, 41),
(159, '42', 42, 'json', '[{"model": "cms.placeholder", "pk": 42, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 41),
(160, '43', 43, 'json', '[{"model": "cms.placeholder", "pk": 43, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 41),
(161, '9', 9, 'json', '[{"model": "cms.page", "pk": 9, "fields": {"path": "00030002", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:41Z", "changed_date": "2016-02-18T11:40:43.271Z", "publication_date": "2016-02-17T13:26:39Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "DesignApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [44, 45]}}]', 'Дизайн-проекты', 2, 42),
(162, '44', 44, 'json', '[{"model": "cms.placeholder", "pk": 44, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 42),
(163, '45', 45, 'json', '[{"model": "cms.placeholder", "pk": 45, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 42),
(164, '9', 9, 'json', '[{"model": "cms.title", "pk": 9, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d-\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "nashi-raboty/dizajn-proekty", "has_url_overwrite": false, "redirect": "", "page": 9, "creation_date": "2016-02-17T13:25:41Z", "published": true}}]', 'Дизайн-проекты (dizajn-proekty, ru)', 17, 42),
(165, '48', 48, 'json', '[{"model": "cms.placeholder", "pk": 48, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 43),
(166, '49', 49, 'json', '[{"model": "cms.placeholder", "pk": 49, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 43),
(167, '10', 10, 'json', '[{"model": "cms.page", "pk": 10, "fields": {"path": "00030003", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:26:04Z", "changed_date": "2016-02-18T11:41:06.822Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "TourApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [48, 49]}}]', 'Виртуальные 3D туры', 2, 43),
(168, '10', 10, 'json', '[{"model": "cms.title", "pk": 10, "fields": {"language": "ru", "title": "\\u0412\\u0438\\u0440\\u0442\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 3D \\u0442\\u0443\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "virtualnye-3d-tury", "path": "nashi-raboty/virtualnye-3d-tury", "has_url_overwrite": false, "redirect": "", "page": 10, "creation_date": "2016-02-17T13:26:04Z", "published": true}}]', 'Виртуальные 3D туры (virtualnye-3d-tury, ru)', 17, 43),
(169, '8', 8, 'json', '[{"model": "cms.page", "pk": 8, "fields": {"path": "00030001", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:26Z", "changed_date": "2016-02-18T11:41:48.145Z", "publication_date": "2016-02-17T13:26:38Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "ProjectApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [42, 43]}}]', 'Выполненные ремонты', 2, 44),
(170, '8', 8, 'json', '[{"model": "cms.title", "pk": 8, "fields": {"language": "ru", "title": "\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "vypolnennye-remonty", "path": "nashi-raboty/vypolnennye-remonty", "has_url_overwrite": false, "redirect": "", "page": 8, "creation_date": "2016-02-17T13:25:26Z", "published": true}}]', 'Выполненные ремонты (vypolnennye-remonty, ru)', 17, 44),
(171, '42', 42, 'json', '[{"model": "cms.placeholder", "pk": 42, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 44),
(172, '43', 43, 'json', '[{"model": "cms.placeholder", "pk": 43, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 44);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(173, '9', 9, 'json', '[{"model": "cms.page", "pk": 9, "fields": {"path": "00030002", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:41Z", "changed_date": "2016-02-18T11:41:49.374Z", "publication_date": "2016-02-17T13:26:39Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "DesignApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [44, 45]}}]', 'Дизайн-проекты', 2, 45),
(174, '44', 44, 'json', '[{"model": "cms.placeholder", "pk": 44, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 45),
(175, '45', 45, 'json', '[{"model": "cms.placeholder", "pk": 45, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 45),
(176, '9', 9, 'json', '[{"model": "cms.title", "pk": 9, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d-\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "nashi-raboty/dizajn-proekty", "has_url_overwrite": false, "redirect": "", "page": 9, "creation_date": "2016-02-17T13:25:41Z", "published": true}}]', 'Дизайн-проекты (dizajn-proekty, ru)', 17, 45),
(177, '48', 48, 'json', '[{"model": "cms.placeholder", "pk": 48, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 46),
(178, '49', 49, 'json', '[{"model": "cms.placeholder", "pk": 49, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 46),
(179, '10', 10, 'json', '[{"model": "cms.page", "pk": 10, "fields": {"path": "00030003", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:26:04Z", "changed_date": "2016-02-18T11:41:50.869Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "TourApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [48, 49]}}]', 'Виртуальные 3D туры', 2, 46),
(180, '10', 10, 'json', '[{"model": "cms.title", "pk": 10, "fields": {"language": "ru", "title": "\\u0412\\u0438\\u0440\\u0442\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 3D \\u0442\\u0443\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "virtualnye-3d-tury", "path": "nashi-raboty/virtualnye-3d-tury", "has_url_overwrite": false, "redirect": "", "page": 10, "creation_date": "2016-02-17T13:26:04Z", "published": true}}]', 'Виртуальные 3D туры (virtualnye-3d-tury, ru)', 17, 46),
(181, '48', 48, 'json', '[{"model": "cms.placeholder", "pk": 48, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 47),
(182, '49', 49, 'json', '[{"model": "cms.placeholder", "pk": 49, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 47),
(183, '10', 10, 'json', '[{"model": "cms.page", "pk": 10, "fields": {"path": "00030003", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:26:04Z", "changed_date": "2016-02-18T11:41:50Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "TourApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [48, 49]}}]', 'Виртуальные 3D туры', 2, 47),
(184, '10', 10, 'json', '[{"model": "cms.title", "pk": 10, "fields": {"language": "ru", "title": "\\u0412\\u0438\\u0440\\u0442\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 3D \\u0442\\u0443\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "virtualnye-3d-tury", "path": "nashi-raboty/virtualnye-3d-tury", "has_url_overwrite": false, "redirect": "", "page": 10, "creation_date": "2016-02-17T13:26:04Z", "published": true}}]', 'Виртуальные 3D туры (virtualnye-3d-tury, ru)', 17, 47),
(185, '13', 13, 'json', '[{"model": "cms.cmsplugin", "pk": 13, "fields": {"path": "000D", "depth": 1, "numchild": 0, "placeholder": 49, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T08:32:47Z", "changed_date": "2016-02-24T08:32:47Z"}}]', '13', 16, 47),
(186, '10', 10, 'json', '[{"model": "cms.page", "pk": 10, "fields": {"path": "00030003", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:26:04Z", "changed_date": "2016-02-24T08:32:48Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "TourApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [48, 49]}}]', 'Виртуальные 3D туры', 2, 48),
(187, '13', 13, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 13, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 48),
(188, '13', 13, 'json', '[{"model": "cms.cmsplugin", "pk": 13, "fields": {"path": "000D", "depth": 1, "numchild": 0, "placeholder": 49, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T08:32:47Z", "changed_date": "2016-02-24T08:33:09Z"}}]', '13', 16, 48),
(189, '48', 48, 'json', '[{"model": "cms.placeholder", "pk": 48, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 48),
(190, '49', 49, 'json', '[{"model": "cms.placeholder", "pk": 49, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 48),
(191, '10', 10, 'json', '[{"model": "cms.title", "pk": 10, "fields": {"language": "ru", "title": "\\u0412\\u0438\\u0440\\u0442\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 3D \\u0442\\u0443\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "virtualnye-3d-tury", "path": "nashi-raboty/virtualnye-3d-tury", "has_url_overwrite": false, "redirect": "", "page": 10, "creation_date": "2016-02-17T13:26:04Z", "published": true}}]', 'Виртуальные 3D туры (virtualnye-3d-tury, ru)', 17, 48),
(192, '10', 10, 'json', '[{"model": "cms.page", "pk": 10, "fields": {"path": "00030003", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:26:04Z", "changed_date": "2016-02-24T08:33:10Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "TourApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [48, 49]}}]', 'Виртуальные 3D туры', 2, 49),
(193, '13', 13, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 13, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 49),
(194, '14', 14, 'json', '[{"model": "cms.cmsplugin", "pk": 14, "fields": {"path": "000E", "depth": 1, "numchild": 0, "placeholder": 49, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-24T09:03:04Z", "changed_date": "2016-02-24T09:03:04Z"}}]', '14', 16, 49),
(195, '13', 13, 'json', '[{"model": "cms.cmsplugin", "pk": 13, "fields": {"path": "000D", "depth": 1, "numchild": 0, "placeholder": 49, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T08:32:47Z", "changed_date": "2016-02-24T08:33:09Z"}}]', '13', 16, 49),
(196, '48', 48, 'json', '[{"model": "cms.placeholder", "pk": 48, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 49),
(197, '49', 49, 'json', '[{"model": "cms.placeholder", "pk": 49, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 49),
(198, '10', 10, 'json', '[{"model": "cms.title", "pk": 10, "fields": {"language": "ru", "title": "\\u0412\\u0438\\u0440\\u0442\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 3D \\u0442\\u0443\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "virtualnye-3d-tury", "path": "nashi-raboty/virtualnye-3d-tury", "has_url_overwrite": false, "redirect": "", "page": 10, "creation_date": "2016-02-17T13:26:04Z", "published": true}}]', 'Виртуальные 3D туры (virtualnye-3d-tury, ru)', 17, 49),
(199, '10', 10, 'json', '[{"model": "cms.page", "pk": 10, "fields": {"path": "00030003", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:26:04Z", "changed_date": "2016-02-24T09:03:04Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "TourApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [48, 49]}}]', 'Виртуальные 3D туры', 2, 50),
(200, '13', 13, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 13, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 50),
(201, '14', 14, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 14, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 50),
(202, '13', 13, 'json', '[{"model": "cms.cmsplugin", "pk": 13, "fields": {"path": "000D", "depth": 1, "numchild": 0, "placeholder": 49, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T08:32:47Z", "changed_date": "2016-02-24T08:33:09Z"}}]', '13', 16, 50),
(203, '48', 48, 'json', '[{"model": "cms.placeholder", "pk": 48, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 50),
(204, '49', 49, 'json', '[{"model": "cms.placeholder", "pk": 49, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 50),
(205, '14', 14, 'json', '[{"model": "cms.cmsplugin", "pk": 14, "fields": {"path": "000E", "depth": 1, "numchild": 0, "placeholder": 49, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-24T09:03:04Z", "changed_date": "2016-02-24T09:03:19Z"}}]', '14', 16, 50),
(206, '10', 10, 'json', '[{"model": "cms.title", "pk": 10, "fields": {"language": "ru", "title": "\\u0412\\u0438\\u0440\\u0442\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 3D \\u0442\\u0443\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "virtualnye-3d-tury", "path": "nashi-raboty/virtualnye-3d-tury", "has_url_overwrite": false, "redirect": "", "page": 10, "creation_date": "2016-02-17T13:26:04Z", "published": true}}]', 'Виртуальные 3D туры (virtualnye-3d-tury, ru)', 17, 50),
(207, '10', 10, 'json', '[{"model": "cms.page", "pk": 10, "fields": {"path": "00030003", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:26:04Z", "changed_date": "2016-02-24T09:03:38.843Z", "publication_date": "2016-02-17T13:26:47Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "TourApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [48, 49]}}]', 'Виртуальные 3D туры', 2, 51),
(208, '13', 13, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 13, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 51),
(209, '14', 14, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 14, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 51),
(210, '13', 13, 'json', '[{"model": "cms.cmsplugin", "pk": 13, "fields": {"path": "000D", "depth": 1, "numchild": 0, "placeholder": 49, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T08:32:47Z", "changed_date": "2016-02-24T08:33:09Z"}}]', '13', 16, 51),
(211, '48', 48, 'json', '[{"model": "cms.placeholder", "pk": 48, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 51),
(212, '49', 49, 'json', '[{"model": "cms.placeholder", "pk": 49, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 51),
(213, '14', 14, 'json', '[{"model": "cms.cmsplugin", "pk": 14, "fields": {"path": "000E", "depth": 1, "numchild": 0, "placeholder": 49, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-24T09:03:04Z", "changed_date": "2016-02-24T09:03:19Z"}}]', '14', 16, 51),
(214, '10', 10, 'json', '[{"model": "cms.title", "pk": 10, "fields": {"language": "ru", "title": "\\u0412\\u0438\\u0440\\u0442\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 3D \\u0442\\u0443\\u0440\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "virtualnye-3d-tury", "path": "nashi-raboty/virtualnye-3d-tury", "has_url_overwrite": false, "redirect": "", "page": 10, "creation_date": "2016-02-17T13:26:04Z", "published": true}}]', 'Виртуальные 3D туры (virtualnye-3d-tury, ru)', 17, 51),
(215, '23', 23, 'json', '[{"model": "cms.title", "pk": 23, "fields": {"language": "ru", "title": "\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "reviews", "path": "reviews", "has_url_overwrite": false, "redirect": null, "page": 23, "creation_date": "2016-02-24T09:19:28Z", "published": false}}]', 'Отзывы (reviews, ru)', 17, 52),
(216, '52', 52, 'json', '[{"model": "cms.placeholder", "pk": 52, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 52),
(217, '53', 53, 'json', '[{"model": "cms.placeholder", "pk": 53, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 52),
(218, '23', 23, 'json', '[{"model": "cms.page", "pk": 23, "fields": {"path": "000H", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-24T09:19:27.982Z", "changed_date": "2016-02-24T09:19:28.260Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [52, 53]}}]', 'Отзывы', 2, 52),
(219, '23', 23, 'json', '[{"model": "cms.title", "pk": 23, "fields": {"language": "ru", "title": "\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "reviews", "path": "reviews", "has_url_overwrite": false, "redirect": "", "page": 23, "creation_date": "2016-02-24T09:19:28Z", "published": false}}]', 'Отзывы (reviews, ru)', 17, 53),
(220, '52', 52, 'json', '[{"model": "cms.placeholder", "pk": 52, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 53),
(221, '53', 53, 'json', '[{"model": "cms.placeholder", "pk": 53, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 53),
(222, '23', 23, 'json', '[{"model": "cms.page", "pk": 23, "fields": {"path": "000H", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-24T09:19:27Z", "changed_date": "2016-02-24T09:20:25.502Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "ReviewApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [52, 53]}}]', 'Отзывы', 2, 53),
(223, '23', 23, 'json', '[{"model": "cms.title", "pk": 23, "fields": {"language": "ru", "title": "\\u041e\\u0442\\u0437\\u044b\\u0432\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "reviews", "path": "reviews", "has_url_overwrite": false, "redirect": "", "page": 23, "creation_date": "2016-02-24T09:19:28Z", "published": true}}]', 'Отзывы (reviews, ru)', 17, 54),
(224, '52', 52, 'json', '[{"model": "cms.placeholder", "pk": 52, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 54),
(225, '53', 53, 'json', '[{"model": "cms.placeholder", "pk": 53, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 54),
(226, '23', 23, 'json', '[{"model": "cms.page", "pk": 23, "fields": {"path": "000H", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-24T09:19:27Z", "changed_date": "2016-02-24T09:20:41.876Z", "publication_date": "2016-02-24T09:20:41.026Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "ReviewApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [52, 53]}}]', 'Отзывы', 2, 54),
(227, '9', 9, 'json', '[{"model": "cms.page", "pk": 9, "fields": {"path": "00030002", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:41Z", "changed_date": "2016-02-18T11:41:49Z", "publication_date": "2016-02-17T13:26:39Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "DesignApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [44, 45]}}]', 'Дизайн-проекты', 2, 55),
(228, '9', 9, 'json', '[{"model": "cms.title", "pk": 9, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d-\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "nashi-raboty/dizajn-proekty", "has_url_overwrite": false, "redirect": "", "page": 9, "creation_date": "2016-02-17T13:25:41Z", "published": true}}]', 'Дизайн-проекты (dizajn-proekty, ru)', 17, 55),
(229, '44', 44, 'json', '[{"model": "cms.placeholder", "pk": 44, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 55),
(230, '45', 45, 'json', '[{"model": "cms.placeholder", "pk": 45, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 55),
(231, '17', 17, 'json', '[{"model": "cms.cmsplugin", "pk": 17, "fields": {"path": "000H", "depth": 1, "numchild": 0, "placeholder": 45, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T11:36:55Z", "changed_date": "2016-02-24T11:36:55Z"}}]', '17', 16, 55),
(232, '17', 17, 'json', '[{"model": "cms.cmsplugin", "pk": 17, "fields": {"path": "000H", "depth": 1, "numchild": 0, "placeholder": 45, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T11:36:55Z", "changed_date": "2016-02-24T11:37:20Z"}}]', '17', 16, 56),
(233, '9', 9, 'json', '[{"model": "cms.page", "pk": 9, "fields": {"path": "00030002", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:41Z", "changed_date": "2016-02-24T11:36:55Z", "publication_date": "2016-02-17T13:26:39Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "DesignApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [44, 45]}}]', 'Дизайн-проекты', 2, 56),
(234, '44', 44, 'json', '[{"model": "cms.placeholder", "pk": 44, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 56),
(235, '45', 45, 'json', '[{"model": "cms.placeholder", "pk": 45, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 56),
(236, '17', 17, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 17, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 56),
(237, '9', 9, 'json', '[{"model": "cms.title", "pk": 9, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d-\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "nashi-raboty/dizajn-proekty", "has_url_overwrite": false, "redirect": "", "page": 9, "creation_date": "2016-02-17T13:25:41Z", "published": true}}]', 'Дизайн-проекты (dizajn-proekty, ru)', 17, 56),
(238, '17', 17, 'json', '[{"model": "cms.cmsplugin", "pk": 17, "fields": {"path": "000H", "depth": 1, "numchild": 0, "placeholder": 45, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T11:36:55Z", "changed_date": "2016-02-24T11:37:20Z"}}]', '17', 16, 57),
(239, '9', 9, 'json', '[{"model": "cms.page", "pk": 9, "fields": {"path": "00030002", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:41Z", "changed_date": "2016-02-24T11:37:20Z", "publication_date": "2016-02-17T13:26:39Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "DesignApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [44, 45]}}]', 'Дизайн-проекты', 2, 57),
(240, '44', 44, 'json', '[{"model": "cms.placeholder", "pk": 44, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 57),
(241, '45', 45, 'json', '[{"model": "cms.placeholder", "pk": 45, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 57),
(242, '17', 17, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 17, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 57),
(243, '18', 18, 'json', '[{"model": "cms.cmsplugin", "pk": 18, "fields": {"path": "000I", "depth": 1, "numchild": 0, "placeholder": 45, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-24T11:37:49Z", "changed_date": "2016-02-24T11:37:49Z"}}]', '18', 16, 57),
(244, '9', 9, 'json', '[{"model": "cms.title", "pk": 9, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d-\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "nashi-raboty/dizajn-proekty", "has_url_overwrite": false, "redirect": "", "page": 9, "creation_date": "2016-02-17T13:25:41Z", "published": true}}]', 'Дизайн-проекты (dizajn-proekty, ru)', 17, 57),
(245, '18', 18, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 18, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 58),
(246, '17', 17, 'json', '[{"model": "cms.cmsplugin", "pk": 17, "fields": {"path": "000H", "depth": 1, "numchild": 0, "placeholder": 45, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T11:36:55Z", "changed_date": "2016-02-24T11:37:20Z"}}]', '17', 16, 58),
(247, '9', 9, 'json', '[{"model": "cms.page", "pk": 9, "fields": {"path": "00030002", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:41Z", "changed_date": "2016-02-24T11:37:49Z", "publication_date": "2016-02-17T13:26:39Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "DesignApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [44, 45]}}]', 'Дизайн-проекты', 2, 58),
(248, '44', 44, 'json', '[{"model": "cms.placeholder", "pk": 44, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 58),
(249, '45', 45, 'json', '[{"model": "cms.placeholder", "pk": 45, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 58),
(250, '17', 17, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 17, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 58),
(251, '18', 18, 'json', '[{"model": "cms.cmsplugin", "pk": 18, "fields": {"path": "000I", "depth": 1, "numchild": 0, "placeholder": 45, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-24T11:37:49Z", "changed_date": "2016-02-24T11:38:04Z"}}]', '18', 16, 58),
(252, '9', 9, 'json', '[{"model": "cms.title", "pk": 9, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d-\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "nashi-raboty/dizajn-proekty", "has_url_overwrite": false, "redirect": "", "page": 9, "creation_date": "2016-02-17T13:25:41Z", "published": true}}]', 'Дизайн-проекты (dizajn-proekty, ru)', 17, 58),
(253, '18', 18, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 18, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 59),
(254, '17', 17, 'json', '[{"model": "cms.cmsplugin", "pk": 17, "fields": {"path": "000H", "depth": 1, "numchild": 0, "placeholder": 45, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T11:36:55Z", "changed_date": "2016-02-24T11:37:20Z"}}]', '17', 16, 59),
(255, '9', 9, 'json', '[{"model": "cms.page", "pk": 9, "fields": {"path": "00030002", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:41Z", "changed_date": "2016-02-24T11:40:33.541Z", "publication_date": "2016-02-17T13:26:39Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "DesignApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [44, 45]}}]', 'Дизайн-проекты', 2, 59),
(256, '44', 44, 'json', '[{"model": "cms.placeholder", "pk": 44, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 59),
(257, '45', 45, 'json', '[{"model": "cms.placeholder", "pk": 45, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 59),
(258, '17', 17, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 17, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 59),
(259, '18', 18, 'json', '[{"model": "cms.cmsplugin", "pk": 18, "fields": {"path": "000I", "depth": 1, "numchild": 0, "placeholder": 45, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-24T11:37:49Z", "changed_date": "2016-02-24T11:38:04Z"}}]', '18', 16, 59),
(260, '9', 9, 'json', '[{"model": "cms.title", "pk": 9, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d-\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "nashi-raboty/dizajn-proekty", "has_url_overwrite": false, "redirect": "", "page": 9, "creation_date": "2016-02-17T13:25:41Z", "published": true}}]', 'Дизайн-проекты (dizajn-proekty, ru)', 17, 59),
(261, '8', 8, 'json', '[{"model": "cms.page", "pk": 8, "fields": {"path": "00030001", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:26Z", "changed_date": "2016-02-18T11:41:48Z", "publication_date": "2016-02-17T13:26:38Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "ProjectApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [42, 43]}}]', 'Выполненные ремонты', 2, 60),
(262, '8', 8, 'json', '[{"model": "cms.title", "pk": 8, "fields": {"language": "ru", "title": "\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "vypolnennye-remonty", "path": "nashi-raboty/vypolnennye-remonty", "has_url_overwrite": false, "redirect": "", "page": 8, "creation_date": "2016-02-17T13:25:26Z", "published": true}}]', 'Выполненные ремонты (vypolnennye-remonty, ru)', 17, 60),
(263, '42', 42, 'json', '[{"model": "cms.placeholder", "pk": 42, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 60),
(264, '43', 43, 'json', '[{"model": "cms.placeholder", "pk": 43, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 60),
(265, '21', 21, 'json', '[{"model": "cms.cmsplugin", "pk": 21, "fields": {"path": "000L", "depth": 1, "numchild": 0, "placeholder": 43, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T12:01:25Z", "changed_date": "2016-02-24T12:01:26Z"}}]', '21', 16, 60),
(266, '8', 8, 'json', '[{"model": "cms.page", "pk": 8, "fields": {"path": "00030001", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:26Z", "changed_date": "2016-02-24T12:01:26Z", "publication_date": "2016-02-17T13:26:38Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "ProjectApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [42, 43]}}]', 'Выполненные ремонты', 2, 61),
(267, '42', 42, 'json', '[{"model": "cms.placeholder", "pk": 42, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 61),
(268, '43', 43, 'json', '[{"model": "cms.placeholder", "pk": 43, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 61),
(269, '8', 8, 'json', '[{"model": "cms.title", "pk": 8, "fields": {"language": "ru", "title": "\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "vypolnennye-remonty", "path": "nashi-raboty/vypolnennye-remonty", "has_url_overwrite": false, "redirect": "", "page": 8, "creation_date": "2016-02-17T13:25:26Z", "published": true}}]', 'Выполненные ремонты (vypolnennye-remonty, ru)', 17, 61),
(270, '21', 21, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 21, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 61),
(271, '21', 21, 'json', '[{"model": "cms.cmsplugin", "pk": 21, "fields": {"path": "000L", "depth": 1, "numchild": 0, "placeholder": 43, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T12:01:25Z", "changed_date": "2016-02-24T12:01:54Z"}}]', '21', 16, 61),
(272, '8', 8, 'json', '[{"model": "cms.page", "pk": 8, "fields": {"path": "00030001", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:26Z", "changed_date": "2016-02-24T12:01:54Z", "publication_date": "2016-02-17T13:26:38Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "ProjectApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [42, 43]}}]', 'Выполненные ремонты', 2, 62),
(273, '42', 42, 'json', '[{"model": "cms.placeholder", "pk": 42, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 62),
(274, '43', 43, 'json', '[{"model": "cms.placeholder", "pk": 43, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 62),
(275, '8', 8, 'json', '[{"model": "cms.title", "pk": 8, "fields": {"language": "ru", "title": "\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "vypolnennye-remonty", "path": "nashi-raboty/vypolnennye-remonty", "has_url_overwrite": false, "redirect": "", "page": 8, "creation_date": "2016-02-17T13:25:26Z", "published": true}}]', 'Выполненные ремонты (vypolnennye-remonty, ru)', 17, 62),
(276, '21', 21, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 21, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 62),
(277, '22', 22, 'json', '[{"model": "cms.cmsplugin", "pk": 22, "fields": {"path": "000M", "depth": 1, "numchild": 0, "placeholder": 43, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-24T12:02:19Z", "changed_date": "2016-02-24T12:02:19Z"}}]', '22', 16, 62),
(278, '21', 21, 'json', '[{"model": "cms.cmsplugin", "pk": 21, "fields": {"path": "000L", "depth": 1, "numchild": 0, "placeholder": 43, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T12:01:25Z", "changed_date": "2016-02-24T12:01:54Z"}}]', '21', 16, 62),
(279, '22', 22, 'json', '[{"model": "cms.cmsplugin", "pk": 22, "fields": {"path": "000M", "depth": 1, "numchild": 0, "placeholder": 43, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-24T12:02:19Z", "changed_date": "2016-02-24T12:03:22Z"}}]', '22', 16, 63),
(280, '8', 8, 'json', '[{"model": "cms.page", "pk": 8, "fields": {"path": "00030001", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:26Z", "changed_date": "2016-02-24T12:02:19Z", "publication_date": "2016-02-17T13:26:38Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "ProjectApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [42, 43]}}]', 'Выполненные ремонты', 2, 63),
(281, '42', 42, 'json', '[{"model": "cms.placeholder", "pk": 42, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 63),
(282, '43', 43, 'json', '[{"model": "cms.placeholder", "pk": 43, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 63),
(283, '8', 8, 'json', '[{"model": "cms.title", "pk": 8, "fields": {"language": "ru", "title": "\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "vypolnennye-remonty", "path": "nashi-raboty/vypolnennye-remonty", "has_url_overwrite": false, "redirect": "", "page": 8, "creation_date": "2016-02-17T13:25:26Z", "published": true}}]', 'Выполненные ремонты (vypolnennye-remonty, ru)', 17, 63),
(284, '21', 21, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 21, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 63),
(285, '22', 22, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 22, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 63),
(286, '21', 21, 'json', '[{"model": "cms.cmsplugin", "pk": 21, "fields": {"path": "000L", "depth": 1, "numchild": 0, "placeholder": 43, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T12:01:25Z", "changed_date": "2016-02-24T12:01:54Z"}}]', '21', 16, 63),
(287, '22', 22, 'json', '[{"model": "cms.cmsplugin", "pk": 22, "fields": {"path": "000M", "depth": 1, "numchild": 0, "placeholder": 43, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-24T12:02:19Z", "changed_date": "2016-02-24T12:03:22Z"}}]', '22', 16, 64),
(288, '8', 8, 'json', '[{"model": "cms.page", "pk": 8, "fields": {"path": "00030001", "depth": 2, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": 3, "creation_date": "2016-02-17T13:25:26Z", "changed_date": "2016-02-24T12:03:36.389Z", "publication_date": "2016-02-17T13:26:38Z", "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "ProjectApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [42, 43]}}]', 'Выполненные ремонты', 2, 64),
(289, '42', 42, 'json', '[{"model": "cms.placeholder", "pk": 42, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 64),
(290, '43', 43, 'json', '[{"model": "cms.placeholder", "pk": 43, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 64),
(291, '8', 8, 'json', '[{"model": "cms.title", "pk": 8, "fields": {"language": "ru", "title": "\\u0412\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "vypolnennye-remonty", "path": "nashi-raboty/vypolnennye-remonty", "has_url_overwrite": false, "redirect": "", "page": 8, "creation_date": "2016-02-17T13:25:26Z", "published": true}}]', 'Выполненные ремонты (vypolnennye-remonty, ru)', 17, 64),
(292, '21', 21, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 21, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 64),
(293, '22', 22, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 22, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 64),
(294, '21', 21, 'json', '[{"model": "cms.cmsplugin", "pk": 21, "fields": {"path": "000L", "depth": 1, "numchild": 0, "placeholder": 43, "parent": null, "position": 0, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-24T12:01:25Z", "changed_date": "2016-02-24T12:01:54Z"}}]', '21', 16, 64),
(295, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13.460Z", "changed_date": "2016-02-29T11:09:13.541Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 65),
(296, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 65),
(297, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": false}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 65),
(298, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 65),
(299, '26', 26, 'json', '[{"model": "cms.page", "pk": 26, "fields": {"path": "000K", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:39.320Z", "changed_date": "2016-02-29T11:09:39.404Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [59, 60]}}]', 'Ремонт коттеджей и таунхаусов', 2, 66);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(300, '59', 59, 'json', '[{"model": "cms.placeholder", "pk": 59, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 66),
(301, '60', 60, 'json', '[{"model": "cms.placeholder", "pk": 60, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 66),
(302, '26', 26, 'json', '[{"model": "cms.title", "pk": 26, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u043e\\u0442\\u0442\\u0435\\u0434\\u0436\\u0435\\u0439 \\u0438 \\u0442\\u0430\\u0443\\u043d\\u0445\\u0430\\u0443\\u0441\\u043e\\u0432", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kottedzhej-i-taunhausov", "path": "remont-kottedzhej-i-taunhausov", "has_url_overwrite": false, "redirect": null, "page": 26, "creation_date": "2016-02-29T11:09:39Z", "published": false}}]', 'Ремонт коттеджей и таунхаусов (remont-kottedzhej-i-taunhausov, ru)', 17, 66),
(303, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": false}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 67),
(304, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000L", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59.741Z", "changed_date": "2016-02-29T11:09:59.804Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 67),
(305, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 67),
(306, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 67),
(307, '64', 64, 'json', '[{"model": "cms.placeholder", "pk": 64, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 68),
(308, '28', 28, 'json', '[{"model": "cms.title", "pk": 28, "fields": {"language": "ru", "title": "\\u0414\\u0435\\u043a\\u043e\\u0440 \\u0434\\u043e\\u043c\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dekor-doma", "path": "dekor-doma", "has_url_overwrite": false, "redirect": null, "page": 28, "creation_date": "2016-02-29T11:10:19Z", "published": false}}]', 'Декор дома (dekor-doma, ru)', 17, 68),
(309, '28', 28, 'json', '[{"model": "cms.page", "pk": 28, "fields": {"path": "000M", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:10:19.576Z", "changed_date": "2016-02-29T11:10:19.631Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [63, 64]}}]', 'Декор дома', 2, 68),
(310, '63', 63, 'json', '[{"model": "cms.placeholder", "pk": 63, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 68),
(311, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:10:41.214Z", "publication_date": "2016-02-29T11:10:40.965Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 69),
(312, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 69),
(313, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 69),
(314, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 69),
(315, '26', 26, 'json', '[{"model": "cms.page", "pk": 26, "fields": {"path": "000L", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:39Z", "changed_date": "2016-02-29T11:10:42.617Z", "publication_date": "2016-02-29T11:10:42.433Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [59, 60]}}]', 'Ремонт коттеджей и таунхаусов', 2, 70),
(316, '59', 59, 'json', '[{"model": "cms.placeholder", "pk": 59, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 70),
(317, '60', 60, 'json', '[{"model": "cms.placeholder", "pk": 60, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 70),
(318, '26', 26, 'json', '[{"model": "cms.title", "pk": 26, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u043e\\u0442\\u0442\\u0435\\u0434\\u0436\\u0435\\u0439 \\u0438 \\u0442\\u0430\\u0443\\u043d\\u0445\\u0430\\u0443\\u0441\\u043e\\u0432", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kottedzhej-i-taunhausov", "path": "remont-kottedzhej-i-taunhausov", "has_url_overwrite": false, "redirect": null, "page": 26, "creation_date": "2016-02-29T11:09:39Z", "published": true}}]', 'Ремонт коттеджей и таунхаусов (remont-kottedzhej-i-taunhausov, ru)', 17, 70),
(319, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 71),
(320, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T11:10:44.277Z", "publication_date": "2016-02-29T11:10:43.769Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 71),
(321, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 71),
(322, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 71),
(323, '64', 64, 'json', '[{"model": "cms.placeholder", "pk": 64, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 72),
(324, '28', 28, 'json', '[{"model": "cms.title", "pk": 28, "fields": {"language": "ru", "title": "\\u0414\\u0435\\u043a\\u043e\\u0440 \\u0434\\u043e\\u043c\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dekor-doma", "path": "dekor-doma", "has_url_overwrite": false, "redirect": null, "page": 28, "creation_date": "2016-02-29T11:10:19Z", "published": true}}]', 'Декор дома (dekor-doma, ru)', 17, 72),
(325, '28', 28, 'json', '[{"model": "cms.page", "pk": 28, "fields": {"path": "000P", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:10:19Z", "changed_date": "2016-02-29T11:10:45.390Z", "publication_date": "2016-02-29T11:10:45.201Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [63, 64]}}]', 'Декор дома', 2, 72),
(326, '63', 63, 'json', '[{"model": "cms.placeholder", "pk": 63, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 72),
(338, '64', 64, 'json', '[{"model": "cms.placeholder", "pk": 64, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 75),
(339, '33', 33, 'json', '[{"model": "cms.cmsplugin", "pk": 33, "fields": {"path": "000X", "depth": 1, "numchild": 0, "placeholder": 63, "parent": null, "position": 0, "language": "ru", "plugin_type": "BaseMenuPlugin", "creation_date": "2016-02-29T11:13:07Z", "changed_date": "2016-02-29T11:13:07Z"}}]', '33', 16, 75),
(340, '28', 28, 'json', '[{"model": "cms.page", "pk": 28, "fields": {"path": "000P", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:10:19Z", "changed_date": "2016-02-29T11:10:45Z", "publication_date": "2016-02-29T11:10:45Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [63, 64]}}]', 'Декор дома', 2, 75),
(341, '28', 28, 'json', '[{"model": "cms.title", "pk": 28, "fields": {"language": "ru", "title": "\\u0414\\u0435\\u043a\\u043e\\u0440 \\u0434\\u043e\\u043c\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dekor-doma", "path": "dekor-doma", "has_url_overwrite": false, "redirect": null, "page": 28, "creation_date": "2016-02-29T11:10:19Z", "published": true}}]', 'Декор дома (dekor-doma, ru)', 17, 75),
(342, '63', 63, 'json', '[{"model": "cms.placeholder", "pk": 63, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 75),
(343, '64', 64, 'json', '[{"model": "cms.placeholder", "pk": 64, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 76),
(344, '33', 33, 'json', '[{"model": "cms_plugins.basemenupl", "pk": 33, "fields": {"pages": [29, 30, 31, 32]}}]', '33', 56, 76),
(345, '33', 33, 'json', '[{"model": "cms.cmsplugin", "pk": 33, "fields": {"path": "000X", "depth": 1, "numchild": 0, "placeholder": 63, "parent": null, "position": 0, "language": "ru", "plugin_type": "BaseMenuPlugin", "creation_date": "2016-02-29T11:13:07Z", "changed_date": "2016-02-29T11:13:51Z"}}]', '33', 16, 76),
(346, '28', 28, 'json', '[{"model": "cms.title", "pk": 28, "fields": {"language": "ru", "title": "\\u0414\\u0435\\u043a\\u043e\\u0440 \\u0434\\u043e\\u043c\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dekor-doma", "path": "dekor-doma", "has_url_overwrite": false, "redirect": null, "page": 28, "creation_date": "2016-02-29T11:10:19Z", "published": true}}]', 'Декор дома (dekor-doma, ru)', 17, 76),
(347, '28', 28, 'json', '[{"model": "cms.page", "pk": 28, "fields": {"path": "000P", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:10:19Z", "changed_date": "2016-02-29T11:13:07Z", "publication_date": "2016-02-29T11:10:45Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [63, 64]}}]', 'Декор дома', 2, 76),
(348, '63', 63, 'json', '[{"model": "cms.placeholder", "pk": 63, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 76),
(353, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T11:15:18.931Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 78),
(354, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 78),
(355, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 78),
(356, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 78),
(357, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T11:15:52.465Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 79),
(358, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 79),
(359, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 79),
(360, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 79),
(361, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T11:15:56.636Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 80),
(362, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 80),
(363, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 80),
(364, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 80),
(365, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T11:15:59.034Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 81),
(366, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 81),
(367, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 81),
(368, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 81),
(382, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 84),
(383, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 84),
(384, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 84),
(385, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:22:22Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 84),
(386, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 84),
(387, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 84),
(388, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 84),
(389, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 84),
(390, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 85),
(391, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 85),
(392, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:22:56Z"}}]', '39', 16, 85),
(393, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 85),
(394, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:22:39Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 85),
(395, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 85),
(396, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 85),
(397, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 85),
(398, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 85),
(399, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 86),
(400, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 86),
(401, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 86),
(402, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 86),
(403, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 86),
(404, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:22:56Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 86),
(405, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 86),
(406, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 86),
(407, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 86),
(408, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 86),
(409, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 87),
(410, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 87),
(411, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 87),
(412, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:30:42Z"}}]', '40', 16, 87),
(413, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 87),
(414, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 87),
(415, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:23:13Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 87),
(416, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 87),
(417, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 87),
(418, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 87),
(419, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 87),
(420, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 88),
(421, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 88),
(422, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 88),
(423, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 88),
(424, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 88),
(425, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 88),
(426, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 88),
(427, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:30:42Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 88),
(428, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 88),
(429, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 88),
(430, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 88),
(431, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 88),
(432, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 89),
(433, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 89),
(434, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 89),
(435, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 89),
(436, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:35:21Z"}}]', '41', 16, 89),
(437, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 89),
(438, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 89),
(439, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 89),
(440, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:32:32Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 89),
(441, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 89),
(442, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 89),
(443, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 89),
(444, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 89),
(445, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 90),
(446, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 90),
(447, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 90),
(448, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 90),
(449, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 90);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(450, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 90),
(451, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 90),
(452, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 90),
(453, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:35:21Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 90),
(454, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:35:33Z"}}]', '41', 16, 90),
(455, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 90),
(456, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 90),
(457, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 90),
(458, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 90),
(459, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 91),
(460, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 91),
(461, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 91),
(462, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 91),
(463, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 91),
(464, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 91),
(465, '42', 42, 'json', '[{"model": "cms.cmsplugin", "pk": 42, "fields": {"path": "0015", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:36:35Z"}}]', '42', 16, 91),
(466, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 91),
(467, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 91),
(468, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:35:33Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 91),
(469, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:35:33Z"}}]', '41', 16, 91),
(470, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 91),
(471, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 91),
(472, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 91),
(473, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 91),
(474, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 92),
(475, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 92),
(476, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 92),
(477, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 92),
(478, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 92),
(479, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 92),
(480, '42', 42, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 42, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 92),
(481, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 92),
(482, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 92),
(483, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:36:35Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 92),
(484, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:35:33Z"}}]', '41', 16, 92),
(485, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 92),
(486, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 92),
(487, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 92),
(488, '42', 42, 'json', '[{"model": "cms.cmsplugin", "pk": 42, "fields": {"path": "0015", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:36:49Z"}}]', '42', 16, 92),
(489, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 92),
(490, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 93),
(491, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 93),
(492, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 93),
(493, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 93),
(494, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 93),
(495, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 93),
(496, '42', 42, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 42, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 93),
(497, '43', 43, 'json', '[{"model": "cms.cmsplugin", "pk": 43, "fields": {"path": "0016", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:37:02Z"}}]', '43', 16, 93),
(498, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 93),
(499, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:36:49Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 93),
(500, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 93),
(501, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:35:33Z"}}]', '41', 16, 93),
(502, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 93),
(503, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 93),
(504, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 93),
(505, '42', 42, 'json', '[{"model": "cms.cmsplugin", "pk": 42, "fields": {"path": "0015", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:36:49Z"}}]', '42', 16, 93),
(506, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 93),
(507, '43', 43, 'json', '[{"model": "cms.cmsplugin", "pk": 43, "fields": {"path": "0016", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:37:20Z"}}]', '43', 16, 94),
(508, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 94),
(509, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 94),
(510, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 94),
(511, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 94),
(512, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 94),
(513, '42', 42, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 42, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 94),
(514, '43', 43, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 43, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 94),
(515, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 94),
(516, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 94),
(517, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:37:03Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 94),
(518, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 94),
(519, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:35:33Z"}}]', '41', 16, 94),
(520, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 94),
(521, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 94),
(522, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 94),
(523, '42', 42, 'json', '[{"model": "cms.cmsplugin", "pk": 42, "fields": {"path": "0015", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:36:49Z"}}]', '42', 16, 94),
(524, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 94),
(525, '43', 43, 'json', '[{"model": "cms.cmsplugin", "pk": 43, "fields": {"path": "0016", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:37:20Z"}}]', '43', 16, 95),
(526, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 95),
(527, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 95),
(528, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 95),
(529, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 95),
(530, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 95),
(531, '42', 42, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 42, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 95),
(532, '43', 43, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 43, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 95),
(533, '44', 44, 'json', '[{"model": "cms.cmsplugin", "pk": 44, "fields": {"path": "0017", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:37:35Z"}}]', '44', 16, 95),
(534, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 95),
(535, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 95),
(536, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:37:20Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 95),
(537, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 95),
(538, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:35:33Z"}}]', '41', 16, 95),
(539, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 95),
(540, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 95),
(541, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 95),
(542, '42', 42, 'json', '[{"model": "cms.cmsplugin", "pk": 42, "fields": {"path": "0015", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:36:49Z"}}]', '42', 16, 95),
(543, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 95),
(544, '43', 43, 'json', '[{"model": "cms.cmsplugin", "pk": 43, "fields": {"path": "0016", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:37:20Z"}}]', '43', 16, 96),
(545, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 96),
(546, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 96),
(547, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 96),
(548, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 96),
(549, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 96),
(550, '42', 42, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 42, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 96),
(551, '43', 43, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 43, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 96),
(552, '44', 44, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 44, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 96),
(553, '44', 44, 'json', '[{"model": "cms.cmsplugin", "pk": 44, "fields": {"path": "0017", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:37:51Z"}}]', '44', 16, 96),
(554, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 96),
(555, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 96),
(556, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:37:35Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 96),
(557, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 96),
(558, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:35:33Z"}}]', '41', 16, 96),
(559, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 96),
(560, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 96),
(561, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 96),
(562, '42', 42, 'json', '[{"model": "cms.cmsplugin", "pk": 42, "fields": {"path": "0015", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:36:49Z"}}]', '42', 16, 96),
(563, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 96),
(564, '43', 43, 'json', '[{"model": "cms.cmsplugin", "pk": 43, "fields": {"path": "0016", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:37:20Z"}}]', '43', 16, 97);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(565, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 97),
(566, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 97),
(567, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 97),
(568, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 97),
(569, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 97),
(570, '42', 42, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 42, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 97),
(571, '43', 43, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 43, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 97),
(572, '44', 44, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 44, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 97),
(573, '44', 44, 'json', '[{"model": "cms.cmsplugin", "pk": 44, "fields": {"path": "0017", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:37:51Z"}}]', '44', 16, 97),
(574, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 97),
(575, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 97),
(576, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:38:07.553Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 97),
(577, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 97),
(578, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:35:33Z"}}]', '41', 16, 97),
(579, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 97),
(580, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 97),
(581, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 97),
(582, '42', 42, 'json', '[{"model": "cms.cmsplugin", "pk": 42, "fields": {"path": "0015", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:36:49Z"}}]', '42', 16, 97),
(583, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 97),
(584, '64', 64, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 64, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 98),
(585, '64', 64, 'json', '[{"model": "cms.cmsplugin", "pk": 64, "fields": {"path": "001R", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '64', 16, 98),
(586, '66', 66, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 66, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 98),
(587, '67', 67, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 67, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 98),
(588, '68', 68, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 68, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 98),
(589, '69', 69, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 69, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 98),
(590, '65', 65, 'json', '[{"model": "cms_plugins.calc", "pk": 65, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 98),
(591, '65', 65, 'json', '[{"model": "cms.cmsplugin", "pk": 65, "fields": {"path": "001S", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '65', 16, 98),
(592, '63', 63, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 63, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 98),
(593, '66', 66, 'json', '[{"model": "cms.cmsplugin", "pk": 66, "fields": {"path": "001T", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '66', 16, 98),
(594, '67', 67, 'json', '[{"model": "cms.cmsplugin", "pk": 67, "fields": {"path": "001U", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '67', 16, 98),
(595, '62', 62, 'json', '[{"model": "cms.cmsplugin", "pk": 62, "fields": {"path": "001P", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:40:02Z"}}]', '62', 16, 98),
(596, '69', 69, 'json', '[{"model": "cms.cmsplugin", "pk": 69, "fields": {"path": "001W", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '69', 16, 98),
(597, '68', 68, 'json', '[{"model": "cms.cmsplugin", "pk": 68, "fields": {"path": "001V", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '68', 16, 98),
(598, '26', 26, 'json', '[{"model": "cms.page", "pk": 26, "fields": {"path": "000L", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:39Z", "changed_date": "2016-02-29T11:10:42Z", "publication_date": "2016-02-29T11:10:42Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [59, 60]}}]', 'Ремонт коттеджей и таунхаусов', 2, 98),
(599, '59', 59, 'json', '[{"model": "cms.placeholder", "pk": 59, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 98),
(600, '60', 60, 'json', '[{"model": "cms.placeholder", "pk": 60, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 98),
(601, '26', 26, 'json', '[{"model": "cms.title", "pk": 26, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u043e\\u0442\\u0442\\u0435\\u0434\\u0436\\u0435\\u0439 \\u0438 \\u0442\\u0430\\u0443\\u043d\\u0445\\u0430\\u0443\\u0441\\u043e\\u0432", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kottedzhej-i-taunhausov", "path": "remont-kottedzhej-i-taunhausov", "has_url_overwrite": false, "redirect": null, "page": 26, "creation_date": "2016-02-29T11:09:39Z", "published": true}}]', 'Ремонт коттеджей и таунхаусов (remont-kottedzhej-i-taunhausov, ru)', 17, 98),
(602, '62', 62, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 62, "fields": {"project": 1}}]', '62', 57, 98),
(603, '63', 63, 'json', '[{"model": "cms.cmsplugin", "pk": 63, "fields": {"path": "001Q", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:40:02Z"}}]', '63', 16, 98),
(604, '64', 64, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 64, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 99),
(605, '64', 64, 'json', '[{"model": "cms.cmsplugin", "pk": 64, "fields": {"path": "001R", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '64', 16, 99),
(606, '66', 66, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 66, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 99),
(607, '67', 67, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 67, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 99),
(608, '68', 68, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 68, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 99),
(609, '69', 69, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 69, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 99),
(610, '65', 65, 'json', '[{"model": "cms_plugins.calc", "pk": 65, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 99),
(611, '65', 65, 'json', '[{"model": "cms.cmsplugin", "pk": 65, "fields": {"path": "001S", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '65', 16, 99),
(612, '63', 63, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 63, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 99),
(613, '66', 66, 'json', '[{"model": "cms.cmsplugin", "pk": 66, "fields": {"path": "001T", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '66', 16, 99),
(614, '67', 67, 'json', '[{"model": "cms.cmsplugin", "pk": 67, "fields": {"path": "001U", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '67', 16, 99),
(615, '62', 62, 'json', '[{"model": "cms.cmsplugin", "pk": 62, "fields": {"path": "001P", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:42:49Z"}}]', '62', 16, 99),
(616, '69', 69, 'json', '[{"model": "cms.cmsplugin", "pk": 69, "fields": {"path": "001W", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '69', 16, 99),
(617, '68', 68, 'json', '[{"model": "cms.cmsplugin", "pk": 68, "fields": {"path": "001V", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '68', 16, 99),
(618, '26', 26, 'json', '[{"model": "cms.page", "pk": 26, "fields": {"path": "000L", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:39Z", "changed_date": "2016-02-29T11:40:03Z", "publication_date": "2016-02-29T11:10:42Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [59, 60]}}]', 'Ремонт коттеджей и таунхаусов', 2, 99),
(619, '59', 59, 'json', '[{"model": "cms.placeholder", "pk": 59, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 99),
(620, '60', 60, 'json', '[{"model": "cms.placeholder", "pk": 60, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 99),
(621, '26', 26, 'json', '[{"model": "cms.title", "pk": 26, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u043e\\u0442\\u0442\\u0435\\u0434\\u0436\\u0435\\u0439 \\u0438 \\u0442\\u0430\\u0443\\u043d\\u0445\\u0430\\u0443\\u0441\\u043e\\u0432", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kottedzhej-i-taunhausov", "path": "remont-kottedzhej-i-taunhausov", "has_url_overwrite": false, "redirect": null, "page": 26, "creation_date": "2016-02-29T11:09:39Z", "published": true}}]', 'Ремонт коттеджей и таунхаусов (remont-kottedzhej-i-taunhausov, ru)', 17, 99),
(622, '62', 62, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 62, "fields": {"project": 2}}]', '62', 57, 99),
(623, '63', 63, 'json', '[{"model": "cms.cmsplugin", "pk": 63, "fields": {"path": "001Q", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:40:02Z"}}]', '63', 16, 99),
(624, '64', 64, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 64, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 100),
(625, '64', 64, 'json', '[{"model": "cms.cmsplugin", "pk": 64, "fields": {"path": "001R", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '64', 16, 100),
(626, '66', 66, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 66, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 100),
(627, '67', 67, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 67, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 100),
(628, '68', 68, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 68, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 100),
(629, '69', 69, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 69, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 100),
(630, '65', 65, 'json', '[{"model": "cms_plugins.calc", "pk": 65, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта коттеджа (таунхауса)', 54, 100),
(631, '65', 65, 'json', '[{"model": "cms.cmsplugin", "pk": 65, "fields": {"path": "001S", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:43:12Z"}}]', '65', 16, 100),
(632, '63', 63, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 63, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 100),
(633, '66', 66, 'json', '[{"model": "cms.cmsplugin", "pk": 66, "fields": {"path": "001T", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '66', 16, 100),
(634, '67', 67, 'json', '[{"model": "cms.cmsplugin", "pk": 67, "fields": {"path": "001U", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '67', 16, 100),
(635, '62', 62, 'json', '[{"model": "cms.cmsplugin", "pk": 62, "fields": {"path": "001P", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:42:49Z"}}]', '62', 16, 100),
(636, '69', 69, 'json', '[{"model": "cms.cmsplugin", "pk": 69, "fields": {"path": "001W", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '69', 16, 100),
(637, '68', 68, 'json', '[{"model": "cms.cmsplugin", "pk": 68, "fields": {"path": "001V", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '68', 16, 100),
(638, '26', 26, 'json', '[{"model": "cms.page", "pk": 26, "fields": {"path": "000L", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:39Z", "changed_date": "2016-02-29T11:42:49Z", "publication_date": "2016-02-29T11:10:42Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [59, 60]}}]', 'Ремонт коттеджей и таунхаусов', 2, 100),
(639, '59', 59, 'json', '[{"model": "cms.placeholder", "pk": 59, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 100),
(640, '60', 60, 'json', '[{"model": "cms.placeholder", "pk": 60, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 100),
(641, '26', 26, 'json', '[{"model": "cms.title", "pk": 26, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u043e\\u0442\\u0442\\u0435\\u0434\\u0436\\u0435\\u0439 \\u0438 \\u0442\\u0430\\u0443\\u043d\\u0445\\u0430\\u0443\\u0441\\u043e\\u0432", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kottedzhej-i-taunhausov", "path": "remont-kottedzhej-i-taunhausov", "has_url_overwrite": false, "redirect": null, "page": 26, "creation_date": "2016-02-29T11:09:39Z", "published": true}}]', 'Ремонт коттеджей и таунхаусов (remont-kottedzhej-i-taunhausov, ru)', 17, 100),
(642, '62', 62, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 62, "fields": {"project": 2}}]', '62', 57, 100),
(643, '63', 63, 'json', '[{"model": "cms.cmsplugin", "pk": 63, "fields": {"path": "001Q", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:40:02Z"}}]', '63', 16, 100),
(644, '64', 64, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 64, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 101),
(645, '64', 64, 'json', '[{"model": "cms.cmsplugin", "pk": 64, "fields": {"path": "001R", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '64', 16, 101),
(646, '66', 66, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 66, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#\\" class=\\"btn fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 101),
(647, '67', 67, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 67, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 101),
(648, '68', 68, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 68, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 101),
(649, '69', 69, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 69, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 101),
(650, '65', 65, 'json', '[{"model": "cms_plugins.calc", "pk": 65, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта коттеджа (таунхауса)', 54, 101),
(651, '65', 65, 'json', '[{"model": "cms.cmsplugin", "pk": 65, "fields": {"path": "001S", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:43:12Z"}}]', '65', 16, 101),
(652, '63', 63, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 63, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 101),
(653, '66', 66, 'json', '[{"model": "cms.cmsplugin", "pk": 66, "fields": {"path": "001T", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '66', 16, 101),
(654, '67', 67, 'json', '[{"model": "cms.cmsplugin", "pk": 67, "fields": {"path": "001U", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '67', 16, 101),
(655, '62', 62, 'json', '[{"model": "cms.cmsplugin", "pk": 62, "fields": {"path": "001P", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:42:49Z"}}]', '62', 16, 101),
(656, '69', 69, 'json', '[{"model": "cms.cmsplugin", "pk": 69, "fields": {"path": "001W", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '69', 16, 101),
(657, '68', 68, 'json', '[{"model": "cms.cmsplugin", "pk": 68, "fields": {"path": "001V", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '68', 16, 101),
(658, '26', 26, 'json', '[{"model": "cms.page", "pk": 26, "fields": {"path": "000L", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:39Z", "changed_date": "2016-02-29T11:43:27.264Z", "publication_date": "2016-02-29T11:10:42Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [59, 60]}}]', 'Ремонт коттеджей и таунхаусов', 2, 101),
(659, '59', 59, 'json', '[{"model": "cms.placeholder", "pk": 59, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 101),
(660, '60', 60, 'json', '[{"model": "cms.placeholder", "pk": 60, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 101),
(661, '26', 26, 'json', '[{"model": "cms.title", "pk": 26, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u043e\\u0442\\u0442\\u0435\\u0434\\u0436\\u0435\\u0439 \\u0438 \\u0442\\u0430\\u0443\\u043d\\u0445\\u0430\\u0443\\u0441\\u043e\\u0432", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kottedzhej-i-taunhausov", "path": "remont-kottedzhej-i-taunhausov", "has_url_overwrite": false, "redirect": null, "page": 26, "creation_date": "2016-02-29T11:09:39Z", "published": true}}]', 'Ремонт коттеджей и таунхаусов (remont-kottedzhej-i-taunhausov, ru)', 17, 101),
(662, '62', 62, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 62, "fields": {"project": 2}}]', '62', 57, 101),
(663, '63', 63, 'json', '[{"model": "cms.cmsplugin", "pk": 63, "fields": {"path": "001Q", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:40:02Z"}}]', '63', 16, 101),
(688, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": false, "all_type": false, "html": "<button class=\\"btn anim fw600 fll\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 106),
(689, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 106),
(690, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 106),
(691, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:07:00Z"}}]', '80', 16, 106),
(692, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 106),
(693, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:05:41Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 106),
(694, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 106),
(695, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 106);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(696, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": false, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 107),
(697, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 107),
(698, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 107),
(699, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:07:33Z"}}]', '80', 16, 107),
(700, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 107),
(701, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:07:01Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 107),
(702, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 107),
(703, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 107),
(704, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 108),
(705, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 108),
(706, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 108),
(707, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 108),
(708, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 108),
(709, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:07:33Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 108),
(710, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 108),
(711, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 108),
(712, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 109),
(713, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 109),
(714, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 109),
(715, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 109),
(716, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:11:31Z"}}]', '81', 16, 109),
(717, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 109),
(718, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:08:58Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 109),
(719, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 109),
(720, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 109),
(721, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 110),
(722, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 110),
(723, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [5, 8, 9, 10]}}]', 'Вот почему Вы выбираете нас', 49, 110),
(724, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 110),
(725, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 110),
(726, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:12:38Z"}}]', '81', 16, 110),
(727, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 110),
(728, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:11:32Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 110),
(729, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 110),
(730, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 110),
(731, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 111),
(732, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 111),
(733, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 111),
(734, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 111),
(735, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 111),
(736, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:12:38Z"}}]', '81', 16, 111),
(737, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:13:49Z"}}]', '82', 16, 111),
(738, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 111),
(739, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:12:38Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 111),
(740, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 111),
(741, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 111),
(742, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 112),
(743, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 112),
(744, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 112),
(745, '82', 82, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 82, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 112),
(746, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 112),
(747, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 112),
(748, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:12:38Z"}}]', '81', 16, 112),
(749, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:14:15Z"}}]', '82', 16, 112),
(750, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 112),
(751, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:13:49Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 112),
(752, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 112),
(753, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 112),
(754, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 113),
(755, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 113),
(756, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 113),
(757, '82', 82, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 82, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 113),
(758, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 113),
(759, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 113),
(760, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:15:02Z"}}]', '81', 16, 113),
(761, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:14:15Z"}}]', '82', 16, 113),
(762, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 113),
(763, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:14:15Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 113),
(764, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 113),
(765, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 113),
(766, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 114),
(767, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 114),
(768, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 114),
(769, '82', 82, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 82, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 114),
(770, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 114),
(771, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 114),
(772, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:15:02Z"}}]', '81', 16, 114),
(773, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:14:15Z"}}]', '82', 16, 114),
(774, '83', 83, 'json', '[{"model": "cms.cmsplugin", "pk": 83, "fields": {"path": "0029", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:15:16Z", "changed_date": "2016-02-29T12:15:16Z"}}]', '83', 16, 114),
(775, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 114),
(776, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:15:02Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 114),
(777, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 114),
(778, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 114),
(779, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 115),
(780, '83', 83, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 83, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 115),
(781, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 115),
(782, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 115),
(783, '82', 82, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 82, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 115),
(784, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 115),
(785, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 115),
(786, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:15:02Z"}}]', '81', 16, 115),
(787, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:14:15Z"}}]', '82', 16, 115),
(788, '83', 83, 'json', '[{"model": "cms.cmsplugin", "pk": 83, "fields": {"path": "0029", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:15:16Z", "changed_date": "2016-02-29T12:15:29Z"}}]', '83', 16, 115),
(789, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 115),
(790, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:15:17Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 115),
(791, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 115),
(792, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 115),
(793, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 116),
(794, '83', 83, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 83, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 116),
(795, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 116),
(796, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 116),
(797, '82', 82, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 82, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 116),
(798, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 116),
(799, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 116),
(800, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:15:02Z"}}]', '81', 16, 116),
(801, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:14:15Z"}}]', '82', 16, 116),
(802, '83', 83, 'json', '[{"model": "cms.cmsplugin", "pk": 83, "fields": {"path": "0029", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:15:16Z", "changed_date": "2016-02-29T12:15:29Z"}}]', '83', 16, 116),
(803, '84', 84, 'json', '[{"model": "cms.cmsplugin", "pk": 84, "fields": {"path": "002A", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:15:45Z", "changed_date": "2016-02-29T12:15:45Z"}}]', '84', 16, 116),
(804, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 116),
(805, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:15:29Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 116),
(806, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 116),
(807, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 116),
(808, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 117),
(809, '83', 83, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 83, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 117),
(810, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 117),
(811, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 117),
(812, '82', 82, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 82, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 117),
(813, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 117),
(814, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 117),
(815, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:15:02Z"}}]', '81', 16, 117);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(816, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:14:15Z"}}]', '82', 16, 117),
(817, '83', 83, 'json', '[{"model": "cms.cmsplugin", "pk": 83, "fields": {"path": "0029", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:15:16Z", "changed_date": "2016-02-29T12:15:29Z"}}]', '83', 16, 117),
(818, '84', 84, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 84, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 117),
(819, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 117),
(820, '84', 84, 'json', '[{"model": "cms.cmsplugin", "pk": 84, "fields": {"path": "002A", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:15:45Z", "changed_date": "2016-02-29T12:15:57Z"}}]', '84', 16, 117),
(821, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:15:45Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 117),
(822, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 117),
(823, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 117),
(824, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 118),
(825, '83', 83, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 83, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 118),
(826, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 118),
(827, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 118),
(828, '82', 82, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 82, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 118),
(829, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 118),
(830, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 118),
(831, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:15:02Z"}}]', '81', 16, 118),
(832, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:14:15Z"}}]', '82', 16, 118),
(833, '83', 83, 'json', '[{"model": "cms.cmsplugin", "pk": 83, "fields": {"path": "0029", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:15:16Z", "changed_date": "2016-02-29T12:15:29Z"}}]', '83', 16, 118),
(834, '84', 84, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 84, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 118),
(835, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 118),
(836, '84', 84, 'json', '[{"model": "cms.cmsplugin", "pk": 84, "fields": {"path": "002A", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:15:45Z", "changed_date": "2016-02-29T12:15:57Z"}}]', '84', 16, 118),
(837, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:15:57Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 118),
(838, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 118),
(839, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 118),
(840, '85', 85, 'json', '[{"model": "cms.cmsplugin", "pk": 85, "fields": {"path": "002B", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:16:14Z", "changed_date": "2016-02-29T12:16:15Z"}}]', '85', 16, 118),
(841, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 119),
(842, '83', 83, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 83, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 119),
(843, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 119),
(844, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 119),
(845, '82', 82, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 82, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 119),
(846, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 119),
(847, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 119),
(848, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:15:02Z"}}]', '81', 16, 119),
(849, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:14:15Z"}}]', '82', 16, 119),
(850, '83', 83, 'json', '[{"model": "cms.cmsplugin", "pk": 83, "fields": {"path": "0029", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:15:16Z", "changed_date": "2016-02-29T12:15:29Z"}}]', '83', 16, 119),
(851, '84', 84, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 84, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 119),
(852, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 119),
(853, '85', 85, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 85, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 119),
(854, '84', 84, 'json', '[{"model": "cms.cmsplugin", "pk": 84, "fields": {"path": "002A", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:15:45Z", "changed_date": "2016-02-29T12:15:57Z"}}]', '84', 16, 119),
(855, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:16:15Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 119),
(856, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 119),
(857, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 119),
(858, '85', 85, 'json', '[{"model": "cms.cmsplugin", "pk": 85, "fields": {"path": "002B", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:16:14Z", "changed_date": "2016-02-29T12:16:26Z"}}]', '85', 16, 119),
(859, '80', 80, 'json', '[{"model": "cms_plugins.calc", "pk": 80, "fields": {"design": true, "all_type": false, "html": "<button class=\\"btn anim fw600 fll fancy\\" href=\\"#callback\\" type=\\"submit\\">\\u041f\\u043e\\u043b\\u0443\\u0447\\u0438\\u0442\\u044c \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u043e</button>\\r\\n\\r\\n                            <div class=\\"call fll\\">\\r\\n                                <span class=\\"fw600\\">\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n                                <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-red\\">+7 (495) 517-75-28</a>\\r\\n                            </div>"}}]', 'Калькулятор стоимости дизайн проекта', 54, 120),
(860, '83', 83, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 83, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 120),
(861, '27', 27, 'json', '[{"model": "cms.title", "pk": 27, "fields": {"language": "ru", "title": "\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dizajn-proekty", "path": "dizajn-proekty", "has_url_overwrite": false, "redirect": null, "page": 27, "creation_date": "2016-02-29T11:09:59Z", "published": true}}]', 'Дизайн проекты (dizajn-proekty, ru)', 17, 120),
(862, '81', 81, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 81, "fields": {"teasers": [9, 10, 5, 8]}}]', 'Вот почему Вы выбираете нас', 49, 120),
(863, '82', 82, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 82, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 120),
(864, '78', 78, 'json', '[{"model": "cms.cmsplugin", "pk": 78, "fields": {"path": "0025", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 0, "language": "ru", "plugin_type": "DesignPlugin", "creation_date": "2016-02-29T11:48:11Z", "changed_date": "2016-02-29T11:58:11Z"}}]', '78', 16, 120),
(865, '80', 80, 'json', '[{"model": "cms.cmsplugin", "pk": 80, "fields": {"path": "0026", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 1, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:05:41Z", "changed_date": "2016-02-29T12:08:58Z"}}]', '80', 16, 120),
(866, '81', 81, 'json', '[{"model": "cms.cmsplugin", "pk": 81, "fields": {"path": "0027", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:11:31Z", "changed_date": "2016-02-29T12:15:02Z"}}]', '81', 16, 120),
(867, '82', 82, 'json', '[{"model": "cms.cmsplugin", "pk": 82, "fields": {"path": "0028", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:13:49Z", "changed_date": "2016-02-29T12:14:15Z"}}]', '82', 16, 120),
(868, '83', 83, 'json', '[{"model": "cms.cmsplugin", "pk": 83, "fields": {"path": "0029", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:15:16Z", "changed_date": "2016-02-29T12:15:29Z"}}]', '83', 16, 120),
(869, '84', 84, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 84, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 120),
(870, '78', 78, 'json', '[{"model": "cms_plugins.designpluginmodel", "pk": 78, "fields": {"design": [1, 2, 3]}}]', 'Блок 78', 59, 120),
(871, '85', 85, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 85, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 120),
(872, '84', 84, 'json', '[{"model": "cms.cmsplugin", "pk": 84, "fields": {"path": "002A", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:15:45Z", "changed_date": "2016-02-29T12:15:57Z"}}]', '84', 16, 120),
(873, '27', 27, 'json', '[{"model": "cms.page", "pk": 27, "fields": {"path": "000N", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:59Z", "changed_date": "2016-02-29T12:16:51.374Z", "publication_date": "2016-02-29T11:10:43Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [61, 62]}}]', 'Дизайн проекты', 2, 120),
(874, '61', 61, 'json', '[{"model": "cms.placeholder", "pk": 61, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 120),
(875, '62', 62, 'json', '[{"model": "cms.placeholder", "pk": 62, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 120),
(876, '85', 85, 'json', '[{"model": "cms.cmsplugin", "pk": 85, "fields": {"path": "002B", "depth": 1, "numchild": 0, "placeholder": 62, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:16:14Z", "changed_date": "2016-02-29T12:16:26Z"}}]', '85', 16, 120),
(877, '33', 33, 'json', '[{"model": "cms.page", "pk": 33, "fields": {"path": "000R", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T12:23:48.280Z", "changed_date": "2016-02-29T12:23:48.335Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [77, 78]}}]', 'Вопросы и ответы', 2, 121),
(878, '77', 77, 'json', '[{"model": "cms.placeholder", "pk": 77, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 121),
(879, '78', 78, 'json', '[{"model": "cms.placeholder", "pk": 78, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 121),
(880, '33', 33, 'json', '[{"model": "cms.title", "pk": 33, "fields": {"language": "ru", "title": "\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b \\u0438 \\u043e\\u0442\\u0432\\u0435\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "voprosy-i-otvety", "path": "voprosy-i-otvety", "has_url_overwrite": false, "redirect": null, "page": 33, "creation_date": "2016-02-29T12:23:48Z", "published": false}}]', 'Вопросы и ответы (voprosy-i-otvety, ru)', 17, 121),
(881, '33', 33, 'json', '[{"model": "cms.page", "pk": 33, "fields": {"path": "000R", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T12:23:48Z", "changed_date": "2016-02-29T12:24:11.107Z", "publication_date": null, "publication_end_date": null, "in_navigation": true, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "QuestApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [77, 78]}}]', 'Вопросы и ответы', 2, 122),
(882, '77', 77, 'json', '[{"model": "cms.placeholder", "pk": 77, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 122),
(883, '78', 78, 'json', '[{"model": "cms.placeholder", "pk": 78, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 122),
(884, '33', 33, 'json', '[{"model": "cms.title", "pk": 33, "fields": {"language": "ru", "title": "\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b \\u0438 \\u043e\\u0442\\u0432\\u0435\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "voprosy-i-otvety", "path": "voprosy-i-otvety", "has_url_overwrite": false, "redirect": "", "page": 33, "creation_date": "2016-02-29T12:23:48Z", "published": false}}]', 'Вопросы и ответы (voprosy-i-otvety, ru)', 17, 122),
(885, '64', 64, 'json', '[{"model": "cms.placeholder", "pk": 64, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 123),
(886, '33', 33, 'json', '[{"model": "cms_plugins.basemenupl", "pk": 33, "fields": {"pages": [29, 30, 31, 32]}}]', '33', 56, 123),
(887, '33', 33, 'json', '[{"model": "cms.cmsplugin", "pk": 33, "fields": {"path": "000X", "depth": 1, "numchild": 0, "placeholder": 63, "parent": null, "position": 0, "language": "ru", "plugin_type": "BaseMenuPlugin", "creation_date": "2016-02-29T11:13:07Z", "changed_date": "2016-02-29T11:13:51Z"}}]', '33', 16, 123),
(888, '28', 28, 'json', '[{"model": "cms.title", "pk": 28, "fields": {"language": "ru", "title": "\\u0414\\u0435\\u043a\\u043e\\u0440 \\u0434\\u043e\\u043c\\u0430", "page_title": "", "menu_title": "", "meta_description": "", "slug": "dekor-doma", "path": "dekor-doma", "has_url_overwrite": false, "redirect": null, "page": 28, "creation_date": "2016-02-29T11:10:19Z", "published": true}}]', 'Декор дома (dekor-doma, ru)', 17, 123),
(889, '28', 28, 'json', '[{"model": "cms.page", "pk": 28, "fields": {"path": "000P", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:10:19Z", "changed_date": "2016-02-29T12:24:22.773Z", "publication_date": "2016-02-29T11:10:45Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [63, 64]}}]', 'Декор дома', 2, 123),
(890, '63', 63, 'json', '[{"model": "cms.placeholder", "pk": 63, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 123),
(891, '33', 33, 'json', '[{"model": "cms.page", "pk": 33, "fields": {"path": "000R", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T12:23:48Z", "changed_date": "2016-02-29T12:24:26.181Z", "publication_date": "2016-02-29T12:24:25.474Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": "", "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": "QuestApp", "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [77, 78]}}]', 'Вопросы и ответы', 2, 124),
(892, '77', 77, 'json', '[{"model": "cms.placeholder", "pk": 77, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 124),
(893, '78', 78, 'json', '[{"model": "cms.placeholder", "pk": 78, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 124),
(894, '33', 33, 'json', '[{"model": "cms.title", "pk": 33, "fields": {"language": "ru", "title": "\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b \\u0438 \\u043e\\u0442\\u0432\\u0435\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "voprosy-i-otvety", "path": "voprosy-i-otvety", "has_url_overwrite": false, "redirect": "", "page": 33, "creation_date": "2016-02-29T12:23:48Z", "published": true}}]', 'Вопросы и ответы (voprosy-i-otvety, ru)', 17, 124),
(921, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:22Z"}}]', '96', 16, 129),
(922, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:30:08Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 129),
(923, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 129),
(924, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 129),
(925, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 129),
(926, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 129),
(927, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 129),
(928, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 129),
(929, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 129),
(930, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 130),
(931, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:30:22Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 130),
(932, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 130),
(933, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 130),
(934, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 130),
(935, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 130),
(936, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 130),
(937, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 130),
(938, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 130),
(939, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 130),
(940, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 131),
(941, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:30:57Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 131),
(942, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:10Z"}}]', '97', 16, 131),
(943, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 131),
(944, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 131),
(945, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 131),
(946, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 131),
(947, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 131),
(948, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 131),
(949, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 131),
(950, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 131),
(951, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 132),
(952, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:31:11Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 132),
(953, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 132),
(954, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 132),
(955, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 132),
(956, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 132),
(957, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 132),
(958, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 132),
(959, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 132),
(960, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 132),
(961, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 132),
(962, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 132),
(963, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 133);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(964, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:31:30Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 133),
(965, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:45Z"}}]', '98', 16, 133),
(966, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 133),
(967, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 133),
(968, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 133),
(969, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 133),
(970, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 133),
(971, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 133),
(972, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 133),
(973, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 133),
(974, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 133),
(975, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 133),
(976, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 134),
(977, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:31:45Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 134),
(978, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 134),
(979, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 134),
(980, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 134),
(981, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 134),
(982, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 134),
(983, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 134),
(984, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 134),
(985, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 134),
(986, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 134),
(987, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 134),
(988, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 134),
(989, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 134),
(990, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 135),
(991, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:31:58Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 135),
(992, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 135),
(993, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:10Z"}}]', '99', 16, 135),
(994, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 135),
(995, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 135),
(996, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 135),
(997, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 135),
(998, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 135),
(999, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 135),
(1000, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 135),
(1001, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 135),
(1002, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 135),
(1003, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 135),
(1004, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 135),
(1005, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 136),
(1006, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:32:10Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 136),
(1007, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 136),
(1008, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 136),
(1009, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 136),
(1010, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 136),
(1011, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 136),
(1012, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 136),
(1013, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 136),
(1014, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 136),
(1015, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 136),
(1016, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 136),
(1017, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 136),
(1018, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 136),
(1019, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 136),
(1020, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 136),
(1021, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 137),
(1022, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:32:29Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 137),
(1023, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 137),
(1024, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 137),
(1025, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 137),
(1026, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 137),
(1027, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 137),
(1028, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 137),
(1029, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 137),
(1030, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 137),
(1031, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 137),
(1032, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 137),
(1033, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 137),
(1034, '100', 100, 'json', '[{"model": "cms.cmsplugin", "pk": 100, "fields": {"path": "002Q", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:32:42Z", "changed_date": "2016-02-29T12:32:42Z"}}]', '100', 16, 137),
(1035, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 137),
(1036, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 137),
(1037, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 137),
(1038, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 138),
(1039, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:32:42Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 138),
(1040, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 138),
(1041, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 138),
(1042, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 138),
(1043, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 138),
(1044, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 138),
(1045, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 138),
(1046, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 138),
(1047, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 138),
(1048, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 138),
(1049, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 138),
(1050, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 138),
(1051, '100', 100, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 100, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 138),
(1052, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 138),
(1053, '100', 100, 'json', '[{"model": "cms.cmsplugin", "pk": 100, "fields": {"path": "002Q", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:32:42Z", "changed_date": "2016-02-29T12:32:54Z"}}]', '100', 16, 138),
(1054, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 138),
(1055, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 138),
(1056, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 139),
(1057, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:33:44.157Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 139),
(1058, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 139),
(1059, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 139),
(1060, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 139),
(1061, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 139),
(1062, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 139),
(1063, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 139),
(1064, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 139),
(1065, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 139),
(1066, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 139),
(1067, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 139),
(1068, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 139),
(1069, '100', 100, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 100, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 139),
(1070, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 139),
(1071, '100', 100, 'json', '[{"model": "cms.cmsplugin", "pk": 100, "fields": {"path": "002Q", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:32:42Z", "changed_date": "2016-02-29T12:32:54Z"}}]', '100', 16, 139),
(1072, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 139),
(1073, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 139),
(1074, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 140);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(1075, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-02-29T12:33:44Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 140),
(1076, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 140),
(1077, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 140),
(1078, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 140),
(1079, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 140),
(1080, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 140),
(1081, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 140),
(1082, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 140),
(1083, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 140),
(1084, '108', 108, 'json', '[{"model": "cms.cmsplugin", "pk": 108, "fields": {"path": "002Y", "depth": 1, "numchild": 0, "placeholder": 6, "parent": null, "position": 0, "language": "ru", "plugin_type": "SliderPlugin", "creation_date": "2016-03-01T10:47:44Z", "changed_date": "2016-03-01T10:47:44Z"}}]', '108', 16, 140),
(1085, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 140),
(1086, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 140),
(1087, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 140),
(1088, '100', 100, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 100, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 140),
(1089, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 140),
(1090, '100', 100, 'json', '[{"model": "cms.cmsplugin", "pk": 100, "fields": {"path": "002Q", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:32:42Z", "changed_date": "2016-02-29T12:32:54Z"}}]', '100', 16, 140),
(1091, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 140),
(1092, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 140),
(1093, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 141),
(1094, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-03-01T10:47:57.044Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 141),
(1095, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 141),
(1096, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 141),
(1097, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 141),
(1098, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 141),
(1099, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 141),
(1100, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 141),
(1101, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 141),
(1102, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 141),
(1103, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 141),
(1104, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 141),
(1105, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 141),
(1106, '100', 100, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 100, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 141),
(1107, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 141),
(1108, '100', 100, 'json', '[{"model": "cms.cmsplugin", "pk": 100, "fields": {"path": "002Q", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:32:42Z", "changed_date": "2016-02-29T12:32:54Z"}}]', '100', 16, 141),
(1109, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 141),
(1110, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 141),
(1111, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 142),
(1112, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-03-01T10:47:57Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 142),
(1113, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 142),
(1114, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 142),
(1115, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 142),
(1116, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 142),
(1117, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 142),
(1118, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 142),
(1119, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 142),
(1120, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 142),
(1121, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 142),
(1122, '109', 109, 'json', '[{"model": "cms.cmsplugin", "pk": 109, "fields": {"path": "002Y", "depth": 1, "numchild": 0, "placeholder": 6, "parent": null, "position": 0, "language": "ru", "plugin_type": "SliderPlugin", "creation_date": "2016-03-01T10:50:27Z", "changed_date": "2016-03-01T10:50:27Z"}}]', '109', 16, 142),
(1123, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 142),
(1124, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 142),
(1125, '100', 100, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 100, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 142),
(1126, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 142),
(1127, '100', 100, 'json', '[{"model": "cms.cmsplugin", "pk": 100, "fields": {"path": "002Q", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:32:42Z", "changed_date": "2016-02-29T12:32:54Z"}}]', '100', 16, 142),
(1128, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 142),
(1129, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 142),
(1130, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 143),
(1131, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-03-01T10:50:27Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 143),
(1132, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 143),
(1133, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 143),
(1134, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 143),
(1135, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 143),
(1136, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 143),
(1137, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 143),
(1138, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 143),
(1139, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 143),
(1140, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 143),
(1141, '109', 109, 'json', '[{"model": "cms.cmsplugin", "pk": 109, "fields": {"path": "002Y", "depth": 1, "numchild": 0, "placeholder": 6, "parent": null, "position": 0, "language": "ru", "plugin_type": "SliderPlugin", "creation_date": "2016-03-01T10:50:27Z", "changed_date": "2016-03-01T10:50:41Z"}}]', '109', 16, 143),
(1142, '109', 109, 'json', '[{"model": "cms_plugins.sliderpluginmodel", "pk": 109, "fields": {"sliders": [1, 2, 3]}}]', '109', 61, 143),
(1143, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 143),
(1144, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 143),
(1145, '100', 100, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 100, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 143),
(1146, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 143),
(1147, '100', 100, 'json', '[{"model": "cms.cmsplugin", "pk": 100, "fields": {"path": "002Q", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:32:42Z", "changed_date": "2016-02-29T12:32:54Z"}}]', '100', 16, 143),
(1148, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 143),
(1149, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 143),
(1150, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 144),
(1151, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-03-01T10:50:55.667Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 144),
(1152, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 144),
(1153, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 144),
(1154, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 144),
(1155, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 144),
(1156, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 144),
(1157, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 144),
(1158, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 144),
(1159, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 144),
(1160, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 144),
(1161, '109', 109, 'json', '[{"model": "cms.cmsplugin", "pk": 109, "fields": {"path": "002Y", "depth": 1, "numchild": 0, "placeholder": 6, "parent": null, "position": 0, "language": "ru", "plugin_type": "SliderPlugin", "creation_date": "2016-03-01T10:50:27Z", "changed_date": "2016-03-01T10:50:41Z"}}]', '109', 16, 144),
(1162, '109', 109, 'json', '[{"model": "cms_plugins.sliderpluginmodel", "pk": 109, "fields": {"sliders": [1, 2, 3]}}]', '109', 61, 144),
(1163, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 144),
(1164, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 144),
(1165, '100', 100, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 100, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 144),
(1166, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 144),
(1167, '100', 100, 'json', '[{"model": "cms.cmsplugin", "pk": 100, "fields": {"path": "002Q", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:32:42Z", "changed_date": "2016-02-29T12:32:54Z"}}]', '100', 16, 144),
(1168, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 144),
(1169, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 144),
(1170, '7', 7, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 7, "fields": {"name": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "html": "<div class=\\"contacts\\">\\r\\n            <div class=\\"container\\">\\r\\n                <h1>\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b</h1>\\r\\n\\r\\n                <a href=\\"#\\" class=\\"print anim c-red flr\\">\\u0420\\u0430\\u0441\\u043f\\u0435\\u0447\\u0430\\u0442\\u0430\\u0442\\u044c</a>\\r\\n\\r\\n                <span class=\\"fw600\\">\\r\\n                    \\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\"\\u041d\\u0438\\u043a\\u0441\\u0442\\u0435\\u0441\\u0442 \\u0441\\u0442\\u0440\\u043e\\u0439\\"\\r\\n                </span>\\r\\n\\r\\n                <div class=\\"block fll\\">\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0410\\u0434\\u0440\\u0435\\u0441:</label>\\r\\n\\r\\n                        <p>107113, \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u0421\\u043e\\u043a\\u043e\\u043b\\u044c\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043b., \\u0434. 4\\u0410</p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d:</label>\\r\\n\\r\\n                        <p>+7 (495) 517-75-28</p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">E-mail:</label>\\r\\n\\r\\n                        <p>\\r\\n                            <a href=\\"mailto:clients@remont-nts.ru\\" class=\\"c-red\\">clients@remont-nts.ru</a>\\r\\n                            <br/><a href=\\"mailto:offers@remont-nts.ru\\" class=\\"c-red\\">offers@remont-nts.ru</a> (\\u043f\\u043e\\r\\n                            \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u0430\\u043c \\u0441\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430)\\r\\n                        </p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0412\\u0440\\u0435\\u043c\\u044f \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b:</label>\\r\\n\\r\\n                        <p>\\r\\n                            <span class=\\"one\\">\\u043f\\u043d\\u2013\\u043f\\u0442</span> <span>9:00\\u201320:00</span>\\r\\n                            <br/><span class=\\"one\\">\\u0441\\u0431</span> <span>10:00\\u201314:00</span>\\r\\n                            <br/><span class=\\"one\\">\\u0432\\u0441</span> <span>\\u0432\\u044b\\u0445\\u043e\\u0434\\u043d\\u043e\\u0439</span>\\r\\n                        </p>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n\\r\\n                <div class=\\"block flr\\">\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0420\\u0435\\u043a\\u0432\\u0438\\u0437\\u0438\\u0442\\u044b:</label>\\r\\n\\r\\n                        <p>\\u041e\\u041e\\u041e \\u00ab\\u041d\\u0438\\u043a\\u0421-\\u0422\\u0435\\u0441\\u0442\\u00bb\\r\\n                            <br/>107113, \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u0421\\u043e\\u043a\\u043e\\u043b\\u044c\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043b., \\u0434. 4\\u0410\\r\\n                            <br/>\\u0418\\u041d\\u041d 7718768715\\r\\n                            <br/>\\u041a\\u041f\\u041f 771801001\\r\\n                            <br/>\\u041e\\u0413\\u0420\\u041d 1097746389751\\r\\n                            <br/>\\u041e\\u0410\\u041e \\u0421\\u0431\\u0435\\u0440\\u0431\\u0430\\u043d\\u043a \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430\\r\\n                            <br/>\\u041a/\\u0441 30101810400000000225\\r\\n                            <br/>\\u0411\\u0418\\u041a 044525225\\r\\n                            <br/>\\u0420/\\u0441 40702810438120061789</p>\\r\\n                    </div>\\r\\n                </div>\\r\\n\\r\\n                <div class=\\"clr\\"></div>\\r\\n\\r\\n                <span class=\\"fw600\\">\\u0420\\u0430\\u0441\\u043f\\u043e\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043d\\u0430 \\u043a\\u0430\\u0440\\u0442\\u0435</span>\\r\\n\\r\\n                <div id=\\"map\\">\\r\\n\\r\\n                </div>\\r\\n\\r\\n            </div>\\r\\n        </div>\\r\\n<script src=\\"http://api-maps.yandex.ru/2.1/?lang=ru_RU\\" type=\\"text/javascript\\"></script>\\r\\n<script>\\r\\n    var map;\\r\\n\\r\\n    ymaps.ready(init);\\r\\n\\r\\n    function init() {\\r\\n        map = new ymaps.Map(''map'', {\\r\\n            center: [55.698345, 37.625522],\\r\\n            zoom: 16,\\r\\n            controls: [''zoomControl''],\\r\\n            behaviors: [''drag'']\\r\\n        });\\r\\n\\r\\n\\r\\n        mark = new ymaps.Placemark([55.698345, 37.625522], {\\r\\n            balloonContentFooter: \\"\\",\\r\\n            hintContent: \\"\\u041c\\u044b \\u0437\\u0434\\u0435\\u0441\\u044c\\"\\r\\n        });\\r\\n\\r\\n        map.geoObjects.add(mark);\\r\\n    }\\r\\n</script>"}}]', 'Контакты', 30, 145),
(1171, '7', 7, 'json', '[{"model": "cms.cmsplugin", "pk": 7, "fields": {"path": "0007", "depth": 1, "numchild": 0, "placeholder": 23, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T08:22:31Z", "changed_date": "2016-02-18T08:23:27Z"}}]', '7', 16, 145),
(1172, '21', 21, 'json', '[{"model": "cms.title", "pk": 21, "fields": {"language": "ru", "title": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "contacts", "path": "contacts", "has_url_overwrite": false, "redirect": null, "page": 21, "creation_date": "2016-02-18T08:21:45Z", "published": true}}]', 'Контакты (contacts, ru)', 17, 145),
(1173, '22', 22, 'json', '[{"model": "cms.placeholder", "pk": 22, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 145),
(1174, '23', 23, 'json', '[{"model": "cms.placeholder", "pk": 23, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 145);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(1175, '21', 21, 'json', '[{"model": "cms.page", "pk": 21, "fields": {"path": "000F", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T08:21:45Z", "changed_date": "2016-03-01T11:05:57.822Z", "publication_date": "2016-02-18T08:21:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [22, 23]}}]', 'Контакты', 2, 145),
(1176, '7', 7, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 7, "fields": {"name": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "html": "<div class=\\"contacts\\">\\r\\n            <div class=\\"container\\">\\r\\n                <h1>\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b</h1>\\r\\n\\r\\n                <a href=\\"#\\" class=\\"print anim c-red flr\\">\\u0420\\u0430\\u0441\\u043f\\u0435\\u0447\\u0430\\u0442\\u0430\\u0442\\u044c</a>\\r\\n\\r\\n                <span class=\\"fw600\\">\\r\\n                    \\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\"\\u041d\\u0438\\u043a\\u0441\\u0442\\u0435\\u0441\\u0442 \\u0441\\u0442\\u0440\\u043e\\u0439\\"\\r\\n                </span>\\r\\n\\r\\n                <div class=\\"block fll\\">\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0410\\u0434\\u0440\\u0435\\u0441:</label>\\r\\n\\r\\n                        <p>107113, \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u0421\\u043e\\u043a\\u043e\\u043b\\u044c\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043b., \\u0434. 4\\u0410</p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d:</label>\\r\\n\\r\\n                        <p>+7 (495) 517-75-28</p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">E-mail:</label>\\r\\n\\r\\n                        <p>\\r\\n                            <a href=\\"mailto:clients@remont-nts.ru\\" class=\\"c-red\\">clients@remont-nts.ru</a>\\r\\n                            <br/><a href=\\"mailto:offers@remont-nts.ru\\" class=\\"c-red\\">offers@remont-nts.ru</a> (\\u043f\\u043e\\r\\n                            \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u0430\\u043c \\u0441\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430)\\r\\n                        </p>\\r\\n                    </div>\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0412\\u0440\\u0435\\u043c\\u044f \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b:</label>\\r\\n\\r\\n                        <p>\\r\\n                            <span class=\\"one\\">\\u043f\\u043d\\u2013\\u043f\\u0442</span> <span>9:00\\u201320:00</span>\\r\\n                            <br/><span class=\\"one\\">\\u0441\\u0431</span> <span>10:00\\u201314:00</span>\\r\\n                            <br/><span class=\\"one\\">\\u0432\\u0441</span> <span>\\u0432\\u044b\\u0445\\u043e\\u0434\\u043d\\u043e\\u0439</span>\\r\\n                        </p>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n\\r\\n                <div class=\\"block flr\\">\\r\\n                    <div class=\\"item\\">\\r\\n                        <label class=\\"fll\\">\\u0420\\u0435\\u043a\\u0432\\u0438\\u0437\\u0438\\u0442\\u044b:</label>\\r\\n\\r\\n                        <p>\\u041e\\u041e\\u041e \\u00ab\\u041d\\u0438\\u043a\\u0421-\\u0422\\u0435\\u0441\\u0442\\u00bb\\r\\n                            <br/>107113, \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u0443\\u043b. \\u0421\\u043e\\u043a\\u043e\\u043b\\u044c\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043b., \\u0434. 4\\u0410\\r\\n                            <br/>\\u0418\\u041d\\u041d 7718768715\\r\\n                            <br/>\\u041a\\u041f\\u041f 771801001\\r\\n                            <br/>\\u041e\\u0413\\u0420\\u041d 1097746389751\\r\\n                            <br/>\\u041e\\u0410\\u041e \\u0421\\u0431\\u0435\\u0440\\u0431\\u0430\\u043d\\u043a \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430\\r\\n                            <br/>\\u041a/\\u0441 30101810400000000225\\r\\n                            <br/>\\u0411\\u0418\\u041a 044525225\\r\\n                            <br/>\\u0420/\\u0441 40702810438120061789</p>\\r\\n                    </div>\\r\\n                </div>\\r\\n\\r\\n                <div class=\\"clr\\"></div>\\r\\n\\r\\n                <span class=\\"fw600\\">\\u0420\\u0430\\u0441\\u043f\\u043e\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043d\\u0430 \\u043a\\u0430\\u0440\\u0442\\u0435</span>\\r\\n\\r\\n                <div id=\\"map\\">\\r\\n\\r\\n                </div>\\r\\n\\r\\n            </div>\\r\\n        </div>\\r\\n<script src=\\"http://api-maps.yandex.ru/2.1/?lang=ru_RU\\" type=\\"text/javascript\\"></script>\\r\\n<script>\\r\\n    var map;\\r\\n\\r\\n    ymaps.ready(init);\\r\\n\\r\\n    function init() {\\r\\n        map = new ymaps.Map(''map'', {\\r\\n            center: [55.698345, 37.625522],\\r\\n            zoom: 16,\\r\\n            controls: [''zoomControl''],\\r\\n            behaviors: [''drag'']\\r\\n        });\\r\\n\\r\\n\\r\\n        mark = new ymaps.Placemark([55.698345, 37.625522], {\\r\\n            balloonContentFooter: \\"\\",\\r\\n            hintContent: \\"\\u041c\\u044b \\u0437\\u0434\\u0435\\u0441\\u044c\\"\\r\\n        });\\r\\n\\r\\n        map.geoObjects.add(mark);\\r\\n    }\\r\\n</script>"}}]', 'Контакты', 30, 146),
(1177, '7', 7, 'json', '[{"model": "cms.cmsplugin", "pk": 7, "fields": {"path": "0007", "depth": 1, "numchild": 0, "placeholder": 23, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-18T08:22:31Z", "changed_date": "2016-02-18T08:23:27Z"}}]', '7', 16, 146),
(1178, '21', 21, 'json', '[{"model": "cms.title", "pk": 21, "fields": {"language": "ru", "title": "\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b", "page_title": "", "menu_title": "", "meta_description": "", "slug": "contacts", "path": "contacts", "has_url_overwrite": false, "redirect": null, "page": 21, "creation_date": "2016-02-18T08:21:45Z", "published": true}}]', 'Контакты (contacts, ru)', 17, 146),
(1179, '22', 22, 'json', '[{"model": "cms.placeholder", "pk": 22, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 146),
(1180, '23', 23, 'json', '[{"model": "cms.placeholder", "pk": 23, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 146),
(1181, '21', 21, 'json', '[{"model": "cms.page", "pk": 21, "fields": {"path": "000F", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-18T08:21:45Z", "changed_date": "2016-03-01T11:06:58.448Z", "publication_date": "2016-02-18T08:21:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [22, 23]}}]', 'Контакты', 2, 146),
(1182, '96', 96, 'json', '[{"model": "cms_plugins.calc", "pk": 96, "fields": {"design": false, "all_type": true, "html": ""}}]', 'Калькулятор стоимости ремонта <br/> коттеджа (таунхауса)', 54, 147),
(1183, '1', 1, 'json', '[{"model": "cms.page", "pk": 1, "fields": {"path": "0001", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-17T13:03:58Z", "changed_date": "2016-03-01T11:11:48.160Z", "publication_date": "2016-02-17T13:03:58Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": true, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [6, 7]}}]', 'Главная', 2, 147),
(1184, '98', 98, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 98, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 147),
(1185, '99', 99, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 99, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 147),
(1186, '97', 97, 'json', '[{"model": "cms.cmsplugin", "pk": 97, "fields": {"path": "002N", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 3, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:31:10Z", "changed_date": "2016-02-29T12:31:30Z"}}]', '97', 16, 147),
(1187, '98', 98, 'json', '[{"model": "cms.cmsplugin", "pk": 98, "fields": {"path": "002O", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 4, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T12:31:45Z", "changed_date": "2016-02-29T12:31:57Z"}}]', '98', 16, 147),
(1188, '6', 6, 'json', '[{"model": "cms.placeholder", "pk": 6, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 147),
(1189, '97', 97, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 97, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy fw600\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 147),
(1190, '96', 96, 'json', '[{"model": "cms.cmsplugin", "pk": 96, "fields": {"path": "002M", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 2, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T12:30:22Z", "changed_date": "2016-02-29T12:30:57Z"}}]', '96', 16, 147),
(1191, '1', 1, 'json', '[{"model": "cms.title", "pk": 1, "fields": {"language": "ru", "title": "\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f", "page_title": "", "menu_title": "", "meta_description": "", "slug": "glavnaya", "path": "", "has_url_overwrite": false, "redirect": null, "page": 1, "creation_date": "2016-02-17T13:03:58Z", "published": true}}]', 'Главная (glavnaya, ru)', 17, 147),
(1192, '7', 7, 'json', '[{"model": "cms.placeholder", "pk": 7, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 147),
(1193, '109', 109, 'json', '[{"model": "cms.cmsplugin", "pk": 109, "fields": {"path": "002Y", "depth": 1, "numchild": 0, "placeholder": 6, "parent": null, "position": 0, "language": "ru", "plugin_type": "SliderPlugin", "creation_date": "2016-03-01T10:50:27Z", "changed_date": "2016-03-01T10:50:41Z"}}]', '109', 16, 147),
(1194, '109', 109, 'json', '[{"model": "cms_plugins.sliderpluginmodel", "pk": 109, "fields": {"sliders": [1, 2, 3]}}]', '109', 61, 147),
(1195, '99', 99, 'json', '[{"model": "cms.cmsplugin", "pk": 99, "fields": {"path": "002P", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 5, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T12:32:10Z", "changed_date": "2016-02-29T12:32:29Z"}}]', '99', 16, 147),
(1196, '94', 94, 'json', '[{"model": "cms.cmsplugin", "pk": 94, "fields": {"path": "002K", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 0, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:29:19Z", "changed_date": "2016-02-29T12:29:40Z"}}]', '94', 16, 147),
(1197, '100', 100, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 100, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 147),
(1198, '95', 95, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 95, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 147),
(1199, '100', 100, 'json', '[{"model": "cms.cmsplugin", "pk": 100, "fields": {"path": "002Q", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 6, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T12:32:42Z", "changed_date": "2016-02-29T12:32:54Z"}}]', '100', 16, 147),
(1200, '94', 94, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 94, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e", "html": "<div class=\\"consult\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <span>\\u0425\\u043e\\u0447\\u0443 \\u0431\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0443\\u044e \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044e</span>\\r\\n\\r\\n                        <div class=\\"clr\\"></div>\\r\\n                        <a href=\\"#callback\\" class=\\"btn fancy anim fw600 flr\\">\\u041f\\u0435\\u0440\\u0435\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone c-blue fw600\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу бесплатную консультацию', 30, 147),
(1201, '95', 95, 'json', '[{"model": "cms.cmsplugin", "pk": 95, "fields": {"path": "002L", "depth": 1, "numchild": 0, "placeholder": 7, "parent": null, "position": 1, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T12:29:55Z", "changed_date": "2016-02-29T12:30:08Z"}}]', '95', 16, 147),
(1202, '43', 43, 'json', '[{"model": "cms.cmsplugin", "pk": 43, "fields": {"path": "0016", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:37:20Z"}}]', '43', 16, 148),
(1203, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 148),
(1204, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 148),
(1205, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 148),
(1206, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 148),
(1207, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fw600 fancy\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 148),
(1208, '42', 42, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 42, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 148),
(1209, '43', 43, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 43, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 148),
(1210, '44', 44, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 44, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 148),
(1211, '44', 44, 'json', '[{"model": "cms.cmsplugin", "pk": 44, "fields": {"path": "0017", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:37:51Z"}}]', '44', 16, 148),
(1212, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 148),
(1213, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 148),
(1214, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-02-29T11:38:07Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 148),
(1215, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 148),
(1216, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-03-01T12:03:37Z"}}]', '41', 16, 148),
(1217, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 148),
(1218, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 148),
(1219, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 148),
(1220, '42', 42, 'json', '[{"model": "cms.cmsplugin", "pk": 42, "fields": {"path": "0015", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:36:49Z"}}]', '42', 16, 148),
(1221, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 148),
(1222, '43', 43, 'json', '[{"model": "cms.cmsplugin", "pk": 43, "fields": {"path": "0016", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:37:20Z"}}]', '43', 16, 149),
(1223, '37', 37, 'json', '[{"model": "cms.cmsplugin", "pk": 37, "fields": {"path": "0010", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:21:50Z"}}]', '37', 16, 149),
(1224, '38', 38, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 38, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 149),
(1225, '39', 39, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 39, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 149),
(1226, '40', 40, 'json', '[{"model": "cms_plugins.calc", "pk": 40, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта квартиры', 54, 149),
(1227, '41', 41, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 41, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fw600 fancy\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 149),
(1228, '42', 42, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 42, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 149),
(1229, '43', 43, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 43, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 149),
(1230, '44', 44, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 44, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 149),
(1231, '44', 44, 'json', '[{"model": "cms.cmsplugin", "pk": 44, "fields": {"path": "0017", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:37:51Z"}}]', '44', 16, 149),
(1232, '25', 25, 'json', '[{"model": "cms.title", "pk": 25, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kvartir", "path": "remont-kvartir", "has_url_overwrite": false, "redirect": null, "page": 25, "creation_date": "2016-02-29T11:09:13Z", "published": true}}]', 'Ремонт квартир (remont-kvartir, ru)', 17, 149),
(1233, '40', 40, 'json', '[{"model": "cms.cmsplugin", "pk": 40, "fields": {"path": "0013", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:32:32Z"}}]', '40', 16, 149),
(1234, '25', 25, 'json', '[{"model": "cms.page", "pk": 25, "fields": {"path": "000J", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:13Z", "changed_date": "2016-03-01T12:04:51.138Z", "publication_date": "2016-02-29T11:10:40Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [57, 58]}}]', 'Ремонт квартир', 2, 149),
(1235, '57', 57, 'json', '[{"model": "cms.placeholder", "pk": 57, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 149),
(1236, '41', 41, 'json', '[{"model": "cms.cmsplugin", "pk": 41, "fields": {"path": "0014", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-03-01T12:03:37Z"}}]', '41', 16, 149),
(1237, '39', 39, 'json', '[{"model": "cms.cmsplugin", "pk": 39, "fields": {"path": "0012", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:23:12Z"}}]', '39', 16, 149),
(1238, '58', 58, 'json', '[{"model": "cms.placeholder", "pk": 58, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 149),
(1239, '38', 38, 'json', '[{"model": "cms.cmsplugin", "pk": 38, "fields": {"path": "0011", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:22:39Z"}}]', '38', 16, 149),
(1240, '42', 42, 'json', '[{"model": "cms.cmsplugin", "pk": 42, "fields": {"path": "0015", "depth": 1, "numchild": 0, "placeholder": 58, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:36:49Z"}}]', '42', 16, 149),
(1241, '37', 37, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 37, "fields": {"project": 1}}]', '37', 57, 149),
(1242, '64', 64, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 64, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 150),
(1243, '64', 64, 'json', '[{"model": "cms.cmsplugin", "pk": 64, "fields": {"path": "001R", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '64', 16, 150),
(1244, '66', 66, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 66, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fw600 fancy\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 150),
(1245, '67', 67, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 67, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 150),
(1246, '68', 68, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 68, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 150),
(1247, '69', 69, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 69, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 150),
(1248, '65', 65, 'json', '[{"model": "cms_plugins.calc", "pk": 65, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта коттеджа (таунхауса)', 54, 150),
(1249, '65', 65, 'json', '[{"model": "cms.cmsplugin", "pk": 65, "fields": {"path": "001S", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:43:12Z"}}]', '65', 16, 150),
(1250, '63', 63, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 63, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 150),
(1251, '66', 66, 'json', '[{"model": "cms.cmsplugin", "pk": 66, "fields": {"path": "001T", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-03-01T12:07:43Z"}}]', '66', 16, 150),
(1252, '67', 67, 'json', '[{"model": "cms.cmsplugin", "pk": 67, "fields": {"path": "001U", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '67', 16, 150),
(1253, '62', 62, 'json', '[{"model": "cms.cmsplugin", "pk": 62, "fields": {"path": "001P", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:42:49Z"}}]', '62', 16, 150),
(1254, '69', 69, 'json', '[{"model": "cms.cmsplugin", "pk": 69, "fields": {"path": "001W", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '69', 16, 150),
(1255, '68', 68, 'json', '[{"model": "cms.cmsplugin", "pk": 68, "fields": {"path": "001V", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '68', 16, 150),
(1256, '26', 26, 'json', '[{"model": "cms.page", "pk": 26, "fields": {"path": "000L", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:39Z", "changed_date": "2016-02-29T11:43:27Z", "publication_date": "2016-02-29T11:10:42Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [59, 60]}}]', 'Ремонт коттеджей и таунхаусов', 2, 150),
(1257, '59', 59, 'json', '[{"model": "cms.placeholder", "pk": 59, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 150),
(1258, '60', 60, 'json', '[{"model": "cms.placeholder", "pk": 60, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 150),
(1259, '26', 26, 'json', '[{"model": "cms.title", "pk": 26, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u043e\\u0442\\u0442\\u0435\\u0434\\u0436\\u0435\\u0439 \\u0438 \\u0442\\u0430\\u0443\\u043d\\u0445\\u0430\\u0443\\u0441\\u043e\\u0432", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kottedzhej-i-taunhausov", "path": "remont-kottedzhej-i-taunhausov", "has_url_overwrite": false, "redirect": null, "page": 26, "creation_date": "2016-02-29T11:09:39Z", "published": true}}]', 'Ремонт коттеджей и таунхаусов (remont-kottedzhej-i-taunhausov, ru)', 17, 150),
(1260, '62', 62, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 62, "fields": {"project": 2}}]', '62', 57, 150),
(1261, '63', 63, 'json', '[{"model": "cms.cmsplugin", "pk": 63, "fields": {"path": "001Q", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:40:02Z"}}]', '63', 16, 150),
(1262, '64', 64, 'json', '[{"model": "cms_plugins.teaserspluginmodel", "pk": 64, "fields": {"teasers": [1, 2, 3, 4, 5, 6, 7, 8]}}]', 'Вот почему Вы выбираете нас', 49, 151),
(1263, '64', 64, 'json', '[{"model": "cms.cmsplugin", "pk": 64, "fields": {"path": "001R", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 2, "language": "ru", "plugin_type": "TeasersPlugin", "creation_date": "2016-02-29T11:22:56Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '64', 16, 151),
(1264, '66', 66, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 66, "fields": {"name": "\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430", "html": "<div class=\\"consult two\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"left-part fll\\">\\r\\n                        <p>\\u0425\\u043e\\u0447\\u0443 \\u0432\\u044b\\u0437\\u0432\\u0430\\u0442\\u044c \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u0430 \\u0438 \\u043e\\u0446\\u0435\\u043d\\u0449\\u0438\\u043a\\u0430</p>\\r\\n                        <span class=\\"fw600 c-blue\\">\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e</span>\\r\\n                    </div>\\r\\n                    <div class=\\"right-part flr\\">\\r\\n                        <a href=\\"#callback\\" class=\\"btn fw600 fancy\\">\\u041f\\u043e\\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043c\\u043d\\u0435</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Хочу вызвать дизайнера и оценщика', 30, 151),
(1265, '67', 67, 'json', '[{"model": "cms_plugins.personpluginmodel", "pk": 67, "fields": {"commands": [1, 2, 3, 4]}}]', 'У нас замечательная команда', 34, 151),
(1266, '68', 68, 'json', '[{"model": "cms_plugins.reviewpluginmodel", "pk": 68, "fields": {"reviews": [1, 2]}}]', 'Что Вы думаете о нас?', 46, 151),
(1267, '69', 69, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 69, "fields": {"name": "\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <div class=\\"name\\">\\u0421\\u0432\\u044f\\u0436\\u0438\\u0442\\u0435\\u0441\\u044c \\u0441 \\u043d\\u0430\\u043c\\u0438</div>\\r\\n                    <div class=\\"block items1\\">\\r\\n                        <span>\\u041d\\u0430\\u043f\\u0438\\u0448\\u0438\\u0442\\u0435 \\u043d\\u0430 \\u043f\\u043e\\u0447\\u0442\\u0443:</span>\\r\\n                        <a class=\\"c-blue fw300\\" href=\\"mailto:clients@remont-nts.ru\\">clients@remont-nts.ru</a>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"block items2\\">\\r\\n                        <span>\\u0417\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435 \\u043f\\u043e \\u0442\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d\\u0443:</span>\\r\\n\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"c-blue fw300\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Свяжитесь с нами', 30, 151),
(1268, '65', 65, 'json', '[{"model": "cms_plugins.calc", "pk": 65, "fields": {"design": false, "all_type": false, "html": ""}}]', 'Калькулятор стоимости ремонта коттеджа (таунхауса)', 54, 151),
(1269, '65', 65, 'json', '[{"model": "cms.cmsplugin", "pk": 65, "fields": {"path": "001S", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 3, "language": "ru", "plugin_type": "CalcPlugin", "creation_date": "2016-02-29T11:30:42Z", "changed_date": "2016-02-29T11:43:12Z"}}]', '65', 16, 151);
INSERT INTO `reversion_version` (`id`, `object_id`, `object_id_int`, `format`, `serialized_data`, `object_repr`, `content_type_id`, `revision_id`) VALUES
(1270, '63', 63, 'json', '[{"model": "cms_plugins.htmlinsertmodel", "pk": 63, "fields": {"name": "\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443", "html": "<div class=\\"consult three\\">\\r\\n            <div class=\\"container\\">\\r\\n                <div class=\\"pink-bg\\">\\r\\n                    <p class=\\"fw600\\">\\u0420\\u0430\\u0441\\u0441\\u043a\\u0430\\u0436\\u0438\\u0442\\u0435 \\u043d\\u0430\\u043c \\u0441\\u0432\\u043e\\u0438 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e \\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0443\\r\\n                        <br/>\\u0438 \\u043c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0438\\u043c \\u043b\\u0443\\u0447\\u0448\\u0435\\u0435 \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u0435</p>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <p>\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e, \\u0437\\u0432\\u043e\\u043d\\u0438\\u0442\\u0435!</p>\\r\\n                    </div>\\r\\n\\r\\n                    <div class=\\"item\\">\\r\\n                        <a href=\\"tel:+74955177528\\" class=\\"phone fw600 c-blue\\">+7 (495) 517-75-28</a>\\r\\n                    </div>\\r\\n\\r\\n                </div>\\r\\n            </div>\\r\\n        </div>"}}]', 'Расскажите нам свои пожелания по ремонту', 30, 151),
(1271, '66', 66, 'json', '[{"model": "cms.cmsplugin", "pk": 66, "fields": {"path": "001T", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 4, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:35:21Z", "changed_date": "2016-03-01T12:07:43Z"}}]', '66', 16, 151),
(1272, '67', 67, 'json', '[{"model": "cms.cmsplugin", "pk": 67, "fields": {"path": "001U", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 5, "language": "ru", "plugin_type": "PersonPlugin", "creation_date": "2016-02-29T11:36:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '67', 16, 151),
(1273, '62', 62, 'json', '[{"model": "cms.cmsplugin", "pk": 62, "fields": {"path": "001P", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 0, "language": "ru", "plugin_type": "ProjectPlugin", "creation_date": "2016-02-29T11:20:27Z", "changed_date": "2016-02-29T11:42:49Z"}}]', '62', 16, 151),
(1274, '69', 69, 'json', '[{"model": "cms.cmsplugin", "pk": 69, "fields": {"path": "001W", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 7, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:37:35Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '69', 16, 151),
(1275, '68', 68, 'json', '[{"model": "cms.cmsplugin", "pk": 68, "fields": {"path": "001V", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 6, "language": "ru", "plugin_type": "ReviewPlugin", "creation_date": "2016-02-29T11:37:02Z", "changed_date": "2016-02-29T11:40:03Z"}}]', '68', 16, 151),
(1276, '26', 26, 'json', '[{"model": "cms.page", "pk": 26, "fields": {"path": "000L", "depth": 1, "numchild": 0, "created_by": "admin", "changed_by": "admin", "parent": null, "creation_date": "2016-02-29T11:09:39Z", "changed_date": "2016-03-01T12:08:28.882Z", "publication_date": "2016-02-29T11:10:42Z", "publication_end_date": null, "in_navigation": false, "soft_root": false, "reverse_id": null, "navigation_extenders": null, "template": "INHERIT", "site": 1, "login_required": false, "limit_visibility_in_menu": null, "is_home": false, "application_urls": null, "application_namespace": null, "languages": "ru", "revision_id": 0, "xframe_options": 0, "placeholders": [59, 60]}}]', 'Ремонт коттеджей и таунхаусов', 2, 151),
(1277, '59', 59, 'json', '[{"model": "cms.placeholder", "pk": 59, "fields": {"slot": "slider", "default_width": null}}]', 'slider', 1, 151),
(1278, '60', 60, 'json', '[{"model": "cms.placeholder", "pk": 60, "fields": {"slot": "center", "default_width": null}}]', 'center', 1, 151),
(1279, '26', 26, 'json', '[{"model": "cms.title", "pk": 26, "fields": {"language": "ru", "title": "\\u0420\\u0435\\u043c\\u043e\\u043d\\u0442 \\u043a\\u043e\\u0442\\u0442\\u0435\\u0434\\u0436\\u0435\\u0439 \\u0438 \\u0442\\u0430\\u0443\\u043d\\u0445\\u0430\\u0443\\u0441\\u043e\\u0432", "page_title": "", "menu_title": "", "meta_description": "", "slug": "remont-kottedzhej-i-taunhausov", "path": "remont-kottedzhej-i-taunhausov", "has_url_overwrite": false, "redirect": null, "page": 26, "creation_date": "2016-02-29T11:09:39Z", "published": true}}]', 'Ремонт коттеджей и таунхаусов (remont-kottedzhej-i-taunhausov, ru)', 17, 151),
(1280, '62', 62, 'json', '[{"model": "cms_plugins.projectpluginmodel", "pk": 62, "fields": {"project": 2}}]', '62', 57, 151),
(1281, '63', 63, 'json', '[{"model": "cms.cmsplugin", "pk": 63, "fields": {"path": "001Q", "depth": 1, "numchild": 0, "placeholder": 60, "parent": null, "position": 1, "language": "ru", "plugin_type": "HtmlInsertPlugin", "creation_date": "2016-02-29T11:22:22Z", "changed_date": "2016-02-29T11:40:02Z"}}]', '63', 16, 151);

-- --------------------------------------------------------

--
-- Структура таблицы `thumbnail_kvstore`
--

CREATE TABLE IF NOT EXISTS `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `thumbnail_kvstore`
--

INSERT INTO `thumbnail_kvstore` (`key`, `value`) VALUES
('sorl-thumbnail||image||009ed045ee3ef117c539c7c0e040e0fb', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d7/05/d705577e9d4d96cc6b5e4dc47970d995.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||011f8fff6b2ec320d8018bd021daa760', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/2d/30/2d304f63e87a45b44da451e35a9d392d.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||017f7378ee6e8c2abc6ca05e6e43529d', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/625acfc2-8962-427c-a111-21f1e4477051.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||0aaf02746708576b19af1cf654af773a', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/93dd9bdb-c418-4858-bc92-ac4d2310b458.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||0cd39bf6e7be783ae4a291dc85aa53c6', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/68/ea/68ea89eadc6b1a1b21015ff24644497f.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||0d4d61b78eb0789520039cf01c14ae79', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/cf1c257c-de79-439a-b675-44b419dddb07.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||102a988964e01f83fbe70e1f0befcbe5', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ae/3f/ae3f2bb79bfbd32f462efbe671bedc74.jpg", "size": [413, 326]}'),
('sorl-thumbnail||image||105c1092be232390e297f92681489602', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/cd/af/cdaf9970d6c1e604259243e3d5934a0c.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||12787ba83da2ee595962428973d63e7f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/irina_gXbbRk7.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||129f319bd36ea5e076a4f13cb46a4437', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/11/73/1173fdf157a49fbbe42e3bbfb26b081e.jpg", "size": [261, 261]}'),
('sorl-thumbnail||image||1347e94799650ec81adf3b0c39991b1e', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/86/3a/863a9b8eb510ad6e9b9bb68e1f801f83.jpg", "size": [261, 261]}'),
('sorl-thumbnail||image||13796f9f17b9426e3449ecb7ff4120c5', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/ea89272a-ebfb-41ac-830d-41b770296fbc.jpg", "size": [260, 190]}'),
('sorl-thumbnail||image||18a65342df6fb443d10bfd73d436ccf9', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/6f7f58a4-2553-45dc-b652-cb1b26101909.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||18c11163c4902fb90db5508947083775', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/66/7e/667eaec18a4a75750b8cee2d43fed1ae.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||199739a48389e02d4915312c7af2d8ea', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/01/55/01556bc61ac470cac6517b0554cc1225.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||1b0ddee9e8b17ef649b157be7c33436b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/06/59/065960aad0f787d69341aa0355aa0f6f.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||1be4105e213545228be3bcfb9afa2548', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/3b/29/3b2924afb664243752845fed6700f383.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||1d27a000e2a890d663384b6628b94a78', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/18/76/1876d30e5adb07c5a2d7824e0615069e_jBApsqT.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||1d93dac2e26a33fa0703ccc1a1843a9e', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/28/66/2866c956d3ec72514e64dd824283b055.jpg", "size": [261, 261]}'),
('sorl-thumbnail||image||1def5cd934df24f097a292e0eac87c4d', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/18/e5/18e5434be8b2bccb541c8b6df21f6269.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||1e49728b1ca93fc21443362bafbfecc5', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/57/2d/572d39faa1298c1fd5e13aa4d1241e8b.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||1f05b30c242fec2306f28cbcf3b4e908', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/f4af5bb9-52d1-4422-a2a0-42fced2a74aa.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||1f32317c8be5901a234ac7f00f817695', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/aceda44c-71dd-44c7-a7c3-48fc33929f0b.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||1f76c92f9afecd01f0175dd0318a1102', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/nikolai_iabIpRN.jpg", "size": [261, 343]}'),
('sorl-thumbnail||image||20086e456e52ba98f7fa09838ffd8f86', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ed/33/ed3312427b0103b02dc4295969102165.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||20525461d037fb3abf59609ec731a7a4', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/9b/9c/9b9c262e8d93cd800198d88178f07540.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||21af65ab27eeec55e2d7b5783461ae75', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/01/c0/01c06ddc3527c0aa6858e2517daca1eb_ArCID7u.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||221121633f8ce5aa75901b0490982a56', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/c3d1c90d-7e8b-4fb8-a398-3c2a215c5817.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||252d3c7a48dbcd75774f38d6676bd88a', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/eb/dc/ebdc22814eb7c81e3609aef87b64ee56.jpg", "size": [570, 326]}'),
('sorl-thumbnail||image||2595cdfa6d0554acbbbd85813ca85456', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/remont-kvartir-i-ofisov-kvartira-v-moskve-na-ul-paustovskogo.jpg", "size": [180, 142]}'),
('sorl-thumbnail||image||2648280d99133d7d11f0d0559ea12cc7', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/de43a748-0a0b-40b8-b4eb-d8f964f2bb70.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||26ac840a716b3285546a9700684720a0', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/d6d675ff-d228-4cff-9b91-badfd2e25cd8.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||278385bb02ace2e70fe42a67db0317e5', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/3a/93/3a93211d94d5cff25234514a3d29d099.jpg", "size": [175, 230]}'),
('sorl-thumbnail||image||28826c4213c464f190525ce131198bfd', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/a3/31/a331162642fc883e14d5864d46b16bd1.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||2b972cdc9063584e12240d797f374937', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/remont-kvartir-i-ofisov-3-kh-komnatnaia-kvartira-v-moskve-na-ul-volzhskii-bulvar.jpg", "size": [180, 142]}'),
('sorl-thumbnail||image||2bcb6e161a3e5cb842d9df4c88425d9c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/fa/73/fa734ed25af8cc158ace180617d55a0e.jpg", "size": [1170, 557]}'),
('sorl-thumbnail||image||2fd4bbc08dfcf7375b88bf390115b959', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/e8889361-fdcd-41d4-af9b-2787ee519ea9.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||31a765a083fb4d074d8e1e330c72a50d', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/bd/c1/bdc1ea73df99135a6825e023258304b4.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||32a53ec2790d2537fccab60061b1cd6f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/78577ba1-b82e-413d-8405-1df71cac46de.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||34ab9a84726949be9857c9fcf6e84ee5', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/7c/09/7c0939201a95dd23e2563d766d86400b.jpg", "size": [261, 343]}'),
('sorl-thumbnail||image||35742eba9280acc34cbc2c640dc0cb7c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ad/d7/add7529d776b2776aabb743af8e719b0.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||3a07d623a5fd1c3796bba11f644945fa', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/26/b3/26b39c2a52f48dcb269585f1bd2f7418.jpg", "size": [413, 326]}'),
('sorl-thumbnail||image||3a240b30a4fd38e1830d281060c93f42', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f4/76/f4763c41d83dde027d93d0f903a90eb2.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||3b6df1de625e252fa58d052b910df48b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f3/cf/f3cf1fd4947e6c1e7ad032a1477d8d90.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||3c526942bb88d21a9ade205980930ec0', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f9/bb/f9bb2598f80f288bc3b0e15451e472d6.jpg", "size": [413, 326]}'),
('sorl-thumbnail||image||3de3562c53f8dbc9cef7202b2bfe7b41', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/7247c59b-7a6c-4d45-a392-ee024a9ff936.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||3e0f29a22473f98a958c0ffd826b917f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/bd/a7/bda7ef3c7a5a8d090c9ed1a25153d310.jpg", "size": [170, 170]}'),
('sorl-thumbnail||image||3ea25bfbe86d3d60882ddf391a62156c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/5c/0c/5c0c6c885c094ff988a653108e133929.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||3ea7b80da5cbe8f35991181c63666287', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ae/4b/ae4b850fcfb21ddcfd94f52372822333.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||400414903fd2701a8904142dd426a089', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/3d-tur-po-taunkhausu-k-p-bristol.jpg", "size": [570, 326]}'),
('sorl-thumbnail||image||40249b1c12a3548852b88c21b9033348', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/remont-3-kh-komnatnoi-kvartiry-ul-volzhskii-bulvar-moskva.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||41da5ad6044b6c820cbcbf30c62b1f41', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/889eedf7-26b2-4956-851c-2019dc63322e.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||431b4c348c8265cfed82d1972b6dfa00', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/b0c8271a-20ff-46cb-8190-e80e594c6cea.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||4326e13531baebcdf395c093044eae7e', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/03/fb/03fbcec510353a976f13b831e9dce396.jpg", "size": [570, 326]}'),
('sorl-thumbnail||image||43bc149fa956900a9312ebf5a805c549', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/irina.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||44b58df8601e92f869ccd58639a0d843', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/98/bb/98bbc00fc27c62eece4db731c6ca335f.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||46be1d47efae6577aeb6dda99abc8ed1', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f2/e8/f2e8950470351100ebadf1d559a1c125.jpg", "size": [570, 326]}'),
('sorl-thumbnail||image||475d71118009f65cc55089ce2386271f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/77/db/77db523c4a5dbb5ca9689111986b54ed.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||496fcaa657b8200bb5e2fb49837306a6', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/7e/c7/7ec7908e813ef42bdc3be13259d9e60b.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||49857584ce7eb62b6d130deefb69c4ca', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/4c/40/4c4007045440c0ab3d41930fc697d8b2.jpg", "size": [570, 326]}'),
('sorl-thumbnail||image||4a0694f010c81804aa04babc1b92a9a2', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/tatiana-ivanova.jpg", "size": [260, 190]}'),
('sorl-thumbnail||image||4afa59fc298ec044c337e5b7aa49967c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/tatiana.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||4b93ecd12db3e5219d1b30c85c00f5ca', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/22/2f/222f5ca39fd531c272fce8f5c44b1924.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||4bcc70871a2c44245788145af7247f9a', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/fb/f5/fbf5a1e99a32dc97a566a276bf53afe1.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||4cf9434c9af92df28846e4f9126f6f4d', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/6b8cecb9-f5d1-4fbb-88d0-5f6423fe1146.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||4f67669df9ea65339b866350f2063890', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/74/a8/74a8053dd066d25ccd0961aed9aa2b82.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||522b2ed6eda8482ccf91253c59dcce00', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/4d/ee/4deed23e642141d0b194f805f753f086.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||52d217bc9bef335cbadbdb7ea23f8971', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/70/9f/709f0d92f1be27a50408171325a7594a.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||55d48de2e3545e8facc7bbc86ca564cc', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/20/5e/205e704c8b341090bfa73b015c7f0e47.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||55e2220ee1e03017895eac3c4d903901', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/11/34/1134de671f981980b53490f10a580a77.jpg", "size": [570, 326]}'),
('sorl-thumbnail||image||565028c73c3c40b98fc6248a62ccb1b3', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b5/1e/b51e16e48a2f71eb6ab65e011d6653d6.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||56df1a6882976ce4710d78be144d22cd', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/60/83/6083103215b0559eab57be3f3f4c5034.jpg", "size": [570, 394]}'),
('sorl-thumbnail||image||586142802351870b519b9c826326ecf5', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/dizain-proekt-v-stile-modern-2.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||590d0a3ade0457a05f4655fe82a4da4b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/0b/e2/0be27381266d711d02bc0ec09b24b34d.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||5aa82a471fe83ddf17dc1f31fd45a5e2', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/margarita-repina.jpg", "size": [260, 190]}'),
('sorl-thumbnail||image||5b6d165578d98c870a955aebae65253a', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/dizain-proekt-v-stile-modern-3.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||5bb6e64fb7ccdde2ba83c589be8ce03f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/6c/96/6c96167119164448d80d7223f72036ed.jpg", "size": [261, 261]}'),
('sorl-thumbnail||image||61aa622cb1aa907ab925f4b90599c395', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/nikolai.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||64d50c1a33cddaffe7c61e902ead6080', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/43/b1/43b123b5b0024f70ec55eef41c8cae78.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||653a960073560c6a8516ea6ca3f5c852', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/10/ac/10ac6f4f66f618b6ad5ded31e9c654a0.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||654d478560e627497545e55237ccb571', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/bee-by.jpg", "size": [199, 92]}'),
('sorl-thumbnail||image||6903e779cb074c7ca3172f17b3e5ab6d', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ea/c0/eac02e8ba9ad459a08b8b1df6253e801.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||6b82f5a0ef644c3b4a3c12b8091e2c50', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/13/31/133116532f8b5777a651094d8610cd05.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||6bd68155a891ae100210e28c3b30159c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/8499b724-2b37-434e-8942-4c06e5c03aa7.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||6c75c609bdd07cdd9e5597d51e25c2b0', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/remont-kottedzha.jpg", "size": [1170, 557]}'),
('sorl-thumbnail||image||6caeba5105ccbf15f5c0b9e93dccd786', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/50/e0/50e0148c1ddb3f5d7c3a37c56580dd4f.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||6dcade23bdf87d249729c0f8c923cd59', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/84/eb/84ebdffc6aeb76295a82ded9fdc3516b.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||6dec73d51b3d4adb516de1273745379a', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/c6/83/c68387a1da64306da623bf8c4d6f31f2.jpg", "size": [375, 457]}'),
('sorl-thumbnail||image||6e29ce11fe8f19983a2932dc68e800cd', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/01/c0/01c06ddc3527c0aa6858e2517daca1eb.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||70357bbada52e84cbd33dfe0e1b9e4e2', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/3b/29/3b2924afb664243752845fed6700f383_cXF8ELQ.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||72dd968e33d9516b57062adc53473061', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/45/73/4573cfdccdf40f3b4b40d92c499eb447.png", "size": [110, 112]}'),
('sorl-thumbnail||image||76ef41ca123bf863d24c245b5d298159', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/62aafbec-3933-41aa-b22b-a03bc3182b71.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||771024e33e7f516b96b57dbabec60e7f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f3/ea/f3ea83cfb30b4577adf7f514389f692d.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||7930d533c4c68fc82672c2739c8fe35c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/22c9b6a5-6e8c-4f71-8f1e-22e19ba391fc.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||7ae2eabb73af91d5f81676101b258d28', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/vasilii_iMKSkQg.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||7b72ceb56326fcacd63d02847b26af13', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/37/0b/370bf877c1f840c49885b7ff9ed0ee83.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||7bff9e3f9565d583e7e91373887a4060', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d7/e4/d7e481188d54d9d798f0d79b6111aaf1.jpg", "size": [570, 326]}'),
('sorl-thumbnail||image||80cb6710511e466ee760c1995fd9f585', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/nikolai_blkziS1.jpg", "size": [261, 343]}'),
('sorl-thumbnail||image||814e3470f1b968b2023d16422f0ed952', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/70/9f/709f0d92f1be27a50408171325a7594a_ltP4EGI.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||8163ffb9efbd18a56f2d37f91dde1701', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/42/55/4255aae8dfbc6e1f6cb1fa990f39cb15.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||82704516600794a9a9c3902f290367e4', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/8d/2a/8d2a333929120a38265ff8c28ce48522.jpg", "size": [413, 326]}'),
('sorl-thumbnail||image||84065fa93982b0335f81060703caae3d', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/ba/61/ba619cc935c93cb279d7b24fb08bc646.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||870f22f2bbe815e97ccbb44f3567f8c9', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/26/48/26480803dc769747c100e0661cf0a933.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||88c46dcde0d127a948e3dedac4855526', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/2c/d2/2cd27fe1a2a802f5a65900b2897386a3.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||8954dbfdd58d663b1c4eb3ea6ab83c4d', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/7e/9c/7e9cbfddbbae576f9cbc6954d5460660.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||89f5f4d9e67924f25b5ba6ade52f6a6d', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/c2/8d/c28d4efc842e4e12f6d3ddcc9416f8df.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||8be203565fbeabd8e4fd49811258859b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/3cb63eca-6964-4b1d-9eec-196d61f75df7.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||8c39ddd0f524a9e054a1bdf533988a98', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/a3/5c/a35c8bf7c31eeea337d62979f210604a.jpg", "size": [261, 261]}'),
('sorl-thumbnail||image||8cc91acf83602d8e2540768c63b3fade', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/14/26/14269c26a1d064b1892cd4440621f2ef.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||8e438e4e800d39e83d2fe66112921813', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d5/d5/d5d5ec3dd3b85a671da96cde4f35a1c7.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||8e53500105d66aa6d9458fe9a438161f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f0/4a/f04a667c441e8579d62c9991c15641b7.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||8e635ee13913bca35ba93a97b26ce021', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/cc/20/cc20cda2e29d6d8cf5341228d6d5e986.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||8ffa77c93c9ecc45eb64bf9fd5a60a06', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/94/b2/94b2f70fc53c49b0f5fec4abb55b4ff3.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||917a27704039706398d30f6ea1f1f60b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/98/81/98811c94cc35d9a92989066a4ffe2282.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||93a51072c73e6e17be52ff1e738cef7b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d7/9b/d79b4fd689890abc519cdf69b3be758d.png", "size": [191, 55]}'),
('sorl-thumbnail||image||95b47f34f7488075bf71509355ed3875', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b2/63/b263f24a821ff0a4ee3f710e76f67ad3.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||99077834774102f91c41e4ce72248a19', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/c4/ed/c4ed38e65f87b3bf805b2cc1cb132d62.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||992543c5a1b35694b015fd47b7a82c4b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/0d/3c/0d3c867a26370f19e10ccad97a1a54ed.png", "size": [212, 76]}'),
('sorl-thumbnail||image||9a37270f72b69608188cbb0efdb184f2', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d0/a8/d0a8f5ac4a299fc7f1dd190ea314b4e6_8z1HgaU.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||a0013ad1e0f3d85ed5204516cbb945f8', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/aa/e1/aae1ec9524538202da9c8045a92ae559.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||a09aec2ceb6e7154855fd777193e7dcb', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/8c/47/8c474871fd27033ade1d5ff12d8b4549.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||a1fd15373c373b54fba66c21a66b9c71', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/c3/98/c398b7ba27caaa68b0f1c58f6292339a.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||a268c5276a517a2f5dcbe43bbae2b9df', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/dizain-proekt-v-stile-modern-1.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||a30bcef3d3fe28d114c2498629c151e3', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/63/a5/63a52d92bedad1ea855fbdac5ac22260.jpg", "size": [260, 190]}'),
('sorl-thumbnail||image||a48309568a857596658d7fa77254170c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/aa/b1/aab120abb67b937739e5ee089648d267.jpg", "size": [261, 343]}'),
('sorl-thumbnail||image||a49833b48bfcb842745356117a535a98', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/81/8f/818f3c67b902cea7858ea2af4cc06fb4.jpg", "size": [260, 149]}'),
('sorl-thumbnail||image||a4c38c77b7a77bf21c5fe45676eae12b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/e659ae48-217a-41a8-8c34-6db300ec5a38.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||a649430521e30589e73d95316e9b50c9', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/bd/cb/bdcb39eb200611328b200113f7e9b363.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||ad4f377c47cb986246339f7fab302db3', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/remont-kvartir-i-ofisov-kvartira-v-moskve-v-kostianskom-pereulke.jpg", "size": [180, 142]}'),
('sorl-thumbnail||image||ad7af077d15cd40f9565a9d0d790d75f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/9f/d0/9fd0e6ac811ff089d2740232ac909753.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||ada6ec65f546785c759af1969e2a8bd3', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/18/76/1876d30e5adb07c5a2d7824e0615069e.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||ae154d2dc1a53ae52706bd3090b167d1', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/nikolai_hjLG8V6.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||b3083f1a588bc5063f1bdace22094891', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/dd/fb/ddfb39fcc70cbbdef04db569bb0149b3.png", "size": [199, 92]}'),
('sorl-thumbnail||image||b4059f3ade1553b0dfb6e05edeac0d3f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/pomidor.jpg", "size": [191, 55]}'),
('sorl-thumbnail||image||b69137287bd908730a96f6ef38838d03', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/2e5be478-4149-45b4-b69e-a9bd3ef38057.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||b7eb5b3a4c4ddac0222fac85cced43a4', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b7/18/b7184efafc970695933e5b82cb740ebc.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||bb112681cd74f20632d0c121a62f8003', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/remont-kvartir-i-ofisov-kottedzh-v-g-taldom-moskovskaia-obl.jpg", "size": [180, 142]}'),
('sorl-thumbnail||image||bcbd56be9f17dba66396669a129975e6', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/b3/b1/b3b10141b5b91cfead5cfa62788c1fd2.jpg", "size": [260, 190]}'),
('sorl-thumbnail||image||be29ec7d86844c8a64b625fbe305f9f4', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/90/8e/908ec3252c02f1d132a95598541151a4.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||be9cc73cdeac2a6b37bde2660c1f7a74', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/e81ce1a5-b799-414f-a2ee-e4c5e104c426.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||c1b2477327eb763d7588729661af5765', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/a1333a9c-b6fa-423f-96c6-172ebf194eba.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||c36e36572e1a3a8b8748e37b3367c646', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f4/61/f4613a7169225d3d891f2ea32e8e09f9.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||c3e63343fc51d31ef71584c2ebaa4a1c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/17b84ecd-7bec-43b6-89b9-83f5b1370f25.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||c3e706b1712607749d2219886bdbb98b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/1c/e9/1ce9f7ec32d973cfe63f3d6bca82c944.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||c4ab37200e606e32736a8b52557a5f24', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/47/38/4738a6e3b331cabec460747bbbb1477d.jpg", "size": [261, 261]}'),
('sorl-thumbnail||image||c5fe34d8f2fbd975b0401432f8f884ce', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/87/76/8776644fab5bd343f761b4a2e02c11fe.jpg", "size": [260, 190]}'),
('sorl-thumbnail||image||c69fe2c7f7026dc2e71078fed7a320f0', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/0c/b4/0cb4e578add39f3e6949ed1c04632f06.png", "size": [97, 123]}'),
('sorl-thumbnail||image||c6b9ae98fae2ad91e6175f569beca97c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/qxsolutions.jpg", "size": [212, 76]}'),
('sorl-thumbnail||image||c7ae684f318b0ed1e02746b0986c0e22', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/76/01/76014f6732fd3cf57996b2e718146a34.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||cab648f6a83640a9ca60543d4ac9e7e4', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/abfe650e-a799-45cb-874a-6e395f660753.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||cacdeadefdef0e7e8399cfdc470dc48b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/web2people.jpg", "size": [97, 123]}'),
('sorl-thumbnail||image||cc955860fee380012aaa7054b7f3aef3', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/245ba46d-f5cf-4c28-beba-d441a8674156.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||ce0e35652c5c8e58b974e2cd3a866d5f', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/headex.jpg", "size": [110, 112]}'),
('sorl-thumbnail||image||cfa8480372b652dbdf30d485b5f6a19e', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/f5/13/f5136d316209b7b6cbf9ca868776f35a.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||d156731334651eb3b1b1116c262afa4b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/4a/e0/4ae075934042bb082ade4e4d3452979c.jpg", "size": [375, 457]}'),
('sorl-thumbnail||image||d4cbe723dc0d169528631ccf1066e1dc', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/07/f9/07f920a3eb4528e2a212044ca75f1cde.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||d4f6103cacec05df94008ec582168bd8', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/64/bc/64bc01b6471313289461df69c055c802.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||d658cfa72e9e1ec7d1667a684872ce2a', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d0/a8/d0a8f5ac4a299fc7f1dd190ea314b4e6.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||db5225de7f144d97bd422913216cdfc7', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/b4f7799f-5dbf-4e36-a9bb-5f55ad355869.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||dd4cbeccf2534fb624d14b984cb5c5ea', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/vasilii.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||df235bede8d97d7de779882223edb433', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/86/08/86082971e989e51aefc2eaa74694a7e5.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||e0516d26a621dafcff32a365ac556d38', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/33/6c/336c525c8f28e63f6ce66e84e78cf9e7.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||e071d2c1ab53036e53c49629720dbfec', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/d8c7d6e9-60e1-41eb-96ef-a867792b133b.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||e122d461328afb256df1b394624e6f41', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/4a/6a/4a6af845f11835f47490d5ed9517bc08.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||e27bce4cc606a61dc5e713320358606a', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/remont-taunkhausa-span-k-p-bristol-span.jpg", "size": [1170, 557]}'),
('sorl-thumbnail||image||e2a53269d36ccec2b041ee752dea03e7', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/59/86/5986f441a1b2ac1b057e1f8a9c150eed.jpg", "size": [570, 326]}'),
('sorl-thumbnail||image||e307b1fc3247a2301163efc3b54048a5', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/remont-taunkhausa-k-p-bristol.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||e57fc68cc60c30316676ad60ff94017c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/43/61/436186a9a7ea9812be79cc8fa602739a.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||e587cf1a229440362c653632db68ae0c', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/63/5a/635af2e0bfa9cc304a76af283a224536.jpg", "size": [413, 326]}'),
('sorl-thumbnail||image||e840ab1e3a52b7068689d13e964b1e8e', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/aa3ff049-dba8-4924-90af-ad3b65a848e0.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||e925425d6a88f5ed50ee340f80935744', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/89/16/8916ebfd6200c65a5e61e08150841d2a.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||ea36e81fbc230dc58457de2c49c5d951', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/43/3e/433e9edc5c8e49ad0299f6407f3d3b42.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||ea8b5022731ff3ec0e768a36aba5fca1', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/c6/40/c6407b0d3975f42e457e3030dd82a3aa.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||ea8c36a8a5e6c8a826d70e5013dac2ea', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/17/1e/171e0982dad0b4d5b3903aed8920794a.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||ec826223a7ad30998fb44115729d0dc3', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/kosmeticheskii-remont.jpg", "size": [1170, 557]}'),
('sorl-thumbnail||image||eca62923ee2ff1dcde895723cba52714', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/7a/99/7a99d6df7bda9152adb0d1c179a5aec5.jpg", "size": [570, 394]}'),
('sorl-thumbnail||image||eca9cf4b1bf3e470e1dcef472c27d3af', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/67/97/67977ebce73f77818ff628b16958cd79.jpg", "size": [1170, 557]}'),
('sorl-thumbnail||image||ecd473d05452d64a45f9e00b6e4d656e', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/68/ea/68ea89eadc6b1a1b21015ff24644497f_y8igWgK.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||ef2808d62052085918f057b394b25ecb', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/62/b7/62b77c821bc33bb820faa335ef73743e.jpg", "size": [1170, 557]}'),
('sorl-thumbnail||image||ef330bcd5a36e6c0cae972a3ad4ab13d', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/remont-kvartir-i-ofisov-odnokomnatnaia-kvartira-v-moskve-na-pr-vernadskogo.jpg", "size": [180, 142]}'),
('sorl-thumbnail||image||f14c7a940be66ecb6c77d28f4d781a64', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/d4b98e6b-4cc0-4a6c-b940-17058814b58b.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||f28410ad479855f0ab61711c639c3dd7', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/94994e02-76f4-431c-952f-eb0becfce8a1.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||f35cf5c52d3b7297e62f54e0fcdef6cd', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/5e/d1/5ed1f345dc58948cad16dcb5082dd564.jpg", "size": [261, 261]}'),
('sorl-thumbnail||image||f39d2b739622462689c83b44ec3ed67e', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/d5/fd/d5fdb8642e550a4517f0e85f62ac152a.jpg", "size": [234, 167]}'),
('sorl-thumbnail||image||f54b3a51e8280cdcf4210a65573118b6', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/tatiana_zvMJzJN.jpg", "size": [230, 230]}'),
('sorl-thumbnail||image||f7bde43066d8ee499f4b87568fc54ce9', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/14/f8/14f85d3103d15da54d9432813174fa00.jpg", "size": [375, 457]}'),
('sorl-thumbnail||image||fa1f4cbb4204886c445c3e9eeefcf605', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/6949e970-7e56-488d-8133-58ea1d3b6fc6.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||fad43a087c952845c1bec0512e719c50', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/11f6d41c-a6a5-473f-92ac-0f46bf14150c.jpg", "size": [126, 94]}'),
('sorl-thumbnail||image||fb23dc489b142f4f71e348f13663b57b', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/3807f651-330a-4083-ab7c-153183f4b75f.jpg", "size": [422, 326]}'),
('sorl-thumbnail||image||fb85f38868db881cf982ea18848bb1e9', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "cache/a9/b8/a9b8d86af1d5a61736220e01894ffb3a.jpg", "size": [170, 170]}'),
('sorl-thumbnail||image||fd2a6e7c541990ec88c200caf8e71b3e', '{"storage": "django.core.files.storage.FileSystemStorage", "name": "catalog/9f33e303-cd3e-43f1-97f4-a64f2b77937b.jpg", "size": [234, 167]}'),
('sorl-thumbnail||thumbnails||017f7378ee6e8c2abc6ca05e6e43529d', '["8e53500105d66aa6d9458fe9a438161f"]'),
('sorl-thumbnail||thumbnails||0aaf02746708576b19af1cf654af773a', '["20525461d037fb3abf59609ec731a7a4"]'),
('sorl-thumbnail||thumbnails||0d4d61b78eb0789520039cf01c14ae79', '["8cc91acf83602d8e2540768c63b3fade"]'),
('sorl-thumbnail||thumbnails||12787ba83da2ee595962428973d63e7f', '["c4ab37200e606e32736a8b52557a5f24"]'),
('sorl-thumbnail||thumbnails||13796f9f17b9426e3449ecb7ff4120c5', '["44b58df8601e92f869ccd58639a0d843"]'),
('sorl-thumbnail||thumbnails||18a65342df6fb443d10bfd73d436ccf9', '["7b72ceb56326fcacd63d02847b26af13"]'),
('sorl-thumbnail||thumbnails||1f05b30c242fec2306f28cbcf3b4e908', '["6caeba5105ccbf15f5c0b9e93dccd786"]'),
('sorl-thumbnail||thumbnails||1f32317c8be5901a234ac7f00f817695', '["c3e706b1712607749d2219886bdbb98b"]'),
('sorl-thumbnail||thumbnails||1f76c92f9afecd01f0175dd0318a1102', '["34ab9a84726949be9857c9fcf6e84ee5"]'),
('sorl-thumbnail||thumbnails||221121633f8ce5aa75901b0490982a56', '["8954dbfdd58d663b1c4eb3ea6ab83c4d"]'),
('sorl-thumbnail||thumbnails||2595cdfa6d0554acbbbd85813ca85456', '["3a07d623a5fd1c3796bba11f644945fa"]'),
('sorl-thumbnail||thumbnails||2648280d99133d7d11f0d0559ea12cc7', '["64d50c1a33cddaffe7c61e902ead6080"]'),
('sorl-thumbnail||thumbnails||26ac840a716b3285546a9700684720a0', '["a09aec2ceb6e7154855fd777193e7dcb"]'),
('sorl-thumbnail||thumbnails||2b972cdc9063584e12240d797f374937', '["e587cf1a229440362c653632db68ae0c"]'),
('sorl-thumbnail||thumbnails||2fd4bbc08dfcf7375b88bf390115b959', '["ea8c36a8a5e6c8a826d70e5013dac2ea"]'),
('sorl-thumbnail||thumbnails||32a53ec2790d2537fccab60061b1cd6f', '["d4cbe723dc0d169528631ccf1066e1dc"]'),
('sorl-thumbnail||thumbnails||3de3562c53f8dbc9cef7202b2bfe7b41', '["199739a48389e02d4915312c7af2d8ea"]'),
('sorl-thumbnail||thumbnails||400414903fd2701a8904142dd426a089', '["4326e13531baebcdf395c093044eae7e"]'),
('sorl-thumbnail||thumbnails||40249b1c12a3548852b88c21b9033348', '["8e635ee13913bca35ba93a97b26ce021"]'),
('sorl-thumbnail||thumbnails||41da5ad6044b6c820cbcbf30c62b1f41', '["522b2ed6eda8482ccf91253c59dcce00"]'),
('sorl-thumbnail||thumbnails||431b4c348c8265cfed82d1972b6dfa00', '["35742eba9280acc34cbc2c640dc0cb7c"]'),
('sorl-thumbnail||thumbnails||43bc149fa956900a9312ebf5a805c549', '["28826c4213c464f190525ce131198bfd"]'),
('sorl-thumbnail||thumbnails||4a0694f010c81804aa04babc1b92a9a2', '["bcbd56be9f17dba66396669a129975e6"]'),
('sorl-thumbnail||thumbnails||4afa59fc298ec044c337e5b7aa49967c', '["011f8fff6b2ec320d8018bd021daa760"]'),
('sorl-thumbnail||thumbnails||4cf9434c9af92df28846e4f9126f6f4d', '["1d27a000e2a890d663384b6628b94a78"]'),
('sorl-thumbnail||thumbnails||586142802351870b519b9c826326ecf5', '["df235bede8d97d7de779882223edb433"]'),
('sorl-thumbnail||thumbnails||5aa82a471fe83ddf17dc1f31fd45a5e2', '["a30bcef3d3fe28d114c2498629c151e3"]'),
('sorl-thumbnail||thumbnails||5b6d165578d98c870a955aebae65253a', '["565028c73c3c40b98fc6248a62ccb1b3"]'),
('sorl-thumbnail||thumbnails||61aa622cb1aa907ab925f4b90599c395', '["653a960073560c6a8516ea6ca3f5c852"]'),
('sorl-thumbnail||thumbnails||654d478560e627497545e55237ccb571', '["b3083f1a588bc5063f1bdace22094891"]'),
('sorl-thumbnail||thumbnails||6bd68155a891ae100210e28c3b30159c', '["105c1092be232390e297f92681489602"]'),
('sorl-thumbnail||thumbnails||6c75c609bdd07cdd9e5597d51e25c2b0', '["6dec73d51b3d4adb516de1273745379a"]'),
('sorl-thumbnail||thumbnails||76ef41ca123bf863d24c245b5d298159', '["a649430521e30589e73d95316e9b50c9"]'),
('sorl-thumbnail||thumbnails||7930d533c4c68fc82672c2739c8fe35c', '["ad7af077d15cd40f9565a9d0d790d75f"]'),
('sorl-thumbnail||thumbnails||7ae2eabb73af91d5f81676101b258d28', '["5bb6e64fb7ccdde2ba83c589be8ce03f"]'),
('sorl-thumbnail||thumbnails||80cb6710511e466ee760c1995fd9f585', '["278385bb02ace2e70fe42a67db0317e5"]'),
('sorl-thumbnail||thumbnails||8be203565fbeabd8e4fd49811258859b', '["e925425d6a88f5ed50ee340f80935744"]'),
('sorl-thumbnail||thumbnails||a268c5276a517a2f5dcbe43bbae2b9df', '["49857584ce7eb62b6d130deefb69c4ca"]'),
('sorl-thumbnail||thumbnails||a4c38c77b7a77bf21c5fe45676eae12b', '["ea8b5022731ff3ec0e768a36aba5fca1"]'),
('sorl-thumbnail||thumbnails||ad4f377c47cb986246339f7fab302db3', '["3c526942bb88d21a9ade205980930ec0"]'),
('sorl-thumbnail||thumbnails||ae154d2dc1a53ae52706bd3090b167d1', '["3ea25bfbe86d3d60882ddf391a62156c"]'),
('sorl-thumbnail||thumbnails||b4059f3ade1553b0dfb6e05edeac0d3f', '["93a51072c73e6e17be52ff1e738cef7b"]'),
('sorl-thumbnail||thumbnails||b69137287bd908730a96f6ef38838d03', '["be29ec7d86844c8a64b625fbe305f9f4"]'),
('sorl-thumbnail||thumbnails||bb112681cd74f20632d0c121a62f8003', '["102a988964e01f83fbe70e1f0befcbe5"]'),
('sorl-thumbnail||thumbnails||be9cc73cdeac2a6b37bde2660c1f7a74', '["8ffa77c93c9ecc45eb64bf9fd5a60a06"]'),
('sorl-thumbnail||thumbnails||c1b2477327eb763d7588729661af5765', '["ecd473d05452d64a45f9e00b6e4d656e"]'),
('sorl-thumbnail||thumbnails||c3e63343fc51d31ef71584c2ebaa4a1c', '["55d48de2e3545e8facc7bbc86ca564cc"]'),
('sorl-thumbnail||thumbnails||c6b9ae98fae2ad91e6175f569beca97c', '["992543c5a1b35694b015fd47b7a82c4b"]'),
('sorl-thumbnail||thumbnails||cab648f6a83640a9ca60543d4ac9e7e4', '["3b6df1de625e252fa58d052b910df48b"]'),
('sorl-thumbnail||thumbnails||cacdeadefdef0e7e8399cfdc470dc48b', '["c69fe2c7f7026dc2e71078fed7a320f0"]'),
('sorl-thumbnail||thumbnails||cc955860fee380012aaa7054b7f3aef3', '["e122d461328afb256df1b394624e6f41"]'),
('sorl-thumbnail||thumbnails||ce0e35652c5c8e58b974e2cd3a866d5f', '["72dd968e33d9516b57062adc53473061"]'),
('sorl-thumbnail||thumbnails||db5225de7f144d97bd422913216cdfc7', '["3a240b30a4fd38e1830d281060c93f42"]'),
('sorl-thumbnail||thumbnails||dd4cbeccf2534fb624d14b984cb5c5ea', '["009ed045ee3ef117c539c7c0e040e0fb"]'),
('sorl-thumbnail||thumbnails||e071d2c1ab53036e53c49629720dbfec', '["ea36e81fbc230dc58457de2c49c5d951"]'),
('sorl-thumbnail||thumbnails||e27bce4cc606a61dc5e713320358606a', '["ef2808d62052085918f057b394b25ecb"]'),
('sorl-thumbnail||thumbnails||e307b1fc3247a2301163efc3b54048a5', '["56df1a6882976ce4710d78be144d22cd"]'),
('sorl-thumbnail||thumbnails||e840ab1e3a52b7068689d13e964b1e8e', '["95b47f34f7488075bf71509355ed3875"]'),
('sorl-thumbnail||thumbnails||ec826223a7ad30998fb44115729d0dc3', '["d156731334651eb3b1b1116c262afa4b"]'),
('sorl-thumbnail||thumbnails||ef330bcd5a36e6c0cae972a3ad4ab13d', '["82704516600794a9a9c3902f290367e4"]'),
('sorl-thumbnail||thumbnails||f14c7a940be66ecb6c77d28f4d781a64', '["6903e779cb074c7ca3172f17b3e5ab6d"]'),
('sorl-thumbnail||thumbnails||f28410ad479855f0ab61711c639c3dd7', '["89f5f4d9e67924f25b5ba6ade52f6a6d"]'),
('sorl-thumbnail||thumbnails||f54b3a51e8280cdcf4210a65573118b6', '["1347e94799650ec81adf3b0c39991b1e"]'),
('sorl-thumbnail||thumbnails||fa1f4cbb4204886c445c3e9eeefcf605', '["4b93ecd12db3e5219d1b30c85c00f5ca"]'),
('sorl-thumbnail||thumbnails||fad43a087c952845c1bec0512e719c50', '["475d71118009f65cc55089ce2386271f"]'),
('sorl-thumbnail||thumbnails||fb23dc489b142f4f71e348f13663b57b', '["d4f6103cacec05df94008ec582168bd8"]'),
('sorl-thumbnail||thumbnails||fd2a6e7c541990ec88c200caf8e71b3e', '["e0516d26a621dafcff32a365ac556d38"]');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Ограничения внешнего ключа таблицы `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Ограничения внешнего ключа таблицы `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_aliaspluginmodel`
--
ALTER TABLE `cms_aliaspluginmodel`
  ADD CONSTRAINT `cms_aliaspluginmodel_plugin_id_9867676e_fk_cms_cmsplugin_id` FOREIGN KEY (`plugin_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `cms_aliaspluginmod_cmsplugin_ptr_id_f71dfd31_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `cms_aliasplu_alias_placeholder_id_6d6e0c8a_fk_cms_placeholder_id` FOREIGN KEY (`alias_placeholder_id`) REFERENCES `cms_placeholder` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_cmsplugin`
--
ALTER TABLE `cms_cmsplugin`
  ADD CONSTRAINT `cms_cmsplugin_parent_id_fd3bd9dd_fk_cms_cmsplugin_id` FOREIGN KEY (`parent_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `cms_cmsplugin_placeholder_id_0bfa3b26_fk_cms_placeholder_id` FOREIGN KEY (`placeholder_id`) REFERENCES `cms_placeholder` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_globalpagepermission`
--
ALTER TABLE `cms_globalpagepermission`
  ADD CONSTRAINT `cms_globalpagepermission_group_id_991b4733_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `cms_globalpagepermission_user_id_a227cee1_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_globalpagepermission_sites`
--
ALTER TABLE `cms_globalpagepermission_sites`
  ADD CONSTRAINT `cms_globalpagepermission_site_site_id_00460b53_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`),
  ADD CONSTRAINT `globalpagepermission_id_46bd2681_fk_cms_globalpagepermission_id` FOREIGN KEY (`globalpagepermission_id`) REFERENCES `cms_globalpagepermission` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_page`
--
ALTER TABLE `cms_page`
  ADD CONSTRAINT `cms_page_parent_id_f89b72e4_fk_cms_page_id` FOREIGN KEY (`parent_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `cms_page_publisher_public_id_d619fca0_fk_cms_page_id` FOREIGN KEY (`publisher_public_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `cms_page_site_id_4323d3ff_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_pagepermission`
--
ALTER TABLE `cms_pagepermission`
  ADD CONSTRAINT `cms_pagepermission_group_id_af5af193_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `cms_pagepermission_page_id_0ae9a163_fk_cms_page_id` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `cms_pagepermission_user_id_0c7d2b3c_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_pageuser`
--
ALTER TABLE `cms_pageuser`
  ADD CONSTRAINT `cms_pageuser_created_by_id_8e9fbf83_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `cms_pageuser_user_ptr_id_b3d65592_fk_auth_user_id` FOREIGN KEY (`user_ptr_id`) REFERENCES `auth_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_pageusergroup`
--
ALTER TABLE `cms_pageusergroup`
  ADD CONSTRAINT `cms_pageusergroup_created_by_id_7d57fa39_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `cms_pageusergroup_group_ptr_id_34578d93_fk_auth_group_id` FOREIGN KEY (`group_ptr_id`) REFERENCES `auth_group` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_page_placeholders`
--
ALTER TABLE `cms_page_placeholders`
  ADD CONSTRAINT `cms_page_placeholders_page_id_f2ce8dec_fk_cms_page_id` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `cms_page_placehold_placeholder_id_6b120886_fk_cms_placeholder_id` FOREIGN KEY (`placeholder_id`) REFERENCES `cms_placeholder` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_placeholderreference`
--
ALTER TABLE `cms_placeholderreference`
  ADD CONSTRAINT `cms_placeholderref_cmsplugin_ptr_id_6977ec85_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `cms_placeholde_placeholder_ref_id_244759b1_fk_cms_placeholder_id` FOREIGN KEY (`placeholder_ref_id`) REFERENCES `cms_placeholder` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_basemenupl`
--
ALTER TABLE `cms_plugins_basemenupl`
  ADD CONSTRAINT `cms_plugins_baseme_cmsplugin_ptr_id_92a911b7_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_basemenupl_pages`
--
ALTER TABLE `cms_plugins_basemenupl_pages`
  ADD CONSTRAINT `cms_plugins_basemenupl_pages_page_id_cef6ee75_fk_cms_page_id` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `f765fea9e47dc55adcb506384eadd85f` FOREIGN KEY (`basemenupl_id`) REFERENCES `cms_plugins_basemenupl` (`cmsplugin_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_basemodelpl`
--
ALTER TABLE `cms_plugins_basemodelpl`
  ADD CONSTRAINT `cms_plugins_basemo_cmsplugin_ptr_id_e5275ecc_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_blockmodel`
--
ALTER TABLE `cms_plugins_blockmodel`
  ADD CONSTRAINT `cms_plugins_blockmodel_block_id_0181c1e7_fk_cms_plugins_block_id` FOREIGN KEY (`block_id`) REFERENCES `cms_plugins_block` (`id`),
  ADD CONSTRAINT `cms_plugins_blockm_cmsplugin_ptr_id_bf214798_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_calc`
--
ALTER TABLE `cms_plugins_calc`
  ADD CONSTRAINT `bdf81ff96eef4e35d858d98e94c38eb0` FOREIGN KEY (`basemodelpl_ptr_id`) REFERENCES `cms_plugins_basemodelpl` (`cmsplugin_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_designpluginmodel_design`
--
ALTER TABLE `cms_plugins_designpluginmodel_design`
  ADD CONSTRAINT `cms_plugi_designproject_id_956e415f_fk_contents_designproject_id` FOREIGN KEY (`designproject_id`) REFERENCES `contents_designproject` (`id`),
  ADD CONSTRAINT `f483b79cddf3ca41bc2ce086da546997` FOREIGN KEY (`designpluginmodel_id`) REFERENCES `cms_plugins_designpluginmodel` (`basemodelpl_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_htmlinsertmodel`
--
ALTER TABLE `cms_plugins_htmlinsertmodel`
  ADD CONSTRAINT `cms_plugins_htmlin_cmsplugin_ptr_id_9df44848_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_keyword`
--
ALTER TABLE `cms_plugins_keyword`
  ADD CONSTRAINT `cms_plugins_keyword_page_id_db921e51_fk_cms_page_id` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_personpluginmodel`
--
ALTER TABLE `cms_plugins_personpluginmodel`
  ADD CONSTRAINT `ebd4831a3b12f87a1ae8baf00314196f` FOREIGN KEY (`basemodelpl_ptr_id`) REFERENCES `cms_plugins_basemodelpl` (`cmsplugin_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_personpluginmodel_commands`
--
ALTER TABLE `cms_plugins_personpluginmodel_commands`
  ADD CONSTRAINT `cms_plugins_personplugi_person_id_a3c09d8d_fk_contents_person_id` FOREIGN KEY (`person_id`) REFERENCES `contents_person` (`id`),
  ADD CONSTRAINT `D4665569029eb8770907391e2ac25c74` FOREIGN KEY (`personpluginmodel_id`) REFERENCES `cms_plugins_personpluginmodel` (`basemodelpl_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_projectpluginmodel`
--
ALTER TABLE `cms_plugins_projectpluginmodel`
  ADD CONSTRAINT `cms_plugins_projectpl_project_id_029c4f61_fk_contents_project_id` FOREIGN KEY (`project_id`) REFERENCES `contents_project` (`id`),
  ADD CONSTRAINT `cms_plugins_projec_cmsplugin_ptr_id_8cb2baa6_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_reviewpluginmodel`
--
ALTER TABLE `cms_plugins_reviewpluginmodel`
  ADD CONSTRAINT `fbbea17b058c300ea4bc9b5b1163e602` FOREIGN KEY (`basemodelpl_ptr_id`) REFERENCES `cms_plugins_basemodelpl` (`cmsplugin_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_reviewpluginmodel_reviews`
--
ALTER TABLE `cms_plugins_reviewpluginmodel_reviews`
  ADD CONSTRAINT `cms_plugins_reviewplugi_review_id_0975f1cb_fk_contents_review_id` FOREIGN KEY (`review_id`) REFERENCES `contents_review` (`id`),
  ADD CONSTRAINT `D778215e887c58b42eb1e01e7afc9cb7` FOREIGN KEY (`reviewpluginmodel_id`) REFERENCES `cms_plugins_reviewpluginmodel` (`basemodelpl_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_sliderpluginmodel`
--
ALTER TABLE `cms_plugins_sliderpluginmodel`
  ADD CONSTRAINT `cms_plugins_slider_cmsplugin_ptr_id_3898429c_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_sliderpluginmodel_sliders`
--
ALTER TABLE `cms_plugins_sliderpluginmodel_sliders`
  ADD CONSTRAINT `cms_plugins_sliderplugi_slider_id_92bb6a5e_fk_contents_slider_id` FOREIGN KEY (`slider_id`) REFERENCES `contents_slider` (`id`),
  ADD CONSTRAINT `D38165ebe8406f84660544064331a28a` FOREIGN KEY (`sliderpluginmodel_id`) REFERENCES `cms_plugins_sliderpluginmodel` (`cmsplugin_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_teaserspluginmodel`
--
ALTER TABLE `cms_plugins_teaserspluginmodel`
  ADD CONSTRAINT `D057786b9f3d240bdebb2e4829e3ebf8` FOREIGN KEY (`basemodelpl_ptr_id`) REFERENCES `cms_plugins_basemodelpl` (`cmsplugin_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_teaserspluginmodel_teasers`
--
ALTER TABLE `cms_plugins_teaserspluginmodel_teasers`
  ADD CONSTRAINT `cms_plugins_teasersplug_teaser_id_eaae546c_fk_contents_teaser_id` FOREIGN KEY (`teaser_id`) REFERENCES `contents_teaser` (`id`),
  ADD CONSTRAINT `D568cb9ad4e7452b8a1590f5b2e802c8` FOREIGN KEY (`teaserspluginmodel_id`) REFERENCES `cms_plugins_teaserspluginmodel` (`basemodelpl_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_textmodel`
--
ALTER TABLE `cms_plugins_textmodel`
  ADD CONSTRAINT `D4ae3d0cb23261eb81f5db150920fb4b` FOREIGN KEY (`basemodelpl_ptr_id`) REFERENCES `cms_plugins_basemodelpl` (`cmsplugin_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_plugins_urlline`
--
ALTER TABLE `cms_plugins_urlline`
  ADD CONSTRAINT `cms_plugins_urlline_popup_id_d1c6d71d_fk_contents_popup_id` FOREIGN KEY (`popup_id`) REFERENCES `contents_popup` (`id`),
  ADD CONSTRAINT `cms_pl_plugin_id_9cbeeece_fk_cms_plugins_calc_basemodelpl_ptr_id` FOREIGN KEY (`plugin_id`) REFERENCES `cms_plugins_calc` (`basemodelpl_ptr_id`);

--
-- Ограничения внешнего ключа таблицы `cms_staticplaceholder`
--
ALTER TABLE `cms_staticplaceholder`
  ADD CONSTRAINT `cms_staticplaceholder_draft_id_5aee407b_fk_cms_placeholder_id` FOREIGN KEY (`draft_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `cms_staticplaceholder_public_id_876aaa66_fk_cms_placeholder_id` FOREIGN KEY (`public_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `cms_staticplaceholder_site_id_dc6a85b6_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_title`
--
ALTER TABLE `cms_title`
  ADD CONSTRAINT `cms_title_page_id_5fade2a3_fk_cms_page_id` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `cms_title_publisher_public_id_003a2702_fk_cms_title_id` FOREIGN KEY (`publisher_public_id`) REFERENCES `cms_title` (`id`);

--
-- Ограничения внешнего ключа таблицы `cms_usersettings`
--
ALTER TABLE `cms_usersettings`
  ADD CONSTRAINT `cms_usersettings_clipboard_id_3ae17c19_fk_cms_placeholder_id` FOREIGN KEY (`clipboard_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `cms_usersettings_user_id_09633b2d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_designimage`
--
ALTER TABLE `contents_designimage`
  ADD CONSTRAINT `contents_design_project_id_a5305a2d_fk_contents_designproject_id` FOREIGN KEY (`project_id`) REFERENCES `contents_designproject` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_designproject`
--
ALTER TABLE `contents_designproject`
  ADD CONSTRAINT `contents_designproject_review_id_81232f24_fk_contents_review_id` FOREIGN KEY (`review_id`) REFERENCES `contents_review` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_price`
--
ALTER TABLE `contents_price`
  ADD CONSTRAINT `contents_price_category_id_f341f671_fk_contents_categoryprice_id` FOREIGN KEY (`category_id`) REFERENCES `contents_categoryprice` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_project`
--
ALTER TABLE `contents_project`
  ADD CONSTRAINT `contents_project_design_id_367dc4d5_fk_contents_designproject_id` FOREIGN KEY (`design_id`) REFERENCES `contents_designproject` (`id`),
  ADD CONSTRAINT `contents_project_person_id_ad87864d_fk_contents_person_id` FOREIGN KEY (`person_id`) REFERENCES `contents_person` (`id`),
  ADD CONSTRAINT `contents_project_place_id_8e2593f1_fk_cms_placeholder_id` FOREIGN KEY (`place_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `contents_project_review_id_227c2d9b_fk_contents_review_id` FOREIGN KEY (`review_id`) REFERENCES `contents_review` (`id`),
  ADD CONSTRAINT `contents_project_tour_id_e232c2de_fk_contents_tdtour_id` FOREIGN KEY (`tour_id`) REFERENCES `contents_tdtour` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_projectimage`
--
ALTER TABLE `contents_projectimage`
  ADD CONSTRAINT `contents_projectimage_project_id_08dcbc4a_fk_contents_project_id` FOREIGN KEY (`project_id`) REFERENCES `contents_project` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_project_secrets`
--
ALTER TABLE `contents_project_secrets`
  ADD CONSTRAINT `contents_project_secret_secret_id_cd686dbb_fk_contents_secret_id` FOREIGN KEY (`secret_id`) REFERENCES `contents_secret` (`id`),
  ADD CONSTRAINT `contents_project_secr_project_id_9e48279c_fk_contents_project_id` FOREIGN KEY (`project_id`) REFERENCES `contents_project` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_reviewimage`
--
ALTER TABLE `contents_reviewimage`
  ADD CONSTRAINT `contents_reviewimage_review_id_a9e39f6d_fk_contents_review_id` FOREIGN KEY (`review_id`) REFERENCES `contents_review` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_secretimage`
--
ALTER TABLE `contents_secretimage`
  ADD CONSTRAINT `contents_secretimage_project_id_c8da668a_fk_contents_secret_id` FOREIGN KEY (`project_id`) REFERENCES `contents_secret` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_tariffdesign_service`
--
ALTER TABLE `contents_tariffdesign_service`
  ADD CONSTRAINT `contents_ta_tariffdesign_id_4e75076f_fk_contents_tariffdesign_id` FOREIGN KEY (`tariffdesign_id`) REFERENCES `contents_tariffdesign` (`id`),
  ADD CONSTRAINT `contents__designservice_id_8c6ad514_fk_contents_designservice_id` FOREIGN KEY (`designservice_id`) REFERENCES `contents_designservice` (`id`);

--
-- Ограничения внешнего ключа таблицы `contents_tdtour`
--
ALTER TABLE `contents_tdtour`
  ADD CONSTRAINT `contents_tdtour_review_id_edf952a1_fk_contents_review_id` FOREIGN KEY (`review_id`) REFERENCES `contents_review` (`id`);

--
-- Ограничения внешнего ключа таблицы `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Ограничения внешнего ключа таблицы `reversion_revision`
--
ALTER TABLE `reversion_revision`
  ADD CONSTRAINT `reversion_revision_user_id_17095f45_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `reversion_version`
--
ALTER TABLE `reversion_version`
  ADD CONSTRAINT `reversion_version_revision_id_af9f6a9d_fk_reversion_revision_id` FOREIGN KEY (`revision_id`) REFERENCES `reversion_revision` (`id`),
  ADD CONSTRAINT `reversion_ver_content_type_id_7d0ff25c_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
